<script>

    function init() {
        clusterer = new ymaps.Clusterer({
            preset: 'islands#blueCircleIcon',
            groupByCoordinates: false,
            clusterDisableClickZoom: false,
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false,
            hasBalloon: false,
        });

        var center = [{{$selected_car->getCoordinates()[0]['lat']}}, {{$selected_car->getCoordinates()[0]['lng']}}];
        var centerText = '<div class="map-car min-width-320">' +
            '<div class="map-car-body d-flex align-items-center">' +
            '<div class="map-car-img-part"><img src="{{Voyager::image($selected_car->image_1)}}" alt="{{$selected_car->title}}" title="{{$selected_car->title}}" class="rounded" width="100px"></div>' +
            '<div class="map-car-info-part">' +
            '<div class="d-flex align-items-center  ml-3">' +
            '<span>{{$selected_car->title}}</span>' +
            '</div>' +
            '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
            '<div class="stars-info d-flex align-items-center">' +
            '<div class="stars stars--large mr-1 fs-16">' +
            '<span style="width: {{$selected_car->avg_score * 20}}%"></span>' +
            '</div>' +
            '<span>({{$selected_car->reviews->count()}})</span>' +
            '</div>' +
            '<span class="px-1">|</span>' +
            '<div class="ml-1"> {{$selected_car->age}}</div>' +
            '<span class="px-1">|</span>' +
            '<div class="ml-1"> {{$selected_car->number_of_seats}} мест </div>' +
            '</div>' +
            '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        var myMap = new ymaps.Map('map', {
                center: center,
                zoom: 12,
                controls: []
            }, {
                searchControlProvider: 'yandex#search',
                balloonPanelMaxMapArea: 0

            }),

            // Создаем коллекцию.
            // myCollection = new ymaps.GeoObjectCollection(),
            // Создаем массив с данными.
            myPoints = [@foreach($cars??[] as $car) {
                coords: [{{$car->getCoordinates()[0]['lat']}} , {{$car->getCoordinates()[0]['lng']}}],
                title: '<strong> {{$car->title}} </strong>',
                text: '<div class="map-car min-width-320">' +
                    '<div class="map-car-body d-flex align-items-center">' +
                    '<div class="map-car-img-part"><img src="{{Voyager::image($car->image_1)}}" alt="{{$car->title}}" title="{{$car->title}}" class="rounded" width="100px"></div>' +
                    '<div class="map-car-info-part">' +
                    '<div class="d-flex align-items-center  ml-3">' +
                    '<span>{{$car->title}}</span>' +
                    '</div>' +
                    '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
                    '<div class="stars-info d-flex align-items-center">' +
                    '<div class="stars stars--large mr-1 fs-16">' +
                    '<span style="width: {{$car->avg_score * 20}}%"></span>' +
                    '</div>' +
                    '<span>({{$car->reviews->count()}})</span>' +
                    '</div>' +
                    '<span class="px-1">|</span>' +
                    '<div class="ml-1"> {{$car->age}}</div>' +
                    '<span class="px-1">|</span>' +
                    '<div class="ml-1"> {{$car->number_of_seats}} мест </div>' +
                    '</div>' +
                    '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
                    '<p class="fs-12 color-light mb-2">\n' +
                    '<img src="{{asset('storage/img/icons/location.png')}} " alt="location" class="inline-img-width" style="width: 17px !important"/>\n' +
                    '<span>{{distanceM($car->distance)}}</span>\n' +
                    '<span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="От выбранного места до этого автомобиля {{distanceM($car->distance)}}"> ' +
                    '<i class="fas fa-info-circle"></i></span>\n' +
                    '</p>' +
                    '<div class="text-center mt-1 ml-3">' +
                    '<a href="{{route('car.single', array_merge(['slug' => $car->slug], request()->only('location', 'date', 'attributes')))}}" class="own-btn rounded d-inline-block ora px-2 py-1 fs-12" target="_blank"><span class="ff-st">{!! currency($car->price_1) !!}</span> / {{__('car.day')}}</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
            } {!! $loop->last ? '' : ',' !!}  @endforeach];
        // Заполняем коллекцию данными.
        geoObjects = [];
        for (var i = 0; i < myPoints.length; i++) {
            var point = myPoints[i];
            geoObjects[i] = new ymaps.Placemark(
                point.coords, {
                    balloonContentBody: point.text,
                    // clusterCaption: '<strong> ' + point.title + ' </strong>',
                }, {
                    preset: 'islands#blueAutoIcon'
                }
            );
        }

        // clusterer.options.set({
        //     gridSize: 60,
        //     clusterDisableClickZoom: true
        // });

        clusterer.add(geoObjects);

        //Кнопки плюс - минус на карте
        myMap.controls.add('zoomControl', {
            size: 'small',
            float: 'none',
            position: {
                bottom: '50px',
                right: '30px'
            }
        });

        var myPlacemark = new ymaps.Placemark(center, {
            balloonContentBody : centerText
        }, {
            preset: 'islands#redDotIcon'
        });

        myMap.geoObjects.add(myPlacemark);
        myMap.balloon.open(center, centerText, {
            // Опция: не показываем кнопку закрытия.
            closeButton: true
        });
        // Добавляем коллекцию меток на карту.
        myMap.geoObjects.add(clusterer);
        myMap.behaviors.disable('scrollZoom');
        // Создаем круг.
        var myCircle = new ymaps.Circle([
            // Координаты центра круга.
            [center[0], center[1]],
            // Радиус круга в метрах.
            2000
        ], {
            // Описываем свойства круга.
            // Содержимое балуна.
            balloonContent: "<span style='vertical-align: sub' class='fw-600'>Радиус круга - 2 км</span>",
            // Содержимое хинта.
            hintContent: ""
        }, {
            // Задаем опции круга.
            // Включаем возможность перетаскивания круга.
            draggable: false,
            // Цвет заливки.
            // Последний байт (77) определяет прозрачность.
            // Прозрачность заливки также можно задать используя опцию "fillOpacity".
            fillOpacity: 0.5,
            opacity: 0.5,
            fillColor: "#f6993f",
            // Цвет обводки.
            strokeColor: "#990066",
            // Прозрачность обводки.
            strokeOpacity: 0.8,
            // Ширина обводки в пикселях.
            strokeWidth: 0
        });
        // Добавляем круг на карту.
        myMap.geoObjects.add(myCircle)

    }
</script>

