@section('css')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
    <link rel="stylesheet" href="{{asset('css/jquery.businessHours.css')}}">
@stop


<div class="d-flex justify-content-center" id="businessHoursContainer"></div>

<div class="d-flex justify-content-center mt-3 rounded">
    <input type="button" class="own-btn btn btn-primary ora rounded-0 border-0 m-auto fw-400 btnSerialize"
           value="Сохранить">
</div>
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
    <script src="{{asset('js/jquery.businessHours.min.js')}}"></script>

    <script>
        console.log({!! $working_times !!})
        var businessHoursManager = $("#businessHoursContainer").businessHours({
            @if($working_times)
                operationTime: {!! $working_times !!},
            @endif
            postInit: function () {
                $('.operationTimeFrom, .operationTimeTill').timepicker({
                    'timeFormat': 'H:i',
                    'step': 30,
                });
            }
        });

        $(".btnSerialize").click(function () {
            var data = JSON.stringify(businessHoursManager.serialize());
            var url = '{{route('owner.settings.working_times.save')}}';

            axios.post(url, {
                data: data
            })
                .then(function (response) {
                    if (response.data.message) {
                        myAlert(response.data.message)
                    }
                });
        });
    </script>
@stop
