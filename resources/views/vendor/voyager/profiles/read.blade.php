@extends('voyager::master')
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop
@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }}
        &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}"
                   title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore"
                   data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                            class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete"
                   data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                            class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.(request()->input('redirect')?:$dataType->slug).'.index') }}"
           class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>

    </h1>
    @include('voyager::multilingual.language-selector')

@stop
@section('content')
    <div class="page-content read container-fluid bg-white">
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    <div class="col-md-6">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <ul class="p-0" style="padding: 0;">
                                    <li class="list-group-item">
                                        <img src="{{Voyager::image($user->avatar)}}" width="400"
                                             class="img-rounded">
                                    </li>
                                    <li class="list-group-item">
                                        <span><strong>Имя </strong> : </span>
                                        <span>{{ $user->first_name}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span><strong>Фамиля </strong> : </span>
                                        <span>{{ $user->last_name}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span><strong>Эл. адрес </strong> : </span>
                                        <span>{{ $user->email}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span><strong>Номер телефона</strong> : </span>
                                        <span>{{ $user->number}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span><strong>День рождения </strong> : </span>
                                        <span>{{ $user->birthday}}</span>
                                    </li>
                                    @foreach($dataType->readRows as $row)
                                        @if($row->type != "image")
                                            <li class="list-group-item">
                                                <span><strong>{{ $row->display_name }}</strong> : </span>
                                                @include('voyager::multilingual.input-hidden-bread-read')
                                                @if($row->field == 'citizenship')
                                                    <span>{{ getCountryName($dataTypeContent->{$row->field}) }}</span>
                                                @elseif($row->field == 'country')
                                                    <span>{{ getCountryName($dataTypeContent->{$row->field}) }}</span>
                                                @elseif($row->field == 'region')
                                                    <span>{{ getStateName($dataTypeContent->{$row->field}) }}</span>
                                                @elseif($row->field == 'experience')
                                                    <span>{{ date('Y') - $dataTypeContent->{$row->field}.' л.  ||| '.$dataTypeContent->{$row->field}.' г.' }}</span>
                                                @elseif($row->field == 'status')
                                                    <span>{{ __('user.'.$dataTypeContent->{$row->field}.'_name') }}</span>
                                                @else
                                                    <span>{{ $dataTypeContent->{$row->field} }}</span>
                                                @endif
                                                @endif


                                            </li>

                                            @endforeach
                                            <li class="list-group-item">
                                                <span><strong>Статус </strong> : </span>
                                                <span>
                                                    @switch($user->profile->status)
                                                        @case('confirm')
                                                        Подтвержден
                                                        @break
                                                        @case('not_verified')
                                                        Не подтвержден
                                                        @break
                                                        @case('rejected')
                                                        Отклонен
                                                        @break
                                                    @endswitch
                                                </span>

                                            </li>
                                </ul>
                                <form action="{{route('profile.status',[$dataTypeContent->getKey()])}}" role="form"
                                      method="POST">
                                    @csrf


                                    <div class="col-md-12 mt-1" style="padding-left: 0;">
                                        <h4>Текст сообщения
                                            <span class="help" data-toggle="tooltip" data-html="true" data-placement="right"
                                                  title="Напишите текст если хотите отправить сообщение по электронному адресу пользователя ">
                                        <i class="fas fa-question-circle"></i>
                                    </span>
                                        </h4>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject"
                                                   placeholder="Заголовка">
                                        </div>
                                        <div class="form-group">
                                        <textarea class="form-control" name="message"
                                                  placeholder="Текст сообщения"></textarea>
                                        </div>
                                    </div>
                                    @if($dataTypeContent->status == 'for_consideration')
                                        <div class="col-md-12" style="padding-left: 0;">
                                            <h4>Запрос на подтверждение
                                                <span class="help" data-toggle="tooltip" data-html="true" data-placement="right"
                                                      title="Пользователь отправил свои данные для просмотра  ">
                                        <i class="fas fa-question-circle"></i>
                                    </span>
                                            </h4>
                                            <ul class="radio" style="overflow: hidden;">
                                                <li style="float: left; width: auto;">
                                                    <input type="radio" id="option-status"
                                                           name="status"
                                                           value="rejected">
                                                    <label for="option-status">Отклонить </label>
                                                    <div class="check"></div>
                                                </li>
                                                <li style="float: left; width: auto;">
                                                    <input type="radio" id="option"
                                                           name="status"
                                                           value="confirm">
                                                    <label for="option">Подтвердить</label>
                                                    <div class="check"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="col-md-8 mt-1 text-center">
                                        <button type="submit" class="btn btn-primary btn-add-new">
                                            <span>{{__('general.save')}}</span>
                                        </button>
                                    </div>

                                    <input type="hidden" name="redirect"
                                           value="{{ route('voyager.'.(request()->input('redirect')?:$dataType->slug).'.index') }}">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($dataType->readRows as $row)
                                    @php($i++)
                                    @if($row->type == "image")
                                        <div class="col-md-6">
                                            <h3>{{ $row->display_name }}</h3>
                                            <div class="profile-images">
                                                <a data-fancybox="gallery" class="pic{{$i}}"
                                                   href="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                                    <img class="img-responsive"
                                                         src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-bordered">
                            <div class="panel-body">

                                @foreach($userNotificationList as $notification)


                                    @if($notification->type == 'App\Notifications\Confirmation')
                                        <ul class="p-0" style="padding: 0;">
                                            <li class="list-group-item">
                                                <span><strong>Admin </strong> : </span>
                                                <span>
                                                    {{Auth::user()->full_name}}
                                                </span>

                                            </li>
                                            <li class="list-group-item">
                                                <span><strong>Статус </strong> : </span>
                                                <span >
                                                    @switch($notification->data['status'])
                                                        @case('confirm')
                                                        Подтвержден
                                                        @break
                                                        @case('not_verified')
                                                        Не подтвержден
                                                        @break
                                                        @case('rejected')
                                                        Отклонен
                                                        @break
                                                    @endswitch
                                                </span>
                                                <span class="float-right">
                                                    {{$notification->created_at->diffForHumans()}}
                                                </span>
                                            </li>
                                            <li class="list-group-item">
                                                <span><strong>Заголовок </strong> : </span>
                                                <span>{{$notification->data['subject']}}</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span><strong>Сообщение </strong> : </span>
                                                <span style="word-break: break-word;">{{$notification->data['message']}}</span>
                                            </li>
                                        </ul>
                                    @endif
                                @endforeach
                                {{$userNotificationList->links()}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });
        $('.help').tooltip()
    </script>
@stop
