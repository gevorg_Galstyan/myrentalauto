<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OwnerChangeProfile extends Notification
{
    use Queueable;
    public $profile;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($profile)
    {
        $this->profile = $profile;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Здравствуйте, дорогой '.$notifiable->first_name)
            ->line('Владелец '.$this->profile->user->full_name.' обновил  свой профиль ')
            ->action('Страница профиля', route('voyager.owner-profiles.show', $this->profile))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'owner_id' => $this->profile->id,
            'url' => route('voyager.owner-profiles.show', $this->profile),
            'text' => 'Владелец '.$this->profile->user->full_name.' обновил  свой профиль ',
            'title' => $this->profile->user->full_name,
            'image' => $this->profile->user->avatar
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => 'Изменения',
            'message' => 'Владелец '.$this->profile->user->full_name.' обновил  свой профиль 
            <br>
            <a href="'.route('voyager.owner-profiles.show', $this->profile).'">профиль</a>'
        ]);
    }
}
