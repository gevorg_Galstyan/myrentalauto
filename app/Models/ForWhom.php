<?php

namespace App\Models;

use App\Traits\MyCache;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class ForWhom extends Model
{
    use Translatable, MyCache;
    public $timestamps = false;
    protected $translatable = ['meta_description', 'meta_keyword', 'meta_title', 'renter_content', 'tenant_content', 'renter_title', 'tenant_title', 'announcement', 'title'];

}
