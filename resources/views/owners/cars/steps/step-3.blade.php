<h3>Фото(скрины)</h3>
<section>

    <div class="row">
        <div class="form-group col ">
            <h5 class="control-label text-nowrap">
                Главная ( с переди )
                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                      title=" {{ tooltips($toolips, 'owner_help_photo_front') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
            </h5>
            <div class="col-xs-12" id="image_1">

                <div data-field-name="image_1"></div>

                <label class="cabinet">
                    <figure data-field-name="image_1">
                        <img data-status="image_1"
                             src="@if( $dataTypeContent->image_1){{ Voyager::image( $dataTypeContent->image_1 ) }}@else{{asset('storage/default/default.png')}}@endif"
                             class="gambar img-responsive img-thumbnail" id="item-img-output"
                             data-file-name="{{ $dataTypeContent->image_1 }}"
                             data-id="{{ $dataTypeContent->id }}"/>
                    </figure>


                    <input type="hidden" class="image_1" name="image_1">
                </label>
            </div>
            <div class="custom-file form-group">
                <input type="file" data-id="image_1"
                       class="item-img custom-file-input "
                       {{ ! $dataTypeContent->image_1 ? 'required' : ''}}
                       name="file_photo_1"/>

                <label class="custom-file-label  center-block">Выбрать фото</label>
            </div>

        </div>
        <div class="form-group col ">
            <h5 class="control-label text-nowrap">
                Фото ( с зади )
                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                      title=" {{ tooltips($toolips, 'owner_help_photo_back') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
            </h5>
            <div class="col-xs-12" id="image_2">
                <div data-field-name="image_2"></div>
                <label class="cabinet center-block">
                    <figure data-field-name="image_2">
                        <img data-status="image_2"
                             src="@if( $dataTypeContent->image_2){{ Voyager::image( $dataTypeContent->image_2 ) }}@else{{asset('storage/default/default.png')}}@endif"
                             class="gambar img-responsive img-thumbnail" id="item-img-output"
                             data-file-name="{{ $dataTypeContent->image_2 }}"
                             data-id="{{ $dataTypeContent->id }}"/>
                    </figure>
                    <input type="hidden" class="image_2" name="image_2">
                </label>
            </div>
            <div class="custom-file form-group">
                <input type="file" data-id="image_2" class="item-img custom-file-input center-block"
                       {{ ! $dataTypeContent->image_2 ? 'required' : ''}}
                       name="file_photo-2"/>

                <label class="custom-file-label  center-block">Выбрать фото</label>
            </div>
        </div>
        <div class="form-group col ">
            <h5 class="control-label text-nowrap">
                Фото ( изнутри панель )
                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                      title=" {{ tooltips($toolips, 'owner_help_photo_inside') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
            </h5>
            <div class="col-xs-12" id="image_3">
                <label class="cabinet center-block">
                    <figure data-field-name="image_3">
                        <img data-status="image_3"
                             src="@if( $dataTypeContent->image_3){{ Voyager::image( $dataTypeContent->image_3 ) }}@else{{asset('storage/default/default.png')}}@endif"
                             class="gambar img-responsive img-thumbnail" id="item-img-output"
                             data-file-name="{{ $dataTypeContent->image_3 }}"
                             data-id="{{ $dataTypeContent->id }}"/>
                    </figure>

                    <input type="hidden" class="image_3" name="image_3">
                </label>
            </div>
            <div class="custom-file form-group">
                <input type="file" data-id="image_3" class="item-img custom-file-input center-block"
                       {{ ! $dataTypeContent->image_3 ? 'required' : ''}}
                       name="file_photo-3"/>

                <label class="custom-file-label  center-block">Выбрать фото</label>
            </div>
        </div>
        <div class="form-group col ">
            <h5 class="control-label text-nowrap">
                Фото ( внутренняя задняя часть )
                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                      title=" {{ tooltips($toolips, 'owner_help_photo_inside_back') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
            </h5>
            <div class="col-xs-12" id="image_4">
                <label class="cabinet center-block">
                    <figure data-field-name="image_4">
                        <img data-status="image_4"
                             src="@if( $dataTypeContent->image_4){{ Voyager::image( $dataTypeContent->image_4 ) }}@else{{asset('storage/default/default.png')}}@endif"
                             class="gambar img-responsive img-thumbnail" id="item-img-output"
                             data-file-name="{{ $dataTypeContent->image_4 }}"
                             data-id="{{ $dataTypeContent->id }}"/>
                    </figure>

                    <input type="hidden" class="image_4" name="image_4">
                </label>
            </div>
            <div class="custom-file form-group">
                <input type="file" data-id="image_4" class="item-img custom-file-input center-block"
                       {{ ! $dataTypeContent->image_4 ? 'required' : ''}}
                       name="file_photo-4"/>

                <label class="custom-file-label  center-block">Выбрать фото</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <h5 class="control-label text-nowrap text-center">
                Техпаспорт лицевая сторона
                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                      title=" {{ tooltips($toolips, 'owner_help_photo_registration_certificate_front') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
            </h5>
            @if(isset($dataTypeContent->data_sheet_1))
                {{--<div data-field-name="data_sheet_1" class="">--}}
                {{--<img src="@if( !filter_var($dataTypeContent->data_sheet_1, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->data_sheet_1 ) }}@else{{ $dataTypeContent->data_sheet_1 }}@endif"--}}
                {{--data-file-name="{{ $dataTypeContent->data_sheet_1 }}"--}}
                {{--data-id="{{ $dataTypeContent->id }}"--}}
                {{--style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">--}}
                {{--</div>--}}
            @endif
            <input class="file-image" type="file" name="data_sheet_1" accept="image/*">
        </div>
        <div class="form-group col-md-6">
            <h5 class="control-label text-nowrap text-center">
                Техпаспорт обратная сторона
                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                      title=" {{ tooltips($toolips, 'owner_help_photo_registration_certificate_back') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
            </h5>
            @if(isset($dataTypeContent->data_sheet_2))
                {{--<div data-field-name="data_sheet_2">--}}
                {{--<img src="@if( !filter_var($dataTypeContent->data_sheet_1, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->data_sheet_2 ) }}@else{{ $dataTypeContent->data_sheet_2 }}@endif"--}}
                {{--data-file-name="{{ $dataTypeContent->data_sheet_2 }}"--}}
                {{--data-id="{{ $dataTypeContent->id }}"--}}
                {{--style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">--}}
                {{--</div>--}}
            @endif
            <input class="file-image2" type="file" name="data_sheet_2" accept="image/*">
        </div>

    </div>

    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Обрезать</h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Отмена
                    </button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">

                    <h4 class="modal-title"><i
                            class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">
                    </button>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span
                            class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    {{--image remove btn styles--}}
    <style>
        /*.remove-single-image-crop {*/
        /*position: absolute;*/
        /*left: 6px;*/
        /*top: 22px;*/
        /*background: #fff;*/
        /*width: 20px;*/
        /*height: 20px;*/
        /*border-radius: 50%;*/
        /*color: #000;*/
        /*line-height: 18px;*/
        /*text-align: center;*/
        /*font-size: 15px;*/
        /*font-weight: 700;*/
        /*-webkit-box-shadow: 0px 0px 15px -3px rgba(0,0,0,0.75);*/
        /*-moz-box-shadow: 0px 0px 15px -3px rgba(0,0,0,0.75);*/
        /*box-shadow: 0px 0px 15px -3px rgba(0,0,0,0.75);*/

        /*}*/

        /*.remove-single-image-crop:hover {*/
        /*text-decoration: none;*/
        /*-webkit-box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);*/
        /*-moz-box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);*/
        /*box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);*/
        /*}*/
    </style>


</section>
