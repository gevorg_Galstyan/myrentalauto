<?php

namespace App\Http\Controllers\Admin;

use App\Events\BreadDataAdded;
use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class OrderDateController extends VoyagerBaseController
{
    public function store(Request $request)
    {
        $date = explode(' ~ ', $request->date);
        $request['start'] = $date[0]??'';
        $request['end'] = $date[1]??'';
        $request['author_id'] = auth()->id();

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'data' => $data
            ]);
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }
}
