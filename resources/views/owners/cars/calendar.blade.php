@extends('owners.layouts.app')


@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Календарь
        </h1>
    </div>

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">
                <div class="page-content read container-fluid">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-bordered d-flex">
                                <div class="col-md-4 align-self-start">
                                    <ul class="list-group">
                                        <li class="list-group-item"><strong>Марка и модель </strong> : <a
                                                href="{{route('voyager.cars.show', $dataTypeContent->id)}}">{{ $dataTypeContent->title}}</a>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <img src="{{Voyager::image($dataTypeContent->image_1)}}"
                                                         class="img-rounded" width="100%">
                                                </div>
                                            </div>

                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-8">
                                    <div class="d-flex flex-wrap border mb-3 justify-content-center">
                                        <div
                                            class="col align-self-center border-1 text-center m-2 d-flex justify-content-center">
                                        <span
                                            style="width: 20px; height: 20px; display: inline-block; background-color:#4ebf22"
                                            class="mx-2"></span>
                                            <span
                                                style="width: 20px; height: 20px; display: inline-block; background-color:#82e25d"
                                                class="mx-2"></span>
                                            <h5> Арендован
                                                <span class="pl-2"
                                                      aria-hidden="true"
                                                      data-toggle="tooltip" data-html="true"
                                                      data-placement="top"
                                                      title="Этими цветами обозначены те дни в которых автомобиль будет в аренде"><i
                                                        class="fas fa-question-circle"></i></span>
                                            </h5>

                                        </div>
                                        <div class="col align-self-center border-1 text-center m-2 d-flex justify-content-center">
                                            <span
                                                style="width: 20px; height: 20px; display: inline-block; background-color:#32A0DA "
                                                class="mx-2"></span>
                                            <h5> Занято
                                                <span class="pl-2"
                                                      aria-hidden="true"
                                                      data-toggle="tooltip" data-html="true"
                                                      data-placement="top"
                                                      title="Этими цветами обозначены те дни которые добились вручную как занятость"><i
                                                        class="fas fa-question-circle"></i></span>
                                            </h5>

                                        </div>
                                    </div>
                                    {!! $calendar->calendar() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="makeOrder" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Добавить Занятость!</h4>
                                <button type="button" class="close" data-dismiss="modal"></button>
                            </div>
                            <div class="modal-body" id="date-block">
                                <form action="{{route('voyager.order-dates.store')}}" method="POST" id="makeOrderForm">
                                    @csrf
                                    <div class="form-group">
                                        <input name="date" class="form-control add-event" placeholder="Дата"
                                               autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="comment"> Замечание :</label>
                                        <textarea name="note" class="form-control" rows="5" id="comment"
                                                  placeholder="Замечание"></textarea>
                                    </div>
                                    <input type="hidden" name="car_id" value="{{$dataTypeContent->id}}">
                                    <button type="submit" class="btn btn-primary">Добавить</button>
                                    <button type="button" class="btn btn-warning align-right" data-dismiss="modal">
                                        Отменить
                                    </button>
                                </form>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>

                    </div>
                </div>
                @php($all_dates = [])
                @foreach($dataTypeContent->busyDays as $item)
                    <?php
                    $startDate = new \Carbon\Carbon($item->start);
                    $endDate = new \Carbon\Carbon($item->end);
                    while ($startDate->lte($endDate)) {
                        $a = $startDate;

                        $all_dates[] = \Carbon\Carbon::parse($a)->format('d-m-Y');

                        $startDate->addDay();
                    }
                    ?>
                @endforeach

            </div>
        </div>
    </div>

@stop

@section('script')
    {!! $calendar->script() !!}
    <script src="{{asset('js/jquery.daterangepicker.js')}}"></script>


    <script>
        var locale = '{{LaravelLocalization::getCurrentLocale()}}';
        var urgency = '{{currency(setting('site.urgency'))}} ';
        var urgency_text = "{!! trim(strip_tags( tooltips($toolips, 'zasrochnost', currency(setting('site.urgency'))))) !!}";
        var ne_rabochie_vremya = '{!! trim(strip_tags( tooltips($toolips, 'ne_rabochie_vremya', currency(setting('site.during_off_hours'))))) !!}';

        order_date = jQuery.parseJSON('{!! json_encode($all_dates) !!}');
        $('.add-event').dateRangePicker({
            startOfWeek: 'monday',
            container: '#date-block',
            separator: ' ~ ',
            format: 'DD.MM.YYYY HH:mm',
            autoClose: false,
            language: locale,
            swapTime: false,

            startDate: moment().format('DD-MM-YYYY'),
            beforeShowDay: function (t) {
                var myDate = moment(t);

                var _class = '';
                var _tooltip = '';
                if (!(order_date.indexOf(moment(t).format('DD-MM-YYYY')) == -1)) {
                    _class = 'red';
                    _tooltip = 'На этот день машина занята ';

                    Арендован
                }
                var valid = (order_date.indexOf(moment(t).format('DD-MM-YYYY')) == -1);  //disable saturday and sunday
                return [valid, _class, _tooltip];
            },
        });
        $('#makeOrderForm').submit(function (form) {
            form.preventDefault();
            if ($('input[name="date"]').val()) {
                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.success) {
                            $('#makeOrder').modal('hide');
                            $('input[name="date"] ,  textarea[name="note"]').val('');
                            calendar.addEvent({
                                color: '#32A0DA',
                                rendering: 'background',
                                className: 'busy-day',
                                title: 'The Title',
                                description: data.data.note,
                                start: moment(data.data.start).format('YYYY-MM-DD'),
                                end: moment(data.data.end).add(1, 'days').format('YYYY-MM-DD'),
                            });
                            toastr.success("Занятость добавлена");

                        }
                    }
                })
            }

        });
        $('[data-toggle="tooltip"]').tooltip();
    </script>
@stop






