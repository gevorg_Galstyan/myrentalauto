<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "SITEMAP STARTED";
        Log::info("SITEMAP STARTED");
        SitemapGenerator::create(config('app.url'))
            ->hasCrawled(function (Url $url) {
                if (Str::contains($url->url, [
                        '/admin',
                        '/public/',
                        '/profile',
                        '/currency',
                        '/auth',
                        '/login',
                        '/register',
                        '/password',
                        '/c-owner',
                        '/posts?slug',
                        '/ru/',
                        '/storage/',
                        '/search?date',
                        '/search?'
                    ]) ||
                    $url->segment(1) === 'en' ||
                    $url->segment(1) === 'ru'
                ) {
                    return;
                }

                    $url->url = \LaravelLocalization::getLocalizedURL('ru', $url->url);
                    if ($url->segment(1) == 'faq') {
                        $url->setPriority(0.7);
                    } elseif ($url->segment(1) == 'car') {
                        $url->setPriority(0.8);
                    } elseif ($url->segment(1) == 'search') {
                        $url->setPriority(0.9);
                    } elseif ($url->segment(1) == 'pages') {
                        $url->setPriority(0.9);
                    } elseif ($url->segment(1) == 'posts') {
                        $url->setPriority(0.6);
                    } elseif (!$url->segment(1)) {
                        $url->setPriority(1)
                            ->addAlternate(config('app.url'), 'ru')
                            ->addAlternate(config('app.url').'/en', 'en');
                    }
                    $url->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY);


                return $url;
            })
            ->getSitemap()
            ->add(
                Url::create(config('app.url').'/en')
                ->addAlternate(config('app.url'), 'ru')
                ->addAlternate(config('app.url').'/en', 'en')
            )
            ->writeToFile(public_path('sitemap.xml'));
        Log::info("site map created");
        return 'site map created';
    }
}
