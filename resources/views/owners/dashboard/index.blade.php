@extends('owners.layouts.app')


@section('content')
    <div class="row row-cards row-deck">
        <div class="col-md-6 col-lg-12">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right text-blue">{{round((auth()->user()->cars && $orders->count() ? ($orders->count() / auth()->user()->cars->count()) * 100 : 0))}}%</div>
                            <h3 class="mb-1">{{auth()->user()->cars->count() ?? 0}} / {{$orders->count()}}</h3>
                            <div class="text-muted">Машины в аренде </div>
                        </div>
                        <div class="card-chart-bg">
                            <div id="chart-bg-users-1" style="height: 100%"></div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right text-red">{{$month_orders_count && $month_orders_success_count ? $month_orders_count / $month_orders_success_count : 0}}%</div>
                            <h3 class="mb-1">
                                {{$month_orders_count}} / {{$month_orders_success_count}}
                            </h3>
                            <div class="text-muted">Заказы  {{__('months.'. strtolower (\Carbon\Carbon::now()->subMonth()->format('F')))}}</div>
                        </div>
                        <div class="card-chart-bg">
                            <div id="chart-bg-users-2" style="height: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="card-body">
{{--                            <div class="card-value float-right text-green">-3%</div>--}}
                            <h3 class="mb-1">{{currency($income)}}</h3>
                            <div class="text-muted">Доход {{__('months.'. strtolower (\Carbon\Carbon::now()->subMonth()->format('F')))}}</div>
                        </div>
                        <div class="card-chart-bg">
                            <div id="chart-bg-users-3" style="height: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right text-yellow">9%</div>
                            <h3 class="mb-1">423</h3>
                            <div class="text-muted">Средний чек {{__('months.'. strtolower (\Carbon\Carbon::now()->subMonth()->format('F')))}}</div>
                        </div>
                        <div class="card-chart-bg">
                            <div id="chart-bg-users-4" style="height: 100%"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table" id="ownerTables">
                        <thead>
                        <tr>
                            <th class="text-center w-1"><i class="icon-people"></i></th>
                            <th>Арендатор</th>
                            <th>использование</th>
                            <th class="text-center">Машина</th>
{{--                            <th>Activity</th>--}}
{{--                            <th class="text-center">Satisfaction</th>--}}
{{--                            <th class="text-center"><i class="icon-settings"></i></th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($orders as $order)

                                <tr>
                                    <td class="text-center">
                                        <div class="avatar d-block" style="background-image: url({{$order->customer ? Voyager::image($order->customer ? $order->customer->avatar :'users/default.png') : '' }})">
                                            <span class="avatar-status bg-green"></span>
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{$order->customer ? $order->customer->full_name : 'пользователь удален '}}</div>
                                    </td>
                                    <td>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <strong>{{round(daysPercent($order->dates->start, $order->dates->end))}}%</strong>
                                            </div>
                                            <div class="float-right">
                                                <small class="text-muted">
                                                    {{$order->dates->dateString()}}
                                                </small>
                                            </div>
                                        </div>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar bg-yellow" role="progressbar" style="width: {{round(daysPercent($order->dates->start, $order->dates->end))}}%"
                                                 aria-valuenow="42" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <a href="#0" class="car-title"
                                           data-url="{{route('car.single', ['slug' => $order->car->slug])}}"
                                           data-title="{{$order->car->title}}"
                                           data-image="{{Voyager::image($order->car->image_1)}}">{{$order->car->title}}</a>
                                    </td>

{{--                                    <td>--}}
{{--                                        <div class="small text-muted">Last login</div>--}}
{{--                                        <div>4 minutes ago</div>--}}
{{--                                    </td>--}}
{{--                                    <td class="text-center">--}}
{{--                                        <div class="mx-auto chart-circle chart-circle-xs" data-value="0.42" data-thickness="3" data-color="blue">--}}
{{--                                            <div class="chart-circle-value">42%</div>--}}
{{--                                        </div>--}}
{{--                                    </td>--}}
{{--                                    <td class="text-center">--}}
{{--                                        <div class="item-action dropdown">--}}
{{--                                            <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>--}}
{{--                                            <div class="dropdown-menu dropdown-menu-right">--}}
{{--                                                <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-tag"></i> Action </a>--}}
{{--                                                <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-edit-2"></i> Another action </a>--}}
{{--                                                <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-message-square"></i> Something else here</a>--}}
{{--                                                <div class="dropdown-divider"></div>--}}
{{--                                                <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-link"></i> Separated link</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </td>--}}
                                </tr>


                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

{{--    modal show car image--}}

    <div class="modal fade" id="show-car-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
{{--                        <span aria-hidden="true">&times;</span>--}}
                    </button>
                </div>
                <div class="modal-body">
                    <div class="customer-order-image-container car-image-block">
                        <img src="" alt="Avatar" class="image img-fluid"
                             style="width:100%">
                        <div class="middle">
                            <a href="#0" class="own-btn rounded d-inline-block ora px-2 py-1 fs-15 ff-st title-block"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')

    <script>
        $('.car-title').click(function (event) {
            event.preventDefault();
           var block =  $('.car-image-block');
           block.find('img').attr('src', $(this).data('image'));
           block.find('.title-block').attr('href', $(this).data('url')).text($(this).data('title'));
            $('#show-car-image').modal();
        });

    </script>
@stop
