<?php

namespace App\Http\Controllers\Owner;

use App\Models\Post;
use App\Notifications\PostEditAdd;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = auth()->user()->posts()->paginate(10);
        return view('owners.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Post $post)
    {
        return view('owners.posts.add-edit', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, Post $post)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'body' => 'required|string',
        ]);

        $post->title = $request->title;
        $post->body = $request->body;
        $post->title_color = '#000000';
        $post->status = 'PENDING';
        $post->save();
        $admins = User::where('role_id', 1)->get();
        Notification::send($admins, new PostEditAdd($post, auth()->user(), 'add'));
        return redirect()->route('owners-posts.index')->with([
            'status' => __('user_post.add_success'),
            'alert_type' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        if ($post->authorId->id != auth()->id()) return  abort(403);
        return view('owners.posts.add-edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if ($post->authorId->id != auth()->id()) return  abort(403);
        $this->validate($request, [
            'title' => 'required|string',
            'body' => 'required|string',
        ]);

        $post->title = $request->title;
        $post->body = $request->body;
        $post->status = 'PENDING';
        $post->save();
        $admins = User::where('role_id', 1)->get();
        Notification::send($admins, new PostEditAdd($post, auth()->user(), 'edit'));
        return redirect()->route('posts.index')->with([
            'status' => __('user_post.update_success'),
            'alert_type' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
