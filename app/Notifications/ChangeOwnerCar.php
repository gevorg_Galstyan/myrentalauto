<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class ChangeOwnerCar extends Notification
{
    use Queueable;

    private $user;
    private $car;
    private $text;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $car, $text)
    {
        $this->user = $user;
        $this->car = $car;
        $this->text = $text;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line($this->car->title)
                    ->line($this->text)
                    ->action('страница', route('voyager.cars.edit', ['id' => $this->car->id]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'car_id' => $this->car->id,
            'url' => route('voyager.cars.edit', ['id' => $this->car->id]),
            'text' => $this->text,
            'title' => $this->car->title,
            'image' => $this->car->image_1
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => 'Изменения',
            'message' => $this->text.'<br><a href="'.route('voyager.cars.edit', ['id' => $this->car->id]).'">'.$this->car->title.'</a>'
        ]);
    }
}
