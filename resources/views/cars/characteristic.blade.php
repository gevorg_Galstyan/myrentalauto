<div id="accordion-spec">
    <div class="">
        <button class="spec-button w-100 " data-toggle="collapse"
                data-target="#collapse-spec" aria-expanded="true"
                aria-controls="collapse-spec">
            {{__('car.spec')}}
            <i class="fa" aria-hidden="true"></i>
        </button>
    </div>

    <div id="collapse-spec"
         class="collapse fs-14 my-1 p-2 rounded border border-secondary show"
         data-parent="#accordion-spec">
        @if ($car->make)
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.mark')}}</span>
                <span>{{$car->make->name}}</span>
            </div>
        @endif
        @if ($car->model)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.model')}}</span>
                <span>{{$car->model->name}}</span>
            </div>
        @endif
        @if ($car->type)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.class')}}</span>
                <span>{{$car->type->translate()->name}}</span>
            </div>
        @endif
        @if ($car->bodyType)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.types_of_ody')}}</span>
                <span>{{$car->bodyType->translate()->name}}</span>
            </div>
        @endif
        @if ($car->age)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.year_of_issue')}}</span>
                <span>{{$car->age}}</span>
            </div>
        @endif

        @if ($car->color)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.color')}}</span>
                <span>{{$car->color->translate()->name}} <i class="fas fa-square" style="color: {{$car->color->color}}"></i></span>
            </div>
        @endif
        @if ($car->transmission)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.transmission')}}</span>
                <span>{{__('general.'.$car->transmission)}}</span>
            </div>
        @endif
        @if ($car->fuel)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.fuel')}}</span>
                <span>{{__('car.'.$car->fuel)}}</span>
            </div>
        @endif
        @if ($car->fuel_consumption)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.average_fuel_consumption')}}</span>
                <span>{{$car->fuel_consumption.' '.__('general.l').'.'}}</span>
            </div>
        @endif
        @if ($car->motor)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.engine_capacity')}}</span>
                <span>{{$car->motor.' '.__('general.l').'.'}}</span>
            </div>
        @endif
        @if ($car->number_of_seats)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.seats')}}</span>
                <span>{{$car->number_of_seats}}</span>
            </div>
        @endif
        @if ($car->number_of_doors)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.door_count')}}</span>
                <span>{{$car->number_of_doors}}</span>
            </div>
        @endif
        @if ($car->number_of_seats_trunk)
            <hr class="my-1">
            <div class="row m-0 align-items-center justify-content-between">
                <span>{{__('car.trunk')}}</span>
                <span>{{$car->number_of_seats_trunk.' '.__('general.l').'.'}}</span>
            </div>
        @endif

    </div>
</div>
