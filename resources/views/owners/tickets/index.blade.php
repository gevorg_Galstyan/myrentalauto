@extends('owners.layouts.app')

@section('css')

@stop

@section('content')

    <div class="page-header">

            <h1 class="page-title">
                Тикеты
            </h1>
            <a class=" ml-5 btn btn-success" href="{{route('tickets.create')}}">
                <i class="fe fe-plus-circle"></i>
                Добавить
            </a>

    </div>
    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">

                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable" id="ownerTables">
                        <thead>
                        <tr>
                            <th class="w-1 text-center">Id</th>
                            <th class="text-center">Заголовок</th>
                            <th class="text-center">Статус</th>
                            <th class="text-center">ДАТА</th>
                            <th class="text-center">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets as $ticket)
                            <tr>
                                <td class="text-center"><span class="text-muted">#{{$ticket->ticket_number}}</span></td>
                                <td class="text-center">
                                    {{$ticket->title}}
                                </td>
                                <td class="text-center">
                                    <span class="badge badge-{{$ticket->status == 'closed' ? 'danger': 'success'}}">
                                        {{__('ticket.status.'.$ticket->status)}}
                                    </span>
                                </td>
                                <td class="text-center">
                                    {{$ticket->created_at->format('d-m-Y H:i')}}
                                </td>
                                <td class="text-center">
                                    <a href="{{route('tickets.show', ['ticket' => $ticket->ticket_number])}}"
                                       data-toggle="tooltip" data-html="true" title="Просмотр | Ответить "
                                       class="btn btn-info">
                                        Просмотр | Ответить
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')

@stop
