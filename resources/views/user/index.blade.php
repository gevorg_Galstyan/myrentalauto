@extends('layouts.app')
@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop

@section('content')

    <div class="own-container ">
        <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center">

            <!-- edit form column -->
            <div class="col-md-12  ff-st row ">
                <div class="col-sm-4 mb-3">
                    <a href="{{url()->previous()}}" class="btn own-btn ora   d-inline-block  border-radius-20"
                       title="{{__('general.back')}}">
                        <i class="fas fa-arrow-left"></i>
                        <span class="">{{__('general.back')}}</span>
                    </a>
                </div>
                <div class="col-sm-4 mb-3">
                    <h1 class="text-center">{{__('user.profile')}}</h1>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="d-flex justify-content-center mb-3">
                        <div class="p-2 align-self-center">
                            <img src="{{Voyager::image(auth()->user()->avatar)}}" class="rounded-circle" width="100px"
                                 height="100px" alt="avatar">
                        </div>

                        <div class="p-2 align-self-center">
                            <h3 class="align-middle">{{auth()->user()->fullName}}</h3>
                        </div>
                        @if (auth()->user()->provider)
                            <div class="p-2 align-self-center">
                                <div class="soc-iconInfo m-0 ml-4" data-toggle="tooltip" data-html="true"
                                     data-placement="top"
                                     title="{{tooltips($toolips, 'via_social_network', auth()->user()->provider)}}">

                                    <i class="fab {{auth()->user()->provider == 'vkontakte' ? 'fa-vk' : 'fa-facebook-f'}}"></i>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="d-flex   mb-3">
                        <div class="flex-column w-100">
                            <div
                                class="p-2 d-flex justify-content-between flex-column flex-sm-column flex-column flex-md-row border-bottom">
                                <div>
                                    <b class="fw-700 col-lg-3">{{__('user.email')}}: </b>
                                </div>
                                <div>
                                    <span class="fw-400 col-lg-9">{{auth()->user()->email}}</span>
                                </div>
                            </div>
                            <div
                                class="p-2 d-flex justify-content-between flex-sm-column flex-column flex-md-row border-bottom">
                                <div>
                                    <b class="fw-700 col-lg-3">{{__('user.phone')}}: </b>
                                </div>
                                <div>
                                    <span class="fw-400 col-lg-9">{{auth()->user()->number??''}}</span>
                                </div>
                            </div>
                            <div
                                class="p-2 d-flex justify-content-between flex-sm-column flex-column flex-md-row border-bottom">
                                <div>
                                    <b class="fw-700 col-lg-3">{{__('user.birthday')}}: </b>
                                </div>
                                <div>
                                    <span
                                        class="fw-400 col-lg-9">{{auth()->user()->birthday?date('d : m : Y', strtotime(auth()->user()->birthday)):''}}</span>
                                </div>
                            </div>
                            <div
                                class="p-2 d-flex justify-content-between flex-sm-column flex-column flex-md-row border-bottom">
                                <div>
                                    <b class="fw-700 col-lg-3">{{__('user.citizenship')}}: </b>
                                </div>
                                @php($citizenship = isset(auth()->user()->profile->citizenship)?auth()->user()->profile->citizenship : null)
                                @php($citizenship = $citizenship ? getCountryName($citizenship) : '')
                                <div>
                                    <span class="fw-400 col-lg-9">{{$citizenship}}</span>
                                </div>
                            </div>
                            <div
                                class="p-2 d-flex justify-content-between flex-sm-column flex-column flex-md-row border-bottom">
                                <div>
                                    <b class="fw-700 col-lg-3">{{__('user.country')}}: </b>
                                </div>
                                {{--{{dd(getCountryName())}}--}}
                                @php($region = isset(auth()->user()->profile->region)?auth()->user()->profile->region : null)
                                @php($country = isset(auth()->user()->profile->country)?auth()->user()->profile->country : null)
                                @php($city = isset(auth()->user()->profile->city)?auth()->user()->profile->city : null)
                                <div>
                                    <span class="fw-400 col-lg-9">{{getCountryName($country)}}
                                        {{$region ? ', '.getStateName($region) : ''}}
                                        {{$city ? ', '.$city : ''}} </span>
                                </div>
                            </div>
                            <div
                                class="p-2 d-flex justify-content-between flex-sm-column flex-column flex-md-row border-bottom">
                                <div>
                                    <b class="fw-700 col-lg-3">{{__('user.driving_experience')}}: </b>
                                </div>
                                @php($experience = isset(auth()->user()->profile->experience)?auth()->user()->profile->experience : null)
                                <div>
                                    <span class="fw-400 col-lg-9">{{$experience ? date('Y') - $experience : '' }}</span>
                                </div>
                            </div>

                            <div class="p-2 mt-5 fs-14 ff-ms">
                                @if (auth()->user()->profile->terms_1 && auth()->user()->profile->terms_2)
                                    <div class="">
                                        <b class="fw-700 col-lg-3 text-success">
                                            <i class="far fa-check-circle"></i>
                                            @php($confidentiality = \App\Models\Page::where('slug', 'confidentiality-policy')->first())
                                            {{$confidentiality ? $confidentiality->translate()->title : ''}}
                                        </b>
                                    </div>
                                    <div class="">
                                        <b class="fw-700 col-lg-3 text-success">
                                            <i class="far fa-check-circle"></i>
                                            @php($ferta = \App\Models\Page::where('slug', 'public-offer')->first())
                                            {{$ferta ? $ferta->translate()->title : ''}}
                                        </b>
                                    </div>
                                @else
                                    <div class="">
                                        <b class="fw-700 col-lg-3 ">
                                            <i class="fas fa-exclamation"></i>
                                            {{__('user.to_book_a_ar_you_need_to_fill_out_a_profile_and_agree_to_the_terms')}}
                                        </b>
                                    </div>
                                @endif

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="col-md-12">
                        <fieldset class="all-border">
                            <legend><h6 class="mb-0">{{__('user.passport_documents')}}</h6></legend>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="confirmation-images">
                                        <a data-fancybox="gallery"
                                           href="{{Voyager::image(auth()->user()->profile->passport_one ?? 'img/icons/img.png')}}">
                                            <img
                                                src="{{Voyager::image(auth()->user()->profile->passport_one ?? 'img/icons/img.png')}}"
                                                class="img-fluid" id="passport_one"
                                                alt="avatar">
                                        </a>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-center confirmation-images">
                                        <a data-fancybox="gallery"
                                           href="{{Voyager::image(auth()->user()->profile->passport_two ?? 'img/icons/img.png')}}">
                                            <img
                                                src="{{Voyager::image(auth()->user()->profile->passport_two ?? 'img/icons/img.png')}}"
                                                class="img-circle" id="passport_two"
                                                alt="avatar ">

                                        </a>
                                    </div>
                                    <div class="unit">

                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="all-border text-right">
                            <legend><h6 class="mb-0">{{__('user.vod_documents_certificates')}}</h6></legend>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="confirmation-images">
                                        <a data-fancybox="gallery"
                                           href="{{Voyager::image(auth()->user()->profile->license_one ?? 'img/icons/img.png')}}">
                                            <img
                                                src="{{Voyager::image(auth()->user()->profile->license_one ?? 'img/icons/img.png')}}"
                                                class="" id="license_one" alt="avatar">
                                        </a>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="text-center confirmation-images">
                                        <a data-fancybox="gallery"
                                           href="{{Voyager::image(auth()->user()->profile->license_two ?? 'img/icons/img.png')}}">
                                            <img
                                                src="{{Voyager::image(auth()->user()->profile->license_two ?? 'img/icons/img.png')}}"
                                                class="img-circle" id="license_two"
                                                alt="avatar">
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </fieldset>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="d-flex justify-content-center mt-3">
                        <a href="{{route('profile.edit')}}"
                           class="own-btn d-inline-block">{{__('user.edit_profile')}}</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Large modal -->
    <div class="modal fade show-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered mw-75 mw-sm-100">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="order-content"></div>
            </div>
        </div>
    </div>
    <div id="overlay">
        <div id="popup" class="popup-content">
            <div id="close">X</div>
            <div class="order-status-container"></div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@stop

