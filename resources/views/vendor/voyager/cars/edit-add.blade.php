@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">

        {{$edit ? 'Редактирование автомобиля' : 'Добавление автомобиля'}}
        {{--<i class="{{ $dataType->icon }}"></i>--}}
        {{--{{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}--}}
    </h1>
    @if ($dataTypeContent->getKey())

        <a href="{{route('voyager.cars.show', $dataTypeContent->id )}}" class="btn btn-warning btn-sm browse_bread"
           style="margin-right: 0;">
            <i class="voyager-plus"></i> Просмотр
        </a>

        <button
            class="btn btn-danger btn-sm browse_bread wrong-fill"
            aria-hidden="true"
            data-toggle="tooltip" data-html="true"
            title="Написать владельцу о неправильном заполнение машины"
            style="margin-right: 0;">
            неправильно заполнен
        </button>

        <button
            class="btn btn-info btn-sm browse_bread carHistory"
            title="История изменений"
            style="margin-right: 0;">
            История изменений
        </button>
    @endif
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows')};
                            @endphp

                            @foreach($dataTypeRows as $row)
                            <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                            style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif
                                <div
                                    class="form-group  @if($row->type == 'hidden') hidden @endif @if($row->type == 'relationship' && ($row->details->column == 'car_make_id' || $row->details->column == 'car_model_id' || $row->details->column == 'car_type_id' || $row->details->column == 'owner_id' || $row->details->column == 'body_type_id' || $row->details->column == 'color_id')) col-md-2 @else col-md-{{ $display_options->width ?? 12 }} @endif {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <h5 class="control-label">{{ $row->display_name }}</h5>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')

                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add')])
                                    @elseif ($row->type == 'relationship')
                                        @include('vendor.voyager.cars.relationship', ['options' => $row->details])
                                    @elseif ($row->type == 'multiple_images')
                                        @include('vendor.voyager.cars.multiple_images', ['options' => $row->details])
                                    @else

                                        @if($row->field == 'green_card')
                                            @php($greenCard = json_decode($dataTypeContent->{$row->field}, true))
                                            <div class="panel relation-group">
                                                <div class="panel-heading" role="tab" id="heading4">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion"
                                                           href="#greenCard" aria-expanded="false"
                                                           aria-controls="collapse3">
                                                            @if(isset($greenCard) && count($greenCard))
                                                                <i class="more-less fas fa-minus"></i>
                                                            @else
                                                                <i class="more-less fas fa-plus"></i>
                                                            @endif
                                                            Грин карта
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="greenCard"
                                                     class="panel-collapse collapse {{isset($greenCard) && count($greenCard) ? 'in' : ''}}"
                                                     role="tabpanel"
                                                     aria-labelledby="heading4">
                                                    <div class="panel-body">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th class=""></th>
                                                                <th class="">Страна:</th>
                                                                @foreach(config('car.green_card.days') as $days)
                                                                    <th class="">{{$days}}</th>
                                                                @endforeach
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach(config('car.green_card.country') as $country_key => $country)

                                                                <tr>
                                                                    <td>
                                                                        <input
                                                                            class="form-check-input change-greenCard-opt"
                                                                            type="checkbox"
                                                                            value=""
                                                                            data-target="greenCard-{{$country_key}}"
                                                                            name="green_card[{{$country_key}}]"
                                                                            {{isset($greenCard[$country_key])?'checked':''}}>
                                                                    </td>
                                                                    <td {{isset($greenCard[$country_key])?'':'disabled'}}>
                                                                        {{$country}}
                                                                    </td>

                                                                    @foreach(config('car.green_card.days') as $days_key => $days)
                                                                        <td>
                                                                            <input type="text" class="form-control "
                                                                                   data-status="greenCard-{{$country_key}}"
                                                                                   name="green_card[{{$country_key}}][{{$days_key}}]"
                                                                                   value="{{ $greenCard[$country_key][$days_key]??''}}"
                                                                                   placeholder="{{$days}}"
                                                                                   {{isset($greenCard[$country_key])?'':'disabled'}} required>
                                                                        </td>
                                                                    @endforeach
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>


                                        @elseif($display_options->class??'' == 'price')

                                            <div class="input-group">
                                                <input type="text" name="{{$row->field}}"
                                                       class="form-control {{$display_options->class??''}}"
                                                       value="{{ $dataTypeContent->{$row->field} }}" placeholder="%">
                                                <div class="input-group-append">
                                                    <span class="input-group-text disabled-price"></span>
                                                </div>

                                            </div>
                                            <span class=""
                                                  aria-hidden="true"
                                                  data-toggle="tooltip" data-html="true"
                                                  data-placement="right"
                                                  title="{{$display_options->my_description}}"><i
                                                    class="fas fa-question-circle"></i></span>
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif


                                    @endif


                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                                @if($display_options->br??false)
                                    <div class="col-md-12"
                                         style="border-bottom: 1px dashed; margin:30px 0 !important; "></div>
                                @endif
                            @endforeach
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit"
                                    style="position: fixed; bottom: 0px;"
                                    class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->

    @include('vendor.voyager.cars.wrong-fill', ['lists' => $dataTypeContent->wrongFills])
@stop

@section('javascript')
    <script>
        $(document).on('blur', '.input-delivery_location', function () {
            var input = $(this);
            $.ajax({
                url: '/get-translate/y/' + input.val(),
                success: function (response) {
                    input.parents('.d-flex').find('.input-delivery_location_en').text(response.translate);
                }
            })
        });

        function addYMap(element) {
            ymaps.ready(function () {
                var suggestView = new ymaps.SuggestView(element, {
                    offset: [0, 8],

                    provider: {
                        suggest: function (request, options) {
                            var y = $('#location').height() + 250;
                            if ($(window).scrollTop() < y) {
                                $('html, body').animate({scrollTop: y + 'px'}, 800)
                            }
                            var parseItems = ymaps.suggest(request).then(function (items) {
                                for (var i = 0; i < items.length; i++) {
                                    var displayNameArr = items[i].displayName.split(',');

                                    var newDisplayName = [];
                                    for (var j = 0; j < displayNameArr.length; j++) {
                                        if (displayNameArr[j].indexOf('район') == -1) {
                                            newDisplayName.push(displayNameArr[j]);
                                        }
                                    }
                                    items[i].displayName = newDisplayName.join();
                                }
                                // Tour.run([
                                //     {
                                //         element: $('#a-1'),
                                //         content: 'sdsadasdasd'
                                //     }
                                // ]);
                                return items;
                            });
                            return parseItems;
                        }
                    }
                });
                suggestView.events.add('select', function (event) {
                    $('.currentLocationBtn').removeClass('currentLocActive')
                });
            });
        }


        $('.car-images').change(function () {
            var files = $(this)[0].files.length;
            readImage(files)
        });
        //        function readImage(files) {
        //            if (window.File && window.FileList && window.FileReader) {
        //
        //                var output = $(".preview-images-zone");
        //
        //                for (let i = 0; i < files.length; i++) {
        //                    var file = files[i];
        //                    if (!file.type.match('image')) continue;
        //
        //                    var picReader = new FileReader();
        //
        //                    picReader.addEventListener('load', function (event) {
        //                        var picFile = event.target;
        //                        var html =  '<div class="preview-image preview-show-' + num + '">' +
        //                                '<div class="image-cancel" data-no="' + num + '">x</div>' +
        //                                '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
        //                                '<div class="tools-edit-image"><a href="javascript:void(0)" data-no="' + num + '" class="btn btn-light btn-edit-image">edit</a></div>' +
        //                                '</div>';
        //
        //                        output.append(html);
        //                        num = num + 1;
        //                    });
        //
        //                    picReader.readAsDataURL(file);
        //                }
        //                $("#pro-image").val('');
        //            } else {
        //                console.log('Browser not support');
        //            }
        //        }


        //        $('#image-content input[type="file"]').change(function () {
        //            var total_file = $('#image-content input[type="file"]')[0].files.length;
        //            $('.preview').remove();
        //            for (var i = 0; i < total_file; i++) {
        //                var image = '<div class="img_settings_container preview"  style="float:left;padding-right:15px;"><span >Предварительно </span><img src="'+ URL.createObjectURL(event.target.files[i]) +'"  style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;"></div>'
        //                if ($('.img_settings_container').length) {
        //                    $('.img_settings_container').last().after(image);
        //                }else{
        //                    $('#image-content').append(image)
        //                }
        //
        //            }
        //        });
        $(document).on('change', '.change-price', function () {
            if ($(this).is(':checked')) {
                $($(this).data('target')).attr('disabled', false).focus();
                $($(this).data('target') + '-order').attr('disabled', false);
            } else {
                $($(this).data('target')).attr('disabled', true);
                $($(this).data('target') + '-order').attr('disabled', true);
            }
        });

        $(document).on('change', '.change-insurances', function () {
            if ($(this).is(':checked')) {
                $('.' + $(this).data('target')).attr('disabled', false)[0].focus();
            } else {
                $('.' + $(this).data('target')).attr('disabled', true);
            }
        });

        $(document).on('change', '.change-greenCard-opt', function () {
            var country_key = $(this).data('target');
            if ($(this).is(':checked')) {
                $('[data-status="' + country_key + '"]').prop('disabled', false)
            } else {
                $('[data-status="' + country_key + '"]').prop('disabled', true)
            }
        });


        var params = {};
        var $file;
        //        $('[name="disabled-price"]').prop('disabled', true);
        percent();
        $('.price, [name="price_1"]').keyup(function () {
            percent();
        });

        function percent() {
            $('.disabled-price').each(function (i) {
                var percent = $('.price').eq(i).val() > 0 ? $('.price').eq(i).val() : 0
                var price = parseInt($('[name="price_1"]').val() > 0 ? $('[name="price_1"]').val() : 0);
                var this_price = price - (price * (percent / 100));
                $(this).text(parseInt(this_price))
            });
        }

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                };

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        function deleteHandlerCrop(tag) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: false,
                    crop: true,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-single-image-crop', deleteHandlerCrop('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        if (params.crop) {
                            $file.attr('src', '{{asset('storage/default/default.png')}}')
                        } else {
                            $file.parent().fadeOut(300, function () {
                                $(this).remove();
                            })
                        }

                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        function create_UUID() {
            var dt = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (dt + Math.random() * 16) % 16 | 0;
                dt = Math.floor(dt / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        }

        $(document).ready(function () {
            // $('.input-delivery_location').each(function () {
            //    addYMap($(this).attr('id'));
            // });
            var addButton = $('.add_button');
            var content = $('.delivery-content'); //Input field wrapper
            //Once add button is clicked
            $(addButton).click(function () {
                var u_id = create_UUID();
                var fieldHTML = '<div class="d-flex removed">\n' +
                    '                                        <div class="form-check col-2 m-2">\n' +
                    '                                            <input type="text" id="' + u_id + '"  class="form-control input-delivery_location" name="delivery_locations[]"\n' +
                    '                                                   value=""\n' +
                    '                                                   placeholder="Место "/>\n' +
                    '                                        </div>\n' +
                    '\n' +
                    '                                        <div class="form-check col-2 m-2">\n' +
                    '\n' +
                    '                                            <input type="text" class="form-control" name="delivery_prices[]"\n' +
                    '                                                   value=""\n' +
                    '                                                   placeholder="Цена "/>\n' +
                    '                                        </div>\n' +
                    '\n' +
                    '                                        <div class="form-check col-1 m-2">\n' +
                    '                                            <a href="javascript:void(0);" class="remove_button" title="Add field">\n' +
                    '                                                <i class="fas fa-minus-circle fa-2x"></i>\n' +
                    '                                            </a>\n' +
                    '                                        </div>\n' +
                    '\n' +
                    '<div class="col-md-12 input-delivery_location_en"></div>' +
                    '                                    </div>'; //New input field html
                var x = 1; //Initial field counter is 1
                $(content).append(fieldHTML); //Add field html
                // addYMap(u_id)

            });

            //Once remove button is clicked
            $(content).on('click', '.remove_button', function (e) {
                e.preventDefault();
                $(this).parents('.d-flex.removed').remove();
            });
        });

        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus fa-minus');
        }

        $('.relation-group').on('hidden.bs.collapse', toggleIcon);
        $('.relation-group').on('shown.bs.collapse', toggleIcon);


        $(document).ready(function () {
            var deposit = $('[name="deposit"]');
            var deposit_type = $('[name="deposit_type"]');
            var deposit_price = $('[name="deposit_price"]');


            deposit.change(function () {
                if ($(this).is(':checked')) {
                    deposit_type.prop('disabled', false);
                    deposit_price.prop('disabled', false);
                    $('#deposit_type, #deposit_price').removeClass("disabledbutton");
                } else {
                    deposit_type.prop('disabled', true);
                    deposit_price.prop('disabled', true);
                    $('#deposit_type, #deposit_price').addClass("disabledbutton");

                }
            });
            if (!deposit.is(':checked')) {
                deposit_type.prop('disabled', true);
                deposit_price.prop('disabled', true);
                $('#deposit_type, #deposit_price').addClass("disabledbutton");
            }

        });

        $("select.select2-ajax-model").select2({
            width: "100%",
            ajax: {
                url: "{{route('voyager.' . $dataType->slug.'.relation')}}",
                data: function (e) {
                    return {
                        search: e.term,
                        type: $(this).data("get-items-field"), page: e.page || 1,
                        parent: $('select[name="car_make_id"]').val(),
                    }
                }
            }
        }), $(this).on("select2:select", function (e) {
            var t = e.params.data;
            $(e.currentTarget).find("option[value='" + t.id + "']").attr("selected", "selected")
        }), $(this).on("select2:unselect", function (e) {
            var t = e.params.data;
            $(e.currentTarget).find("option[value='" + t.id + "']").attr("selected", !1)
        })
        $('select[name="car_make_id"]').change(function () {
            $("select.select2-ajax-model").select2('val', '');
            $("select.select2-ajax-model").html('');
        });

        $('.wrong-fill').click(function () {
            $('#wrong-fill-modal').modal();
        })


        $('.carHistory').click(() => {
            if (!$(`#carHistory`).length) {
                let dialog = jsPanel.create({
                    id: 'carHistory',
                    closeOnEscape: true,
                    headerToolbar: '',
                    headerTitle: 'История изменений ',
                    position: {
                        my: "center top",
                        at: "center top",
                        offsetX: parseInt(window.pageXOffset),
                        offsetY: parseInt(window.pageYOffset) + 50
                    },
                    contentSize: '1000 auto',
                    contentOverflow: 'auto',
                    contentFetch: {
                        resource: '{{route('voyager.cars.history.show', $dataTypeContent)}}',
                        beforeSend: function () {
                            this.headerlogo.innerHTML = "<span class='fa fa-spinner fa-spin' style='margin-left:8px'></span>";
                        },
                        done: (panel, response) => {
                            panel.headerlogo.innerHTML = "";
                            panel.content.style.padding = '0.5rem';
                            panel.content.innerHTML = response;
                            panel.titlebar.innerHTML = title;
                        },
                    }
                });
                jsPanel.setClass(dialog, 'position-absolute');
            } else {
                document.getElementById('carHistory').front();
                $(`#${selector}`).css({
                    top: parseInt(window.pageYOffset) + 'px',
                });
            }
        });

    </script>
@stop
