<div class="col-xl-6  my-3">
    <div class="map-car min-width-320 shadow-lg p-3 bg-white border-20 rounded">
        <div class="map-car-body d-flex align-items-center">
            <div class="map-car-img-part"><img src="{{Voyager::image($car->image_1)}}"
                                               alt="{{$car->title}}" title="{{$car->title}}"
                                               class="rounded" width="100px"></div>
            <div class="map-car-info-part">
                <div class="d-flex align-items-center  ml-3">
                    <span>{{$car->title}}</span>
                </div>
                <div class="d-flex align-items-center fs-12 color-light ml-3">
                    <div class="stars-info d-flex align-items-center">
                        <div class="stars stars--large mr-1 fs-16">
                            <span style="width: 50%"></span>
                        </div>
                        <span>(50)</span>
                    </div>
                    <span class="px-1">|</span>
                    <div class="ml-1"> {{$car->age}}</div>
                    <span class="px-1">|</span>
                    <div class="ml-1"> {{$car->number_of_seats}} мест</div>
                </div>
                <div class="d-flex flex-wrap align-items-center  fs-12 color-light ml-3">
                    <div class="pl-0 col-6">
                        <p class="fs-13 color-light mb-2 text-truncate" data-toggle="tooltip"
                           title="{{$car->translate()->location_title}}">
                            <img src="{{asset('storage/img/icons/location.png')}} " alt="location"
                                 class="inline-img-width"/>
                            <span>{{$car->translate()->location_title}}</span>
                        </p>
                    </div>
                    <div class="col-6 p-0">
                        <a href="{{route('car.single', array_merge(['slug' => $car->slug], request()->only('location', 'date', 'attributes')))}}"
                           class="own-btn rounded d-inline-block ora px-2 py-1 fs-12">
                            <span class="ff-st">{!! currency($car->price_1) !!}</span>/{{__('car.day')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
