<?php

namespace App\Actions;

use App\User;
use TCG\Voyager\Actions\AbstractAction;

class ShowProfile extends AbstractAction
{
    public function getTitle()
    {
        return 'Профиль';
    }

    public function getIcon()
    {
        return 'fas fa-user';
    }

    public function getPolicy()
    {
        return 'profile';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-default pull-right ml-2',
        ];
    }

    public function getDefaultRoute()
    {
        $user = User::find($this->data->id);
        if ($user){
            if ($user->role_id == 2) return route('voyager.profiles.show', ['id' => $user->profile->id ?? 0, 'redirect' => 'users']);
            elseif ($user->role_id == 3) return route('voyager.owner-profiles.show', ['id' => $user->profile->id ?? 0, 'redirect' => 'users']);
        }

    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }


}
