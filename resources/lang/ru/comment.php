<?php
return [
    'load_more' => 'Загрузи больше',
    'reply' => 'Ответить',
    'enter_your_message_here' => 'Введите ваше сообщение здесь:',
    'send' => 'ОТПРАВИТЬ',
    'cancel' => 'отменить',
    'reply_to_comment' => 'Ответить на комментарий',
    'edit' => 'редактировать',
    'edit_comment' => 'Редактировать комментарий',
    'update_your_message_here' => 'Обновите ваше сообщение здесь:',
    'update' => 'Обновить',
    'no_comments_yet' => 'Пока нет комментариев.',
    'authorization_required' => 'Требуется авторизация',
    'you_must_be_logged_in_to_leave_a_comment' => 'Вы должны войти, чтобы оставить комментарий.',

];
