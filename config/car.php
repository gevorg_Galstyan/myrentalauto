<?php


return [
    'green_card' => [
        'country' =>[
            'eur' => 'Европа',
            'rus' => 'Россия',
            'ukr_mold' => 'Украина и Молдова',
        ],
        'days' => [
            '0_15' => 'От 1 до 15 дней։',
            '16_30' => 'От 16 до 1-го месяца:',
            '31_365' => 'От 1-го месяца до года:'
        ]
    ],
    'order' => [
        'class' => [
            'requested' => 'warning',
            'confirmed' => 'primary',
            'pending' => 'secondary',
            'completed' => 'success',
            'canceled' => 'danger',
            'paid' => 'success',
            'failed' => 'secondary',
            'rejected' => 'secondary',
            'no_paid' => 'secondary',
            'expired' => 'secondary',
            'fully_paid' => 'success'
        ]
    ]
];
