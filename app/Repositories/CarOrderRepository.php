<?php

namespace App\Repositories;


use App\Models\Car;
use App\Models\DeliveryLocation;
use App\Models\Order;
use App\Models\OrderDate;
use App\Models\OrderHitory;
use App\Notifications\CarRequestAdmin;
use App\Notifications\CarRequestOwner;
use App\Services\MpiService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class CarOrderRepository
{

    public static function getDate($date)
    {
        $data['dates'] = explode(' ~ ', $date);
        $data['start'] = Carbon::parse($data['dates'][0]);
        $data['end'] = Carbon::parse($data['dates'][1]);
        return $data;
    }

    public static function getHtml(Request $request, Car $car, $days)
    {
        $price = self::getPrice($car, $days);
        $html = view('cars.total-block', compact('car', $request))->render();
        return [
            'day_price' => currency($price),
            'html' => $html
        ];
    }

    private static function getPrice($car, $days)
    {
        return getCarPrice($car, $days);
    }

    public static function getDays($dates)
    {
        return $dates['end']->addMinutes(-31)->diffInDays($dates['start']) + 1;
    }

    public static function getOrder($barCode = false)
    {
        if ($barCode && checkBarCode($barCode)) {
            $order = Order::where('barcode_number', $barCode)->firstOrFail();
        } else {
            $order = new Order();
        }
        return $order;

    }

    public static function fillingOrder($data, $order, $car, $dates, $customer,  $status = false)
    {
        $order->car_id = $car->id;
        $order->customer_id = auth()->id();
        $order->start_date = $dates['start'];
        $order->end_date = $dates['end'];

        if ($order->status != 'confirmed') {
            if ($status){
                $order->status = $status;
            }else{
                $order->status = $car->on_request ? 'requested' : 'pending';
            }
        }
        $days = self::getDays($dates);
        $price = self::getPrice($car, $days);
        $order->currency = session('currency');
        $order->price_per_day = currency($price, null, false);
        $order->note = @$data['comment'];
        $order->days = $days;
        $attributes = [];

        if (@$data['attributes'] && is_array(@$data['attributes'])) {
            foreach ($data['attributes'] as $attribute) {
                $attr = $car->filters()->where('filters.id', $attribute)->first();
                $attributes[] = [
                    'id' => $attr->id,
                    'price' => ceil(getAttributePricePerPercent($car, $attr->pivot->price, @$data['days'])),
                    'disposable' => $attr->disposable,
                ];
            }
        }

        $order->attributes = json_encode($attributes);

        $insurances = [];
        if (@$data['insurances'] && is_array(@$data['insurances'])) {
            foreach ($data['insurances'] as $item) {
                $insurance = $car->insurances()->where('insurances.id', $item)->first();
                $insurances[] = [
                    'id' => $insurance->id,
                    'price' => $insurance->pivot->price,
                ];
            }
        }
        $order->insurances = json_encode($insurances);

        $green_card = [];
        if (@$data['green_card']) {
            $green_cards = json_decode($car->green_card, true);
            $green_card = [
                'name' => $data['green_card'],
                'price' => $green_cards[$data['green_card']][getGreenCardDayKey($data['days'])],
            ];
        }
        $order->green_card = json_encode($green_card);


        $other_details = [];
        $other_details['take_no_work_time'] = @$data['other_details']['take_no_work_time'] ? setting('site.during_off_hours') : false;
        $other_details['return_in_non_working_time'] = @$data['other_details']['return_in_non_working_time'] ? setting('site.during_off_hours') : false;
        $other_details['urgency'] = @$data['other_details']['urgency'] ? setting('site.urgency') : false;
        $other_details['took'] = ($took = DeliveryLocation::where('id', @$data['took'])->first()) ? [
            'name' => $took->location,
            'price' => currency($took->price, null, false),
        ] : false;
        $other_details['give'] = ($give = DeliveryLocation::where('id', @$data['give'])->first()) ? [
            'name' => $give->location,
            'price' => currency($give->price, null, false),
        ] : false;
        $order->other_details = json_encode($other_details);

        $total_prices = carTotalPrice($car, @$data['days'], (@$data['attributes'] ?? []), (@$data['green_card'] ?? []), @$data['insurances'] ?? []);
        $origin_total = carTotalPrice($car, @$data['days'], (@$data['attributes'] ?? []), (@$data['green_card'] ?? []), @$data['insurances'] ?? [], 'BYN');

        $order->total_price = currency($total_prices['total'], null, false);
        $order->price_to_pay = currency($total_prices['to_pay'], null, false);
        $order->origin_amount = currency($origin_total['to_pay'], 'BYN', false);
        $order->origin_total = currency($origin_total['total'], 'BYN', false);
        $order->deposit = currency($car->deposit_price, null, false);
        $order->barcode_number = $order->barcode_number ?? generateBarcodeNumber();
        $order->save();

        $order_date = new OrderDate();
        $order_date->start = $dates['start'];
        $order_date->end = $dates['end'];
        $order_date->car_id = $car->id;
        $order_date->order_id = $order->id;
        $order_date->save();

        if ( $order->status == 'requested') {
            $history['author'] = 'user';
            $history['user_name'] = $customer->full_name;
            $history['user_id'] = $customer->id;
            $history['act'] = 'request';
            $history['type'] = 'request';
            $history['date'] = Carbon::now();

            $orderHistory = new OrderHitory();
            $orderHistory->order_id = $order->id;
            $orderHistory->details = json_encode($history);
            $orderHistory->save();
        }

    }

}
