<section class="own-container py-3 mt-5 " id="how-its-works">
    <h4 class="text-center mb-3 fw-700 sub-heading">{{__('home.how_it_work')}}</h4>

{{--            <ul class="nav nav-tabs row justify-content-center mb-3 border-0" id="ow-it-work" role="tablist">--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link active left-border-radius-20" data-toggle="tab" href="#user" role="tab"--}}
{{--                       aria-selected="true">Арендатор</a>--}}
{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link right-border-radius-20" data-toggle="tab" href="#owner" role="tab"--}}
{{--                       aria-selected="false">Владелец</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
    <div class="tab-content bg-white border-radius-20 mt-5" id="ow-it-work-content">
        <div class="tab-pane fade show active " id="user" role="tabpanel">
            <div class="row m-0 p-3 align-items-center">
                <div class="col-md-6">
                    <div id="how-it-work-accordion" class="pl-3">
                        @foreach ($how_its_works['user']??[] as $item)
                            <div class="card">
                                <div class="side-animate">
                                    <span class="side-number">{{$loop->index + 1}}</span>
                                </div>
                                <div class="card-header {{$loop->last?'border-0' :''}}  ml-3"
                                     id="heading-user{{$item->id}}">
                                    <h6 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse"
                                                data-target="#ow-it-work-{{$item->id}}"
                                                aria-expanded="true" aria-controls="collapse-{{$item->id}}">
                                            {{$item->myTranslate()->title}}
                                            <span class="float-right">

                                                <i class="fas fa-chevron-down {{--{{$loop->first ? 'fa-chevron-up' : 'fa-chevron-down'}}--}}"></i>
                                            </span>
                                        </button>
                                    </h6>
                                </div>
                                <div id="ow-it-work-{{$item->id}}"
                                     class="collapse ml-3 {{$loop->last?'border-top' :''}}  {{--{{$loop->first ? 'show' : ''}}--}}"
                                     data-index="{{$loop->index}}"
                                     data-image="{{ownMakeImage( 647,  0 , 75 ,  $item->image)}}"
                                     data-parent="#how-it-work-accordion">
                                    <div class="card-body ff-ms">
                                        {!! $item->myTranslate()->body !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-6 mt-5 mt-lg-0 mt-xl-0 mt-md-0 d-none d-sm-block">
                    <div class="how-it-work-carousel owl-carousel">
                        <div class="item how-it-work-image-content">
                            <a href="{{ Voyager::image($how_its_works['user']->first()->image) }}" data-fancybox="gallery">
                                <img src="{{ ownMakeImage( 647,  0 , 75 ,  $how_its_works['user']->first()->image)}}" alt="{{$how_its_works['user']->first()->title}}" class="w-100 border-radius-20 border-silver">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        OWNER --}}
        {{--        <div class="tab-pane fade " id="owner" role="tabpanel">--}}
        {{--            <div class="row m-0 p-3">--}}
        {{--                <div class="col-md-6">--}}
        {{--                    <div id="how-it-work-accordion-owner" class="pl-3">--}}
        {{--                        @foreach ($how_its_works['owner']??[] as $item)--}}
        {{--                            <div class="card">--}}
        {{--                                <div class="side-animate">--}}
        {{--                                    <span class="side-number">{{$loop->index + 1}}</span>--}}
        {{--                                </div>--}}
        {{--                                <div class="card-header ml-3" id="heading-owner-{{$item->id}}">--}}
        {{--                                    <h6 class="mb-0">--}}
        {{--                                        <button class="btn btn-link" data-toggle="collapse"--}}
        {{--                                                data-target="#ow-it-work-{{$item->id}}"--}}
        {{--                                                aria-expanded="true" aria-controls="collapse-{{$item->id}}">--}}
        {{--                                            {{$item->title}}--}}
        {{--                                            <span class="float-right">--}}

        {{--                                                <i class="fas {{$loop->first ? 'fa-chevron-up' : 'fa-chevron-down'}}"></i>--}}
        {{--                                            </span>--}}
        {{--                                        </button>--}}
        {{--                                    </h6>--}}
        {{--                                </div>--}}

        {{--                                <div id="ow-it-work-{{$item->id}}" class="collapse ml-3 {{$loop->first ? 'show' : ''}}"--}}
        {{--                                     data-index="{{$loop->index}}"--}}
        {{--                                     data-parent="#how-it-work-accordion-owner">--}}
        {{--                                    <div class="card-body">--}}
        {{--                                        {!! $item->body !!}--}}
        {{--                                    </div>--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                        @endforeach--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--                <div class="col-md-6">--}}
        {{--                    <div class="how-it-work-carousel owl-carousel">--}}
        {{--                        @foreach ($how_its_works['owner']??[] as $item)--}}
        {{--                            <div class="item">--}}
        {{--                               <img src="{{Voyager::image($item->image)}}" alt="{{$item->title}}" class="w-100 border-radius-20">--}}
        {{--                            </div>--}}
        {{--                        @endforeach--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
</section>
