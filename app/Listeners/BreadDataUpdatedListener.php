<?php

namespace App\Listeners;

use App\Events\Registered;
use App\Models\Car;
use App\Models\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use TCG\Voyager\Events\BreadDataUpdated;

class BreadDataUpdatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(BreadDataUpdated $event)
    {

//       if ($event->dataType->slug == 'cars'){
//            $log = new Log();
//            $log->logable_id = $event->data->id;
//            $log->logable_type = $event->dataType->model_name;
//            $log->user_id = auth()->id();
//            $log->body = $event->data->withAll()->first()->toJson(JSON_INVALID_UTF8_IGNORE);
//            $log->save();
//       }
    }
}
