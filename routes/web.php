<?php

use Illuminate\Support\Str;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;


Route::group(['middleware' => 'verified'], function () {

    Route::get('/index.html', 'HomeController@index');
    Route::get('/index.php', 'HomeController@index');
    Route::get('/index', 'HomeController@index');
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['prefix' => 'profile', 'middleware' => ['auth', 'verified', 'user']], function () {
        Route::get('/', 'User\UserController@index')->name('profile');
        Route::get('/edit', 'User\UserController@edit')->name('profile.edit');
        Route::post('/update', 'User\UserController@update')->name('profile.update');
        Route::post('/password', 'User\UserController@password')->name('profile.password');
        Route::post('/ajax-upload', 'User\UserController@ajaxUpload')->name('profile.ajax');
        Route::get('/states/{code?}', 'User\UserController@getStates')->name('profile.states');

        Route::get('/history', 'User\ProfileController@history')->name('profile.history');
        Route::get('/favorites', 'User\ProfileController@favorites')->name('profile.favorites');
        // PROFILE POSTS

        Route::resource('/posts', 'User\PostController');
        Route::post('/posts/upload', 'User\PostController@upload')->name('posts.upload');

        Route::get('/action-favorite/{car}', 'User\UserController@actionFavorite')->name('profile.action-favorite');

        Route::get('/check-profile', 'User\UserController@checkProfile')->name('profile.check');
        Route::get('/edit-profile-pasport-images/{profile}', 'User\UserController@pasportImages')->name('profile.posport_images');
        Route::get('/check-profile-dock/{profile}', 'User\UserController@checkProfileDock')->name('profile.check_doc');

        Route::get('order/{order}', 'User\OrderController@getOrder')->name('user.order.get');
        Route::post('order/cancel/{order}', 'User\OrderController@cancel')->name('user.order.cancel');

        Route::post('make-review/{car}', 'User\UserController@makeReview')->name('user.review.make');

        Route::delete('delete/{user}', 'User\UserController@deleteProfile')->name('user.delete_profile');
    });
// home posts
    Route::get('post_in_home/{width?}', 'HomeController@getPost')->name('post_in_home');


    Route::match(['get', 'post'], '/search/{location?}', 'CarController@index')->name('search');
// post routes
    Route::get('/posts', 'PostsController@index')->name('posts');
    Route::get('/posts/{slug}', 'PostsController@show')->name('post.single');

//faq routes
    Route::get('/faq/{for_whom?}/{cat?}/{question?}', 'FaqsController@index')->name('faq');
    Route::get('/faq/unlike/like/{faq}/{like}', 'FaqsController@like')->name('faq.like')->middleware('auth');

    //page routes

    Route::get('/pages/{slug?}', 'PagesController@index')->name('pages');
    Route::get('/for-whom', 'PagesController@forWhom')->name('for-whom');
    Route::get('/for-car-owner/{slug?}', 'PagesController@forWhomOwner')->name('pages.for_whom_owner');
    Route::get('/for-tenants/{slug?}', 'PagesController@forTenants')->name('pages.for_tenants');
    Route::get('/insurance/{slug?}', 'PagesController@insurance')->name('pages.insurance');


    Route::get('page/get-document/{page}', 'PagesController@getDocument')->name('page.getDocument');


    Route::get('/car/{slug}', 'CarController@show')->name('car.single');
    Route::get('/car', function () {
        return redirect()->route('search');
    });
    Route::match(['GET', 'POST'], '/car/get-prices/{car}/{barcode?}', 'CarController@getPrices')->name('car.get_prices');
    Route::get('/similar-cars/{car}', 'CarController@similarCars')->name('car.similar');


//COMMENT

    Route::get('get-comments/{model}/{item}', 'CommentsController@index')->name('comments');
    Route::group(['middleware' => 'auth'], function () {
        Route::post('comments', 'CommentsController@store');
        Route::delete('comments/{comment}', 'CommentsController@destroy');
        Route::put('comments/{comment}', 'CommentsController@update');
        Route::post('comments/{comment}', 'CommentsController@reply');
    });


    Route::get('/currency/{currency}', 'CurrencyController@setCurrency')->name('currency.change');

    //CONTACT US

    Route::get('/contact-us', 'HomeController@contact')->name('contact_us');

    Route::post('contact-us', 'HomeController@contactUsPost')->name('contact_us.store');


//    Paynebt return url

    Route::get('/payment/{status}', 'User\PaymentController@paymentResponse')->name('payment.response');
    Route::get('/payment/get-order-status/{order}', 'User\PaymentController@orderStatus')->name('payment.order_status');
    Route::get('/full-pay/{order}', 'User\PaymentController@fullPay')->name('user.full_pay');


});

// GET ALL CARS FOR MAP

Route::get('get-all-cars', 'CarController@getAllCars')->name('car.get-all');

Route::get('robots.txt', 'RobotsController@__invoke');

//Route::get('/sitemap', function () {
//    SitemapGenerator::create(config('app.url'))
//        ->hasCrawled(function (Url $url) {
//            if (Str::contains($url->url, [
//                    '/admin',
//                    '/public/',
//                    '/profile',
//                    '/currency',
//                    '/auth',
//                    '/login',
//                    '/register',
//                    '/password',
//                    '/c-owner',
//                    '/posts?slug',
//                    '/ru/',
//                    '/en/',
//                    '/storage/',
//                    '/search?date'
//                ]) || ($url->segment(1) === 'en' && !$url->segment(2)) || $url->segment(1) === 'ru') {
//                return;
//            }
//            if ($url->segment(1) === 'en') {
//                $url->addAlternate($url->url, 'en');
//            } else {
//                $url->url = LaravelLocalization::getLocalizedURL('ru', $url->url);
//                if ($url->segment(1) == 'faq') {
//                    $url->setPriority(0.6);
//                } elseif ($url->segment(1) == 'car') {
//                    $url->setPriority(0.4);
//                } elseif ($url->segment(1) == 'search') {
//                    $url->setPriority(0.3);
//                } elseif ($url->segment(1) == 'pages') {
//                    $url->setPriority(0.2);
//                } elseif ($url->segment(1) == 'posts') {
//                    $url->setPriority(0.5);
//                } elseif (!$url->segment(1)) {
//                    $url->setPriority(1);
//                }
//            }
//            return $url;
////                return !strpos($url->getPath(), '/admin/') ||
////                    !strpos($url->getPath(), '/public/') ||
////                    !strpos($url->getPath(), '/profile/') ||
////                    !strpos($url->getPath(), '/c-owner/') ||
////                    !strpos($url->getPath(), '/currency/') ||
////                    !strpos($url->getPath(), '/auth/') ||
////                    !strpos($url->getPath(), '/login') ||
////                    !strpos($url->getPath(), '/register') ||
////                    !strpos($url->getPath(), '/password/') ||
////                    !strpos($url->getPath(), '/images/') ||
////                    !strpos($url->getPath(), '/posts?slug');
//
//        })
//        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)->writeToFile('sitemap.xml');
//
//    return 'site map created';
//});

//Route::get('/config-clear', function () {
//    \Illuminate\Support\Facades\Artisan::call('config:clear');
//    return back();
//});
Auth::routes(['verify' => true]);
Route::post('guest-reg/{car}', 'Auth\RegisterController@guestReg')->name('auth.guest_reg')->middleware('guest');

// OAuth Routes
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/logout', 'Auth\AuthController@logout')->name('logout')->middleware('auth');

Route::get('get-translate/y/{word?}', 'TranslateController@getTranslate');

Route::get('images/{w?}_{h?}_{q?}/{encode}/{src}', 'ImageController@getCacheImage')->name('get-image');

Route::post('/push', 'PushController@store');
//Route::get('/push','PushController@push')->name('push');
