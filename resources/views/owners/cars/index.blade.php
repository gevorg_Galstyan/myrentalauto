@extends('owners.layouts.app')


@section('content')
    <div class="page-header">
        @if(isset($carsDeletedPage))
            <h1 class="page-title">
                Список удаленных машин
            </h1>
        @else
            <h1 class="page-title">
                Машины
            </h1>
            <a class="ml-5 btn btn-success" href="{{route('cars.create')}}">
                <i class="fe fe-plus-circle"></i>
                Добавить
            </a>
            <a class="ml-5 btn btn-danger" href="{{route('cars.deleted')}}">
                <i class="fa fa-trash"></i>
                Удаленные
            </a>

        @endif
        @if (request()->get('key') == 'on_request' && request()->get('s') == 0)
            <a href="{{route('cars.index')}}"
               class="ml-5 btn btn-warning btn-add-new">
                <span>Все</span>
            </a>
        @else
            <a href="{{route('cars.index', ['key' => 'on_request', 'filter' => 'equals', 's' => 0])}}"
               class="ml-5 btn btn-info btn-add-new">
                <span>мгновенное</span>
            </a>

        @endif
        @if (request()->get('key') == 'on_request' && request()->get('s') == 1)
            <a href="{{route('cars.index')}}"
               class="ml-5 btn btn-warning btn-add-new">
                <span>Все</span>
            </a>
        @else
            <a href="{{route('cars.index', ['key' => 'on_request', 'filter' => 'equals', 's' => 1])}}"
               class="ml-5 btn btn-info btn-add-new">
                <span>с запросом</span>
            </a>
        @endif
    </div>

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">

                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable" id="ownerTables">
                        <thead>
                        <tr>
                            <th class="w-1 text-center">Id</th>
                            <th class="text-center">Название</th>
                            <th class="text-center">Цена</th>
                            <th class="text-center">Статус</th>
                            <th class="text-center">Изображение</th>
                            <th class="text-center">дата добавления</th>
                            <th class="text-center">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 1)
                        @foreach($cars as $car)
                            <tr>
                                <td class="text-center"><span class="text-muted">{{$i++}}</span></td>
                                <td class="text-center"><a href="{{route('cars.show', ['show' => $car->id])}}"
                                                           class="text-inherit">{{$car->title}}</a></td>
                                <td class="text-center">
                                    {{$car->price_1}}
                                </td>
                                <td class="text-center">
                                    <span
                                        class="badge badge-{{$car->status ? 'success': ($car->error_status == 'error' ? 'danger' : 'info' )}}">
                                        @if ($car->status)
                                            {{__('car.error_status.publish')}}
                                        @else
                                            {{__('car.error_status.'.($car->error_status != ''? $car->error_status: 'moderation'))}}
                                        @endif
</span>
                                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                              title=" {{ tooltips($toolips, 'owner_help_car_error_help_'.($car->status ? 'success': ($car->error_status == 'error' ? 'error' : 'moderation' ))) }}">
                                <i class="fas fa-info-circle"></i>

                                    </span>
                                </td>
                                <td class="text-center">
                                    <img src="{{Voyager::image($car->image_1)}}" alt="{{$car->title}}"
                                         title="{{$car->title}}" style="width: 100px;">
                                </td>
                                <td class="text-center">
                                    {{$car->created_at}}
                                </td>
                                <td class="text-center">
                                    @if(isset($carsDeletedPage))

                                        <a class=" ml-5 btn btn-success"
                                           href="{{route('cars.restore', ['id' => $car->id])}}">
                                            Восстановить
                                        </a>

                                    @else

                                        <a class="icon mx-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                           title="Календарь"
                                           href="{{route('cars.calendar', ['car' => $car->id])}}">
                                            <i class="fa fa-calendar"></i>
                                        </a>

                                        <a class="icon mx-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                           title="Просмотр"
                                           href="{{route('cars.show', ['show' => $car->id])}}">
                                            <i class="fe fe-eye"></i>
                                        </a>

                                        <a class="icon mx-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                           title="Изменить"
                                           href="{{route('cars.edit', ['id' => $car->id])}}">
                                            <i class="fe fe-edit"></i>
                                        </a>

                                        <a href="javascript:;"
                                           data-toggle="tooltip" data-html="true" title="Удалить"
                                           class="icon mx-2 delete" data-id="{{$car->id}}"
                                           id="delete-{{$car->id}}">
                                            <i class="fe fe-trash"></i>
                                        </a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i
                            class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}
                    </h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('script')

    <script>
        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('cars.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
        $('[data-toggle="tooltip"]').tooltip()

        $(document).ready(function () {
            $('#ownerTables').DataTable({
                responsive: true,
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [5, 6, 7, 8]}
                ]
            });
        });

    </script>
@stop
