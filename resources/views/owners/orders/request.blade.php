@extends('owners.layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop

@section('content')

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Страница Запросов</h3>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="text-center">
                                <img src="{{$order->customer ? Voyager::image($order->customer->avatar) : 'Пользователь не найден '}}" class="img-fluid" width="100%"
                                     alt="avatar">
                            </div>
                        </div>
                        <div class="col-md-6 personal-info ff-st d-flex flex-column">
                            <p class="row">
                                <b class="fw-700 col-lg-3">Customer: </b>
                                <span class="fw-400 col-lg-9">{{$order->customer ? $order->customer->full_name : 'Пользователь не найден ' }}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Дата: </b>
                                <span class="fw-400 col-lg-9">
                                    <span>{{$order->dates->start->format('Y:m:d g:i')}}</span><span> - </span><span>{{$order->dates->end->format('Y:m:d g:i')}}</span>
                                </span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Валюта: </b>
                                <span class="fw-400 col-lg-9">{{$order->currency}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Цена за один день: </b>

                                <span class="fw-400 col-lg-9">{{$order->price_per_day}}</span>
                            </p>
                            @if ($order->note)
                                <p class="row">
                                    <b class="fw-700 col-lg-3">Заметка: </b>

                                    <span class="fw-400 col-lg-9">{{$order->note}}</span>
                                </p>
                            @endif

                            <p class="row">
                                <b class="fw-700 col-lg-3">Окончательная цена: </b>

                                <span
                                    class="fw-400 col-lg-9">{!!  $order->total_price  !!} {{ currency_symbol($order->currency) }}</span>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="customer-order-image-container">
                                        <img src="{{Voyager::image($order->car->image_1)}}" alt="Avatar"
                                             class="image img-fluid"
                                             style="width:100%">
                                        <div class="middle">
                                            <a href="{{route('car.single', ['slug' => $order->car->slug])}}"
                                               class="own-btn rounded d-inline-block ora px-2 py-1 fs-15">
                                                {!! $order->car->title !!}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mt-5">
                                    @if (!$order->histories()->where('details->author', 'owner')->where('details->type', 'response')->first())
                                        <div class="" role="group" aria-label="Basic example">
                                            <a href="{{route('orders.request.answer', ['barcode_number' => $order->barcode_number, 'act' => 'confirmed'])}}"
                                               class="btn btn-success">Подтвердить</a>
                                            <a href="{{route('orders.request.answer', ['barcode_number' => $order->barcode_number, 'act' => 'rejected'])}}"
                                               class="btn btn-warning request-cancel">Отклонить </a>
                                        </div>
                                    @else
                                        <span>Стату</span>
                                        <span class="badge badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.'.$order->status)}}</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="single-options my-2 col-md-6">
                            <div id="accordion-spec">
                                <div class="align-middle" id="headingOne">
                                    <button class="spec-button w-100 collapsed" data-toggle="collapse"
                                            data-target="#collapse-spec" aria-expanded="true"
                                            aria-controls="collapse-spec">
                                        Детали
                                        <i class="fa" aria-hidden="true"></i>
                                    </button>
                                </div>

                                <div id="collapse-spec"
                                     class="collapse fs-14 my-1 p-2 rounded border border-secondary"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordion-spec">
                                    <div class="mx-3  fs-14 fw-700">
                                        <div class="total-block-info color-red">
                                            @php($green_card = json_decode($order->green_card, true))
                                            @if (count($green_card))
                                                <div>
                                                    <hr>
                                                </div>

                                                <div class="total-block-info">
                                                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                        <span class="col "> Грин Карта {{config('car.green_card.country.'.($green_card['name']))}}</span>
                                                        <span class="col text-right">
                                                            {!!  $green_card['price']  !!} {{ currency_symbol($order->currency) }}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <hr>
                                                </div>
                                            @endif

                                            @php($other_details = json_decode($order->other_details, true))

                                            @if ($other_details['take_no_work_time'])
                                                <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                    <span class="col">
                                                        Взять не рабочее время
                                                        <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                                              title=" {{ tooltips($toolips, 'ne_rabochie_vremya', $other_details['take_no_work_time'] . currency_symbol($order->currency)) }}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                    </span>
                                                    <span class="col text-right">{!! $other_details['take_no_work_time'] . currency_symbol($order->currency) !!}</span>
                                                </div>
                                            @endif

                                            @if ($other_details['return_in_non_working_time'])
                                                <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                    <span class="col">
                                                        Вернуть в не рабочее время
                                                    </span>
                                                    <span class="col text-right">{!! $other_details['return_in_non_working_time'] . currency_symbol($order->currency) !!}</span>
                                                </div>
                                            @endif

                                            @if ($other_details['urgency'])
                                                <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                    <span class="col">
                                                        Вернуть в не рабочее время
                                                    </span>
                                                    <span class="col text-right">{!! $other_details['urgency'] . currency_symbol($order->currency) !!}</span>
                                                </div>

                                            @endif

                                            @if ($other_details['urgency'] || $other_details['return_in_non_working_time'] || $other_details['take_no_work_time'])
                                                <hr>
                                            @endif


                                            @if ($other_details['took'])
                                                <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                    <span class="col">
                                                        {{$other_details['took']['name']}}
                                                    </span>
                                                    <span class="col text-right">{!!  $other_details['took']['price'] . currency_symbol($order->currency)!!}</span>
                                                </div>
                                            @endif

                                            @if ($other_details['give'])
                                                <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                    <span class="col">
                                                        {{$other_details['give']['name']}}
                                                    </span>
                                                    <span class="col text-right">{!!  $other_details['give']['price'] . currency_symbol($order->currency)!!}</span>
                                                </div>
                                            @endif

                                            @if ($other_details['give'] || $other_details['took'])
                                                <hr>
                                            @endif


                                            @php($orderInsurances = json_decode($order->insurances, true))
                                            @if (count($orderInsurances))
                                                @foreach($orderInsurances as $item)
                                                    @php($insurances = \App\Models\Insurance::get())

                                                    <div>
                                                        <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                            <span class="col ">
                                                                {{$insurances->where('id', $item['id'])->first()->translate()->name}}
                                                            </span>
                                                            <span class="col text-right">
                                                                {!!  $item['price']  !!} {{ currency_symbol($order->currency) }}
                                                            </span>
                                                        </div>
                                                        <div>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif

                                            @php($orderAttributes = json_decode($order->attributes, true))
                                            @if (count($orderAttributes))
                                                @foreach($orderAttributes as $item)
                                                    @php($attributes = \App\Models\Filter::get())
                                                    <div>
                                                        <div class="row m-0 mt-2 align-items-center justify-content-between">
                                                            <span class="col ">
                                                                {{$attributes->where('id', $item['id'])->first()->translate()->title}}
                                                            </span>
                                                            @if(!$item['disposable'])
                                                                <span class="col text-center">
                                                                    {{$order->days}} Дней х {!! $item['price'] !!} {{currency_symbol($order->currency)}}
                                                                </span>
                                                                <span class="col text-right">
                                                                    {!! $item['price'] * $order->days !!} {{currency_symbol($order->currency)}}
                                                                </span>
                                                            @else
                                                                <span
                                                                    class="col text-center">{!! $item['price'] .' '.currency_symbol($order->currency) !!}</span>
                                                                <span
                                                                    class="col text-right">{!! $item['price'] .' '.currency_symbol($order->currency) !!}</span>
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif


                                        </div>
                                        <div class="row m-0 mt-2 align-items-center justify-content-between">
                                            <span class="col">{{__('car.rent')}}</span>
                                            <span class="col text-center">
                                                {{$order->days}} {{__('car.days')}} х  {!!  $order->price_per_day  !!} {{ currency_symbol($order->currency) }}</span>
                                                                <span class="col text-right">
                                                {!! $order->days * $order->price_per_day !!} {{ currency_symbol($order->currency) }}
                                            </span>
                                        </div>
                                        <hr/>


                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($order->histories()->count())
                            <div class="col-md-6  my-2">
                                <div id="accordion-history">
                                    <div id="headingHistory">
                                        <button class="spec-button w-100 collapsed" data-toggle="collapse"
                                                data-target="#collapse-history" aria-expanded="true"
                                                aria-controls="collapse-history">
                                            История
                                            <i class="fa" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div id="collapse-history"
                                         class="collapse fs-14 my-1 p-2 rounded border border-secondary"
                                         aria-labelledby="headingHistory"
                                         data-parent="#accordion-history">
                                        <div class="panel relation-group ">
                                            <div class="d-flex removed">
                                                <ul class="p-0" style=" width: 100%">
                                                    @foreach ($order->histories->sortByDesc('created_at') as $history)
                                                        <li class="list-group-item">
                                                            <ul class="p-0" style="padding: 0; width: 100%">
                                                                @php($details = json_decode($history->details, true))
                                                                <li class="list-group-item">
                                                                    <span><strong>Статус:</strong> </span><span>{{__('general.order.'.$details['type'])}}</span>
                                                                </li>

                                                                <li class="list-group-item">
                                                                    <span><strong>Автор:</strong> </span><span>{{__('general.order.'.$details['author'])}}</span>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span><strong>Действие:</strong> </span><span>{{__('general.order.'.$details['act'])}}</span>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <span><strong>Дата:</strong> </span><span>{{\Carbon\Carbon::parse($details['date'])->format('Y:m:d g:i')}}</span>
                                                                </li>
                                                                @isset($details['note'])
                                                                    <li class="list-group-item">
                                                                        <span><strong>Заметка:</strong> </span><span>{{$details['note']}}</span>
                                                                    </li>
                                                                @endisset
                                                            </ul>
                                                        </li>

                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('script')
            <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
            <script>

                $(document).on('click', '.request-cancel', function (event) {
                    event.preventDefault();
                    var url = $(this).attr('href');
                    Swal.fire({
                        title: 'Напишите причину отмены ',
                        input: 'textarea',
                        confirmButtonColor: '#d33',
                        confirmButtonText: '{!! __('general.confirm') !!}',
                        inputValidator: function (result) {
                            return !result && 'необходимо написать причину '
                        }
                    }).then((result) => {
                        if (result.value) {
                            axios.post(url, {
                                note: result.value,
                                act: 'reject'
                            }).then(function (response) {
                                if (response.data.success) {
                                    location.reload();
                                }
                            })
                        }
                    })

                })

            </script>

@stop
