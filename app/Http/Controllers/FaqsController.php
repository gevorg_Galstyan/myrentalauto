<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Faq;
use Illuminate\Http\Request;

class   FaqsController extends Controller
{
    /*
     * F.A.Q page
     *
     */
    public function index(Request $request)
    {
//        similar_text('1245', '5678945555', $perc);
//dd($perc);
        if ($request->ex){
            return redirect($request->url());
        }
        $faqCategories = false;
        $items = false;
        $item = false;
        $for_whom = $request->for_whom;
        $faqPopular = false;
        $categories = false;
        $activeCat = false;
        if ($request->keyword && $request->ajax()) {
            $keyword = $request->keyword;
            $items = Faq::whereTranslation('title', 'like', '%' . $keyword . '%')->orderBy('sort', 'asc')->get();
        } else {
            if ($request->cat) {
                $activeCat = Category::where('slug', $request->cat)->firstOrFail();
                $for_whom = $activeCat->for_whom;

                $faqCategories = Category::where('for_whom', $for_whom)->orderBy('order', 'asc')->get();
                if ($request->question) {
                    $item = Faq::where('slug', $request->question)->firstOrFail();
                }else{
//                    $meta_tags =
                }
                $items = Faq::where('category_id', $activeCat->id)->orderBy('sort', 'asc')->get();

            } elseif ($for_whom){
              return redirect()->route('faq');
            } else {
                $categories = Category::orderBy('order')->get()->groupBy('for_whom');
                foreach ($categories as $for_whom => $category) {
                    $categories[$for_whom]->faqPopular = Faq::whereHas('category', function ($query) use ($for_whom) {
                        $query->where('categories.for_whom', $for_whom);
                    })->orderByLikesCount('desc')->limit(3)->get();
                }
            }
//            dd($categories);

//                $faqPopular = Faq::whereHas('category', function ($query) use ($for_whom) {
//                    $query->where('categories.for_whom', $for_whom);
//                })->orderByLikesCount('desc')->limit(3)->get();
        }

        if ($request->ajax()) {
            if ($request->keyword) {
                $html = view('faqs.search-result', compact('items'))->render();
            } else if ($request->cat) {
                $html = view('faqs.content', compact('faqCategories', 'for_whom', 'items', 'faqPopular', 'activeCat', 'item'))->render();
            } else {
                $html = view('faqs.faq', compact('categories'))->render();
            }
            return response()->json([
                'success' => true,
                'html' => $html,
            ]);
        }
        return view('faqs.index', compact(
            'faqCategories',
            'for_whom',
            'items',
            'item',
            'faqPopular',
            'activeCat',
            'categories'
        ));
    }


    /**
     * @param Request $request
     * @param Faq $faq
     * @param $like
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function like(Request $request, Faq $faq, $like)
    {
        if ($request->ajax()) {
            $faq->{$like}();
            $faq->increment($like);
            return response()->json(['success' => true]);
        }
        return abort(403);
    }


}
