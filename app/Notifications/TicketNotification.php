<?php

namespace App\Notifications;

use App\Models\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketNotification extends Notification
{
    use Queueable;

    private $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return  $notifiable->role_id == 1 ? ['mail', 'broadcast'] : ['mail', 'database', 'broadcast'];

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $ticket = $this->ticket;
        $images = @json_decode($ticket->images);
        $url = $notifiable->role_id == 1 ? route('voyager.tickets.show', $ticket->id) : route('tickets.show',$ticket->ticket_number);

        $mail = (new MailMessage)
            ->subject('Новый тикет')
            ->greeting($this->ticket->title)
            ->line($this->ticket->text)
            ->action('Открыть Тикет ', $url);
        if ($images) {
            foreach ($images as $image) {
                $mail->attach(storage_path('app/public/'.$image));
            }
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

            'title' => 'Новый тикет',
            'message' => $this->ticket->title,
            'url' => route('tickets.show',$this->ticket->ticket_number),
        ];
    }

    public function toBroadcast($notifiable)
    {
        $ticket = $this->ticket;
        $url = $notifiable->role_id == 1 ? route('voyager.tickets.show', $ticket->id) : route('tickets.show',$ticket->ticket_number);

        return new BroadcastMessage([
            'title' => 'Новый тикет',
            'message' => '<a href="'.$url.'">Страница тикета</a>',

        ]);
    }
}
