<?php
return [
    'edit' => 'Edit',
    'add' => 'Add Post',
    'title' => 'Title',
    'content' => 'Content',
    'save' => 'Save',
    'add_success' => 'Post successfully added',
//    'on_moderation' => 'Пост успешно добавлен',
    'update_success' => 'Post updated successfully'
];
