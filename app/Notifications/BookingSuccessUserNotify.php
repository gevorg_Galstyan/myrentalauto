<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BookingSuccessUserNotify extends Notification
{
    use Queueable;

    public $order;
    public $type;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order, $type)
    {
        $this->order = $order;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.car-booking.customer.title', [
                'name' => $notifiable->first_name,
            ]))
            ->line(__('notification.car-booking.customer.'.($this->type == 'full' ? 'text_full_pay' : 'text_booking'), [
                    'car' => $this->order->car->title,
                ]).__('notification.car-booking.customer.text', [

                'start' => $this->type == 'full' ? $this->order->parent->dates->start->format('d:m:Y H:i') : $this->order->dates->start->format('d:m:Y H:i'),
                'end' => $this->type == 'full' ? $this->order->parent->dates->end->format('d:m:Y H:i') : $this->order->dates->end->format('d:m:Y H:i'),
            ]))
            ->action(__('notification.car-booking.customer.link_title'), route('profile'))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
