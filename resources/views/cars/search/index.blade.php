@extends('layouts.app')

@section('meta')
    <title>{{$metaTags->translate()->title}}</title>
    <meta name="keywords" content="{{$metaTags->translate()->Keyword}}"/>
    <meta name="description" content="{{$metaTags->translate()->description}}"/>

    <meta property="og:locale"
          content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$metaTags->translate()->title}}"/>
    <meta property="og:description" content="{{$metaTags->translate()->description}}"/>
    <meta property="og:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title" content="{{$metaTags->translate()->title}}">
    <meta name="twitter:description" content="{{$metaTags->translate()->description}}">
    <meta name="twitter:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}">

{{--    <link href="{{request()->url()}}" rel="canonical">--}}
@stop

@section('content')

    <section class="search my-3">
        <h1 class="text-center fw-600 sub-heading text-uppercase fs-30">{{$metaTags->translate()->h1}}</h1>
        <div class="container-fluid">
            <form action="{{route('search')}}" id="searchForm"
                  class="row justify-content-center align-items-center search-row">

                <div class=" mob-click col-lg-3 col-11  mob-loc-open k-pointer p-0">
                    <div class="shadow-lg col-md-3 bg-white border border-secondary rounded d-flex border-20">
                        <div class="col-2 fs-14  align-self-center">
                            <span><i class="fas fa-search-plus"></i></span>
                        </div>
                        <div class="col-10 m-0 p-0">
                            <div class="m-0 p-0">
                                <span>
                                    {{request()->input('location')}}
                                </span>
                            </div>
                            <div class="fw-600 fs-12">
                                <span>
                                    {{request()->date??\Carbon\Carbon::now()
                                        ->setTimeFromTimeString('09:00')
                                        ->addDay(5)
                                        ->format('d.m.Y H:00' ).' ~ '. \Carbon\Carbon::now()
                                        ->setTimeFromTimeString('09:00')
                                        ->addDay(7)
                                        ->format('d.m.Y H:00' )}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mob-db">
                    <input type="text" class="form-control date-tame-x  text-left border-20 text-center text-truncate"
                           id="location"
                           name="location"
                           autocomplete="off"
                           value="{{request()->input('location')}}"/>

                    <span class="text-secondary k-pointer color-black currentLocationBtn position-absolute"
                          data-toggle="tooltip" data-html="true" data-placement="top"
                          title="{{ tooltips($toolips, 'current_location') }}">
                </span>

                </div>
                <div class="col-md-4 mob-db text-center" id="search-block">
                    <input type="hidden" class="date-input" name="date" autocomplete="off"
                           value="{{request()->date??\Carbon\Carbon::now()
                        ->setTimeFromTimeString('09:00')
                        ->addDay(5)
                        ->format('d.m.Y H:i' ).' ~ '. \Carbon\Carbon::now()
                        ->setTimeFromTimeString('09:00')
                        ->addDay(7)
                        ->format('d.m.Y H:i' )}}"/>

                    <div class="form-control search-date text-center  date-block  border-20">
                        {{request()->date??\Carbon\Carbon::now()
                        ->setTimeFromTimeString('09:00')
                        ->addDay(5)
                        ->format('d.m.Y H:i' ).' ~ '. \Carbon\Carbon::now()
                        ->setTimeFromTimeString('09:00')
                        ->addDay(7)
                        ->format('d.m.Y H:i' )}}
                    </div>
                </div>
                <div class="col-md-3 mob-db {{--d-md-block d-none--}}  text-center">
                    <div class="search-select">
                        <select class="form-control car-params border-20 search-select-select2" name="transmission"
                                data-placeholder="{{__('general.transmission')}}" data-width="100%"
                                id="car_eng">
                            <option disabled="" title=" ">{{__('general.transmission')}}</option>
                            <option value="0" title=" ">{{__('general.all')}}</option>
                            <option value="automatic" {{request()->transmission == 'automatic' ? 'selected' : ''}}>
                                {{__('general.automatic')}}
                            </option>
                            <option value="manual" {{request()->transmission == 'manual' ? 'selected' : ''}}>
                                {{__('general.manual')}}
                            </option>
                        </select>
                    </div>

                </div>

                <div class="pr-3 d-none">
                    <button class="btn own-btn search py-2 filter" type="button">
                        <img src="{{asset('storage/img/icons/filter.png')}}" alt="filter" class="img-fluid"
                             width="40">
                    </button>
                </div>
                <div class="pr-3 d-none">
                    <button class="btn own-btn search py-2" type="button">
                        <img src="{{asset('storage/img/icons/search.png')}}" alt="search" class="img-fluid"
                             width="40">
                    </button>
                </div>
            </form>
        </div>
    </section>
    <!-- SEARCHED CARS -->
    <div class="container-fluid">
        <div class="row d-lg-flex">
            @include('cars.search.left-filters')
            <div class="col-lg-9  mb-3">
                <div class="search-content">
                    @include('cars.search.search-result')
                </div>
            </div>
        </div>
    </div>
    <input type="checkbox" name="open_map" form="searchForm" class="checkbox-hidden open_map">
    <!--BLOG-->
    {{--    @include('home.blog')--}}
    <section class="post-content"></section>
@endsection
@section('script')
    <script  src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=195a01c1-11df-40b3-bf48-96004f1783eb&mode=debug"></script>
    <script src="{{asset('js/sticky-sidebar.js')}}"></script>
    <script>
        $('.currentLocationBtn').click(function (e) {
            var location = ymaps.geolocation.get({provider: 'browser'});

            location.then(
                function (result) {
                    var address = result.geoObjects.get(0).properties.get('text');
                    $("#location").val(address);
                    $('.currentLocationBtn').addClass('currentLocActive');
                    getCars();
                },
                function (err) {
                    if (err.code == 1) {
                        alert('Вы должны изменить настройки конфиденциальности вашего браузера, чтобы предоставить доступ к геолокации на веб-странице MyRentAuto!');
                    } else {
                        alert(err.message);
                    }
                }
            );
        });

        ymaps.ready(init);
        $(document).on('click', ".dropdown-menu li a", function (e) {
            e.preventDefault();
            var selText = $(this).text();
            var selected = $(this).data('select')
            $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
            $(this).parents('.btn-group').find('input[type="hidden"]').val(selected).change();
        });
        $('.sidebar-open').click(function (e) {
            e.preventDefault();
            if ($('.mob-sidebar').is(':visible')) {
                $('.sidebar .mob-db').slideUp();
            } else {
                $('.sidebar .mob-db').slideDown();
            }
            $('.mob-sidebar').slideToggle();
            $('.more-filters').slideUp()
            var textHide = $(this).text();
            var textShow = $(this).data('text');
            $(this).data('text', textHide).text(textShow);
            // $(this).toggleClass('activeBtn collapse-button mb-2');
        });

        $(document).on('click', function (e) {
            let mob_sidebar = $('.mob-sidebar');
            let sidebar_open = $('.sidebar-open');
            if($(window).width() <= 991 && !$(e.target).parents('.sidebar-container').length && mob_sidebar.is(':visible') ){
                   mob_sidebar.slideUp();
                   let textHide = sidebar_open.text();
                   let textShow = sidebar_open.data('text');
                   sidebar_open.data('text', textHide).text(textShow);



           }
        });

        //============RANGE
        var rangeSlider = document.getElementById("rs-range-line");
        var rangeBullet = document.getElementById("rs-bullet");

        rangeSlider.addEventListener("input", showSliderValue, false);

        function showSliderValue() {
            rangeBullet.innerHTML = rangeSlider.value + ' {{currency_symbol()}}';
        }

        $('.mob-click').click(() => {
            $('.mob-db').slideDown()
            $('.mob-click').remove();
        });
        var flag;
        $(document).on('click', '.map-button', () => {

            $('#map').html('');
            if (flag == undefined) flag = true;
            if (flag) {
                // $('.map-button').css('box-shadow', '0px 0px 11px rgba(0, 0, 0, 1.5)');
                $('.map-button').html("{{__('car.close_map')}} <img src='{{asset('storage/img/icons/location.png')}}' class='img-fluid ml-2' alt=''>")
                $('.open_map').prop('checked', true);
                $('.map-button').addClass('activeBtn');
                ymaps.ready(init);
                flag = false;
            } else {
                // $('.map-button').css('box-shadow', '0px 0px 11px rgba(0, 0, 0, .5)');
                $('.map-button').html("{{__('car.open_map')}} <img src='{{asset('storage/img/icons/location.png')}}' class='img-fluid ml-2' alt=''>")
                flag = true;
                $('.open_map').prop('checked', false)
                $('.map-button').removeClass('activeBtn')
            }
            $('.map-button').blur();
            $('#map').slideToggle();

        });


        $('.more-filter-open').click(function (e) {
            e.preventDefault();
            $('.more-filters').slideToggle();
            var textHide = $(this).text();
            var textShow = $(this).data('text');
            $(this).data('text', textHide).text(textShow);
            $(this).toggleClass('activeBtn');
            $('.own-scroll').animate({
                scrollTop: 250,
            })
        });
        $('.own-scroll').css('max-height', $('.mob-db').height() + $('.mob-sidebar').height() + 100)

        $(window).resize(() => {
            if ($(window).width() >= 991
            ) {
                $('.mob-sidebar').attr('style', 'display:block !important')
            } else {
                // $('.mob-sidebar').attr('style', '')
            }
            $('.own-scroll').css('max-height', $('.mob-db').height() + $('.mob-sidebar').height() + 100)
        })
        ;


        function init() {

            var suggestView = new ymaps.SuggestView('location', {
                offset: [0, 8],
                provider: {
                    suggest: function (request, options) {
                        var parseItems = ymaps.suggest((locale == 'en' ? "Belarus, " : "Беларусь, ") + request).then(function (items) {
                            for (var i = 0; i < items.length; i++) {
                                var displayNameArr = items[i].displayName.split(',');

                                var newDisplayName = [];
                                for (var j = 0; j < displayNameArr.length; j++) {
                                    if (displayNameArr[j].indexOf('район') == -1) {
                                        newDisplayName.push(displayNameArr[j]);
                                    }
                                }
                                items[i].displayName = newDisplayName.join();
                            }
                            return items;
                        });
                        return parseItems;
                    }
                }
            });


            suggestView.events.add(["add", "select"], function (event) {
                $('.currentLocationBtn').removeClass('currentLocActive');
                getCars(true);
            });

            clusterer = new ymaps.Clusterer({
                preset: 'islands#blueCircleIcon',
                groupByCoordinates: false,
                clusterDisableClickZoom: false,
                clusterHideIconOnBalloonOpen: false,
                geoObjectHideIconOnBalloonOpen: false,
                hasBalloon: false,
            });


            var center = [{{request()->input('lat')}}, {{request()->input('lng')}}];
            var geolocation = ymaps.geolocation;
            var myMap = new ymaps.Map('map', {
                    center: center,
                    zoom: 12,
                    controls: []
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаем коллекцию.
                // myCollection = new ymaps.GeoObjectCollection(),
                // Создаем массив с данными.
                myPoints = [@foreach(request()->get('map-cars')??[] as $car) {
                    coords: [{{$car->getCoordinates()[0]['lat']}} , {{$car->getCoordinates()[0]['lng']}}],
                    title: '<strong> {{$car->title}} </strong>',
                    text: '<div class="map-car min-width-320">' +
                        '<div class="map-car-body d-flex align-items-center">' +
                        '<div class="map-car-img-part"><img src="{{Voyager::image($car->image_1)}}" alt="{{$car->title}}" title="{{$car->title}}" class="rounded" width="100px"></div>' +
                        '<div class="map-car-info-part">' +
                        '<div class="d-flex align-items-center  ml-3">' +
                        '<span>{{$car->title}}</span>' +
                        '</div>' +
                        '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
                        '<div class="stars-info d-flex align-items-center">' +
                        '<div class="stars stars--large mr-1 fs-16">' +
                        '<span style="width: {{$car->avg_score * 20}}%"></span>' +
                        '</div>' +
                        '<span>({{$car->reviews->count()}})</span>' +
                        '</div>' +
                        '<span class="px-1">|</span>' +
                        '<div class="ml-1"> {{$car->age}}</div>' +
                        '<span class="px-1">|</span>' +
                        '<div class="ml-1"> {{$car->number_of_seats}} мест </div>' +
                        '</div>' +
                        '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
                        '<p class="fs-12 color-light mb-2">\n' +
                        '<img src="{{asset('storage/img/icons/location.png')}} " alt="location" class="inline-img-width" style="width: 17px !important"/>\n' +
                        '<span>{{distanceM($car->distance)}}</span>\n' +
                        '<span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="От выбранного места до этого автомобиля {{distanceM($car->distance)}}"> ' +
                        '<i class="fas fa-info-circle"></i></span>\n' +
                        '</p>' +
                        '<div class="text-center mt-1 ml-3">' +
                        '<a href="{{route('car.single', array_merge(['slug' => $car->slug], request()->only('location', 'date', 'attributes')))}}" class="own-btn rounded d-inline-block ora px-2 py-1 fs-12"><span class="ff-st">{!! currency(getCarPrice($car, request()->input('days'))) !!}</span> / {{__('car.day')}}</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                } {!! $loop->last ? '' : ',' !!}  @endforeach];
            // Заполняем коллекцию данными.
            geoObjects = [];
            for (var i = 0; i < myPoints.length; i++) {
                var point = myPoints[i];
                geoObjects[i] = new ymaps.Placemark(
                    point.coords, {
                        balloonContentBody: point.text,
                        // clusterCaption: '<strong> ' + point.title + ' </strong>',
                    }, {
                        preset: 'islands#blueAutoIcon'
                    }
                );
            }

            // clusterer.options.set({
            //     gridSize: 60,
            //     clusterDisableClickZoom: true
            // });

            clusterer.add(geoObjects);

            //Кнопки плюс - минус на карте
            myMap.controls.add('zoomControl', {
                size: 'small',
                float: 'none',
                position: {
                    bottom: '50px',
                    right: '30px'
                }
            });


            // myMap.setBounds(clusterer.getBounds(), {
            //     checkZoomRange: true
            // });
            var myPlacemark = new ymaps.Placemark(center, {}, {
                preset: 'islands#redDotIcon'
            });

            myMap.geoObjects.add(myPlacemark);
            // Добавляем коллекцию меток на карту.
            myMap.geoObjects.add(clusterer);
            myMap.behaviors.disable('scrollZoom');
            // Создаем круг.
            var myCircle = new ymaps.Circle([
                // Координаты центра круга.
                [center[0], center[1]],
                // Радиус круга в метрах.
                2000
            ], {
                // Описываем свойства круга.
                // Содержимое балуна.
                balloonContent: "<span style='vertical-align: sub' class='fw-600'>Радиус круга - 2 км</span>",
                // Содержимое хинта.
                hintContent: ""
            }, {
                // Задаем опции круга.
                // Включаем возможность перетаскивания круга.
                draggable: false,
                // Цвет заливки.
                // Последний байт (77) определяет прозрачность.
                // Прозрачность заливки также можно задать используя опцию "fillOpacity".
                fillOpacity: 0.5,
                opacity: 0.5,
                fillColor: "#f6993f",
                // Цвет обводки.
                strokeColor: "#990066",
                // Прозрачность обводки.
                strokeOpacity: 0.8,
                // Ширина обводки в пикселях.
                strokeWidth: 0
            });
            // Добавляем круг на карту.
            myMap.geoObjects.add(myCircle)

        }


        $('.wishlist').click(function (e) {
            e.preventDefault();
            $(this).find('.wish-none').toggleClass('d-none');
            $(this).find('.wish').toggleClass('d-inline-block');
        })


        var options = [];

        $(document).on('click', '.car-types', function (event) {
            event.stopPropagation();
            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ($inp.is(':checked')) {
                $inp.prop('checked', false);
                $('#' + $(this).data('target')).remove();
            } else {
                $inp.prop('checked', true);

                $('.auto-type').append('<input type="hidden" id="' + $(this).data('target') + '" form="searchForm" class="type-select" name="type[]"  value="' + val + '">');
            }

            $(event.target).blur();
            return false;
        });
        $(document).on('click', '.search-types', function (e) {
            e.preventDefault();
            getCars();
        });


        function getVisible() {
            if ($(window).width() >= 991) {
                var sidebar = new StickySidebar('.sidebar-container', {
                    topSpacing: 51,
                    bottomSpacing: 0,
                    containerSelector: '.main-content',
                    // innerWrapperSelector: '.sidebar'
                });
            }
        }

        if ($(window).width() >= 991) {
            $(window).on('scroll resize', getVisible);
        }
        $(document).on('click', (e) => {
            if (!event.target.classList.contains('border-0 color-black') && event.target.id != 'location') {
                $(".currentLocation").hide();
            }
        });
        axios.get('{{route('post_in_home')}}/' + $(window).width())
            .then(function (response) {
                $('.post-content').html(response.data.html);
            })

        $('.currency-symbol').text(currency_symbol);
        $('.day').text(day_translate);
    </script>
@endsection

