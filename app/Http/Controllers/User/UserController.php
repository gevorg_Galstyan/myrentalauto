<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Password;
use App\Models\{
    Car, Profile
};
use App\Notifications\ProfileCheck;
use App\Traits\UploadeAvatar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    App, Auth, File, Hash, Notification, Validator
};
use MenaraSolutions\Geographer\{
    Country, Earth
};

class UserController extends Controller
{

    use UploadeAvatar;

    public function index()
    {
        return view('user.index');
    }

    public function edit()
    {
        $earth = new Earth();
        $countries = $earth->getCountries()->setLocale(App::getLocale())->toArray();
        return view('user.edit', compact('countries'));
    }

    public function update(\App\Http\Requests\Profile $request)
    {

        if ($request->ajax()) {
            $check_email = false;
            $user = auth()->user();

            $user->last_name = $request->last_name;
            $user->first_name = $request->first_name;
            $user->patronymic = $request->patronymic;
            if (!auth()->user()->provider && $request->email != $user->email) {
                $user->email = $request->email;
                $user->email_verified_at = null;
                $check_email = true;
                $user->sendEmailVerificationNotification();
            }

            $user->number = $request->phone;
            $user->birthday = $request->birthday;
            $user->save();


            $profile = auth()->user()->profile;


            $profile->update([
                'experience' => $request->experience,
                'citizenship' => $request->citizenship,
                'country' => $request->country,
                'region' => $request->region ?? '',
                'city' => $request->city,
            ]);
            $profile->terms_1 = $request->terms_1 ? 1 : 0;
            $profile->terms_2 = $request->terms_2 ? 1 : 0;
            $profile->save();
            return response()->json([
                'message' => 'Ваша личная информация обновляется!',
                'check_email' => $check_email ? 'Вы изменили емейл для дальнейших действии надо подтвердить емейл' : '',
                'alert' => 'success'
            ]);
        }
    }

    public function password(Password $request)
    {

        $user = auth()->user();

        if (Hash::check($request->currently_password, $user->password)) {
            $validate = Validator::make($request->all(), [
//                'currently_password' => 'required',
                'password' => 'required|confirmed|min:6',
            ]);

            if ($validate->fails()) {
                return response()->json(['errors' => $validate->messages(), 'success' => false], 422);
            }
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'message' => __('user.pass_change'),
                'alert' => 'success'
            ]);
        }

        return response()->json([
            'message' => __('user.pass_change_error'),
            'alert' => 'warning'
        ]);

    }

    public function ajaxUpload(Request $request)
    {
        if ($request->type == 'avatar') {
            $old_avatar = storage_path('app/public/' . auth()->user()->avatar);
            $filename = $this->uploadFile($request->img, 'users', 100);
            $user = auth()->user();
            $user->avatar = $filename;
            $user->save();
            if (File::exists($old_avatar) && $old_avatar != 'default.png') {
                File::delete($old_avatar);
            }
            return asset('storage/' . $filename);
        }
        $filename = $this->uploadFile($request->img, 'profile', 75);
        $type = $request->type;
        $profile = auth()->user()->profile;

        if ($profile) {
            $old_image = storage_path('app/public/' . auth()->user()->profile()->pluck($type)[0]);
            $profile->update([
                $type => $filename,
                'status' => 'not_verified',
            ]);
            $profile->save();
            if (File::exists($old_image != 'default.png')) {
                File::delete($old_image);
            }
            return asset('storage/' . $filename);
        }

        Profile::create([
            $type => $filename,
            'user_id' => auth()->user()->id,
        ]);

        return asset('storage/' . $filename);
    }

    public function actionFavorite(Car $car, Request $request)
    {

        $car->toggleFavorite();
        return response()->json(['class' => ($car->isFavorited() ? 'heart' : 'heart-o')]);
    }

    public function getStates($code)
    {
        $states = null;
        if ($code) {
            $country = Country::build($code);
            $states = $country->getStates()->setLocale(App::getLocale())->toArray();
            $view = view('user.profile.region', compact('states'))->render();
            return response()->json(['success' => true, 'view' => $view]);
        }
        return response()->json(['success' => false]);
    }

    public function checkProfile()
    {
        $users = User::where('role_id', 1)->get();
        auth()->user()->profile->status = 'for_consideration';
        auth()->user()->profile->save();
        Notification::send($users, new ProfileCheck(auth()->user()));

        return back()->with([
            'message' => __('user.check_profile_send'),
            'alert-type' => 'success',
        ]);

    }

    public function pasportImages(Profile $profile)
    {
        $edit = true;
        $html = view('user.profile.confirmation', compact('profile', 'edit'))->render();
        return response()->json(compact('html'), 200);
    }


    public function checkProfileDock(Request $request, Profile $profile)
    {
        $data = array_merge($profile->user()->select('first_name', 'email', 'avatar', 'number', 'birthday', 'last_name')->first()->toArray(), $profile->toArray());

        $rules = [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
//            'email' => 'required|string|email|max:255',
            'number' => 'required|string|max:16',
            'birthday' => 'required|date|before:13 years ago',
            'experience' => 'required',
            'citizenship' => 'required',
            'city' => 'required',
            'passport_one' => 'required',
            'passport_two' => 'required',
            'license_one' => 'required',
            'license_two' => 'required',
            'country' => 'required',
        ];

        $code = $data['country'];
        $country = Country::build($code);
        $states = $country->getStates()->setLocale(App::getLocale())->toArray();
        if (count($states) > 0) {
            $rules['region'] = 'required';
        }
        $validate = Validator::make($data, $rules);


        if ($validate->fails()) {
            return response()->json(['errors' => $validate->messages(), 'success' => false]);
        }
        return response()->json(['success' => true]);
    }

    public function makeReview(Request $request, Car $car)
    {
        $car->createReview($request->rating, $request->review, auth()->user());
        return response()->json();
    }

    public function deleteProfile(Request $request, User $user){
//        $user->profile()->delete();
        $user->delete();
        return redirect('/');
    }
}
