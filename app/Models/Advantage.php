<?php

namespace App\Models;

use App\Traits\MyCache;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Advantage extends Model
{
    use Translatable, MyCache;
    protected $translatable = ['title','description'];
    protected $fillable = [
        'title', 'description', 'publish'
    ];
    public function getTranslatedAttributeMeta($attribute, $locale = null, $fallback = true)
    {
        // Attribute is translatable
        //
        if (!in_array($attribute, $this->getTranslatableAttributes())) {
            return [$this->getAttribute($attribute), config('voyager.multilingual.default'), false];
        }

        if (!$this->relationLoaded('translations')) {
            $this->load('translations');
        }

        if (is_null($locale)) {
            $locale = app()->getLocale();
        }

        if ($fallback === true) {
            $fallback = config('app.fallback_locale', 'en');
        }

        $default = config('voyager.multilingual.default');

        $translations = $this->getRelation('translations')
            ->where('column_name', $attribute);

        if ($default == $locale) {
            return [$this->getAttribute($attribute), $default, true];
        }

        $localeTranslation = $translations->where('locale', $locale)->first();
        if ($localeTranslation && $localeTranslation->value) {
            return [$localeTranslation->value, $locale, true];
        }

        if ($fallback == $locale) {
            return [$this->getAttribute($attribute), $locale, false];
        }

        if ($fallback == $default) {
            return [$this->getAttribute($attribute), $locale, false];
        }

        $fallbackTranslation = $translations->where('locale', $fallback)->first();

        if ($fallbackTranslation && $fallback !== false) {
            return [$fallbackTranslation->value, $locale, true];
        }

        return [null, $locale, false];
    }
}
