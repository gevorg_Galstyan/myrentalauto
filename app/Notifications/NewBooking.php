<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewBooking extends Notification
{
    use Queueable;
public $order;
public $type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order, $type)
    {
        $this->order = $order;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($notifiable->role_id == 1) return ['mail', 'broadcast'];
        else return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->role_id != 1 && $this->type != 'full'){
            return (new MailMessage)
                ->greeting(__('notification.car-booking.owner.title', [
                    'name' => $notifiable->first_name,
                ]))
                ->line(__('notification.car-booking.owner.text', [
                    'car' => $this->order->car->title,
                    'start' => $this->order->dates->start->format('d:m:Y H:i'),
                    'end' => $this->order->dates->end->format('d:m:Y H:i'),
                    'customer' => $this->order->customer->full_name,
                    'owner' => $this->order->car->owner->full_name,
                ]))
                ->action(__('notification.car-booking.owner.link_title'), route('owner.orders', ['status' => 'booking', 'barcode_number' => $this->order->barcode_number]))
                ->salutation(__('notification.salutation', ['name' => config('app.name')]));
        }else{
            return (new MailMessage)
                ->greeting(__('notification.car-booking.admin.title', [
                    'name' => $notifiable->first_name,
                ]))
                ->line(__('notification.car-booking.admin.'.($this->type == 'full' ? 'text_full_pay':'text_booking'), [
                        'customer' => $this->order->customer->full_name,]).__('notification.car-booking.admin.text', [
                    'car' => $this->order->car->title,
                        'start' => $this->type=='full'? $this->order->parent->dates->start->format('d:m:Y H:i'): $this->order->dates->start->format('d:m:Y H:i'),
                        'end' => $this->type=='full'? $this->order->parent->dates->end->format('d:m:Y H:i'): $this->order->dates->end->format('d:m:Y H:i'),
                    'owner' => $this->order->car->owner->full_name,
                ]))
                ->action(__('notification.car-booking.admin.link_title'), route('voyager.orders.show', $this->order))
                ->salutation(__('notification.salutation', ['name' => config('app.name')]));
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if ($notifiable->role_id != 1 && $this->type != 'full') {
            return [
                'type' => 'New booking',
                'car_slug' => $this->order->car->slug,
                'user' => [
                    'name' => $this->order->customer->first_name,
                    'id' => $this->order->customer->id,
                ],
                'title' => __('car.rent'),
                'message' => __('notification.car-booking.owner.text', [
                    'car' => $this->order->car->title,
                    'start' => $this->order->dates->start->format('d:m:Y g:i'),
                    'end' => $this->order->dates->end->format('d:m:Y g:i'),
                ]),
                'url' => route('owner.orders', ['status' => 'paid', 'barcode_number' => $this->order->barcode_number]),
                'order_id' => $this->order->id,
            ];
        }else{
            return [
                //
            ];
        }
    }

    public function toBroadcast($notifiable)
    {
        if ($notifiable->role_id != 1 && $this->type != 'full') {
            return new BroadcastMessage([
                'title' => __('car.booking'),
                'message' => __('notification.car-booking.owner.text', [
                    'car' => $this->order->car->title,
                    'start' => $this->order->dates->start->format('d:m:Y g:i'),
                    'end' => $this->order->dates->end->format('d:m:Y g:i')
                ]),
                'url' => route('owner.orders', ['status' => 'paid', 'barcode_number' => $this->order->barcode_number]),
            ]);
        }else{
            return new BroadcastMessage([
                'title' => __('car.rent'),
                'message' => '<a href="'.route('voyager.orders.show', $this->order).'">'.__('notification.car-booking.admin.'.($this->type == 'full' ? 'text_full_pay':'text_booking'), [
                        'customer' => $this->order->customer->full_name,]). __('notification.car-booking.admin.text',[
                        'car' => $this->order->car->title,
                        'start' => $this->type=='full'?$this->order->parent->dates->start->format('d:m:Y H:i'):$this->order->dates->start->format('d:m:Y H:i'),
                        'end' => $this->type=='full'?$this->order->parent->dates->end->format('d:m:Y H:i'):$this->order->dates->end->format('d:m:Y H:i'),
                        'owner' => $this->order->car->owner->full_name,
                    ]).'</a>',
            ]);
        }
    }
}
