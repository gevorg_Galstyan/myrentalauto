<?php
return [
  'admin' => 'Administrator',
  'moderator' => 'Moderator',
  'owner' => 'Owner',
  'user' => 'User',
];
