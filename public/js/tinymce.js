function tinymce_setup_callback(editor) {
    editor.remove();
    editor = null;
    tinymce.init({
        selector: 'textarea.richTextBox',
        // valid_elements : '#p',
        height: 550,
        language_url: '/admin/langs/ru.js',
        language: 'ru',
        setup: (editor) => {
            editor.ui.registry.addButton('left', {
                text: 'с лева ',
                onAction: () => {
                    editor.focus();
                    editor.selection.setContent('<div class="row"><p class="col-md-6 ">Составьте текст или картинку</p><p class="col-md-6 ">Составьте текст или картинку</p></div>');
                }
            });
            editor.ui.registry.addButton('right', {
                text: 'с права ',
                onAction: () => {
                    // editor.focus();
                    editor.insertContent('<p class="col-md-12">Составьте текст или картинку</p>');
                }
            });
        },
        plugins: 'print preview  searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  imagetools colorpicker textpattern help',
        extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
        file_picker_callback: function (callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload_file').trigger('click')
            }
        },
        toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link code template image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | left | right ',
        image_advtab: true,
        media_alt_source: false,
        media_dimensions: false,
        media_live_embeds: true,
        media_poster: false, // 1
        media_url_resolver: function (data, resolve, reject) {
            console.log(data, resolve, reject);
            let url = data.url;
            let regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
            let match = url.match(regExp);

            if (match && match[2].length === 11){
                url = match[2]
            }
            let embedHtml = `<iframe width="100%"  height="200" src="//www.youtube.com/embed/${url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
            resolve({html: embedHtml});

        },
        templates: [
            // {title: 'пример 1', description: 'Some desc 1', content: 'My content'},
            {title: 'Фото слева', description: 'Фото слева', url: '/admin/templates/example-1'},
            {title: 'Фото справа', description: 'Фото справа', url: '/admin/templates/example-2'}
        ],
        content_css: [
            'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
            'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
            // '//www.tiny.cloud/css/codepen.min.css'
        ],
        content_style: [
            'body{width: 100%;padding-right: 15px;padding-left: 15px; margin-right: auto;margin-left: auto; color: #504d4d !important;} .mce-object-iframe{width:100% !important}'
        ],
        visual_table_class: 'tiny-table',
        convert_urls: false,
        image_caption: true,
        image_title: true,


    });
    // tinymce.init({
    //     selector: 'textarea.richTextBox-title',
    //     height: 250,
    //     language: 'ru',
    //     plugins: 'preview fullpage     template  charmap hr  textcolor wordcount  colorpicker ',
    //     extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
    //     file_picker_callback: function (callback, value, meta) {
    //         if (meta.filetype == 'image') {
    //             $('#upload_file').trigger('click')
    //         }
    //     },
    //     toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link code template image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat',
    //     image_advtab: true,
    //     templates: [
    //         // {title: 'пример 1', description: 'Some desc 1', content: 'My content'},
    //         // {title: 'Фото слева', description: 'Фото слева', url: '/admin/templates/example-1'},
    //         // {title: 'Фото справа', description: 'Фото справа', url: '/admin/templates/example-2'}
    //     ],
    //     content_css: [
    //         'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
    //         'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
    //         // '//www.tiny.cloud/css/codepen.min.css'
    //     ],
    //     content_style: [
    //         'body{padding : 15px;} h1::after {text-align:center; font-weight: bolder}'
    //     ],
    //     visual_table_class: 'tiny-table',
    //     convert_urls: false,
    //     image_caption: true,
    //     image_title: true,
    //
    // });
}

// function tinymce_init_callback(editor) {
//
//
//
//     tinymce.init({
//         selector: 'textarea.richTextBox',
//         height: 550,
//         language: 'ru',
//         plugins: 'print preview fullpage searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  imagetools colorpicker textpattern help',
//         extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
//         file_picker_callback: function (callback, value, meta) {
//             if (meta.filetype == 'image') {
//                 $('#upload_file').trigger('click')
//             }
//         },
//         toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link code template image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat',
//         image_advtab: true,
//         templates: [
//             // {title: 'пример 1', description: 'Some desc 1', content: 'My content'},
//             {title: 'пример 1', description: 'пример', url: '/admin/templates/example-1'}
//         ],
//         content_css: [
//             'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
//             'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
//             // '//www.tiny.cloud/css/codepen.min.css'
//         ],
//         // content_style: [
//         //     'body{width:90%; padding:30px; margin:auto;font-size:16px;font-family:Lato,"Helvetica Neue",Helvetica,Arial,sans-serif; line-height:1.3; letter-spacing: -0.03em;color:#222} h1,h2,h3,h4,h5,h6 {font-weight:400;margin-top:1.2em} h1 {} h2{} .tiny-table {width:100%; border-collapse: collapse;} .tiny-table td, th {border: 1px solid #555D66; padding:10px; text-align:left;font-size:16px;font-family:Lato,"Helvetica Neue",Helvetica,Arial,sans-serif; line-height:1.6;} .tiny-table th {background-color:#E2E4E7}'
//         // ],
//         visual_table_class: 'tiny-table',
//         convert_urls: false,
//         image_caption: true,
//         image_title: true,
//
//     });
//
// }

$(document).on('submit', '#my_form', function (form) {
    form.preventDefault();
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            $('.tox-control-wrap').find('input[type="text"]').val(response.path);
        }
    })
});
// $("p:empty").remove();
