<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('meta')
    @php($meta_tags = \Cache::remember('meta_home_page', setting('site.cache_time'), function () {
            return App\Models\MetaTag::where('name', 'home_page')->first();
        }))
    <title>{{$meta_tags->myTranslate()->title}}</title>
    <meta name="keywords" content="{{$meta_tags->myTranslate()->Keyword}}"/>
    <meta name="description" content="{{$meta_tags->myTranslate()->description}}"/>

    <meta property="og:locale"
          content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$meta_tags->myTranslate()->title}}"/>
    <meta property="og:description" content="{{$meta_tags->myTranslate()->description}}"/>
    <meta property="og:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name"
          content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site"
          content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title" content="{{$meta_tags->myTranslate()->title}}">
    <meta name="twitter:description" content="{{$meta_tags->myTranslate()->description}}">
    <meta name="twitter:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}">
@show
<link rel="alternate" href="{{LaravelLocalization::getLocalizedURL('ru', request()->url())}}" hreflang="ru" />
<link rel="alternate" href="{{LaravelLocalization::getLocalizedURL('en', request()->url())}}" hreflang="en" />
<link href="{{request()->url()}}" rel="canonical" />
<link rel="preload" href="{{asset('fonts/vendor/@fortawesome/fontawesome-free/webfa-brands-400.woff2?0425d661f34ffa46604c9dfa344c03bb')}}" as="font" type="font/woff2" crossorigin>

@if (request()->getHost() != 'myrentauto.com')
    <meta name="robots" content="noindex, nofollow">
@endif

<meta name="yandex-verification" content="377ccc79bf97999b"/>
<link rel="shortcut icon" href="{{asset('/storage/img/icons/ms-icon-310x310.png')}}" type="image/x-icon">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/custom-style.css') }}">
{{--<link rel="stylesheet" href="{{ asset('css/fonts.css') }}"><link rel="stylesheet" href="{{ asset('css/main.css') }}"><link rel="stylesheet" href="{{ asset('css/style.css') }}"><link rel="stylesheet" href="{{ asset('css/media.css') }}"><link rel="stylesheet" href="{{ asset('css/daterangepicker.min.css') }}">--}}
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="{{asset('css/fortawesome.css')}}">

{{--@laravelPWA--}}
@include('modules.laravelpwa.meta')
@section('script_top')
@show
@section('style')
@show
