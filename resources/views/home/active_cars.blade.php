<div class="own-container active-cars mt-5 container-90" id="active-cars">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center mb-5 fw-700 sub-heading">{{__('home.cars_for_rent')}}</h2>
            <div class="display-none car-container">
                @foreach ($cars as $car)
                    <div class="p-3 d-flex justify-content-center">
                        <div class="single-auto bg-transparent  mw-250" style="min-width: 300px">
                        <div class="bg-white  position-relative">
                            <img data-src="{{ ownMakeImage(326, 0, 75,$car->image_1)}}"
                                 data-srcset="{{ownMakeImage(326, 0, 75,$car->image_1)}}"
                                 src="{{ownMakeImage(0,0,50,str_replace('.jpg', '-tumble.jpg',$car->image_1))}}"
                                 data-sizes="auto" class="card-img-top lazyload" alt="{{$car->title}}"
                                 title="{{$car->title}}">
                            <div class="card-footer row w-100 m-0 px-0 py-1 ">
                                <div class="col-auto p-0  m-auto  car-params">
                                    <div class="svg-icon mr-1  year-car-icon">
                                    </div>
                                    <span class="fs-10 fs-md-8 fw-700 d-flex align-items-center ">{{$car->age}}</span>
                                </div>
                                |
                                <div class="col-auto p-0 m-auto  car-params">
                                    <div class="svg-icon mr-1 motor">
                                    </div>
                                    <span
                                        class="fs-10 fs-md-8 fw-700 d-flex align-items-center ">{{__('general.'.$car->fuel)}} {{$car->motor}}  </span>
                                </div>
                                |
                                <div class="col-auto p-0 m-auto  car-params">
                                    <div class="svg-icon mr-1 gear">
                                    </div>
                                    <span class="fs-10 fs-md-8 fw-700 d-flex align-items-center ">{{__('general.'.$car->transmission)}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white card-body position-relative d-flex flex-wrap py-3 justify-content-between">
                            <div class="row m-0 pb-3 w-100">
                                <div class="col-8 pl-0">
                                    <h5 class="card-title cart-after position-relative fw-600 mb-0">{{$car->make->name}}
                                        <br> {{$car->model->name}}</h5>
                                </div>
                                @if (!$car->on_request)
                                    <div class="col-4 d-flex justify-content-end car-instant-booking">
                                    </div>
                                @endif
                            </div>
                            <div class="px-0 col-7">
                                <p class="fs-13 color-light mb-2 pr-2 text-truncate" data-toggle="tooltip"
                                   title="{{$car->myTranslate()->location_title}}">
                                    <span class="location-icon-svg d-inline-block"></span>
                                    <span>{{$car->myTranslate()->location_title}}</span>
                                </p>
                                <div class="fs-13 d-flex flex-wrap align-items-center">
                                    <div class="stars stars--large mr-1">
                                        <span style="width: {{$car->avg_score * 20}}%"></span>
                                    </div>
                                    <span>({{$car->reviews->count()}})</span>
                                </div>
                            </div>
                            <div class=" col-5 p-0">
                                <div class="price-block px-0">
                                    <span class="price"><span
                                            class="ff-st fs-18">{!! currency($car->price_1, false, false) !!} <span class="currency-symbol"></span></span></span>
                                    <br>
                                    <span class="day"></span>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white  p-0 m-0  border-none pb-3">
                            <div class="col-12 text-center">
                                <a href="{{route('car.single', ['slug' => $car->slug])}}"
                                   class="own-btn text-uppercase d-inline-block btn-shadow ora">{{__('car.rent_it')}}</a>
                            </div>
                        </div>
                    </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>

