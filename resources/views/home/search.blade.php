<section class="search home-search pt-3 translate-90 {{--bg-own-blue--}}">

    <div class="text-part col-lg-12 p-0">
        <div>
            <ul class="w-100 row justify-content-center textSlider"></ul>
        </div>
    </div>
    <div class="container-fluid">
        <form action="{{route('search','minsk')}}" id="homeSearchForm" method="GET"
              class="row justify-content-center pt-0 {{--mt-lg-2 mt-md-2--}} align-items-center {{--myTranslate-50--}} search-row position-relative text-center">
            <div class="col-lg-3">

                <div class="dropdown" id="location-dropdown">
                    <div class="form-control border-20  location  location-parent" id="location-dropdown-label"
                         data-toggle="dropdown" data-flip="false" aria-expanded="false">
                        {{$locations->where('slug', 'minsk')->translate()->first()->location}}
                    </div>
                    <ul class="dropdown-menu w-100 text-center px-2 w-95 left-2-5" aria-labelledby="location-dropdown-label"
                        id="search-location-dropdown">
                        @foreach ($locations as $location)
                            <a href="javascript:void(0)" class="dropdown-item location-item {{$location->slug == 'minsk' ? 'active' : ''}}"
                               onclick="setActive(this)"
                               data-value="search/{{$location->slug}}">
                                {{$location->translate()->location}}
                            </a>
                        @endforeach
                    </ul>
                </div>


                {{--                <input type="text" class="form-control border-20 pr-25 position-relative text-center text-truncate" id="location" name="location"--}}
                {{--                       placeholder="{{__('home.location')}}" autocomplete="off"/>--}}
                {{--                --}}{{--<div class="location-icon-container">--}}
                {{--                    <i class="location-loader"></i>--}}
                {{--                </div>--}}
                {{--                <span class="text-secondary k-pointer color-black currentLocationBtn position-absolute" data-toggle="tooltip" data-html="true" data-placement="top"--}}
                {{--                      title="{{tooltips($toolips, 'current_location')}}">--}}
                {{--                    <i class=""></i>--}}
                {{--                </span>--}}
            </div>

            <div class="col-lg-4" id="search-block">
                <input type="hidden" class="date-input" name="date" autocomplete="off"
                       value=""/>

                <div class="form-control search-date  date-block border-20">
                    @php
                        $start = \Carbon\Carbon::now() ->setTimeFromTimeString('09:00')->addDay(5);
                        $end = \Carbon\Carbon::now()->setTimeFromTimeString('09:00')->addDay(7);
                    @endphp
{{--                    {{$start->format('d.m.Y H:i' ).' ~ '. $end->format('d.m.Y H:i' )}}--}}
                    <span class="date-1-block"><strong>{{$start->format('d.m.Y ')}}</strong></span> <span class="time-1-clock">{{$start->format('H:i' )}}</span> ~
                    <span class="date-2-block"><strong>{{$end->format('d.m.Y ' )}}</strong></span> <span class="time-2-clock">{{$end->format('H:i' )}}</span>
                </div>
            </div>
            <!--<div class="col-md-2">-->
            <!--<input type="date" class="form-control" name="end" placeholder="Дата возврата" />-->
            <!--</div>-->
            <div class="col-lg-3">
                <div class="search-select">
                    {{--                    <select class="form-control border-20 search-select-select2 transmission"  data-placeholder="{{__('general.transmission')}}" data-width="100%"  name="transmission" >--}}
                    {{--                        <option disabled="" title=" ">{{__('general.transmission')}}</option>--}}
                    {{--                        <option value="0" title=" ">{{__('general.all')}}</option>--}}
                    {{--                        <option value="automatic" title=" ">{{__('general.automatic')}}</option>--}}
                    {{--                        <option value="manual" title=" ">{{__('general.manual')}}</option>--}}
                    {{--                    </select>--}}

                    <div class="dropdown" id="transmission-dropdown">
                        <input type="hidden" name="transmission" value="">
                        <div class="form-control border-20  transmission  transmission-parent"
                             id="transmission-dropdown-label" data-toggle="dropdown" data-flip="false"
                             aria-expanded="false">
                            {{__('general.all')}}
                        </div>
                        <ul class="dropdown-menu w-100 text-center px-2 w-95 left-2-5" aria-labelledby="transmission-dropdown-label"
                            id="search-transmission-dropdown">
                            <a href="javascript:void(0)"
                               class="dropdown-item disabled fw-700">{{__('general.transmission')}}</a>
                            <a href="javascript:void(0)" class="dropdown-item transmission-item active"
                                onclick="setActive(this)"
                               data-value="0">{{__('general.all')}}</a>
                            <a href="javascript:void(0)" class="dropdown-item transmission-item"
                                onclick="setActive(this)"
                               data-value="automatic">{{__('general.automatic')}}</a>
                            <a href="javascript:void(0)" class="dropdown-item transmission-item"
                                onclick="setActive(this)"
                               data-value="manual">{{__('general.manual')}}</a>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="pr-3 min-3" id="scrin">
                <div class="stage own-btn search py-2 filter open-home-filters border-20">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         viewBox="0 0 25.5 22" style="max-width: 40px; enable-background:new 0 0 25.5 22;fill: #fff"
                         xml:space="preserve">
               <g id="bar-bottom">
                   <rect x="4" y="16.2" class="st0" width="18" height="2"/>
               </g>
                        <g id="bar-middle">
                            <rect x="4" y="10.2" class="st0" width="18" height="2"/>
                        </g>
                        <g id="bar-top">
                            <rect x="4" y="4.2" class="st0" width="18" height="2"/>
                        </g>
                        <g id="control-bottom">
                            <g>
                                <circle class="st1" cx="11.5" cy="17.2" r="1.5"/>
                                <path class="st0" d="M11.5,19.5c-1.2,0-2.2-1-2.2-2.2s1-2.2,2.2-2.2s2.2,1,2.2,2.2S12.7,19.5,11.5,19.5z M11.5,16.5
			c-0.4,0-0.8,0.4-0.8,0.8s0.4,0.8,0.8,0.8s0.8-0.4,0.8-0.8S11.9,16.5,11.5,16.5z"/>
                            </g>
                        </g>
                        <g id="control-middle">
                            <g>
                                <circle class="st1" cx="18.8" cy="11.2" r="1.5"/>
                                <path class="st0" d="M18.8,13.5c-1.2,0-2.2-1-2.2-2.2s1-2.2,2.2-2.2s2.2,1,2.2,2.2S20,13.5,18.8,13.5z M18.8,10.5
			c-0.4,0-0.8,0.4-0.8,0.8s0.4,0.8,0.8,0.8s0.8-0.4,0.8-0.8S19.2,10.5,18.8,10.5z"/>
                            </g>
                        </g>
                        <g id="control-top">
                            <g>
                                <circle class="st1" cx="7.2" cy="5.2" r="1.5"/>
                                <path class="st0" d="M7.2,7.5C6,7.5,5,6.5,5,5.2S6,3,7.2,3s2.2,1,2.2,2.2S8.4,7.5,7.2,7.5z M7.2,4.5c-0.4,0-0.8,0.4-0.8,0.8
			S6.8,6,7.2,6S8,5.7,8,5.2S7.6,4.5,7.2,4.5z"/>
                            </g>
                        </g>
</svg>
                </div>
            </div>
            <div class="pr-3 min-3">
                <button class="btn own-btn search py-2 border-20" type="submit">
                    <img src="{{ownMakeImage(40, 0, 75, 'img/icons/search.png', 'png')}}" alt="search"
                         class="img-fluid">
                </button>
            </div>

            <div class="row justify-content-center align-items-center flex-wrap m-0 mt-3 w-100">
                <div class="tags home-tags-block"></div>
            </div>
            <div class="col-lg-9 mt-2 mx-auto">
                <div class="search-filter ">
                    <div class="row m-0 justify-content-between">
                        <h5 class="col-12  fw-700 text-center">{{__('home.basic_filters')}}</h5>
                        <div class="col-md-3 text-center auto-type-inputs">
                            <h6 class="fw-700 color-light mb-3">{{__('home.class')}}</h6>
                            <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                                <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.all')}}</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="checkbox" data-type="type" class="home-params choose-all all-autoTypes"
                                           name="type" value="" checked/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            @foreach($car_types as $type)
                                <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                                    <span class="fs-13 fw-700 ml-2 fil-name">{{$type->myTranslate()->name}}</span>
                                    <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                           class="toggleSwitch checkbox mb-0">
                                        <input type="checkbox" data-type="type" class="home-params"
                                               id="filter-type-{{$type->id}}"
                                               name="type[]" data-name="{{$type->myTranslate()->name}}"
                                               value="{{$type->id}}"/>
                                        <span class="knob"></span>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-3 text-center">
                            <h6 class="font-uppercase color-light fw-700 mb-3">{{__('home.fuel')}}</h6>
                            <div class="pl-2">
                                <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                                    <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.petrol')}}</span>
                                    <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                           class="toggleSwitch checkbox mb-0">
                                        <input type="checkbox"
                                               id="filter-fuel-petrol"
                                               class="home-params" name="fuel[]" data-type="fuel"
                                               data-name="{{__('general.petrol')}}" value="petrol"/>
                                        <span class="knob"></span>
                                    </label>
                                </div>
                                <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                                    <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.diesel')}}</span>
                                    <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                           class="toggleSwitch checkbox mb-0">
                                        <input type="checkbox" class="home-params"
                                               id="filter-fuel-diesel"
                                               name="fuel[]" data-type="fuel" data-name="{{__('general.diesel')}}"
                                               value="diesel"/>
                                        <span class="knob"></span>
                                    </label>
                                </div>
                                <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                                    <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.electric')}}</span>
                                    <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                           class="toggleSwitch checkbox mb-0">
                                        <input type="checkbox" class="home-params"
                                               id="filter-fuel-electric"
                                               name="fuel[]" data-type="fuel" data-name="{{__('general.electric')}}"
                                               value="electric"/>
                                        <span class="knob"></span>
                                    </label>
                                </div>
                                <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                                    <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.hybrid')}}</span>
                                    <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                           class="toggleSwitch checkbox mb-0">
                                        <input type="checkbox" class="home-params"
                                               id="filter-fuel-hybrid"
                                               name="fuel[]" data-type="fuel" data-name="{{__('general.hybrid')}}"
                                               value="hybrid"/>
                                        <span class="knob"></span>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 text-center">
                            <h6 class="fw-700 color-light mb-3">{{__('home.seats')}}</h6>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">{{__('general.all')}}</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat" data-name="{{__('general.all')}}"
                                           class="home-params choose-all"
                                           name="seats" value="" checked/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">2</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat"
                                           data-name="{{__('general.seats', ['seats' => 2])}}"
                                           class="home-params"
                                           id="filter-seat-2"
                                           name="seats" value="2"/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">4</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat"
                                           data-name="{{__('general.seats', ['seats' => 4])}}"
                                           class="home-params"
                                           id="filter-seat-4"
                                           name="seats" value="4"/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">5</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat"
                                           data-name="{{__('general.seats', ['seats' => 5])}}"
                                           class="home-params"
                                           id="filter-seat-5"
                                           name="seats" value="5"/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">7</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat"
                                           data-name="{{__('general.seats', ['seats' => 7])}}"
                                           class="home-params"
                                           id="filter-seat-7"
                                           name="seats" value="7"/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">8</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat"
                                           data-name="{{__('general.seats', ['seats' => 8])}}"
                                           class="home-params"
                                           id="filter-seat-8"
                                           name="seats" value="8"/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                            <div class="position-relative d-flex align-items-center justify-content-center mb-2">
                                <span class="fs-13 fw-700 mr-4 fil-name">9</span>
                                <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                       class="toggleSwitch checkbox mb-0">
                                    <input type="radio" data-type="seat"
                                           data-name="{{__('general.seats', ['seats' => 9])}}"
                                           class="home-params"
                                           id="filter-seat-9"
                                           name="seats" value="9"/>
                                    <span class="knob"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-right">
                        <button type="button" class="btn btn-dark clear-home-filters">{{__('home.clear')}}</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</section>
