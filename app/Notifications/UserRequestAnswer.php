<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRequestAnswer extends Notification
{
    use Queueable;

    public $order;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->order->status == 'confirmed'){
            return (new MailMessage)
                ->greeting(__('notification.answer-request.confirmed.title', [
                    'name' => $notifiable->first_name,
                ]))
                ->line(__('notification.answer-request.confirmed.text', [
                    'car' => $this->order->car->title
                ]))
                ->action(__('notification.car-booking.owner.link_title'), route('car.single', ['slug' => $this->order->car->slug]))
                ->salutation(__('notification.salutation', ['name' => config('app.name')]));
        }else{
            return (new MailMessage)
                ->greeting(__('notification.answer-request.rejected.title', [
                    'name' => $notifiable->first_name,
                ]))
                ->line(__('notification.answer-request.rejected.text', [
                    'car' => $this->order->car->title,
                    'link' => route('search'),
                ]))
                ->salutation(__('notification.salutation', ['name' => config('app.name')]));
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
