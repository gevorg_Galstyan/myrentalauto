<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Redirect;

class CheackVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $redirectToRoute
     * * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     * @return mixed
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        if ($request->url() != route( 'verification.notice') && $request->user() &&  (($request->user() instanceof MustVerifyEmail && ! $request->user()->hasVerifiedEmail()) && ($request->user()->provider_id == null)) ) {
            return $request->expectsJson()
                ? abort(403, 'Ваш адрес электронной почты не подтвержден.')
                : Redirect::route($redirectToRoute ?: 'verification.notice');
        }
        return $next($request);
    }
}
