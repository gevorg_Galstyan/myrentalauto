@extends('owners.layouts.app')


@section('content')
    <div class="container">

        <div class="card">

            <div class="card-header">
                <ul class="d-flex  justify-content-center w-100 nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{request()->url() == route('owner.settings.index') ? 'active': ''}}"
                           href="{{route('owner.settings.index')}}">Договор</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">Уведомление</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('owner.settings.working_times')}}"
                           class="nav-link  {{request()->url() == route('owner.settings.working_times') ? 'active': ''}}">
                            Рабочее время
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('owner.settings.security')}}"
                           class="nav-link  {{request()->url() == route('owner.settings.security') ? 'active': ''}}">
                            Безопасность
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    @include('owners.settings.'.$view)
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
    </script>
@stop
