require('./owner-bootstrap');


Echo.private('App.User.' + userId)
    .notification((notification) => {
        playNotify();
        // const Toast = Swal.mixin({
        //     toast: true,
        //     position: 'top-right',
        //     showConfirmButton: false,
        //     showCloseButton: true,
        // });
        // Toast.fire({
        //     html: '<strong>' + notification.title + '</strong><br><p>' + notification.message + '</p>',
        // });

        toastr.info(notification.message, '<strong>' + notification.title + '</strong>', {timeOut: 1000});
        console.log(notification.id)
        $('.nav-unread').removeClass('d-none');
        $('.notifications-lists').prepend(' <div class="text-center"><a data-notify="/c-owner/notify/' + notification.id + '" href="' + (notification.url ? notification.url : '#0') + '" class="dropdown-item d-flex read-notify">\n' +
            '                                <strong class="pr-2">' + notification.title + ' </strong> ' + notification.message +
            '                        </a>' +
            '                       </div><div class="dropdown-divider"></div>')
    });

function playNotify() {
    var audio = new Audio('/storage/notify.mp3');
    audio.play();
}

$(document).on('click', '.read-notify', function (e) {
    // e.preventDefault()
    $.ajax({
        url: $(this).data('notify')
    })
});
