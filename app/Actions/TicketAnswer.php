<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class TicketAnswer extends AbstractAction
{
    public function getTitle()
    {
        return 'Ответить';
    }

    public function getIcon()
    {
        return 'fas fa-award';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.tickets.show', $this->data);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'tickets';
    }


}
