@extends('voyager::master')

@section('page_title', 'На модерацию')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="icon fas fa-check-double"></i> На модерацию
        </h1>

    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        {{--<div class="actions-descInfo p-2" style="max-width: {{count(Voyager::actions()) * 130}}px">--}}
        {{--<h4 class="d-block text-center">Действие кнопок</h4>--}}
        {{--<div class="d-flex ">--}}

        {{--@foreach(Voyager::actions() as $action)--}}
        {{--@if (!method_exists($action, 'massAction'))--}}
        {{--@include('voyager::bread.partials.actions-desc', ['action' => $action])--}}
        {{--@endif--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Главная фото</th>
                                    <th>Описание</th>
                                    <th>Страница</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($notifications as $notify)
                                    <tr>
                                        <td>
                                            <img src="{{Voyager::image($notify->data['image'])}}" width="100px">
                                        </td>
                                        <td>
                                            {!! $notify->data['text'] !!}
                                        </td>
                                        <td>
                                            <a href="{{$notify->data['url']}}">{{$notify->data['title']}}</a>
                                        </td>
                                        <td>
                                            <a>{{$notify->created_at}}</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@stop

@section('javascript')
    <!-- DataTables -->

    <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>

    <script>
        $(document).ready(function () {

            var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});

        });

    </script>
@stop
