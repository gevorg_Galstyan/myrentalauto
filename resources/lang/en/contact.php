<?php
return [
    'your_first_name' => 'Your First Name',
    'your_last_name' => 'Your Last Name',
    'your_email' => 'Your Email',
    'message' => 'message',
    'title' => 'GET IN TOUCH',
    'send' => 'SEND',
    'desc' => 'You can contact us any way that is convenient for you. We are available 24/7 via fax or email. You can also use a quick contact form below or visit our office personally. Email us with any questions or inquiries or use our contact data. We would be happy to answer your questions.',
    'follow_us' => 'Follow Us',
    'email' => 'Email',
    'contact_form' => 'Contact Form',
    'thanks' => 'Thanks for contacting us!',
    'subject' => 'Contact Form Query',
];
