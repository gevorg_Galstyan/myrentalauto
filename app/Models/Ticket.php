<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Ticket extends Model
{
    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->creator_id && Auth::user()) {
            $this->creator_id = Auth::user()->getKey();
        }
        if (!$this->target_id && Auth::user()) {
            $this->target_id = Auth::user()->getKey();
        }

        if (!$this->ticket_number) {
            $this->ticket_number = generateTicketNumber();
        }

        parent::save();
    }
    public function comments(){
        return $this->hasMany(TicketComment::class);
    }

    public function target()
    {
        return $this->belongsTo(User::class, 'target_id', 'id');
    }

    public function priority()
    {
        return $this->belongsTo(TicketPriority::class, 'priority_id', 'id');
    }
}
