<?php
return [
    'email' => 'Email',
    'phone' => 'Phone Number',
    'birthday' => 'Birthday',
    'age' => 'Age',
    'driving_experience' => 'Driving experience',
    'country' => 'Country registration',
    'city' => 'City',
    'region' => 'Region',
    'address' => 'Адрес',
    'registered' => 'Registered in',
    'check_profile' => 'Save and send for confirmation',
    'check_profile_info' => 'You have a new confirmation',
    'check_profile_send' => 'Your confirmation sended to',
    'pass_change' => 'Your password changed!',
    'pass_change_error' => 'The current password does not match.',
    'profile_update' => 'Your personal information is updated!',
    'rejected' => 'Your data is incompatible with our terms!',
    'confirm' => 'Your profile is activated!',
    'rejected_name' => 'Rejected',
    'confirm_name' => 'Confirm',
    'confirm_message' => 'Send your confirmation message...',

    'active' => 'Active',
    'story' => 'Story',

    'profile' => 'Profile',
    'edit_profile' => 'Edit',
    'upload_avatar' => 'Upload another photo...',
    'upload' => 'Upload image',

    'posts' => 'Posts',

    'full_name' => 'Last Name',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'patronymic' => 'Patronymic',
    'citizenship' => 'Citizenship',
    'save' => 'Save',

    'password' => 'Password',
    'new_password' => 'New Password',
    'currently_password' => 'Currently password',
    'change_password' => 'Change Password',
    'add_password' => 'Add password',
    'confirm_password' => 'Confirm the password',
    'save_password' => 'Save Password',

    'documents' => 'Documents',
    'description_profile' => 'Service guarantees data confidentiality. Your documents will not be displayed in the public profile.',
    'upload_passport' => 'Upload a passport photo',
    'upload_license' => 'Upload a photo of your driver license',

    'no_active_cars' => 'No active cars',
    'no_story' => 'You don`t have rent story yet',
    'no_favorites' => 'You don`t have favorite cars yet',
    'choose_avatar_photo' => 'Choose <i class="fas fa-user-circle"></i>',

    'passport_documents' => 'Passport Documents',
    'vod_documents_certificates' => 'Vod documents certificates',
    'to_book_a_ar_you_need_to_fill_out_a_profile_and_agree_to_the_terms' => 'to book a car you need to fill out a profile and agree to the terms',

    'back_profile' => 'Profile',

    'delete_profile' => 'Delete profile',

    'delete' => [
        'are_you_sure' => 'Are you sure?',
        'yes_delete' => 'Yes, delete',
        'cancel' => 'Cancel',
    ],
    'personal_area' => 'Personal Area',

    'order' => [
        'photo' => 'Photo',
        'car' => 'Car',
        'rental_date' => 'Rental Date',
        'return_date' => 'Return Date',
        'transaction_date' => 'Transaction Date',
        'status' => 'Status',
    ]

];
