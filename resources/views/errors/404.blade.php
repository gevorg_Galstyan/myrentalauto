@extends('layouts.app')

@section('content')
    <style>
        @import url(https://fonts.googleapis.com/css?family=Raleway:700);

        *, *:before, *:after {
            box-sizing: border-box;
        }

        .title {
            font-size: 6em !important;
            font-weight: 700 !important;
            color: #EE4B5E !important;
        }

        .error-text {
            color: #EE4B5E !important;
        }
        .slider-logo {
            top: 10px;
        }

    </style>
    <section class="slider m-0">
        <div class="w-100">
            <div class="d-flex p-0 mt-0 mt-sm-3 justify-content-center">
                <div class="slider-logo position-absolute">
                    <div class="slider-logo "><span class="own-blue">My</span><span class="own-orange">Rent</span><span
                            class="own-blue">Auto</span></div>
                </div>
                <div class="mt-2">
                    <div class="col-12 text-center">
                        <a href="{{route('home')}}" class="own-btn text-uppercase d-inline-block btn-shadow ora fs-sm-14">
                            {{__('payment.return_home_page')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="pt-3 translate-90">
        <div class="text-part col-lg-12 p-0">
            <div>
                <ul class="w-100 row justify-content-center textSlider list-unstyled">
                    <li class="">
                        <h2 class="mb-3 text-center error-code title" data-content="404">
                            404
                        </h2>
                        <p class="text-center error-text">
                            {{__('general.error-404')}}
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </section>

@endsection





