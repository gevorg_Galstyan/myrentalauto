<div class="modal fade" id="auth-booking-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <div>{!! tooltips($toolips, 'title_before_register_of_car_page', $car->title) !!}</div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div>{!! tooltips($toolips, 'text_before_register_of_car_page') !!}</div>
                <div class="form">
                    <form  method="POST" action="{{route('auth.guest_reg', $car)}}"
                           class="guest-before-booking"
                          accept-charset="UTF-8">
                        @csrf
                        <div class="form-group">
                            <input id="guestFirstName" type="text" class="form-control  border-25 mb-2"
                                   name="guestFirstName"  required
                                   placeholder="{{__('auth.first_name')}}">
                        </div>

                        <div class="form-group">
                            <input id="guestLastName" type="text" class="form-control  border-25 mb-2"
                                   name="guestLastName" required
                                   placeholder="{{__('auth.last_name')}}">
                        </div>

                        <div class="form-group">
                            <input type="email"
                                   class="form-control  border-25 mb-2"
                                   name="guestEmail"  required
                                   placeholder="{{__('auth.email')}}">
                        </div>
                        <div class="form-group">
                            <input id="phone" class="form-control w-100{{ $errors->has('number') ? ' is-invalid' : '' }}"
                                   name="phone"
                                   type="tel" value="">
                        </div>

                        <div class="text-center mt-2">
                            <button type="submit"
                                    class="own-btn text-capitalize fs-16 d-inline-block border-5 ora  btn-submit-reg">
                                Продолжить
                            </button>
                        </div>
                        <div class="alert-danger d-inline-block"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
