@extends('layouts.app')
@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop

@section('content')

    <div class="own-container ">
        <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center">

            <!-- edit form column -->
            <div class="col-md-12  ff-st row ">
                <div class="col-sm-4 mb-3">
                    <a href="{{url()->previous()}}" class="btn own-btn ora   d-inline-block  border-radius-20"
                       title="{{__('general.back')}}">
                        <i class="fas fa-arrow-left"></i>
                        <span class="">{{__('general.back')}}</span>
                    </a>
                </div>
                <div class="col-sm-4 mb-3">
                    <h1 class="text-center">{{__('header.favorites')}}</h1>
                </div>
                <div class="col-md-12 col-6">
                    @forelse (auth()->user()->favorite(\App\Models\Car::class) as $car)
                        @include('user.car-cart-item')
                    @empty

                        <h5 class="text-center w-100">{{__('user.no_favorites')}}</h5>

                    @endforelse
                </div>
            </div>

        </div>

    </div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

@stop

