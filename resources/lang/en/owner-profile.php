<?php

return [
    'information' => 'The contact person',
    'name' => 'Name',
    'sour_name' => 'Sour name',
    'email' => 'E-mail',
    'phone_number' => 'Phone number',
    'birthday' => 'Birthday',
    'general_information' => 'Legal entity data',
    'company' => 'Company (i.p.)',
    'reg_number' => 'Reg number',
    'legal_address' => 'Legal Address',
    'physical_address' => 'Physical Address',
    'unp' => 'UNP',
    'okpo' => 'OKPO',
    'owner_name' => 'Owner Name',
    'assigned' => 'Basis of what is assigned',
    'cont_number' => 'Cont. Number',
    'cont_mail' => 'Cont. E-mail',
    'bank' => 'Bank',
    'bank_reg_number' => 'PIK',
    'additional_information' => 'Additional information',
    'bank_account_number' => 'Bank Account Number',
];
