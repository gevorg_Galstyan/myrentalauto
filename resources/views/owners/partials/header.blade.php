<div class="header">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="{{route('home')}}">
                <img src="{{ ownMakeImage(50 , 50 , 85,'img/logo.png', 'png') }}" alt="MyRentAuto">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
                <div class="nav-item d-none d-md-flex">

                </div>
                <div class="dropdown d-none d-md-flex">
                    <a class="nav-link icon" data-toggle="dropdown">
                        <i class="fe fe-bell"></i>
                        <span
                            class="nav-unread {{ auth()->user()->unreadNotifications->count() ? '' : 'd-none' }}"></span>
                    </a>
                    @if (auth()->user()->unreadNotifications->count())
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow notifications-lists ">

                            @foreach (auth()->user()->unreadNotifications as $notifications)
                                <a href="{{$notifications->data['url']??'#0'}}"
                                   class="dropdown-item read-notify notify-item"
                                   data-notify="{{route('owner.notify.read',['notify' => $notifications->id])}}">
                                    <strong
                                        class="pr-2">{{@$notifications->data['title']??''}} </strong> {!! @$notifications->data['message']  !!}
                                </a>
                                <div class="dropdown-divider"></div>
                            @endforeach
                            <a href="{{route('owner.notify.read')}}" class="dropdown-item text-center">отметить все как
                                прочитанное</a>
                        </div>
                    @endif
                </div>
                <div class="dropdown d-flex">
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="avatar"
                              style="background-image: url({{Voyager::image(auth()->user()->avatar)}})">
                        </span>
                        <span class="ml-2 d-none d-lg-block">
                      <span class="text-default">{{auth()->user()->first_name.' '.auth()->user()->last_name}}</span>
                    </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="{{route('owner.profile')}}">
                            <i class="dropdown-icon fe fe-user"></i> Профиль
                        </a>
                        <a class="dropdown-item" href="{{route('owner.settings.index')}}">
                            <i class="dropdown-icon fe fe-settings"></i> Настройки
                        </a>
                        <a class="dropdown-item" href="{{route('tickets.index')}}">
{{--                                                        <span class="float-right"><span class="badge badge-primary">6</span></span>--}}
                            <i class="dropdown-icon fe fe-mail"></i> Тикеты
                        </a>
                        {{--                        <a class="dropdown-item" href="#">--}}
                        {{--                            <i class="dropdown-icon fe fe-send"></i> Message--}}
                        {{--                        </a>--}}
                        {{--                        <div class="dropdown-divider"></div>--}}
                        {{--                        <a class="dropdown-item" href="#">--}}
                        {{--                            <i class="dropdown-icon fe fe-help-circle"></i> Need help?--}}
                        {{--                        </a>--}}
                        <a class="dropdown-item" href="{{route('logout')}}">
                            <i class="dropdown-icon fe fe-log-out"></i>{{__('auth.logout')}}
                        </a>
                    </div>
                </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
               data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
            </a>
        </div>
    </div>
</div>
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            {{--<div class="col-lg-3 ml-auto">--}}
            {{--<form class="input-icon my-3 my-lg-0">--}}
            {{--<input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">--}}
            {{--<div class="input-icon-addon">--}}
            {{--<i class="fe fe-search"></i>--}}
            {{--</div>--}}
            {{--</form>--}}
            {{--</div>--}}
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="{{route('owner.dashboard')}}"
                           class="nav-link {{ Route::is('owner.dashboard') ? 'active' : '' }}">
                            <i class="fe fe-home"></i>
                            Статистика
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('cars.index')}}"
                           class="nav-link {{ Route::is('cars.index') ? 'active' : '' }}"><i class="fa fa-car mr-2"></i>
                            Машины</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('customers')}}"
                           class="nav-link {{ Route::is('customers') ? 'active' : '' }}"><i class="fa fa-users"></i>
                            Арендаторы</a>
                    </li>
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="{{route('orders')}}" class="nav-link {{ Route::is('orders') ? 'active' : '' }}"><i--}}
                    {{--                                class="fa fa-users"></i>--}}
                    {{--                            Заказы</a>--}}
                    {{--                    </li>--}}
                    <li class="nav-item">
                        <a href="javascript:void(0)"
                           class="nav-link {{(request()->is('c-owner/order/*')) ? 'active' : ''}}"
                           data-toggle="dropdown">
                            <i class="fe fe-box"></i>Заказы</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="{{route('owner.orders.index', ['status' => 'all'])}}"
                               class="dropdown-item ">Все</a>
                            <a href="{{route('owner.orders.index', ['status' => 'requested'])}}" class="dropdown-item">Запрошенные</a>
                            <a href="{{route('owner.orders.index', ['status' => 'confirmed'])}}" class="dropdown-item">Потвержденные</a>
                            <a href="{{route('owner.orders.index', ['status' => 'rejected'])}}" class="dropdown-item">Отклоненные</a>
                            <a href="{{route('owner.orders.index', ['status' => 'canceled'])}}" class="dropdown-item">Отмена</a>
                            <a href="{{route('owner.orders.index', ['status' => 'paid'])}}" class="dropdown-item">Оплаченные</a>
                            <a href="{{route('owner.orders.index', ['status' => 'completed'])}}" class="dropdown-item">Завершенные</a>
                            {{--                            <a href="{{route('owner.orders.index', ['status' => 'no_paid'])}}" class="dropdown-item">Неоплаченные</a>--}}
                            {{--                            <a href="{{route('owner.orders.index', ['status' => 'expired'])}}" class="dropdown-item">Истекшие</a>--}}
                        </div>
                    </li>
                    @can ('post-read')
                        <li class="nav-item">
                            <a href="{{route('owners-posts.index')}}"
                               class="nav-link {{ request()->segment(2) ? 'active' : '' }}">
                                <i class="fas fa-blog"></i>
                                Посты
                            </a>
                        </li>
                    @endcan
                </ul>
            </div>
        </div>
    </div>
</div>
