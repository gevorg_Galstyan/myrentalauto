<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerUserController;
use App\Mail\AdminUserSendEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends VoyagerUserController
{



    public function block(Request $request, User $user)
    {
        $this->authorize('block', app(User::class));
        $user->block = ($request->status == 'block' ? 1 : 0);
        $user->save();
        $data = $user->block == 1
            ? [
                'message' => 'Пользователь '.$user->fullName.' заблокирован',
                'alert-type' => 'warning',
            ]
            : [
                'message' => 'Пользователь '.$user->fullName.' разблокирован ',
                'alert-type' => 'success',
            ];
        return back()->with($data);
    }

    public function sendUserEmail(Request $request)
    {
        Mail::to($request->to)->send(new AdminUserSendEmail($request->all()));

        return response()->json(['success' => true, 'status' => 'Письмо успешно отправлен ']);
    }


}
