<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_with' => 'Login with',
    'or' => 'or',
    'email' => 'Email',
    'password' => 'Password',
    'repeat_password' => 'Repeat Password',
    'remember_me' => 'Remember me',
    'login' => 'Log In',
    'login_before_order' => 'Rent',
    'logout' => 'Log Out',
    'name' => 'Name',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'select_role' => 'Please select your role',
    'role_user' => 'User',
    'role_owner' => 'Owner',
    'register' => 'Register',
    'looking_to' => 'Looking to',
    'create_an_account' => 'create an account',
    'already_account' => 'Already have an account?',
    'forgot_password' => 'Forgot Your Password?',
    'reset_password' => 'Reset password',
    'confirm_password' => 'Confirm Password',
    'reset_link' => 'Send Password Reset Link',

    'verify_title' => 'Verify Your Email Address',
    'verify_alert' => 'A fresh verification link has been sent to your email address.',
    'verify_content' => 'Before proceeding, please check your email for a verification link.',
    'verify_info' => 'If you did not receive the email',
    'verify_link' => 'click here to request another',

    'email_exist' => 'user with your email address is already registered please log in with this name or change account',
    'email_not_exist' => 'Социальная сеть :provider Не предоставил нам ваш емайл адрес пожалуста исправьте это или войдите с другова акунта',
    'accountIsBlocked' => 'Your account is blocked for information <a href="">here</a> .'

];
