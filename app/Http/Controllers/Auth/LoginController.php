<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Traits\UploadeAvatar;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    Auth, Hash, Response
};
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    use UploadeAvatar;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    public $failedMessage;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
        $this->redirectTo = url()->previous();
    }

    /**
     **_ Redirect the user to the OAuth Provider.
     * _**
     **_ @return Response
    _**/
    public function redirectToProvider(Request $request, $provider)
    {
        session(['socialRedirect' => url()->previous()]);
        if ($provider == 'vkontakte') {
            return Socialite::with($provider)->redirect();
        } elseif ($provider == 'facebook') {
            return Socialite::with($provider)->fields([
                'first_name', 'user_gender', 'user_birthday', 'user_location'
            ])->scopes([
                'email'
            ])->redirect();
        }

//        return Socialite::driver($provider)->redirect();
    }

    /**
     **handleProviderCallback.
     * _**
     **_ @return Responsex`
     * _**/
    public function handleProviderCallback(Request $request, $provider)
    {

        $this->redirectTo = session('socialRedirect') ?: '/';
        $request->session()->forget('socialRedirect');
        if (!$request->has('code')) {
            return redirect($this->redirectTo);
        }

        if ($provider == 'vkontakte') {
            $user = Socialite::driver($provider)->fields([
                'id', 'email', 'first_name', 'last_name',
                'photo_max_orig', 'bdate', 'city',
                'country', 'contacts'
            ])->user();
        } elseif ($provider == 'facebook') {
            $user = Socialite::driver($provider)->fields([
                'first_name', 'last_name', 'email', 'gender', 'birthday', 'location'
            ])->user();
        }

        $authUser = User::where('provider_id', $user->getId())->first();
        if ($authUser && $authUser->block) {
            alert()->html('', __('auth.accountIsBlocked'))->showConfirmButton('ok', '#062947');
            return redirect($this->redirectTo);
        }
        if (!$authUser) { // первый  вход из соц сетей
            if (($provider == 'vkontakte' && !isset($user->accessTokenResponseBody['email']))
                || ($provider == 'facebook' && !$user->getEmail())) //емайл отсутствует
            {
                alert()->alert('', __('auth.email_not_exist', ['provider' => $provider]))->showConfirmButton('ok', '#062947');
                return redirect($this->redirectTo);

            } elseif (User::where('email', $user->getEmail() ?: $user->accessTokenResponseBody['email'])->first()) // если пользователь с этим мелом зарегистрирован
            {

                alert()->alert('', __('auth.email_exist'))->showConfirmButton('ok', '#062947');
                return redirect($this->redirectTo);
            }

            $authUser = $this->findOrCreateUserProvider($user, $provider);
        }

        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUserProvider($user, $provider)
    {
        if ($provider == 'vkontakte') {
            $first_name = isset($user->user['first_name']) ? $user->user['first_name'] : '';
            $last_name = isset($user->user['last_name']) ? $user->user['last_name'] : '';
            $email = isset($user->accessTokenResponseBody['email']) ? $user->accessTokenResponseBody['email'] : null;
            $country = isset($user->user['country']['title']) ? $user->user['country']['title'] : '';
            $city = isset($user->user['city']['title']) ? $user->user['city']['title'] : '';
            $phone = isset($user->user['home_phone']) ? $user->user['home_phone'] : '';
            $birthday = isset($user->user['bdate']) ? $user->user['bdate'] : '';
            $avatar = isset($user->user['photo_max_orig']) ? $user->user['photo_max_orig'] : null;
            $avatar = $avatar ? $this->uploadFile($avatar, 'users', 100) : 'users/default.png';

        } elseif ($provider == 'facebook') {
            $first_name = isset($user->user['first_name']) ? $user->user['first_name'] : '';
            $last_name = isset($user->user['last_name']) ? $user->user['last_name'] : '';
            $email = $user->getEmail() ?: null;
            $country = isset($user->user['country']['title']) ? $user->user['country']['title'] : '';
            $city = isset($user->user['city']['title']) ? $user->user['city']['title'] : '';
            $phone = isset($user->user['home_phone']) ? $user->user['home_phone'] : '';
            $birthday = isset($user->user['birthday']) ? $user->user['birthday'] : '';
            $avatar = isset($user->avatar_original) ? $user->avatar_original : null;
            $avatar = $avatar ? $this->uploadFile($avatar, 'users', 100) : 'users/default.png';
        }


        $authUser = User::create([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'avatar' => $avatar,
            'email' => $email,
            'provider' => $provider,
            'provider_id' => $user->getId(),
            'number' => $phone,
            'birthday' => $birthday
        ]);

        $profile = new Profile();
        $profile->country = $country;
        $profile->city = $city;
        $profile->user_id = $authUser->id;
        $profile->save();

        return $authUser;
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name' => $user->name,
            'email' => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ($request->ajax()) {
            return response()->json([
                'intended' => $this->redirectPath(),
            ]);
        }
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
       if (! $this->checkUserDeleted($request)){
           return false;
       }
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    public function checkUserDeleted(Request $request)
    {
        $this->failedMessage = trans('auth.failed');

        $user = User::where('email', $request->email)->where('block', 1)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            $this->failedMessage = __('auth.accountIsBlocked');
            return false;
        }
        return true;

    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => $this->failedMessage,
        ]);
    }
}
