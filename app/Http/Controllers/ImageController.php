<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function getCacheImage(Request $request)
    {
        $image = $request->src;
        $src = str_replace('_-_', '/', $image);
        $width = $request->w > 0 ? $request->w : '100%';
        $height = $request->h > 0 ? $request->h : NULL;
        $quality = $request->q > 0 ? $request->q : NULL;
        $encode = $request->encode;


        $cacheimage = \Image::cache(function ($image) use ($src, $width, $height, $quality, $encode) {
            return $image->make($src)->resize("{$width}", "{$height}", function ($constraint) {
                $constraint->aspectRatio();
            })->encode(($encode == 'png' ? 'png' : 'jpeg'), $quality);
        });

        return response()->make($cacheimage, 200, array('Content-Type' => 'image/'.$encode))
            ->setMaxAge(604800)
            ->setPublic();
    }
}
