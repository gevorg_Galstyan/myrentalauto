<script>
    var userId = '{{auth()->id()?? true }}';
</script>

<script src="{{asset('js/owner.js')}}"></script>
<script src="{{asset('owner/js/vendors/jquery.steps.min.js')}}"></script>
<script src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="{{asset('js/fullcalendar/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/interaction/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/bootstrap/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/daygrid/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/timegrid/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/list/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/luxon/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/locales/ru.js')}}"></script>
<script src="{{asset('owner/plugins/input-mask/js/jquery.mask.min.js')}}"></script>
<script src="{{asset('js/fileinput/piexif.min.js')}}"></script>
<script src="{{asset('js/fileinput/purify.min.js')}}"></script>
<script src="{{asset('js/fileinput/sortable.min.js')}}"></script>
<script src="{{asset('js/fileinput/fileinput.min.js')}}"></script>
<script src="{{asset('js/fileinput/locales/ru.js')}}"></script>
<script src="{{asset('js/fileinput/themes/fas/theme.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
{{--exif js--}}
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>

<script>
    @if (session('status'))
    myAlert('{{session('status')}}', '{{session('alert_type') ?? 'success'}}');
    @endif
    function myAlert(message, type) {

        switch(type) {
            case 'success':
                toastr.success(message,'', {timeOut: 5000});
                break;
            case 'warning':
                toastr.warning(message,'', {timeOut: 5000});
                break;
            case 'error':
                toastr.error(message,'', {timeOut: 5000});
                break;
            default:
                toastr.info(message,'', {timeOut: 5000});
        }

        // const Toast = Swal.mixin({
        //     toast: true,
        //     position: 'top-right',
        //     showConfirmButton: false,
        //     timer: 7000
        // });
        //
        // Toast.fire({
        //     type: type,
        //     title: message
        // })

    }

</script>
