<?php

namespace App\Services;


use Illuminate\Support\Facades\URL;

class MpiService
{
    public static function sendPayment($order, $type){
        if ($type == 'full'){
            $userName = config('app.payment_username_full_pay');
            $password = config('app.payment_password_full_pay');
            $desc = 'car_full_pay_desc';
            $start = $order->parent->dates->start->format('d:m:Y%20H:i');
            $end = $order->parent->dates->end->format('d:m:Y%20H:i');
        }else{
            $userName = config('app.payment_username_reservation');
            $password = config('app.payment_password_reservation');
            $desc = 'car_book_desc';
            $start = $order->dates->start->format('d:m:Y%20H:i');
            $end = $order->dates->end->format('d:m:Y%20H:i');
        }

        $desc = __('car.'.$desc,['car_title' => $order->car->make->name.'%20'.$order->car->model->name, 'start' => $start, 'end' => $end]);

       $returnUrl = URL::signedRoute(
            'payment.response', ['status' => 'success']
        );
       $failUrl = URL::signedRoute(
            'payment.response', ['status' => 'failed']
        );
        $req = config('app.payment_url').'register.do?amount='.$order->origin_amount.'00&currency=933&language='.\LaravelLocalization::getCurrentLocale().'&orderNumber='.$order->barcode_number.'&returnUrl='.$returnUrl.'&failUrl='.$failUrl.'&userName='.$userName.'&password='.$password.'&description='.$desc.'&clientId='.$order->customer_id;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $req);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSLCERT,public_path(config('app.payment_sslcert')));
        curl_setopt($curl, CURLOPT_SSLKEY,public_path(config('app.payment_sslkey')));
        curl_setopt($curl, CURLOPT_SSLKEYPASSWD,config('app.payment_sslpassword'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    public static function getOrderStatus($order, $type){

        if ($type == 'full'){
            $userName = config('app.payment_username_full_pay');
            $password = config('app.payment_password_full_pay');
        }else{
            $userName = config('app.payment_username_reservation');
            $password = config('app.payment_password_reservation');
        }

        $req = config('app.payment_url').'getOrderStatus.do?language='.\LaravelLocalization::getCurrentLocale().'&orderId='.$order->transaction_id.'&userName='.$userName.'&password='.$password;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $req);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSLCERT,public_path(config('app.payment_sslcert')));
        curl_setopt($curl, CURLOPT_SSLKEY,public_path(config('app.payment_sslkey')));
        curl_setopt($curl, CURLOPT_SSLKEYPASSWD,config('app.payment_sslpassword'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;

    }
}
