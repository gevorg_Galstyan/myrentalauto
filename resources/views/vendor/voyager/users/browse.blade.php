@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary btn-add-new">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle"
                       data-width="200"
                       data-on="{{ __('voyager::bread.soft_deletes_off') }}"
                       data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach(Voyager::actions() as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        {{--<div class="actions-descInfo p-2" style="max-width: {{count(Voyager::actions()) * 130}}px">--}}
        {{--<h4 class="d-block text-center">Действие кнопок</h4>--}}
        {{--<div class="d-flex ">--}}

        {{--@foreach(Voyager::actions() as $action)--}}
        {{--@if (!method_exists($action, 'massAction'))--}}
        {{--@include('voyager::bread.partials.actions-desc', ['action' => $action])--}}
        {{--@endif--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <select id="search_key" name="key">
                                        @foreach($searchable as $key)
                                            <option
                                                value="{{ $key }}" @if($search->key == $key || $key == $defaultSearchKey){{ 'selected' }}@endif>
                                                {{ ucwords(str_replace('_', ' ', $key)) }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <select id="filter" name="filter">
                                        <option
                                            value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>
                                            contains
                                        </option>
                                        <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>
                                            =
                                        </option>
                                    </select>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control"
                                               placeholder="{{ __('voyager::generic.search') }}" name="s"
                                               value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                @if (Request::has('sort_order') && Request::has('order_by'))
                                    <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
                                    <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
                                @endif
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    @can('delete',app($dataType->model_name))
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                    @endcan
                                    @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}">
                                                    @endif
                                                    {{ $row->display_name }}
                                                    @if ($isServerSide)
                                                        @if ($row->isCurrentSortField($orderBy))
                                                            @if ($sortOrder == 'asc')
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            @else
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            @endif
                                                        @endif
                                                </a>
                                            @endif
                                        </th>
                                    @endforeach
                                    <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dataTypeContent as $data)
                                    {{--@if($data->id == auth()->id()) @continue @endif--}}
                                    @can('show_admin', $data)
                                        <tr>
                                            @can('delete',app($dataType->model_name))
                                                <td>
                                                    <input type="checkbox" name="row_id"
                                                           id="checkbox_{{ $data->getKey() }}"
                                                           value="{{ $data->getKey() }}">
                                                </td>
                                            @endcan
                                            @foreach($dataType->browseRows as $row)

                                                @php
                                                    if ($data->{$row->field.'_browse'}) {
                                                        $data->{$row->field} = $data->{$row->field.'_browse'};
                                                    }
                                                @endphp
                                                <td class="text-center">
                                                    @if (isset($row->details->view))
                                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $data->{$row->field}, 'action' => 'browse'])
                                                    @elseif($row->type == 'image')
                                                        <img
                                                            src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif"
                                                            style="width:100px">
                                                    @elseif($row->type == 'relationship')
                                                        @include('voyager::formfields.relationship', ['view' => 'browse','options' => $row->details])
                                                    @elseif($row->type == 'select_multiple')
                                                        @if(property_exists($row->details, 'relationship'))

                                                            @foreach($data->{$row->field} as $item)
                                                                {{ $item->{$row->field} }}
                                                            @endforeach

                                                        @elseif(property_exists($row->details, 'options'))
                                                            @if (!empty(json_decode($data->{$row->field})))
                                                                @foreach(json_decode($data->{$row->field}) as $item)
                                                                    @if (@$row->details->options->{$item})
                                                                        {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                {{ __('voyager::generic.none') }}
                                                            @endif
                                                        @endif

                                                    @elseif($row->type == 'multiple_checkbox' && property_exists($row->details, 'options'))
                                                        @if (@count(json_decode($data->{$row->field})) > 0)
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif

                                                    @elseif(($row->type == 'select_dropdown' || $row->type == 'radio_btn') && property_exists($row->details, 'options'))
                                                        @if ($row->field == 'provider')
                                                            @if ($data->{$row->field} == 'vkontakte')
                                                                <span class="label label-info">
                                                                 <i class="fab fa-vk"></i>
                                                                    {{ $data->{$row->field} }}
                                                            </span>
                                                            @elseif ($data->{$row->field} == 'facebook')
                                                                <span class="label label-primary">
                                                                <i class="fab fa-facebook-f"></i>
                                                                    {{ $data->{$row->field} }}
                                                            </span>
                                                            @else
                                                                <span class="label label-success">
                                                                 прямая
                                                            </span>
                                                            @endif
                                                            @if (($data->role_id == 2 && $data->profile &&  $data->profile->status == 'confirm') || ($data->role_id == 3) && $data->profile && $data->profile->checked == 1)
                                                                <span class=" label info-icon text-success">
                                                                        <i class="far fa-check-circle"></i>
                                                                    </span>
                                                            @else
                                                                <span class="label info-icon text-warning">
                                                                        <i class="fas fa-exclamation-triangle"></i>
                                                                    </span>
                                                            @endif
                                                        @else
                                                            {!! $row->details->options->{$data->{$row->field}} ?? '' !!}
                                                        @endif
                                                    @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                        @if ($row->field == 'deleted_at')
                                                            @if ( $data->{$row->field})
                                                                <span class="label label-danger">Удален</span>
                                                            @else
                                                                <span class="label label-success">Открытый</span>
                                                            @endif

                                                        @else
                                                            {{ property_exists($row->details, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) : $data->{$row->field} }}
                                                        @endif

                                                    @elseif($row->type == 'checkbox')
                                                        @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                            @if($data->{$row->field})
                                                                <span
                                                                    class="label label-info">{{ $row->details->on }}</span>
                                                            @else
                                                                <span
                                                                    class="label label-primary">{{ $row->details->off }}</span>
                                                            @endif
                                                        @else
                                                            {{ $data->{$row->field} }}
                                                        @endif
                                                    @elseif($row->type == 'color')
                                                        <span class="badge badge-lg"
                                                              style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                    @elseif($row->type == 'text')
                                                        @if ($row->field == 'percent_to_total' && $data->role_id == 2)

                                                            - - -
                                                        @else
                                                            @include('voyager::multilingual.input-hidden-bread-browse')
                                                            <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                        @endif
                                                    @elseif($row->type == 'text_area')
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                    @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        @if(json_decode($data->{$row->field}))
                                                            @foreach(json_decode($data->{$row->field}) as $file)
                                                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
                                                                   target="_blank">
                                                                    {{ $file->original_name ?: '' }}
                                                                </a>
                                                                <br/>
                                                            @endforeach
                                                        @else
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}"
                                                               target="_blank">
                                                                Download
                                                            </a>
                                                        @endif
                                                    @elseif($row->type == 'rich_text_box')
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <div>{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                    @elseif($row->type == 'coordinates')
                                                        @include('voyager::partials.coordinates-static-image')
                                                    @elseif($row->type == 'multiple_images')
                                                        @php $images = json_decode($data->{$row->field}); @endphp
                                                        @if($images)
                                                            @php $images = array_slice($images, 0, 3); @endphp
                                                            @foreach($images as $image)
                                                                <img
                                                                    src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif"
                                                                    style="width:50px">
                                                            @endforeach
                                                        @endif
                                                    @elseif($row->type == 'media_picker')
                                                        @php
                                                            if (is_array($data->{$row->field})) {
                                                                $files = $data->{$row->field};
                                                            } else {
                                                                $files = json_decode($data->{$row->field});
                                                            }
                                                        @endphp
                                                        @if ($files)
                                                            @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                                @foreach (array_slice($files, 0, 3) as $file)
                                                                    <img
                                                                        src="@if( !filter_var($file, FILTER_VALIDATE_URL)){{ Voyager::image( $file ) }}@else{{ $file }}@endif"
                                                                        style="width:50px">
                                                                @endforeach
                                                            @else
                                                                <ul>
                                                                    @foreach (array_slice($files, 0, 3) as $file)
                                                                        <li>{{ $file }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                            @if (count($files) > 3)
                                                                {{ __('voyager::media.files_more', ['count' => (count($files) - 3)]) }}
                                                            @endif
                                                        @elseif (is_array($files) && count($files) == 0)
                                                            {{ trans_choice('voyager::media.files', 0) }}
                                                        @elseif ($data->{$row->field} != '')
                                                            @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                                <img
                                                                    src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif"
                                                                    style="width:50px">
                                                            @else
                                                                {{ $data->{$row->field} }}
                                                            @endif
                                                        @else
                                                            {{ trans_choice('voyager::media.files', 0) }}
                                                        @endif
                                                    @else
                                                        @include('voyager::multilingual.input-hidden-bread-browse')
                                                        <span>{{ $data->{$row->field} }}</span>
                                                    @endif
                                                    @if ($row->field == 'email')
                                                        <button class="" title="Отправит письмо "
                                                                onclick="openModalSendEmail('{{ $data->{$row->field} }}')">
                                                            <i class="fas fa-envelope-square fa-2x"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            @endforeach
                                            <td class="no-sort no-click" id="bread-actions">
                                                @foreach(Voyager::actions() as $action)
                                                    @if (!method_exists($action, 'massAction'))
                                                        @include('voyager::bread.partials.actions', ['action' => $action])
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endcan
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder,
                                    'showSoftDeleted' => $showSoftDeleted,
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i
                            class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}
                        ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
               onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="mail-files">
        {{ csrf_field() }}
    </form>
@stop

@section('css')
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
    @endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        $(document).ready(function () {
                @if (!$dataType->server_side)
            var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => $orderColumn,
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
            $('#search-input select').select2({
                minimumResultsForSearch: Infinity
            });
            @endif

            @if ($isModelTranslatable)
            $('.side-body').multilingual();
            //Reinitialise the multilingual features when they change tab
            $('#dataTable').on('draw.dt', function () {
                $('.side-body').data('multilingual').init();
            })
            @endif
            $('.select_all').on('click', function (e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        @if($usesSoftDeletes)
        @php
            $params = [
                's' => $search->value,
                'filter' => $search->filter,
                'key' => $search->key,
                'order_by' => $orderBy,
                'sort_order' => $sortOrder,
            ];
        @endphp
        $(function () {
            $('#show_soft_deletes').parent().css("width", "200px");
            $('#show_soft_deletes').change(function () {
                if ($(this).prop('checked')) {
                    $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                } else {
                    $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                }

                $('#redir')[0].click();
            })
        });

        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });

        function openModalSendEmail(email) {
            let time = new Date().getTime();
            Swal.fire({
                title: 'Отправить письмо',
                html: `<form class="send-data">
                          <div class="form-group">
                            <label>От кого :</label>
                            <input type="email" class="form-control send-data-from" name="from" value="admin@myrentauto.com">
                          </div>
                          <div class="form-group">
                            <label>Тема :</label>
                            <input type="text" class="form-control send-data-subject" name="subject">
                          </div>
                          <div class="form-group">
                            <label>Содержание :</label>
                            <textarea  class="form-control  send-data-body" id="${time}" name="body"></textarea>
                          </div>
                        </form>`,
                confirmButton: true,
                confirmButtonText: 'Отправить',
                showCancelButton: true,
                width: '50%',
                cancelButtonText: 'Отменить',
                showLoaderOnConfirm: true,
                preConfirm: async () => {
                    let formData = new FormData();
                    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                    formData.append('to', email);
                    formData.append('from', $('.send-data-from').val());
                    formData.append('subject', $('.send-data-subject').val());
                    formData.append('body', tinyMCE.editors[time].getContent());
                    return await fetch('{{route('admin.send_user_email')}}', {
                        method: 'POST',
                        body: formData
                    }).then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((data) => {
                if (data.value) {
                    Swal.fire({
                        text: data.value.status,
                    })
                }
            });

            tinymce_setup_callback(`#${time}`);
        }

        function tinymce_setup_callback(editor) {
            // editor.remove();
            // editor = null;
            tinymce.init({
                selector: editor,
                // valid_elements : '#p',
                height: 550,
                language_url: '/admin/langs/ru.js',
                language: 'ru',
                setup: (editor) => {
                    editor.ui.registry.addButton('left', {
                        text: 'с лева ',
                        onAction: () => {
                            editor.focus();
                            editor.selection.setContent('<div class="row"><p class="col-md-6 ">Составьте текст или картинку</p><p class="col-md-6 ">Составьте текст или картинку</p></div>');
                        }
                    });
                    editor.ui.registry.addButton('right', {
                        text: 'с права ',
                        onAction: () => {
                            // editor.focus();
                            editor.insertContent('<p class="col-md-12">Составьте текст или картинку</p>');
                        }
                    });
                },
                plugins: 'print preview  searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  imagetools colorpicker textpattern help',
                extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
                file_picker_callback: function (callback, value, meta) {
                    if (meta.filetype == 'image') {
                        $('#upload_file').trigger('click')
                    }
                },
                toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link code template image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | left | right ',
                image_advtab: true,
                media_alt_source: false,
                media_dimensions: false,
                media_live_embeds: true,
                media_poster: false, // 1
                media_url_resolver: function (data, resolve, reject) {
                    console.log(data, resolve, reject);
                    let url = data.url;
                    let regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
                    let match = url.match(regExp);

                    if (match && match[2].length === 11) {
                        url = match[2]
                    }
                    let embedHtml = `<iframe width="100%"  height="200" src="//www.youtube.com/embed/${url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                    resolve({html: embedHtml});

                },
                templates: [
                    // {title: 'пример 1', description: 'Some desc 1', content: 'My content'},
                    {title: 'Фото слева', description: 'Фото слева', url: '/admin/templates/example-1'},
                    {title: 'Фото справа', description: 'Фото справа', url: '/admin/templates/example-2'}
                ],
                content_css: [
                    'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
                    'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
                    // '//www.tiny.cloud/css/codepen.min.css'
                ],
                content_style: [
                    'body{width: 100%;padding-right: 15px;padding-left: 15px; margin-right: auto;margin-left: auto; color: #504d4d !important;} .mce-object-iframe{width:100% !important}'
                ],
                visual_table_class: 'tiny-table',
                convert_urls: false,
                image_caption: true,
                image_title: true,
            });
        }

        $(document).on('submit', '#my_form', function (form) {
            form.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    $('.tox-control-wrap').find('input[type="text"]').val(response.path);
                }
            })
        });
    </script>
@stop
