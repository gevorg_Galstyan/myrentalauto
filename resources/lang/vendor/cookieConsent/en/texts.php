<?php

return [
    'message' => 'By using our website, you agree to the use of cookies, which will allow us to simplify your navigation, offer you content tailored to your interests, and run statistics to improve our website.',
    'agree' => 'Allow cookies',
];
