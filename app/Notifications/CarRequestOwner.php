<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CarRequestOwner extends Notification
{
    use Queueable;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
            ->greeting(__('notification.car-request-owner.title', [
                'name' => $notifiable->first_name,
            ]))
            ->line(__('notification.car-request-owner.text-1', [
                'car' => $this->order->car->title
            ]))
            ->line(__('notification.car-request-owner.text-2', ['link' => route('owner.orders', ['status' => 'requested', 'barcode_number' => $this->order->barcode_number])]))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'request',
            'car_slug' =>  $this->order->car->slug,
            'user' =>  [
                'name' =>  $this->order->customer->first_name,
                'id' =>  $this->order->customer->id,
            ],
            'title' => __('car.rent'),
            'message' => __('notification.car-request-owner.text-1', [
                'car' =>  $this->order->car->title ,
            ]),
            'url' => route('owner.orders', ['status' => 'requested', 'barcode_number' => $this->order->barcode_number]),
            'order_id' => $this->order->id,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => __('car.rent'),
            'message' => __('notification.car-request-owner.text-1', [
                'car' =>  $this->order->car->title,
            ]),
            'url' => route('owner.orders', ['status' => 'requested', 'barcode_number' => $this->order->barcode_number]),
        ]);
    }
}
