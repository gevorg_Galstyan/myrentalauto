@extends('layouts.app')
{{--meta tags--}}
@section('meta')
    <title>{{$post->translate()->seo_title}}</title>
    <meta name="keywords" content="{{$post->translate()->meta_keywords}}">
    <meta name="description" content="{{$post->translate()->meta_description}}">

    <meta property="og:locale"
          content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$post->translate()->seo_title}}"/>
    <meta property="og:description" content="{{$post->translate()->meta_description}}"/>
    <meta property="og:image" content="{{Voyager::image($post->image)}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name"
          content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site"
          content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title" content="{{$post->translate()->seo_title}}">
    <meta name="twitter:description" content="{{$post->translate()->meta_description}}">
    <meta name="twitter:image" content="{{Voyager::image($post->image)}}">
{{--    <link href="{{request()->url()}}" rel="canonical">--}}

@stop
{{--page title--}}
@section('title')
    {{strip_tags($post->translate()->seo_title)}}
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
@stop

@section('content')
    <!-- Page Content -->
    <div class="blog-search-container" style=" background-image: url({{Voyager::image($post->image)}});">

        {{--        <div class="row col-3  d-inline-block">--}}
        <div class="slider-logo position-absolute mt-3 ">
            <span class="own-blue">My</span><span class="own-orange">Rent</span><span class="own-blue">Auto</span>
        </div>
        {{--        </div>--}}

        <h1 class="py-5 px-sm-5 px-0 text-center fw-700 post-title" style="color: {{$post->title_color}};">
            {!! $post->translate()->title !!}
        </h1>
    </div>
    <div class="posts ">
        <div class="my-container bg-white border-radius-20 p-3 p-md-5">
            <div class="row">
                <!-- Post Content Column -->
                {{--                <div class="col-lg-4 order-lg-1 order-md-0">--}}
                {{--                    @include('posts.right-bar', ['tags' => $post->tags])--}}
                {{--                </div>--}}

                <div class="container card border-0 ">
                    <div class="row justify-content-center justify-content-md-between align-items-center">
                        <div class="col-md-6 author d-flex order-1 order-md-0 order-sm-1">
                            <div class="avatar mr-3"
                                 style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                            </div>
                            <div>
                                <a href="#0"
                                   class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</a>
                                <small class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                                @if ($post->updated_at->addDay(1)->ne($post->created_at))
                                    <p>{{__('post.updated')}} : {{$post->updated_at->format('Y-m-d')}}</p>
                                @else
                                    <p>{{__('post.updated')}} : {{$post->created_at->format('Y-m-d')}}</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 order-0 order-md-1 mb-3 mb-md-0 mb-sm-3 order-sm-0">
                            <form action="{{route('posts')}}" method="get" class="input-group " role="search"
                                  id="search-form">
                                <div class="input-group">
                                    <input type="text"
                                           class="form-control brd-orange bw-2 py-4 border-radius-left-5 faq-search"
                                           name="search"
                                           placeholder="{{__('post.search')}}.." aria-label="find answer"
                                           aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn own-btn border-radius-0 py-2 px-3 border-radius-right-5 ora "
                                                type="submit"
                                                id="button-addon2"><i class="fas fa-search fs-20"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <hr>

                    <div class="ff-ms">
                        {!! $post->translate()->body !!}
                    </div>
                    <hr>
                    <div class=" border-0  mb-5">
                        <div class="list-group list-group-flush">
                            <div class="">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-unstyled mb-0 d-flex flex-wrap">
                                            @if(isset($selected_tag) && $selected_tag)
                                                <li class="tag ">
                                                    <a href="{{route('posts', ['slug' => $selected_tag->slug])}}"
                                                       class="badge badge-pill badge-secondary p-2 selected-tag clear-tag">
                                                        {{$selected_tag->name}}
                                                        <span class="clear-tag"><i
                                                                class="fas fa-times-circle"></i></span>
                                                    </a>
                                                </li>
                                            @endif
                                            @foreach($post->tags as $tag)
                                                <li class="tag ">
                                                    <a href="{{route('posts', ['slug' => $tag->slug])}}"
                                                       class="badge  badge-pill badge-secondary p-2 ">{{$tag->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <hr>

                    <!-- Comments  -->
                    <div class="comments mb-5">
                        @include('vendor.comments.components.comments', ['model' => $post])
{{--                        @comments(['model' => $post])--}}
{{--                        @endcomments--}}
                    </div>
                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
@endsection
