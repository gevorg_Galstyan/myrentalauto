@if (request()->url() == route('home'))
    <div class="d-flex">
        <div class="js-cookie-consent cookie-consent fs-14  ff-ms col-12 col-md-7 m-0 m-md-4">
            <div
                class="own-container d-flex justify-content-md-between justify-content-sm-center justify-content-center align-items-center flex-wrap flex-md-nowrap flex-sm-wrap">
                <div class=" text-left px-0 py-2">
             <span class="cookie-consent__message">
                {!! trans('cookieConsent.message') !!}
             </span>
                </div>
                <div class=" text-center pb-2 pb-md-0">
                    <button class="js-cookie-consent-agree cookie-consent__agree ">
                        {{ trans('cookieConsent.agree') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif

