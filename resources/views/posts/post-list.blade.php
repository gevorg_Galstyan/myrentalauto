@forelse($posts as $post)
    <div class="card border-0 shadow mb-5 border-radius-20">
        <div class="card-body p-3">
            <div class="row">

                <div class="col-md-4 d-flex align-items-center justify-content-center flex-column">
                    <img class="img-fluid w-100 border-radius-20" src="{{asset("storage/$post->image")}}" alt="{{$post->translate()->title}}">
                </div>

                <div class="col-12 col-md-8">
                    <div class="row  mt-3 ">
                        <div class="col-12">
                            <h4 class="card-title">
                                <a href="{{route('post.single', ['slug' => $post->slug])}}">{!! strip_tags($post->translate()->title) !!}</a>
                            </h4>
                            @if ($post->updated_at->diffInHours($post->created_at) > 24)
                                <div class="small text-muted mb-3"><span>{{__('post.updated')}}:</span><span
                                        class="pl-2">{{$post->updated_at->format('Y-m-d')}}</span></div>
                            @else
                                <div class="small text-muted mb-3">{{$post->created_at->format('Y-m-d')}}</div>
                            @endif
                        </div>

                        <div class="author d-flex  col-12 mb-3">
                            <div class="avatar mr-3"
                                 style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                            </div>
                            <div>
                                <span
                                    class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</span>
                                <small class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12 ff-ms">
                            <p class="mb-4">
                                {{$post->translate()->excerpt}}
                                <a href="{{route('post.single', ['slug' => $post->slug])}}"
                                   class="d-inline-block read-post">
                                    {{__('post.read_article')}} →
                                </a>
                            </p>
                        </div>

                        <div class="col-12 text-center">
                            <ul class="list-unstyled mb-0 d-flex flex-wrap">
                                @if(isset($selected_tag) && $selected_tag)
                                    <li class="tag ">
                                        <a href="{{route('posts', ['slug' => $selected_tag->slug])}}"
                                           class="badge badge-secondary selected-tag">
                                            {{$selected_tag->name}}
                                        </a>
                                    </li>
                                @endif
                                @foreach($post->tags->where('id', '!=', (isset($selected_tag) && $selected_tag? $selected_tag->id:''))->sortByDesc('rating')->slice(0, 4) as $tag)
                                    <li class="tag ">
                                        <a href="{{route('posts', ['slug' => $tag->slug])}}"
                                           class="badge badge-secondary  ">{{$tag->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="post-paginate">
        <!-- Pagination -->
        {{$posts->links()}}

    </div>
    @empty
    <div class="w-100">
        <p class="text-center w-100">{{__('post.no_search_result')}}</p>
        <hr>
        @foreach($otherPosts as $post)
            <p class="text-center w-100">{{__('post.can_read')}}</p>
            <div class="card border-0 shadow mb-5 border-radius-20">
                <div class="card-body p-3">
                    <div class="row">

                        <div class="col-md-4 d-flex align-items-center justify-content-center flex-column">
                            <img class="img-fluid w-100 border-radius-20" src="{{asset("storage/$post->image")}}" alt="{{$post->translate()->title}}">
                        </div>

                        <div class="col-12 col-md-8">
                            <div class="row  mt-3 ">
                                <div class="col-12">
                                    <h4 class="card-title">
                                        <a href="{{route('post.single', ['slug' => $post->slug])}}">{!! strip_tags($post->translate()->title) !!}</a>
                                    </h4>
                                    @if ($post->updated_at->diffInHours($post->created_at) > 24)
                                        <div class="small text-muted mb-3"><span>{{__('post.updated')}}:</span><span
                                                class="pl-2">{{$post->updated_at->format('Y-m-d')}}</span></div>
                                    @else
                                        <div class="small text-muted mb-3">{{$post->created_at->format('Y-m-d')}}</div>
                                    @endif
                                </div>

                                <div class="author d-flex  col-12 mb-3">
                                    <div class="avatar mr-3"
                                         style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                                    </div>
                                    <div>
                                <span
                                    class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</span>
                                        <small class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-12 ff-ms">
                                    <p class="mb-4">
                                        {{$post->translate()->excerpt}}
                                        <a href="{{route('post.single', ['slug' => $post->slug])}}"
                                           class="d-inline-block read-post">
                                            {{__('post.read_article')}} →
                                        </a>
                                    </p>
                                </div>

                                <div class="col-12 text-center">
                                    <ul class="list-unstyled mb-0 d-flex flex-wrap">
                                        @if(isset($selected_tag) && $selected_tag)
                                            <li class="tag ">
                                                <a href="{{route('posts', ['slug' => $selected_tag->slug])}}"
                                                   class="badge badge-secondary selected-tag">
                                                    {{$selected_tag->name}}
                                                </a>
                                            </li>
                                        @endif
                                        @foreach($post->tags->where('id', '!=', (isset($selected_tag) && $selected_tag? $selected_tag->id:''))->sortByDesc('rating')->slice(0, 4) as $tag)
                                            <li class="tag ">
                                                <a href="{{route('posts', ['slug' => $tag->slug])}}"
                                                   class="badge badge-secondary  ">{{$tag->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
    </div>
@endforelse




