<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\App;
use MenaraSolutions\Geographer\{
    Country, Earth
};

class Profile extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $data = [
            'birthday' => 'required|date|before:13 years ago',
            'experience' => 'required',
            'citizenship' => 'required',
            'country' => 'required',
            'city' => 'required',
//            'phone' => 'required|unique:users,number,'.auth()->id(),
            'phone' => 'phone:AUTO|required|unique:users,number,'.auth()->id(),
        ];
        $code = $this->request->get('country');
        if ($code){
            $country = Country::build($code);
            $states = $country->getStates()->setLocale(App::getLocale())->toArray();
            if(count($states) > 0) {
                $data['region'] =  'required';
            }
        }else{
            $data['region'] =  'required';
        }

        $data['last_name'] =  'required|string|max:255';
        $data['first_name'] =  'required|string|max:255';
        if (!auth()->user()->provider){

            $data['email'] =  'required|string|email|max:255|unique:users,email,'.auth()->id();
        }
        if ($this->request->get('terms')){
            $data['terms_1'] = 'required';
            $data['terms_2'] = 'required';
        }

        return $data;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->messages()], 200));
    }
}
