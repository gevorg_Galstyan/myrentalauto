<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check if is admin
        if (\Auth::user() && \Auth::user()->role_id != 1) {
            return abort(403);
        }
        return $next($request);

    }
}
