<?php

namespace App\Http\Middleware;

use Closure;

class isUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check if is admin
        if (\Auth::user() && \Auth::user()->role_id != 2) {
            return redirect()->back();
        }
        return $next($request);
    }
}
