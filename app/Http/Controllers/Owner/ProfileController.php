<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Password;
use App\Notifications\OwnerChangeProfile;
use App\Models\{
    Car, Profile
};
use App\Notifications\ProfileCheck;
use App\Traits\UploadeAvatar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    App, Auth, File, Hash, Notification, Validator
};
use MenaraSolutions\Geographer\{
    Country, Earth
};

class ProfileController extends Controller
{
    use UploadeAvatar;

    public function edit()
    {
        $earth = new Earth();
        $countries = $earth->getCountries()->setLocale(App::getLocale())->toArray();
        return view('owners.profile.edit', compact('countries'));
    }

    public function update(\App\Http\Requests\OwnerProfile $request)
    {
        $check_email = false;
        $user = auth()->user();
        if (!auth()->user()->provider) {
            $user->last_name = $request->last_name;
            $user->first_name = $request->first_name;
            if ($request->email != $user->email) {
                $user->email = $request->email;
                $user->email_verified_at = null;
                $check_email = true;
                $user->sendEmailVerificationNotification();
            }
        }
        $user->number = $request->number;
        $user->birthday = $request->birthday;
        $user->about_me = $request->about_me;
        $user->save();


        $profile = auth()->user()->profile;
        $profile->company = $request->company;
        $profile->registration_number = $request->registration_number;
        $profile->legal_address = $request->legal_address;
        $profile->physical_address = $request->physical_address;
        $profile->unp = $request->unp;
        $profile->owner = $request->owner;
        $profile->assigned = $request->assigned;
        $profile->cont_tel = $request->cont_tel;
        $profile->cont_mail = $request->cont_mail;
        $profile->bank = $request->bank;
        $profile->bank_reg_number = $request->bank_reg_number;
        $profile->bank_account_number = $request->bank_account_number;
        $profile->checked = 0;
        $profile->save();

        $users = User::where('role_id', 1)->get();
        Notification::send($users, new OwnerChangeProfile($profile));
        return back()->with('status', 'Профиль успешно обновлен');

    }

    public function ajaxUpload(Request $request)
    {
        if ($request->type == 'avatar') {
            $old_avatar = storage_path('app/public/' . auth()->user()->avatar);
            $filename = $this->uploadFile($request->img, 'users', 100);
            $user = auth()->user();
            $user->avatar = $filename;
            $user->save();
            if (File::exists($old_avatar)) {
                File::delete($old_avatar);
            }
            return asset('storage/' . $filename);
        }
        if ($request->type == 'logo') {
            $old_logo = storage_path('app/public/' . auth()->user()->profile->logo);
            $filename = $this->uploadFile($request->img, 'profile', 100);
            $userProfile = auth()->user()->profile;
            $userProfile->logo = $filename;
            $userProfile->save();
            if (File::exists($old_logo)) {
                File::delete($old_logo);
            }
            return asset('storage/' . $filename);
        }
        $filename = $this->uploadFile($request->img, 'profile', 75);
        $type = $request->type;
        $profile = auth()->user()->profile;

        if ($profile) {
            $old_image = storage_path('app/public/' . auth()->user()->profile()->pluck($type)[0]);
            $profile->update([
                $type => $filename,
                'status' => 'not_verified',
            ]);
            $profile->save();
            if (File::exists($old_image)) {
                File::delete($old_image);
            }
            return asset('storage/' . $filename);
        }

        Profile::create([
            $type => $filename,
            'user_id' => auth()->user()->id,
        ]);

        return asset('storage/' . $filename);
    }


}
