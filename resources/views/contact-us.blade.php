@extends('layouts.app')

@section('meta')
    <title>{{getMetaTags('contact')->translate()->title}}</title>
    <meta name="keywords" content="{{getMetaTags('contact')->translate()->Keyword}}"/>
    <meta name="description" content="{{getMetaTags('contact')->translate()->description}}"/>
@stop

@section('content')
    <div class="own-container  my-5 p-3 p-md-5 bg-white border-radius-20">
        <div class="row">
            <div class="col-lg-4 order-lg-0 order-1">
                <div id="contact-map" class="w-100 h-100"></div>
            </div>
            <div class="col-lg-8 order-lg-1 order-0">
                <h1 class="text-center fw-700 mb-5 sub-heading text-uppercase">{{getMetaTags('contact')->translate()->h1}}</h1>
                <p class="ff-ms">
                    @lang('contact.desc')
                </p>
                <div class="row">
                    <div class="col-md-4 d-flex">
                        <div>
                            <span class="own-blue"><i class="far fa-envelope-open"></i></span>
                        </div>
                        <div class="pl-3">
                            <div>
                                <span class="own-blue fw-700">@lang('contact.email')</span>
                            </div>
                            <div>
                                <span>
                                    <a href="mailto:info@myrentauto.com?Subject=Hello%20again" target="_top">info@myrentauto.com</a>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8 d-flex">
                        <div>
                            <span class="own-blue"><i class="fas fa-share-alt"></i></span>
                        </div>
                        <div class="pl-3">
                            <div>
                                <span class="own-blue fw-700">@lang('contact.follow_us')</span>
                            </div>
                            <div>
                                <div class="row justify-content-center align-items-center">
                                    <div class="pl-3">
                                        <a href="https://www.facebook.com/MyRentAuto/" target="_blank">
                                            <img src="{{asset('storage/img/icons/facebook-icon-contact.png')}}" width="35" alt="facebook" class="rounded-circle" title="Facebook">
                                        </a>
                                    </div>
                                    <div class="pl-3">
                                        <a href="https://vk.com/myrentauto" target="_blank">
                                            <img src="{{asset('storage/img/icons/vk-icon-contact.png')}}" width="35" alt="Vkontakte" class="rounded-circle" title="Vkontakte">
                                        </a>
                                    </div>
                                    <div class="pl-3">
                                        <a href="https://www.instagram.com/myrentautocom/" target="_blank">
                                            <img src="{{asset('storage/img/icons/instagram-icon-contact.png')}}" width="35" alt="instagram" class="rounded-circle" title="Instagram">
                                        </a>
                                    </div>
                                    <div class="pl-3">
                                        <a href="https://telegram.me/MyRentAuto" target="_blank">
                                            <img src="{{asset('storage/img/icons/telegram-icon-contact.png')}}" width="35" alt="telegram" class="rounded-circle" title="Telegram">
                                        </a>
                                    </div>
                                    <div class="pl-3">
                                        <a href="https://twitter.com/myrentauto" target="_blank">
                                            <img src="{{asset('storage/img/icons/twitter-icon-contact.png')}}" width="35" alt="twitter" class="rounded-circle" title="Twitter">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-5">
                        <h5 class="own-blue fw-700">@lang('contact.contact_form')</h5>
                        <form class="row" action="{{route('contact_us.store')}}" method="POST">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('contact.your_first_name')</label>
                                    <input type="text" class="form-control" name="first_name"
                                           placeholder="@lang('contact.your_first_name')" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('contact.your_last_name')</label>
                                    <input type="text" class="form-control" name="last_name"
                                           placeholder="@lang('contact.your_last_name')" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>@lang('contact.your_email')</label>
                                    <input type="text" class="form-control" name="email" placeholder="@lang('contact.your_email')" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>@lang('contact.message')</label>
                                    <textarea class="form-control" rows="5" name="message"
                                              placeholder="@lang('contact.message')" required></textarea>
                                </div>
                            </div>
                           <div class="col-md-12 my-3"> <div id="contact_us_id"></div></div>
                            <div class="col-md-12">
                                <button type="submit" class="own-btn text-uppercase border-0 ora ">@lang('contact.send')</button>
                            </div>
                        </form>
                        {!!  GoogleReCaptchaV3::render(['contact_us_id'=>'contact_us']) !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    {!!  GoogleReCaptchaV3::init() !!}
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=195a01c1-11df-40b3-bf48-96004f1783eb&mode=debug"></script>
    <script>
        ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("contact-map", {
                center: [53.876794, 27.553263],
                zoom: 12,
                controls: []
            }, {
                searchControlProvider: 'yandex#search'
            });

            var center = [53.876794, 27.553263];
            var myPlacemark = new ymaps.Placemark(center, {
                iconCaption: 'My Rent Auto'
            }, {
                preset: 'islands#redDotIcon'
            });
            myMap.geoObjects.add(myPlacemark);

        }
    </script>
@stop
{{--@component('partials.schema')--}}
{{--    {!! Spatie\SchemaOrg\Schema::organization()--}}
{{--        ->name(config('app.name'))--}}
{{--        ->url(route('home'))--}}
{{--        ->logo(ownMakeImage(50 , 50 ,85,'img/logo.png', 'png'))--}}
{{--        ->contactPoint(Spatie\SchemaOrg\Schema::contactPoint()--}}
{{--            ->telephone('+375-44-575-999-1')--}}
{{--            ->email('info@myrentauto.com')--}}
{{--        )--}}
{{--        ->sameAs([--}}
{{--            'https://www.facebook.com/MyRentAuto/',--}}
{{--            'https://vk.com/myrentauto',--}}
{{--            'https://www.instagram.com/myrentautocom/',--}}
{{--            'https://telegram.me/MyRentAuto',--}}
{{--            'https://twitter.com/myrentauto'--}}
{{--        ])--}}
{{--        ->toScript() !!}--}}
{{--@endcomponent--}}
