@extends('owners.layouts.app')


@section('content')
    <div class="page-header">

    </div>

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">

                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable" id="orderTables">
                        <thead>
                        <tr>
                            {{--<th class="w-1">Id</th>--}}
                            <th>Арендатор</th>
                            <th>Дата создания </th>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th>Заметка</th>
                            <th>Просмотр</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    <a href="{{$order->customer ? route('customer', ['slug' => $order->customer->slug]) : '#0'}}"
                                       class="text-inherit">
                                        {{$order->customer->full_name??'------'}}
                                    </a>
                                </td>
                                <td>
                                    {{$order->created_at->format('d:m:Y H:i')}}
                                </td>
                                <td>
                                    {{$order->dates->start->format('d:m:Y H:i')}}
                                    -
                                    {{$order->dates->end->format('d:m:Y H:i')}}
                                </td>
                                <td>
                                    <a href="#" class="badge badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.'.$order->status)}}</a>
                                </td>
                                <td class="mw-50">
                                    {{$order->note??'нет'}}
                                </td>
                                <td>
                                    <a href="{{route('owner.orders', ['status' => $order->status, 'barcode_number' => $order->barcode_number])}}" class="icon text-center">
                                        <i class="fa fa-eye"></i></a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')

    <script>

        $('[data-toggle="tooltip"]').tooltip()
        $(document).ready(function () {
            $('.datatable').DataTable({
                responsive: true,
                "aaSorting": [],
                'columnDefs': [ {
                    'orderable': false,
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
                }
            });
        });


    </script>
@stop
