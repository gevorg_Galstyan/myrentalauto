@extends('owners.layouts.app')

@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp
@section('css')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@stop

@section('content')

@if ($dataTypeContent->error_status == 'error')
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"></button>
        {{$dataTypeContent->wrongFills()->orderBy('id', 'desc')->first()->text}}
    </div>
@endif


    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-center w-100">{{$dataTypeContent->getKey()?"Редактирование автомобиля ". $dataTypeContent->title: 'Добавление автомобиля'}}</h3>
                </div>
                <form role="form" class="form-edit-add " id="owners-form"
                      action="{{ $edit ? route('cars.update', $dataTypeContent->getKey()) : route('cars.store') }}"
                      method="POST" enctype="multipart/form-data">
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    {{ csrf_field() }}

                    {{-- STEP-1 --}}

                    @include('owners.cars.steps.step-1')

                    {{-- STEP-2 --}}

                    @include('owners.cars.steps.step-2')

                    {{-- STEP-3 --}}

                    @include('owners.cars.steps.step-3')

                    {{-- STEP-4 --}}

                    @include('owners.cars.steps.step-4')

                    {{-- STEP-5 --}}

                    {{--@include('owners.cars.steps.step-5')--}}

                </form>
            </div>
        </div>
    </div>

    <style>
        .error, .wizard > .content > .body label.error {
            width: 100%;
            margin-left: 0px;
        }
    </style>

@stop

@section('script')
    @include('owners.cars.coordinates')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>




    <script>

        // var rules_set = {};
        // $("#owners-form").find('input').not(':button, :submit, :reset, :hidden, :checkbox').each(function () {
        //
        //     var name = $(this).attr('name')
        //     rules_set[name] = 'required';
        // });
        //
        // $("#owners-form").validate({
        //     errorPlacement: function (error, element) { /*element.before(error);*/
        //     },
        //     rules: rules_set,
        // });
        // $("#owners-form").validate({
        //     errorPlacement: function (error, element) { /*element.before(error);*/
        //     }
        //
        // });
        //        $("input:file").hasClass( "error" )


        //validation rule messages
        $.extend($.validator.messages, {
            required: "Это поле необходимо заполнить.",
            remote: "Пожалуйста, введите правильное значение.",
            email: "Пожалуйста, введите корректный адрес электронной почты.",
            url: "Пожалуйста, введите корректный URL.",
            date: "Пожалуйста, введите корректную дату.",
            dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
            number: "Пожалуйста, введите число.",
            digits: "Пожалуйста, вводите только цифры.",
            creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
            equalTo: "Пожалуйста, введите такое же значение ещё раз.",
            extension: "Пожалуйста, выберите файл с правильным расширением.",
            maxlength: $.validator.format("Пожалуйста, введите не больше {0} символов."),
            minlength: $.validator.format("Пожалуйста, введите не меньше {0} символов."),
            rangelength: $.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."),
            range: $.validator.format("Пожалуйста, введите число от {0} до {1}."),
            max: $.validator.format("Пожалуйста, введите число, меньшее или равное {0}."),
            min: $.validator.format("Пожалуйста, введите число, большее или равное {0}.")
        });


        $("#owners-form").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex) {
                $("#owners-form").validate({
                    errorPlacement: function (error, element) { /*element.before(error);*/
                        if (element.hasClass('error') && element.attr('type') == 'file') {
                            element.next().css('border', '1px solid #fbc2c4')
                        } else {
//                            element.next().css('border', '1px solid rgba(0, 40, 100, 0.12)')
                        }
                        element.parent().append(error);
                    }
                });
                return $("#owners-form").valid();
            },
            onInit: function () {
                $('.serial-number').mask('0000 SS-0', {
                    translation: {
                        'S': {pattern: /[A-Z]/}
                    }
                });

                $('.motor').mask('0.0', {reverse: true});
                $('.fuel-consumption').mask('00.0', {reverse: true});
                $('.number-of-seats-trunk').mask('00000', {reverse: true});
                $('.mileage-limit').mask('000000', {reverse: true});
                $('.mileage-limit-for-every').mask('000', {reverse: true});
            },
            // onFinishing: function (event, currentIndex) {
            //
            // },
            onFinishing: function (event, currentIndex) {
                $("#owners-form").validate().settings.ignore = ":disabled";
                $("#owners-form").submit();
                return $("#owners-form").valid();
            },
            labels: {
                cancel: "отменить",
                current: "текущий шаг:",
                pagination: "пагинация",
                finish: "Сохранить",
                next: "следующий",
                previous: "предыдущий",
                loading: "загрузка ..."
            }
        });


        $(document).ready(function () {
            $('.select2').select2();

            var make_id;
            $('.select2-ajax-make').select2({
                placeholder: 'Mарка',
                width: '100%',
                ajax: {
                    url: "{{route('cars.create')}}",
                    dataType: 'json',
                    data: function (data) {
                        return {
                            search: data.term,
                        }
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                },
            }).on('change', (e) => {
                make_id = $('.select2-ajax-make').val();

                $('.select2-ajax-model').select2({
                    placeholder: 'Модел',
                    width: '100%',
                    ajax: {
                        url: "{{route('cars.create')}}",
                        dataType: 'json',
                        data: function (data) {

                            return {
                                search: data.term,
                                make_id: make_id
                            }
                        },
                        processResults: function (data) {
                            return {
                                results: data

                            };

                        },
                    },
                });

            });

            function formatColor(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span style="display:inline-block; width:15px; height:15px; background: ' + $(state.element).data('val') + ' !important;" class="rounded border border-secondary">' +
                    '</span><span> ' + state.text + '</span>'
                );
                return $state;
            };


            $('.select2.colors').select2({
                templateResult: formatColor,
                templateSelection: formatColor
            });
            $(document).on('change', '.change-insurances', function () {
                if ($(this).is(':checked')) {
                    $('.' + $(this).data('target')).attr('disabled', false).eq(0).focus();
                } else {
                    $('.' + $(this).data('target')).attr('disabled', true);

                }
            });
        });

        $(document).on('change', '.change-greenCard-opt', function () {
            var country_key = $(this).data('target');
            if ($(this).is(':checked')) {
                $('[data-status="' + country_key + '"]').prop('disabled', false)
            } else {
                $('[data-status="' + country_key + '"]').prop('disabled', true)
            }
        });
        $(document).on('change', '.change-price', function () {
            if ($(this).is(':checked')) {
                $($(this).data('target')).attr('disabled', false).focus();
                $($(this).data('target') + '-order').attr('disabled', false);
            } else {
                $($(this).data('target')).attr('disabled', true);
                $($(this).data('target') + '-order').attr('disabled', true);
            }
        });

        percent();
        $('.price, [name="price_1"]').keyup(function () {
            percent();
        });

        function percent() {
            $('.disabled-price').each(function (i) {
                let percent = $('.price').eq(i).val()
                let price = parseInt($('[name="price_1"]').val()) ? parseInt($('[name="price_1"]').val()) : 0;
                let this_price = price - (price * (percent / 100));
                $(this).text(parseInt(this_price) + ' (BYN)')
            });
        }


        let addButton = $('.add_button');
        let content = $('.delivery-content'); //Input field wrapper
        let fieldHTML = '<div class="d-flex flex-wrap removed">\n' +
            '                                        <div class="form-check col-2 m-2">\n' +
            '                                            <input type="text" class="form-control input-delivery_location" name="delivery_locations[]"\n' +
            '                                                   value=""\n' +
            '                                                   placeholder="Место "/>\n' +
            '                                        </div>\n' +
            '\n' +
            '                                        <div class="form-check col-2 m-2">\n' +
            '\n' +
            '                                            <input type="text" class="form-control" name="delivery_prices[]"\n' +
            '                                                   value=""\n' +
            '                                                   placeholder="Цена "/>\n' +
            '                                        </div>\n' +
            '\n' +
            '                                        <div class="form-check col-2 m-2">\n' +
            '                                            <a href="javascript:void(0);" class="remove_button" onmouseover="deliveryRemoveText(this)" data-toggle="tooltip" data-html="true" >\n' +
            '                                                <i class="fa fa-minus fa-2x"></i>\n' +
            '                                            </a>\n' +
            '                                        </div>\n' +
            '\n' +
            '                                        <div class="col-md-12 input-delivery_location_en"></div>' +
            '                                    </div>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function () {
            $(content).append(fieldHTML); //Add field html

        });

        function deliveryRemoveText(event) {
            let delivry = $(event).parents('.d-flex.removed').find('input').val();
            if (delivry) delivry = `( ${delivry} )`;
            else delivry = '';
            $(event).attr(`title`, `Удалить место доставки ${delivry}`)
        }

        //Once remove button is clicked
        $(content).on('click', '.remove_button', function (e) {
            e.preventDefault();
            $(this).parents('.d-flex.removed').remove();
        });

        $('#image-content input[type="file"]').change(function () {
            var total_file = $('#image-content input[type="file"]')[0].files.length;
            $('.preview').remove();
            for (var i = 0; i < total_file; i++) {
                var image = '<div class="img_settings_container preview"  style="float:left;padding-right:15px;"><span >Предварительно </span><img src="' + URL.createObjectURL(event.target.files[i]) + '"  style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;"></div>'
                if ($('.img_settings_container').length) {
                    $('.img_settings_container').last().after(image);
                } else {
                    $('#image-content').append(image)
                }

            }
        });


        $(document).ready(function () {
            var deposit = $('[name="deposit"]');
            var deposit_type = $('[name="deposit_type"]');
            var deposit_price = $('[name="deposit_price"]');


            deposit.change(function () {
                if ($(this).is(':checked')) {
                    deposit_type.prop('disabled', false);
                    deposit_price.prop('disabled', false);
                    $('#deposit_type, #deposit_price').removeClass("disabledbutton");
                } else {
                    deposit_type.prop('disabled', true);
                    deposit_price.prop('disabled', true);
                    $('#deposit_type, #deposit_price').addClass("disabledbutton");

                }
            });
            if (!deposit.is(':checked')) {
                deposit_type.prop('disabled', true);
                deposit_price.prop('disabled', true);
                $('#deposit_type, #deposit_price').addClass("disabledbutton");
            }

        })
    </script>


    {{--IMAGE PREVIEW--}}
    <script>
        var $uploadCrop, tempFilename, rawImg, imageId;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $('#cropImagePop').modal('show');
                    rawImg = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 400,
                height: 250,
            },
            // enforceBoundary: false,
            enableExif: true,

        });
        $('#cropImagePop').on('shown.bs.modal', function () {
            if (!$(this).find('.info-box').length) {
                $(this).find('.modal-body').append('' +
                    '<div class="info-box d-flex justify-content-center mt-2">' +
                    '<span>Если не входит измените размер фото </span>' +
                    '<span class="k-pointer ml-2" data-toggle="tooltip" data-html="true"\n' +
                    'data-placement="top"\n' +
                    'title="Левой кнопкой мыши - перемещайте фото для центровки изображения.">\n' +
                    '<i class="fas fa-info-circle"></i>\n' +
                    '</span>' +
                    '</div>')
            }
            $uploadCrop.croppie('bind', {
                url: rawImg
            }).then(function () {

            });
            $('[data-toggle="tooltip"]').tooltip();
        });
        $(document).on('change', '.item-img', function () {
            imageId = $(this).data('id');
            var ext = $(this).val().split('.').pop().toLowerCase();
            var error_message;
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                error_message = 'Запрещенное расширение Только "gif, png, jpg, jpeg" разрешены.';
            } else {
                var picsize = (this.files[0].size);
                if (picsize > 5242880) {
                    error_message = 'Файл превышает максимальный размер 5 МБ.';
                } else {
                    tempFilename = $(this).val();
                    $('#cancelCropBtn').data('id', imageId);
                    readFile(this);
                }
            }
            if (error_message) {
                $('#' + imageId).find('.error_content').html('' +
                    '<div class="alert alert-danger alert-dismissible fade in">\n' +
                    '    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                    error_message +
                    '  </div>');
            }


        });

        $('#cropImageBtn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'base64',
                quality: 0.75,
                format: 'jpeg',
                size: {width: 1000, height: 625}
            }).then(function (resp) {
                $('[data-status="' + imageId + '"]').attr('src', resp);
                $('#cropImagePop').modal('hide');
                $('.' + imageId).val(resp)
            });
        });
        // End upload preview image
        $('.file-image').fileinput({
            language: "ru",
            quality: 0.75,
            showUpload: false,
            maxFileSize: 5120,
            allowedFileExtensions: ['gif', 'png', 'jpg', 'jpeg'],
            dropZoneEnabled: false,
            showCaption: false,
            showCancel: false,
            showRemove: false,
            layoutTemplates: {footer: ''},
            initialPreview: [
                // IMAGE RAW MARKUP

                '<img src="@if( !filter_var($dataTypeContent->data_sheet_1, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->data_sheet_1 ) }}@else{{ $dataTypeContent->data_sheet_1 }}@endif" class="kv-preview-data file-preview-image">',

            ],

        });

        $('.file-image2').fileinput({
            language: "ru",
            quality: 0.75,
            showUpload: false,
            maxFileSize: 5120,
            allowedFileExtensions: ['gif', 'png', 'jpg', 'jpeg'],
            dropZoneEnabled: false,
            showCaption: false,
            showCancel: false,
            showRemove: false,
            removeIcon: ' ',
            layoutTemplates: {footer: ''},
            initialPreview: [
                // IMAGE RAW MARKUP
                '<img src="@if( !filter_var($dataTypeContent->data_sheet_1, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->data_sheet_2 ) }}@else{{ $dataTypeContent->data_sheet_2 }}@endif" class="kv-preview-data file-preview-image">',
            ],

        });

        $(document).on('blur', '.input-delivery_location', function () {
            var input = $(this);
            $.ajax({
                url: '/get-translate/y/' + input.val(),
                success: function (response) {
                    input.parents('.d-flex').find('.input-delivery_location_en').text(response.translate);
                }
            })
        });

        (function($){
            $.fn.datepicker.dates['ru'] = {
                days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
                daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
                daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                today: "Сегодня",
                clear: "Очистить",
                format: "dd.mm.yyyy",
                weekStart: 1,
                monthsTitle: 'Месяцы'
            };
        }(jQuery));
        $('.form-control.own-date').datepicker({
            format: "dd-mm-yyyy",
            language: "ru",
        });
    </script>
@stop
