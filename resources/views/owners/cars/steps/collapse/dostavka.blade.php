<div id="accordion-dostavka">
    <div class="card shadow-sm  bg-light  rounded">
        <div class="card-header" id="heading-dostavka">
            <h5 class="mb-0">
                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-dostavka"
                   aria-expanded="true" aria-controls="collapse-dostavka">
                    <i class="fa"></i>
                    Место доставки
                </a>
            </h5>
        </div>

        <div id="collapse-dostavka" class="collapse {{$dataTypeContent->deliveries()->count()?'show':''}}" aria-labelledby="heading-dostavka"
             data-parent="#accordion-dostavka">
            <div class="card-body">
                <div class="delivery-content">
                    <div class="d-flex removed">
                        <div class="form-check col-2 m-2">
                            <label>Место доставки
                                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                                title=" {{ tooltips($toolips, 'owner_help_delivery_place') }}">
                                <i class="fas fa-info-circle"></i>
                            </span></label>
                            <input type="text" id="{{'suggest-'.time()}}"
                                   class="form-control input-delivery_location"
                                   value="Местонахождение Автомобиля"
                                   placeholder="Место " disabled/>
                        </div>

                        <div class="form-check col-2 m-2">
                            <label>Цена доставки
                                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                      title=" {{ tooltips($toolips, 'owner_help_delivery_price') }}">
                                <i class="fas fa-info-circle"></i>
                            </span></label>
                            <input type="text" class="form-control"
                                   value="0"
                                   placeholder="Цена " disabled/>
                        </div>

                        <div class="form-check col-2 m-2"
                             title="Добавить место доставки">
                            <p>Действие
                                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                              title=" {{ tooltips($toolips, 'owner_help_delivery_add_delete') }}">
                                <i class="fas fa-info-circle"></i>
                            </span></p>
                            <a href="javascript:void(0);" class="add_button">
                                <i class="fa fa-plus fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-md-12 input-delivery_location_en"></div>
                    </div>
                    @foreach($dataTypeContent->deliveries as $delivery)
                        @if ($delivery->location == 'Местонахождение Автомобиля') @continue @endif
                        <div class="d-flex flex-wrap removed">

                            <div class="form-check col-2 m-2">
                                <input type="text" class="form-control input-delivery_location"
                                       name="delivery_locations[]"
                                       value="{{$delivery->location}}"
                                       placeholder="Место "/>

                            </div>

                            <div class="form-check col-2 m-2">

                                <input type="text" class="form-control" name="delivery_prices[]"
                                       value="{{$delivery->price}}"
                                       placeholder="Цена "/>
                            </div>
{{--                            @if ($loop->first)--}}
{{--                                <div class="form-check col-1 m-2">--}}
{{--                                    <a href="javascript:void(0);" class="add_button"--}}
{{--                                       title="Добавить место доставки">--}}
{{--                                        <i class="fa fa-plus fa-2x"></i>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            @else--}}
                                <div class="form-check col-2 m-2">
                                    <a href="javascript:void(0);" class="remove_button"
                                       onmouseover="deliveryRemoveText(this)">
                                        <i class="fa fa-minus fa-2x"></i>
                                    </a>
                                </div>
{{--                            @endif--}}
                            <div class="col-md-12 input-delivery_location_en">
                                {{$delivery->translate('en')->location}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
