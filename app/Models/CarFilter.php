<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarFilter extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'car_id', 'filter_id', 'price'
    ];
}
