<script src="{{asset('js/lazysizes.min.js')}}"></script>
<script>
    var locale = '{{LaravelLocalization::getCurrentLocale()}}';
    var urgency = '{{currency(setting('site.urgency'))}} ';
    var urgency_text = "{!! trim(strip_tags( tooltips($toolips, 'zasrochnost', currency(setting('site.urgency'))))) !!}";
    var ne_rabochie_vremya = '{!! trim(strip_tags( tooltips($toolips, 'ne_rabochie_vremya', currency(setting('site.during_off_hours'))))) !!}';
    var car_serach = '{{request()->url() == route('home')? false : true }}';
    var userId = '{{auth()->id()?? false }}';
    var currency_symbol = '{!! currency_symbol() !!}';
    var day_translate = ' / {{__('car.day')}}';
</script>

<script src="{{asset('js/app.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{asset('js/jquery.daterangepicker.js')}}"></script>
<script src="{{asset('js/preloader.min.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
{{--<script src="{{asset('js/dknotus-tour.min.js')}}"></script>--}}

<script  >
    function myAlert(message, type, autoHideDisabled = false ) {

        if (autoHideDisabled){
            toastr.options = {
                "closeButton": true,
                "timeOut": "0",
                "extendedTimeOut": "0",
            }
        }
        switch(type) {
            case 'success':
                toastr.success(message,'', {timeOut: 5000});
                break;
            case 'warning':
                toastr.warning(message,'', {timeOut: 5000});
                break;
            case 'error':
                toastr.error(message,'', {timeOut: 5000});
                break;
            default:
                toastr.info(message,'', {timeOut: 5000});
        }

        if (autoHideDisabled){
            toastr.options = {
                "closeButton": true,
                "timeOut": "0",
                "extendedTimeOut": "0",
            }
        }
    }
    @if (session('message'))
        myAlert('{{session('message')}}', '{{session('alert-type')??'success'}}' , {{session('autoHideDisabled')}});
    @endif
    @if (session('status'))
        myAlert('{{session('status')}}', '{{session('alert_type') ?? 'success'}}' , {{session('autoHideDisabled')}});
    @endif

</script>


<div id="op"><i class="fas fa-comments"></i></div>

<div id="myApp">
    <div class="position-relative here">
        <div id="fb-root"></div>
        <div id="vk_community_messages"></div>
        <div class="facebook-messenger-custom"><i class="fab fa-facebook-messenger"></i></div>
        <div class="vk-custom"><i class="fab fa-vk"></i></div>
    </div>
</div>

{{--<script async src="https://vk.com/js/api/openapi.js?160"></script>--}}
{{--<script async src="https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js"></script>--}}


<script>

   $(document).ready(function () {
       $('.lang').each(function () {
          $(this).attr('href', $(this).data('href'));
       });

      setTimeout(function () {
          var script = document.createElement('script');
          script.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
          document.body.append(script);
          script.onload = function(){
              $('body').append(
                  '<div class="fb-customerchat"\n' +
                  '           page_id="2261978797373491"\n' +
                  '           theme_color="#6688b2">\n' +
                  '</div>'
              );
              window.fbAsyncInit = function () {
                  FB.init({
                      xfbml: false,
                      version: 'v3.3'
                  });
                  script = document.createElement('script');
                  script.src = "https://vk.com/js/api/openapi.js?160";
                  document.body.append(script);
                  script.onload = function(){
                      widget = VK.Widgets.CommunityMessages("vk_community_messages", 181312282, {
                          disableButtonTooltip: "1",
                          widgetPosition: "right",
                          buttonType: "no_button"
                      });
                      vkOpen = false;
                      $('#op').on('click', function () {
                          $('#myApp').slideToggle(300).toggleClass('active');
                          widget.minimize();
                          FB.XFBML.parse(FB.CustomerChat.hideDialog());
                      });
                  };
                  $('#op').css('display', 'flex');
              };
          };

          $('.vk-custom').on('click', function () {
              FB.CustomerChat.hideDialog();
              vkOpen = true;
              widget.expand();

          });
          $('.facebook-messenger-custom').on('click', function () {
              widget.minimize();
              FB.CustomerChat.showDialog();
          });
      }, 15000);
   });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136603343-1"></script>

@if (request()->getHost() == 'myrentauto.com')
    <script>

                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());
                gtag('config', 'UA-136603343-1');

    </script>
    <!-- Yandex.Metrika counter -->
    <script>

            (function (m, e, t, r, i, k, a) {
                m[i] = m[i] || function () {
                    (m[i].a = m[i].a || []).push(arguments)
                };
                m[i].l = 1 * new Date();
                k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a);
            })
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(52893535, "init", {
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });

    </script>

    <!-- /Yandex.Metrika counter -->
@endif

