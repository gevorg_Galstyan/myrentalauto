<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function carChange(Request $request)
    {
        $notifications =  auth()->user()->unreadNotifications()->where('type', 'App\Notifications\ChangeOwnerCar')->get();
        return view('vendor.voyager.notifications.car-change-notify', compact('notifications'));
    }

    public function ownerProfileChange(Request $request)
    {
        $notifications =  auth()->user()->unreadNotifications()->where('type', 'App\Notifications\OwnerChangeProfile')->get();
        return view('vendor.voyager.notifications.car-owner-profile-notify', compact('notifications'));
    }
}
