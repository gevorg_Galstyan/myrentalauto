<?php

namespace App\Http\Controllers;

use App\Models\ForWhom;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Facades\Storage;

class PagesController extends Controller
{
    /**
     * Get single page
     *
     * @return view
     */
    public function index(Request $request)
    {
        $slug = $request->slug;
if (!$slug) return redirect()->route('pages', ['slug' => 'o-service']);
        $page = Page::where('slug', $slug)->first();
        if ($page) {
            return view('pages.index', compact('page'));
        } else {
            abort(404);
        }


    }

    public function forWhomOwner(Request $request)
    {
        if ($request->slug) {

            $selectedItem = Page::where('in_documents', 2)->where('slug', $request->slug)->firstOrFail();
        }else{
            $selectedItem = Page::where('in_documents', 2)->first();
        }
        $items =  Page::where('in_documents', 2)->get();

        return view('pages.for_whom', compact('items', 'selectedItem'));
    }

    public function forTenants(Request $request)
    {
        if ($request->slug) {

            $selectedItem = Page::where('in_documents', 3)->where('slug', $request->slug)->firstOrFail();
        }else{
            $selectedItem = Page::where('in_documents', 3)->first();
        }
        $items =  Page::where('in_documents', 3)->get();

        return view('pages.for_whom', compact('items', 'selectedItem'));
    }

    public function insurance(Request $request)
    {
        if ($request->slug) {

            $selectedItem = Page::where('in_documents', 4)->where('slug', $request->slug)->firstOrFail();
        }else{
            $selectedItem = Page::where('in_documents', 4)->first();
        }
        $items =  Page::where('in_documents', 4)->get();

        return view('pages.for_whom', compact('items', 'selectedItem'));
    }

    public function forWhom(){
        $page = ForWhom::first();
        return view('pages.page-for-whom', compact('page'));
    }

    public function getDocument(Page $page)
    {

        $pdf = json_decode($page->pdf, true);
        $filePath = isset($pdf[0]['download_link']) ? 'public/'.$pdf[0]['download_link'] : false;
        $fileName = isset($pdf[0]['original_name']) ? $pdf[0]['original_name'] : false;

        // file not found
        if( ! Storage::exists($filePath) ) {
            abort(404);
        }

        $pdfContent = Storage::get($filePath);

        // for pdf, it will be 'application/pdf'
        $type = Storage::mimeType($filePath);

        return response()->make($pdfContent, 200, [
            'Content-Type'        => $type,
            'Content-Disposition' => 'inline; filename="'.$fileName.'"'
        ]);
    }
}
