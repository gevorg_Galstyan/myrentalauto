<div class="w-100 ">
    <div class="w-50 text-center d-flex mx-auto">
        <h4 class="fw-700 mx-auto">
            {{__('user.documents')}}
        </h4>
    </div>
</div>
<div class="col-md-6">
    <fieldset class="all-border">
        <legend><h6 class="mb-0">{{__('user.upload_passport')}}</h6></legend>
        <div class="row">
            <div class="col-md-6">
                <div class="confirmation-images">
                    <img src="{{Voyager::image($profile->passport_one ?? 'img/icons/img.png')}}"
                         class="img-fluid" id="passport_one"
                         alt="avatar">

                </div>
                <div class="unit ">
                    @if($profile->status != 'for_consideration')
                        @if($edit || $profile->status == 'not_verified' || $edit || $profile->status == 'rejected')
                            <input name="passport_one" type="file" class="form-control ajaxUpload d-none"
                                   data-target="#passport_one" data-name="passport_one">
                            <button
                                class="own-btn  rounded-0 border-0 w-100 fw-400 click-to-ajaxUpload mt-2">{!! __('user.choose_avatar_photo') !!}</button>
                        @endif
                    @endif
                </div>

            </div>
            <div class="col-md-6">
                <div class="text-center confirmation-images">
                    <img src="{{Voyager::image($profile->passport_two ?? 'img/icons/img.png')}}"
                         class="img-circle" id="passport_two"
                         alt="avatar">


                </div>
                <div class="unit">
                    @if($profile->status != 'for_consideration')
                        @if($edit || $profile->status == 'not_verified' || $edit || $profile->status == 'rejected')
                            <input name="passport_two" type="file" class="form-control ajaxUpload d-none"
                                   id="passport_two"
                                   data-target="#passport_two" data-name="passport_two">
                            <button
                                class="own-btn  rounded-0 border-0 w-100 fw-400 click-to-ajaxUpload mt-2">{!! __('user.choose_avatar_photo') !!}</button>
                        @endif
                    @endif
                </div>
            </div>
        </div>

    </fieldset>
</div>
<div class="col-md-6">
    <fieldset class="all-border text-right">
        <legend><h6 class="mb-0">{{__('user.upload_license')}}</h6></legend>
        <div class="row">
            <div class="col-md-6">
                <div class="confirmation-images">
                    <img src="{{Voyager::image($profile->license_one ?? 'img/icons/img.png')}}"
                         class="" id="license_one" alt="avatar">

                </div>
                <div class="unit">
                    @if($profile->status != 'for_consideration')
                        @if($edit || $profile->status == 'not_verified' || $edit || $profile->status == 'rejected')
                            <input name="license_one" type="file" class="form-control ajaxUpload d-none"
                                   data-target="#license_one" data-name="license_one">
                            <button
                                class="own-btn  rounded-0 border-0 w-100 fw-400 click-to-ajaxUpload mt-2">{!! __('user.choose_avatar_photo') !!}</button>
                        @endif
                    @endif
                </div>

            </div>
            <div class="col-md-6">
                <div class="text-center confirmation-images">
                    <img src="{{Voyager::image($profile->license_two ?? 'img/icons/img.png')}}"
                         class="img-circle" id="license_two"
                         alt="avatar">


                </div>
                <div class="unit">

                    @if($profile->status != 'for_consideration')
                        @if($edit || $profile->status == 'not_verified' || $edit || $profile->status == 'rejected')
                            <input name="license_two" type="file" class="form-control ajaxUpload d-none"
                                   data-target="#license_two" data-name="license_two">
                            <button
                                class="own-btn  rounded-0 border-0 w-100 fw-400 click-to-ajaxUpload mt-2">{!! __('user.choose_avatar_photo') !!}</button>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </fieldset>

    <style>
        .confirmation-images {
            height: 200px;
            width: auto;
            position: relative;
        }

        .confirmation-images img {
            max-height: 100%;
            max-width: 100%;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>

</div>


@if($profile->status != 'for_consideration')
    @if($edit || $profile->status == 'not_verified' || $edit || $profile->status == 'rejected')
        <div class="form-check col-md-12 mt-2 text-center">
            <div class="terms-block d-lg-inline-block p-1">
                @php($confidentiality = \App\Models\Page::where('slug', 'confidentiality-policy')->first())
                <input type="checkbox" name="terms_1" form="editProfile"
                       {{$profile->terms_1 ? 'checked' :''}} class="terms">
                <a href="{{$confidentiality ? route('pages', ['slug' => $confidentiality->slug]) : '#0'}}" class="form-check-label">
                    {{$confidentiality ? $confidentiality->translate()->title : ''}}
                </a>
            </div>
        </div>
        <div class="form-check col-md-12 mt-2 text-center">
            <div class="terms-block d-lg-inline-block p-1">
                @php($ferta = \App\Models\Page::where('slug', 'public-offer')->first())
                <input type="checkbox" name="terms_2" form="editProfile"
                       {{$profile->terms_2 ? 'checked' :''}} class="terms">
                <a href="{{$ferta ? route('pages', ['slug' => $ferta->slug]) : '#0'}}" class="form-check-label">
                    {{$ferta ? $ferta->translate()->title : ''}}
                </a>
            </div>
        </div>
        <div class="col-md-12 mt-2 text-center">
            <a class="own-btn ora rounded-0 border-0 m-auto fw-400 border-radius-20 d-inline-block send-document"
               href="{{route('profile.check')}}"
               role="button">{{__('user.check_profile')}}</a>
        </div>
    @endif
@endif
@if($profile->status == 'confirm' && !$edit)
    <div class="p-2 fs-14 ff-ms">
        @if (auth()->user()->profile->terms_1 && auth()->user()->profile->terms_2)
            <div class="">
                <b class="fw-700 col-lg-3 text-success">
                    <i class="far fa-check-circle"></i>
                    @php($confidentiality = \App\Models\Page::where('slug', 'confidentiality-policy')->first())
                    {{$confidentiality ? $confidentiality->translate()->title : ''}}
                </b>
            </div>
            <div class="">
                <b class="fw-700 col-lg-3 text-success">
                    <i class="far fa-check-circle"></i>
                    @php($ferta = \App\Models\Page::where('slug', 'public-offer')->first())
                    {{$ferta ? $ferta->translate()->title : ''}}
                </b>
            </div>
        @else
            <div class="">
                <b class="fw-700 col-lg-3 ">
                    <i class="fas fa-exclamation"></i>
                    {{__('user.to_book_a_ar_you_need_to_fill_out_a_profile_and_agree_to_the_terms')}}
                </b>
            </div>
        @endif

    </div>
@endif
@if($profile->status == 'confirm' && !$edit)
    <div class="d-flex justify-content-center align-items-center mt-3 w-100">
        <span class="k-pointer edit-profile-document py-2 px-3 mr-2 doc-change" data-toggle="tooltip"
              data-html="true" data-placement="top"
              title="{{tooltips($toolips, 'documents_edit')}}">
            {{__('user.edit_profile')}}
                <i class="fas fa-user-edit "></i>
         </span>
    </div>

@endif

<small class="pl-3 mt-2 d-block fs-13 color-red w-100 fw-600">* {{__('user.description_profile')}}</small>
