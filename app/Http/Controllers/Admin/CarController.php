<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Models\Car;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\DeliveryLocation;
use App\Models\Insurance;
use App\Models\WrongFillCar;
use App\Notifications\OwnerWrong;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;

class CarController extends VoyagerBaseController
{
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

//        if (!$request->slug) {
        $carMake = CarMake::find($request->car_make_id);
        $carModel = CarModel::find($request->car_model_id);
        $carMake = $carMake->name ?? null;
        $carModel = $carModel->name ?? null;
        $request['slug'] = SlugService::createSlug(Car::class, 'slug', $carMake . '-' . $carModel, ['unique' => true]);
//        }


        // location_title
        $ruLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'ru_RU');
        $enLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'en_GB');
        $location_title = [
            'ru' => $ruLocation->getFullAddress(),
            'en' => $enLocation->getFullAddress(),
        ];
        $metaLocationTitle = [
            'ru' => $ruLocation->getLocality() ?? $ruLocation->getRegion(),
            'en' => $enLocation->getLocality() ?? $enLocation->getRegion(),
        ];

//        dd($title_location);
        $request['meta_location_title'] = json_encode($metaLocationTitle);
        $request['meta_location_title_i18n'] = json_encode($metaLocationTitle);

        $request['location_title'] = json_encode($location_title);
        $request['location_title_i18n'] = json_encode($location_title);

        //green card data
        $request['green_card'] = json_encode($request->green_card && is_array($request->green_card) ? $request->green_card : []);
//     CITY
        $old_cities = @json_decode($request->city_i18n, true);
        $en_city = $old_cities['en'];
        $ru_city = $old_cities['ru'];
        $city = [
            'en' => ($en_city ? $en_city : ($ru_city ? myTranslate($ru_city, 'en') : "")),
            'ru' => ($ru_city ? $ru_city : ($en_city ? myTranslate($en_city, 'ru') : "")),
        ];
        $request['city_i18n'] = json_encode($city);

//     NOTE
        $old_notes = @json_decode($request->note_i18n, true);
        $en_note = $old_notes['en'];
        $ru_note = $old_notes['ru'];
        $note = [
            'en' => ($en_note ? $en_note : ($ru_note ? myTranslate($ru_note, 'en') : "")),
            'ru' => ($ru_note ? $ru_note : ($en_note ? myTranslate($en_note, 'ru') : "")),
        ];
        $request['note_i18n'] = json_encode($note);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        //Relationship
        $data->filters()->delete();
        $data->deliveries()->delete();
        $data->insurances()->delete();

        $delivery = new DeliveryLocation();
        $delivery->car_id = $data->id;
        $delivery->location = 'Местонахождение Автомобиля';
        $delivery->price = 0;
        $delivery->save();
        $delivery = $delivery->translate('en');
        $delivery->location = myTranslate('Местонахождение Автомобиля', 'en');
        $delivery->save();
        if (is_array($request->delivery_locations) && !empty($request->delivery_locations)) {
            foreach ($request->delivery_locations as $index => $location) {
                if (is_null($location)) continue;
                $delivery = new DeliveryLocation();
                $delivery->car_id = $data->id;
                $delivery->location = $location;
                $delivery->price = $request->delivery_prices[$index] ?? null;
                $delivery->save();
                $delivery = $delivery->translate('en');
                $delivery->location = myTranslate($location, 'en');
                $delivery->save();
            }
        }
        if (is_array($request->filter)) {
            foreach ($request->filter as $index => $filter) {
                $data->filters()->attach($filter, [
                    'price' => $request->filter_price[$index],
                    'order' => $request->filter_order[$index],
                ]);
            }
        }

        $obligatoryInsurances = Insurance::where('obligatory', 1)->pluck('id')->toArray();
        if (is_array($request->insurances)) {
            foreach ($request->insurances as $insurance_id => $insurance) {
                $data->insurances()->attach($insurance_id, [
                    'serial_number' => $insurance['serial_number'],
                    'start_date' => Carbon::parse($insurance['start_date'])->format('Y-m-d H:i:s'),
                    'end_date' => Carbon::parse($insurance['end_date'])->format('Y-m-d H:i:s'),
                    'price' => $insurance['price'],
                    'order' => $insurance['order'],
                    'franchise' => isset($insurance['franchise']) ? $insurance['franchise'] : 0,
                ]);
            }
            foreach ($obligatoryInsurances as $obligatory) {
                if (!in_array($obligatory, array_keys($request->insurances))) {
                    $data->insurances()->attach($obligatory, [
                        'price' => 0,
                    ]);
                }
            }
        } else {
            foreach ($obligatoryInsurances as $obligatory) {
                $data->insurances()->attach($obligatory, [
                    'price' => 0,
                ]);
            }
        }


        event(new BreadDataAdded($dataType, $data));
        $data->save();
        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function edit(Request $request, $id)
    {

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $notifications = auth()->user()->notifications()->where('type', 'App\Notifications\ChangeOwnerCar')->whereJsonContains('data->car_id', $dataTypeContent->id)->get();
        if ($notifications && !empty($notifications)) $notifications->markAsRead();
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }


    public function update(Request $request, $id)
    {
        $request['status'] = 0;
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // location_title
        $ruLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'ru_RU');
        $enLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'en_GB');
        $location_title = [
            'ru' => $ruLocation ? $ruLocation->getFullAddress() : $data->translate('ru')->location_title,
            'en' => $enLocation ? $enLocation->getFullAddress() : $data->translate('en')->location_title,
        ];
        $metaLocationTitle = [
            'ru' => $ruLocation ? ($ruLocation->getLocality() ?? $ruLocation->getRegion()) : $data->translate('ru')->meta_location_title,
            'en' => $enLocation ? ($enLocation->getLocality() ?? $enLocation->getRegion()) : $data->translate('ru')->meta_location_title,
        ];

//        dd($title_location);
        $request['meta_location_title'] = json_encode($metaLocationTitle);
        $request['meta_location_title_i18n'] = json_encode($metaLocationTitle);

        $request['location_title'] = json_encode($location_title);
        $request['location_title_i18n'] = json_encode($location_title);
//dd($request->all());
        //green card data
        $request['green_card'] = json_encode($request->green_card && is_array($request->green_card) ? $request->green_card : []);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
//        dd($request->all());
        if (!$data->slug) {
            $make = CarMake::find($request->car_make_id);
            $model = CarModel::find($request->car_model_id);
            $make = $make->name ?? null;
            $model = $model->name ?? null;
            $request['slug'] = SlugService::createSlug(Car::class, 'slug', $make . '-' . $model, ['unique' => true]);
        } else {
            $request['slug'] = $data->slug;
        }
        $request['status'] = $data->status;
//      NOTE
        $old_cities = @json_decode($request->city_i18n, true);
        $en_city = isset($old_cities['en']) ? $old_cities['en'] : '';
        $ru_city = isset($old_cities['ru']) ? $old_cities['ru'] : '';
        $city = [
            'en' => ($en_city ? $en_city : ($ru_city ? myTranslate($ru_city, 'en') : "")),
            'ru' => ($ru_city ? $ru_city : ($en_city ? myTranslate($en_city, 'ru') : "")),
        ];

        //     NOTE
        $old_notes = @json_decode($request->note_i18n, true);
        $en_note = isset($old_notes['en']) ? $old_notes['en'] : '';
        $ru_note = isset($old_notes['ru']) ? $old_notes['ru'] : '';
        $note = [
            'en' => ($en_note ? $en_note : ($ru_note ? myTranslate($ru_note, 'en') : "")),
            'ru' => ($ru_note ? $ru_note : ($en_note ? myTranslate($en_note, 'ru') : "")),
        ];
        $request['note_i18n'] = json_encode($note);

        $request['city_i18n'] = json_encode($city);

        $data = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        //Relationship
        $data->filters()->delete();
        $data->deliveries()->delete();
        $data->insurances()->delete();

        $delivery = new DeliveryLocation();
        $delivery->car_id = $data->id;
        $delivery->location = 'Местонахождение Автомобиля';
        $delivery->price = 0;
        $delivery->save();
        $delivery = $delivery->translate('en');
        $delivery->location = myTranslate('Местонахождение Автомобиля', 'en');
        $delivery->save();
        if (is_array($request->delivery_locations) && !empty($request->delivery_locations)) {
            foreach ($request->delivery_locations as $index => $location) {
                if ($location) {
                    if (is_null($location)) continue;
                    $delivery = new DeliveryLocation();
                    $delivery->car_id = $data->id;
                    $delivery->location = $location;
                    $delivery->price = $request->delivery_prices[$index] ?? null;
                    $delivery->save();
                    $delivery = $delivery->translate('en');
                    $delivery->location = myTranslate($location, 'en');
                    $delivery->save();
                }

            }
        }

        if (is_array($request->filter)) {
            foreach ($request->filter as $index => $filter) {
                $data->filters()->attach($filter, [
                    'price' => $request->filter_price[$index],
                    'order' => $request->filter_order[$index],
                ]);
            }
        }

        $obligatoryInsurances = Insurance::where('obligatory', 1)->pluck('id')->toArray();
        if (is_array($request->insurances)) {
            foreach ($request->insurances as $insurance_id => $insurance) {
                $data->insurances()->attach($insurance_id, [
                    'serial_number' => $insurance['serial_number'],
                    'start_date' => Carbon::parse($insurance['start_date'])->format('Y-m-d H:i:s'),
                    'end_date' => Carbon::parse($insurance['end_date'])->format('Y-m-d H:i:s'),
                    'price' => $insurance['price'],
                    'order' => $insurance['order'],
                    'franchise' => isset($insurance['franchise']) ? $insurance['franchise'] : 0,
                ]);
            }
            foreach ($obligatoryInsurances as $obligatory) {
                if (!in_array($obligatory, array_keys($request->insurances))) {
                    $data->insurances()->attach($obligatory, [
                        'price' => 0,
                    ]);
                }
            }
        } else {
            foreach ($obligatoryInsurances as $obligatory) {
                $data->insurances()->attach($obligatory, [
                    'price' => 0,
                ]);
            }
        }

        $data->save();
        event(new BreadDataUpdated($dataType, $data));

        return redirect()
            ->back()
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        $request['date'] = $request->date ?? Carbon::now()->setTimeFromTimeString('09:00')->addDay(5)->format('d.m.Y H:00') . ' ~ ' . Carbon::now()->setTimeFromTimeString('09:00')->addDay(7)->format('d.m.Y H:00');
        $dates = explode(' ~ ', $request->date);
        $start = Carbon::parse($dates[0]);
        $end = Carbon::parse($dates[1]);
        $request['days'] = $end->diffInDays($start) + 1;
        $request['start_day'] = $start;
        $request['end_day'] = $end;
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        $tariffPlan = [];
        foreach ($dataTypeContent->filters as $filter) {
            if ($filter->pivot->price <= 0) {
                $tariffPlan[] = ['name' => $filter->translate()->title];
            }
        }
        foreach ($dataTypeContent->insurances as $insurances) {
            if ($insurances->pivot->price <= 0) {
                $tariffPlan[] = ['name' => $insurances->translate()->name, 'desc' => '<span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="' . $insurances->translate()->description . '"><i class="fas fa-info-circle"></i></span>'];
            }
        }

        $green_card = json_decode($dataTypeContent->green_card ?? '[]', true);

        if (count($green_card)) {
            foreach ($green_card as $key => $item) {
                if ($item[getGreenCardDayKey(request()->input('days'))] <= 0) {
                    $tariffPlan[] = ['name' => 'Грин Карта' . config('car.green_card.country.' . $key)];
                }
            }
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'tariffPlan'));

    }

    public function updateStatus(Car $car, $status)
    {
        if ($status == 'publish') {
            $car->status = 1;
            $car->error_status = '';
            $message = 'Автомобиль опубликован ';
            $alert = 'success';
        } else {
            $car->status = 0;
            $message = 'Автомобиль снят с публикации ';
            $alert = 'error';
        }
        $car->save();
        return redirect()
            ->route("voyager.cars.index")
            ->with([
                'message' => $message,
                'alert-type' => $alert,
            ]);
    }

    /**
     * Get BREAD relations data.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function relation(Request $request)
    {
        $slug = $this->getSlug($request);
        $page = $request->input('page');
        $on_page = 50;
        $search = $request->input('search', false);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        foreach ($dataType->editRows as $key => $row) {
            if ($row->field === $request->input('type')) {
                $options = $row->details;
                $skip = $on_page * ($page - 1);

                // If search query, use LIKE to filter results depending on field label
                if ($search) {
                    if ($options->model == "App\\Models\\CarModel") {
                        $total_count = app($options->model)->where('make_id', $request->parent)->where($options->label, 'LIKE', '%' . $search . '%')->count();
                    } else {
                        $total_count = app($options->model)->where($options->label, 'LIKE', '%' . $search . '%')->count();
                    }

                    $relationshipOptions = app($options->model)->where(function ($query) use ($options) {
                        if ($options->model == "App\\User") {
                            $query->where('role_id', 3);
                        }
                    })->take($on_page)->skip($skip)
                        ->where($options->label, 'LIKE', '%' . $search . '%')
                        ->get();
                } else {
                    $total_count = app($options->model)->count();
                    $relationshipOptions = app($options->model)->where(function ($query) use ($request, $options) {
                        if ($options->model == "App\\User") {
                            $query->where('role_id', 3);
                        } elseif ($options->model == "App\\Models\\CarModel") {
                            $query->where('make_id', $request->parent);
                        }
                    })->take($on_page)->skip($skip)->get();
                }

                $results = [];
                foreach ($relationshipOptions as $relationshipOption) {
                    $results[] = [
                        'id' => $relationshipOption->{$options->key},
                        'text' => $relationshipOption->{$options->label},
                    ];
                }

                return response()->json([
                    'results' => $results,
                    'pagination' => [
                        'more' => ($total_count > ($skip + $on_page)),
                    ],
                ]);
            }
        }

        // No result found, return empty array
        return response()->json([], 404);
    }

    public function calendar(Request $request, Car $car)
    {
        $dataTypeContent = $car;
        $dataType = Voyager::model('DataType')->where('slug', '=', 'cars')->first();

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $events = [];
        $colors = [
            'orders' => [
                '4ebf22',
                '82e25d'
            ],
            'added' => [
                '32A0DA',
            ]
        ];

        foreach ($car->busyDays as $index => $dates) {

            if ($dates->order) {
                $color = $colors['orders'][$index % 2 ? 1 : 0];
            } else {
                $color = $colors['added'][0];
            }
            $events[] = \Calendar::event(

                $dates->getTitle(),
                true,
                new \DateTime($dates->getStart()),
                new \DateTime(Carbon::parse($dates->getEnd())->addDays(1)),
                null,
                [
                    'color' => '#' . $color,
                    "overlap" => false,
                    'className' => 'busy-day',
                    'description' => $dates->note ?? '',
                ]
            );
        }
        $calendar = \Calendar::addEvents($events, [

            'defaultView' => 'timeGridWeek',
            'rendering' => 'background',
            'allDay' => true,
            'editable' => true,
            'eventDurationEditable' => true,
        ])//add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'plugins' => ['dayGrid', 'bootstrap'],
            'themeSystem' => 'bootstrap',
            'header' => ["center" => 'makeOrder'],

            "views" => [
                "dayGridMonth" => [ // name of view
                    "titleFormat" => ["year" => 'numeric', "month" => '2-digit', "day" => '2-digit'],
                    "duration" => ["month" => 2]
                ],

            ],

            "locale" => \LaravelLocalization::getCurrentLocale(),
            'showNonCurrentDates' => false,
            'fixedWeekCount' => false,
            'bootstrap' => true,
            'gotoDate' => '2019-07-05',

        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)

        ]);
        return view('vendor.voyager.cars.calendar', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'calendar'));
    }

    public function wrongFill(Request $request, Car $car)
    {
        $text = $request->text;
        $car->error_status = 'error';
        $car->status = 0;
        $car->save();
        $wrongFill = new WrongFillCar();
        $wrongFill->car_id = $car->id;
        $wrongFill->text = $text;
        $wrongFill->save();
        $car->owner->notify(new OwnerWrong($wrongFill));
        return redirect()
            ->back()
            ->with([
                'message' => 'Ошибка заполнения создана , Автомобиль снят из ранжирования ',
                'alert-type' => 'success',
            ]);
    }

    public function showHistory(Car $car)
    {
        $carArray = $car->withAll()->first()->toArray();
        foreach ($car->logs as $log){
            $item = json_decode($log->body, true);
            dd($item);
            $diff = array_diff(array_map('serialize', $item), array_map('serialize', $carArray));
        }
        return response()->json($diff);
    }

}
