<?php

return [
    'message' => 'By using our website, you agree to the <a href="'.route('pages',['slug' => 'politika-cookies']).'" class="own-orange" target="_blank">use of cookies</a>, which will allow us to simplify navigation, offer content tailored to your interests and perform statistics to improve our website.',
    'agree' => 'Allow cookies',
];
