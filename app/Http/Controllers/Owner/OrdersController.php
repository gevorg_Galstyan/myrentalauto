<?php

namespace App\Http\Controllers\Owner;


use App\Models\OrderHitory;
use App\Notifications\CarOrderOwnerResponse;
use App\Notifications\CarRequestAdmin;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Order;
use App\Models\OrderDate;
use Illuminate\Notifications\Notification;

class OrdersController extends Controller
{
    /*
     * orders
     */
    public function index(Request $request)
    {

//    $cars = auth()->user()->cars()->whereHas('orders')->with('orders')->get();
        $orders = Order::whereHas('car', function ($query){
            $query->where('owner_id', auth()->id());
        })->where(function ($query) use ($request){
            if ($request->status == 'all'){
                $query->where('status', '!=', 'failed');
            }else{
                $query->where('status', $request->status);
            }
        })->doesntHave('parent')->orderBy('created_at', 'desc')->get();
        return view('owners.orders.index', [
            'orders' => $orders
        ]);
    }

    /*
     * transaction single page
     */
    public function transaction(Request $request, $slug)
    {

        $order = Order::with('customer')->where('transaction_id', $slug)->firstOrFail();

        return view('owners.orders.show', [
            'order' => $order
        ]);
    }

    public function show($status, $barcode_number)
    {
        $order = Order::where('status', $status)->where('barcode_number', $barcode_number)->firstOrFail();
        if ($order) {
            return view('owners.orders.request', compact('order'));
        } else {
            return back();
        }
    }

    public function readNotify(Request $request)
    {
        if ($request->notify) {
            $notification = auth()->user()->notifications()->where('id', $request->notify)->firstOrFail();
            if ($notification) {
                $notification->markAsRead();
            }
        } else {
            auth()->user()->unreadNotifications->markAsRead();
        }
        return back();
    }

    public function setResponse(Request $request)
    {
        $order = Order::where('barcode_number', $request->barcode_number)->firstOrFail();
        $history['author'] = 'owner';
        $history['user_name'] = auth()->user()->full_name;
        $history['user_id'] = auth()->id();
        $history['act'] = $request->act;
        $history['type'] = 'response';
        $history['note'] = $request->note;
        $history['date'] = Carbon::now();


        $orderHistory = new OrderHitory();
        $orderHistory->order_id = $order->id;
        $orderHistory->details = json_encode($history);
        $orderHistory->save();
        $users = User::where('role_id', 1)->get();
        \Illuminate\Support\Facades\Notification::send($users, new CarOrderOwnerResponse($order));
        if ($request->ajax()){
            return response()->json(['success' => true], 200);
        }else{
            return back()->with([
                'status' => 'Спасибо ваш ответ отправлен Администратору!'
            ]);
        }

    }
}
