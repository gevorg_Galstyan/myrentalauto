<div class="card mx-5">
    <div class="card-body">
        <form method="POST" action="{{ url('comments') }}" class="create-comment-form">
            @csrf
            <input type="hidden" name="commentable_type" value="\{{ get_class($model) }}" />
            <input type="hidden" name="commentable_id" value="{{ $model->id }}" />
            <div class="form-group">
                <label for="message">Введите ваше сообщение здесь:</label>
                <textarea class="form-control comment @if($errors->has('message')) is-invalid @endif"  name="message" rows="3"></textarea>
                <div class="invalid-feedback">
                    Ваше сообщение обязательно.
                </div>
            </div>
            <button type="submit" class="own-btn text-uppercase d-inline-block ora border-0">Отправить</button>
        </form>
    </div>
</div>
