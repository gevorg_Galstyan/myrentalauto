<?php

return [
    'name' => 'MyRentAuto',
    'manifest' => [
        'name' => env('APP_NAME', 'MyRentAuto'),
        'short_name' => 'MyRentAuto',
        'start_url' => '/',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation'=> 'any',
        'icons' => [
            '72x72' => '/storage/img/logo-72x72.png',
            '96x96' => '/storage/img/logo-96x96.png',
            '128x128' => '/storage/img/logo-128x128.png',
            '144x144' => '/storage/img/logo-144x144.png',
            '152x152' => '/storage/img/logo-152x152.png',
            '192x192' => '/storage/img/logo-192x192.png',
            '384x384' => '/storage/img/logo-384x384.png',
            '512x512' => '/storage/img/logo.png',
        ],
        'splash' => [
            '640x1136' => '',
            '750x1334' => '',
            '828x1792' => '',
            '1125x2436' =>'',
            '1242x2208' =>'',
            '1242x2688' =>'',
            '1536x2048' =>'',
            '1668x2224' =>'',
            '1668x2388' =>'',
            '2048x2732' =>'',
        ],
        'custom' => []
    ],
    'VAPID_PUBLIC_KEY' => env('VAPID_PUBLIC_KEY')
];
