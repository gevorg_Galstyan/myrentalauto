<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WrongFillCar extends Model
{
    protected $table = 'wrong_fill_cars';

    public function car(){
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }
}
