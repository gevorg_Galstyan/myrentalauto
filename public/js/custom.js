$('.owl-stage').addClass('card-group');


function showRegisterForm() {
    $('.loginBox').fadeOut('fast', function () {
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast', function () {
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
    });
    $('.error').removeClass('alert alert-danger').html('');

}

function showLoginForm() {
    $('#loginModal .registerBox').fadeOut('fast', function () {
        $('.loginBox').fadeIn('fast');
        $('.forgotPasswordBox').fadeOut('fast');
        $('.register-footer').fadeOut('fast', function () {
            $('.rest-password-footer').fadeOut('fast');
            $('.login-footer').fadeIn('fast');
        });

        $('.modal-title').html('Login with');
    });
    $('.error').removeClass('alert alert-danger').html('');
}

$('.forgot-password').click(function (event) {
    event.preventDefault();
    $('.loginBox').fadeOut('fast', function () {
        $('.forgotPasswordBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast', function () {
            $('.rest-password-footer').fadeIn('fast');
        });
    });
});


function openRegisterModal() {
    showRegisterForm();
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 230);

}

function loginAjax() {
    shakeModal();
}

function shakeModal() {
    $('#loginModal .modal-dialog').addClass('shake');
    $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
    $('input[type="password"]').val('');
    setTimeout(function () {
        $('#loginModal .modal-dialog').removeClass('shake');
    }, 1000);
}

$(document).ready(function () {
    $("#registerForm").submit(function (e) {
        e.preventDefault();
        var t = $(this).attr("action"), n = $(this), i = new FormData(this);
        $(n).preloader();
        $.ajax({
            url: t,
            type: "POST",
            dataType: "json",
            data: i,
            processData: !1,
            contentType: !1,
            beforeSend: function () {
                $("body").css("cursor", "progress"), $(".has-error").removeClass("has-error"), $(".help-block").remove()
            },
            success: function (e) {
                $(n).preloader('remove');
                $("body").css("cursor", "auto");
                if (e.errors) {
                    $(n).preloader('remove');
                    $.each(e.errors, function (t, n) {
                        var i = $("#registerForm").find("[name='" + t + "']"), r = i.first().parent().offset().top,
                            o = $("nav.navbar").height();
                        0 === Object.keys(e.errors).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 50 + "px"}, "fast"), i.parent().addClass("has-error").append("<span class='help-block' style='color:#f96868;font-size: 14px;'>" + n + "</span>")
                    })
                } else {
                    location.href = e.intended;
                }

            },
        })
    });

    $(".btn-submit-login").on('click', function (e) {
        e.preventDefault();
        var $data = $("#loginForm").serialize();
        var $url = $("#loginForm").attr('action');
        var $alert = $("#loginForm").children('.alert-danger');
        var n = $(this);
        $(n).preloader();
        axios.post($url, $data)
            .then(function (respons) {
                location.href = respons.data.intended;
            })
            .catch(function (error) {
                json = $.parseJSON(error.response.request.responseText);
                $('.help-block').remove();
                $(n).preloader('remove').parent().addClass("has-error").append("<p class='help-block'><span  style='color:#f96868;font-size: 14px;'>" + json.errors.email + "</span></p>");
            });
    });
});


$(document).ready(function () {
    if ($('.search-date').length) {
        $('.search-date').dateRangePicker({
            startOfWeek: 'monday',
            container: '#search-block',
            separator: ' ~ ',
            format: 'DD.MM.YYYY HH:mm',
            autoClose: false,
            language: locale,
            swapTime: false,
            minDays: 1,
            startDate: moment().format('DD-MM-YYYY'),
            time: {
                enabled: true
            },
            hoveringTooltip: function (e, t, a) {
                return "";
            },
            getValue: function () {
                this.value = $('.date-input').val();
                return this.value;
            },
            setValue: function (s) {
                this.value = $('.date-input').val(s);
                this.innerHTML = date_dive_content(s);
            },
            beforeShowDay: function (t) {
                let myDate = moment(t);
                let _class = '';
                let _tooltip = '';
                if (myDate.isBefore(moment().add(3, 'd'))) {
                    _class = 'urgency';
                    _tooltip = urgency_text;
                }
                return [true, _class, _tooltip];
            },
        }).bind('datepicker-apply', function (event, obj) {
            $('.date-input').val(obj.value);
            if (car_serach) {
                getCars();
            } else {
                $('#search-transmission-dropdown').dropdown('show');
            }
            $('.time1 input[type="range"], .time2 input[type="range]').tooltip('hide');
        }).bind('datepicker-opened', function () {
            if (!car_serach) {
                let y = $('.date-picker-wrapper').height() + $(this).height();
                if ($(window).scrollTop() < y) {
                    $('html, body').animate({scrollTop: y + 'px'}, 800)
                }
                if ($('#search-transmission-dropdown').is(':visible')) {
                    $('#search-transmission-dropdown').dropdown('hide')
                }
                if ($('#search-location-dropdown').is(':visible')) {
                    $('#search-location-dropdown').dropdown('hide')
                }
                if ($('.search-filter').is(':visible')) {
                    $(".search-filter").slideUp();

                    var animTags = $('.open-home-filters').children().find("g[id]");
                    var animIds = animTags.map(function () {
                        return this.id;
                    }).get();

                    for (var i = 0; i < animTags.length; i++) {
                        //
                        var animClasses = animIds[i] + '-animate';
                        $(animTags[i]).removeClass(animClasses)
                    }

                    $('.open-home-filters').removeClass('activeBtn')
                }
            } else {
                let mob_sidebar = $('.mob-sidebar');
                let sidebar_open = $('.sidebar-open');
                if ($(window).width() <= 991 && mob_sidebar.is(':visible')) {
                    mob_sidebar.slideUp('slow', function () {
                        let textHide = sidebar_open.text();
                        let textShow = sidebar_open.data('text');
                        console.log(textHide, textShow);
                        sidebar_open.data('text', textHide).text(textShow);
                    });
                }
            }

        });
    }
});

$(document).on('click', '.close-filter', function () {
    var id = $(this).data('target');
    var type = $(this).data('type');
    $('[data-status="' + id + '"]').remove();
    if (type == 'checkbox') {

        $(id).prop('checked', false).change();
    } else if (type == 'range') {
        $('[data-id="' + id + '"]').val($('[data-id="' + id + '"]').attr('max')).change();
    } else if (type == 'input') {

        $(id).val('');
        getCars();
        if ($(this).data('dropdown')) {
            var selText = $(id).parents('.btn-group').find('.dropdown-menu li a[data-select=""]').text();

            $(id).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
        }
    } else if (type == 'input-type-checkbox') {
        $(id).remove();
        getCars();
    } else if (type == 'transmission') {
        $('.search-select-select2').val(null).trigger('change');
    }
});
$(document).on('change', '.car-params', function () {
    getCars();
});
$('.car-params-input').on('keyup', function () {
    if ($(this).val().length > 2 || $(this).val().length == 0) {
        getCars();
    }
});

function getCars(reload = false) {
    var form = $("#searchForm");
    var data = form.serialize();
    var url = form.attr('action');
    var container = $('.search-content');
    if (reload) {
        window.location.href = `${url}/?${data}`;
    } else {
        container.preloader();
        $.ajax({
            url: url,
            data: data,
            type: 'get',
            success: function (data) {
                if (data.success) {
                    if ($(window).width() >= 991) {
                        $('html, body').animate({scrollTop: 0}, 200);
                    }
                    container.preloader('remove').html(data.html);
                    getVisible();
                    single_car_params();
                }
                $('.currency-symbol').text(currency_symbol);
                $('.day').text(day_translate);
            }
        })
    }
}

function single_car_params() {
    $('.single-page').each(function () {
        var car = $(this);
        car.attr('href', car.attr('href') + '?' + car.data('href'))
    })
}

single_car_params();


$('.home-params').change(function () {
    var type = $(this).data('type');
    var id = $(this).attr('id');
    if ($(this).attr('type') == 'radio') {
        $('[data-rm="' + type + '"').remove();
    }

    if ($(this).parents(".auto-type-inputs").length == 1) {
        if ($('.auto-type-inputs .home-params').is(':checked')) {
            $('.auto-type-inputs .all-autoTypes').prop('checked', false);
        } else {
            $('.auto-type-inputs .all-autoTypes').prop('checked', true);
        }
    }


    if ($(this).is(':checked') && $(this).val()) {
        var name = $(this).data('name');
        $('.home-tags-block').append('<button type="button" data-rm="' + type + '" data-target="#' + id + '" class="btn btn-xs btn-tag fs-12 mr-1">' +
            '<span class="mr-2">' + name + '</span>' +
            '<span class="close-home-filter" data-status="#' + id + '">' +
            '<i class="fas fa-times-circle"></i>' +
            '</span>' +
            '</button>');
    } else {
        $('[data-target="#' + id + '"]').remove();
    }
});


$(document).on('click', '.close-home-filter', function () {
    var id = $(this).data('status');
    $(id).prop('checked', false);
    if ($(id).attr('type') != 'checkbox') {
        $('.choose-all[data-type="' + $(id).data('type') + '"]').prop('checked', true);
    }
    $('[data-target="' + id + '"').remove();
    if ($('.home-tags-block').children().length == 0) {
        $('.auto-type-inputs .all-autoTypes').prop('checked', true);
    }
});

$('.clear-home-filters').click(function () {
    $('.home-params').prop('checked', false);
    $('.choose-all').prop('checked', true);
    $('.home-tags-block').html('');

});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
/************ PAGINATION  ******************/
$(document).on('click', '.car-paginate .pagination a', function (event) {
    event.preventDefault();
    var url = $(this).attr('href');
    var container = $('.search-content')
    container.preloader();
    if ($(window).width() >= 991) {
        $('html, body').animate({scrollTop: 0}, 200);
    }
    $.ajax({
        url: url,
        success: function (data) {
            container.preloader('remove').html(data.html);
            if ($(window).width() >= 991) {
                getVisible();
            }
            $('.currency-symbol').text(currency_symbol);
            $('.day').text(day_translate);
        }
    })

});
/************ FAVORITE  ******************/
var token = $('meta[name="csrf-token"]').attr('content')
$(document).on('click', '.ajaxFavorite', function (event) {
    event.preventDefault();
    var id = $(this).data('target');
    var url = $(this).data('href');
    var _this = $(this);
    var del = $(this).data('delete');
    $(id).preloader();
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            $(id).preloader('remove');
            if (del) {
                $(id).fadeOut();
                $(id).remove();
            }
            _this.find('.car-options').html(data);
        }
    });
});

function date_dive_content(date) {
    var date_array = date.split(' ~ ');
    var date_1 = date_array[0].split(' ');
    var date_2 = date_array[1].split(' ');
    date_1 = '<span class="date-1-block"><strong>' + date_1[0] + '</strong></span> <span class="time-1-clock">' + date_1[1] + '</span>';
    date_2 = '<span class="date-2-block"><strong>' + date_2[0] + '</strong></span> <span class="time-2-clock">' + date_2[1] + '</span>';
    return date_1 + ' ~ ' + date_2;
}

$(document).ready(function () {
    $('.date-block, .date-block-single').click(function (evt) {
        evt.stopPropagation();
        // $('.date-block').data('dateRangePicker').open();
    });
    if ($('.date-input').val()) {
        $('.date-block').html(date_dive_content($('.date-input').val()));
    }
    if ($('date-block-singlenpm').val()) {
        $('.date-block-single').html(date_dive_content($('date-block-singlenpm').val()))
    }
});


/********* SOCIAL LOGIN ********/

$('.social-login').click(function () {
    $('#loginModal').modal('hide');
    $('body').preloader();
});

/******** NABAR DROPDOWN ********/

$('.navbar-light .dmenu').hover(function () {
    $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
}, function () {
    $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
});

/******** COMMENT LOAD ********/

$(document).on('click', '.load-comment', function (e) {
    e.preventDefault();
    var container = $('.comment-container');
    container.preloader();
    axios.get($(this).attr('href')).then((response) => {
        $('.load-mor-container').remove();
        container.preloader('remove').prepend(response.data.html);
    })
});

$('.create-comment-form').submit(function (form) {
    form.preventDefault();
    var container = $('.comments');
    container.preloader();
    if ($(this).find('.comment').val()) {
        form.preventDefault();
        axios.post($(this).attr('action'), $(this).serialize()).then((response) => {
            container.preloader('remove').find('.comment-container').append(response.data.html);
            $(this).find('.comment').val('')
            $('.comment-empty').remove();
        })
    }

});
$(document).on('submit', '.edit-comment-form', function (form) {
    form.preventDefault();
    $($(this).data('target')).modal('hide');
    var container = $('#comment-' + $(this).data('id'));
    if ($(this).find('.comment').val()) {
        container.preloader();
        form.preventDefault();
        axios.put($(this).attr('action'), $(this).serialize()).then((response) => {
            container.preloader('remove');
            $('#comment-text-' + $(this).data('id')).html(response.data.message);
        })
    }
});
$(document).on('submit', '.comment-reply-form', function (form) {
    form.preventDefault();
    $($(this).data('target')).modal('hide');
    var container = $('#comment-' + $(this).data('id'));
    if ($(this).find('.comment').val()) {
        container.preloader();
        form.preventDefault();
        axios.post($(this).attr('action'), $(this).serialize()).then((response) => {
            container.preloader('remove');
            $('#reply-content-' + $(this).data('id')).append(response.data.html);
            $(this).find('.comment').val('')
        })
    }
});


$(".dropdownWithClick").click(function () {
    $('[data-status="' + $(this).data('dropdown') + '"]').collapse('toggle');
});


$('.search-select-select2').select2({
    minimumResultsForSearch: -1,
    enoughRoomAbove: true,
    enoughRoomBelow: false,
});

//READ MORE
// Configure/customize these variables.
var showChar = 100;  // How many characters are shown by default
var ellipsestext = "...";
var moretext = "Показать больше >";
var lesstext = "Показывай меньше";


$('.more').each(function () {
    var content = $(this).html();

    if (content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar, content.length - showChar);

        var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

        $(this).html(html);
    }

});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});

$("#location").click(function () {
    $(this).select();
});

$('.currency-symbol').text(currency_symbol);
$('.day').text(day_translate);
$('.currency-item').on('click', function () {
    var url = $(this).data('href');
    window.location.href = url;
});

function setActive(event) {
    $(event).parent().find('.active').removeClass('active');
    $(event).addClass('active');
}
