<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarType;
use App\Models\DeliveryLocation;
use App\Models\Filter;
use App\Models\Location;
use App\Models\Order;
use App\Models\OrderDate;
use App\Models\OrderHitory;
use App\Notifications\CarRequestAdmin;
use App\Notifications\CarRequestOwner;
use App\Repositories\CarOrderRepository;
use App\Services\MpiService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CarController extends Controller
{

    private $date;

    public function __construct(Request $request)
    {
        $dates = explode(' ~ ', $request->date);
        if (!$request->date || !is_array($dates) || (Carbon::parse($dates[0])->isPast() || Carbon::parse($dates[0])->isPast())) {
            $this->date = $request['date'] = \Carbon\Carbon::now()->setTimeFromTimeString('09:00')->addDay(5)->format('d.m.Y H:i') . ' ~ ' . \Carbon\Carbon::now()->setTimeFromTimeString('09:00')->addDay(7)->format('d.m.Y H:i');
        } else {
            $this->date = $request->date;
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        $dates = explode(' ~ ', $this->date);
        $start = Carbon::parse($dates[0]);
        $end = Carbon::parse($dates[1]);
        $request['days'] = $days = $end->addMinutes(-31)->diffInDays($start) + 1;
        $loqcation = Location::where('slug', $request->location)->first();
        if ($loqcation){
            $request['location'] = \Lang::has('location.'.$loqcation->slug) ? __('location.'.$loqcation->slug) : $loqcation->slug;
        }else{
            $request['location'] = $request->location;
        }

        $dates = explode(' ~ ', $this->date);
        $start = Carbon::parse($dates[0])->format('Y-m-d H:i:s');
        $end = Carbon::parse($dates[1])->format('Y-m-d H:i:s');
        $sort_criteria = [];
        if ($request->input('order') && ($request->input('order') == 'asc' || $request->input('order') == 'desc')) {
            $sort_criteria['price_1'] = $request->input('order');
        } elseif ($request->input('order') == 'immediate' || $request->input('order') == 'further') {
            $sort_criteria['distance'] = $request->input('order') == 'immediate' ? 'asc' : 'desc';
        }
        if ($request->seats) {
            $sort_criteria['number_of_seats'] = 'asc';
        }
        if ($request->doors) {
            $sort_criteria['number_of_doors'] = 'asc';
        }
        if ($request->age) {
            $sort_criteria['age'] = 'desc';
        }
        if (empty($sort_criteria)) {
            $request['order'] = 'immediate';
            $sort_criteria['distance'] = 'asc';
        }
        if (!$request->location) $request['location'] = 'Minsk';

        $data = \YaGeo::setQuery($request->location)->load();

        if (!$data->getResponse()) {
            $request['location'] = 'Minsk';
            $data = \YaGeo::setQuery($request->location)->load();
        }

        $lat = $data->getResponse() ? $data->getResponse()->getData()['Latitude'] : config('voyager.yandexmap.center.lat');
        $lng = $data->getResponse() ? $data->getResponse()->getData()['Longitude'] : config('voyager.yandexmap.center.lng');
        $request['lat'] = $lat;
        $request['lng'] = $lng;

        $car_types = \Cache::remember('car_types', setting('site.cache_time'), function () {
            return CarType::get();
        });

        $cars = Car::published()->where(function ($query) use ($request, $car_types, $lat, $lng, $start, $end) {
            $request['map-cars'] = $query->published()->where('deleted_at', null)->get();
            $query->whereNotIn('id', function ($query) use ($start, $end) {
                $query->select('car_id')
                    ->from(with(new OrderDate())->getTable())
                    ->where(function ($query) use ($start, $end) {
                        $query->whereBetween('start', [$start, $end])
                            ->orWhere(function ($q) use ($start, $end) {
                                $q->whereBetween('end', [$start, $end]);
                            })
                            ->orWhere(function ($q) use ($start, $end) {
                                $q->whereBetween('end', [$start, $end]);
                            })
                            ->orWhere(function ($q) use ($start, $end) {
                                $q->whereDate('start', '<=', $start)->whereDate('end', '>=', $end);
                            });

                    });
            });
            if ((!$request->instant_booking && $request->on_request) || ($request->instant_booking && !$request->on_request)) {
                if ($request->instant_booking) {
                    $query->where('on_request', 0);
                } else {
                    $query->where('on_request', 1);
                }
            }
            if ($request->location && $request->location != 'Беларусь' && $request->location != 'Belarus') {
                $query->closeTo($lat, $lng, 50000);
            }
            if ($request->fuel && is_array($request->fuel)) {
                $query->whereIn('fuel', $request->fuel);
            }
            if ($request->seats && is_array($request->seats)) {
                $query->whereIn('number_of_seats', $request->seats);
            }
            if ($request->price) {
                $query->where('price_1', '<=', getBackCurrency($request->price));
            }
            if ($request->fuel_consumption_min || $request->fuel_consumption_max) {
                $query->where('fuel_consumption', '<=', $request->fuel_consumption_max ?? $query->max('fuel_consumption'))->where('fuel_consumption', '>=', $request->fuel_consumption_min ?? 0);
            }
            if ($request->input('attributes') && is_array(request()->input('attributes')) && !empty($request->input('attributes'))) {
                $query->whereHas('filters', function ($q) use ($request) {
                    $q->whereIn('filters.id', $request->input('attributes'));
                });
            }
            if ($request->input('age')) {
                $query->where('age', '>=', date("Y") - $request->input('age'));
            }
            if ($request->input('seats')) {
                $query->where('number_of_seats', '>=', $request->input('seats'));
            }
            if ($request->input('doors')) {
                $query->where('number_of_doors', '>=', $request->input('doors'));
            }
            $request['data'] = $query->published()->where('deleted_at', null)->get();
            if ($request->type) {
                $query->whereIn('car_type_id', $request->type);
            }
            if ($request->transmission) {
                $query->where('transmission', $request->transmission);
            }
        })->distance($lat, $lng)->ofSort($sort_criteria)->paginate(12);

        $filters = \Cache::remember('car_filters', setting('site.cache_time'), function () {
            return Filter::get();
        });
        $cars->appends([
            'type' => $request->type,
            'transmission' => $request->transmission,
            'fuel' => $request->fuel,
            'seats' => $request->seats,
            'doors' => $request->doors,
            'instant_booking' => $request->instant_booking,
            'on_request' => $request->on_request,
            'price' => $request->price,
            'fuel_consumption_min' => $request->fuel_consumption_min,
            'fuel_consumption_max' => $request->fuel_consumption_max,
            'attributes' => $request->input('attributes'),
            'age' => $request->input('age'),
            'date' => $request->input('date'),
            'location' => $request->input('location'),
        ]);
        if ($request->ajax()) {
            $html = view('cars.search.search-result', compact('cars', 'request', 'car_types', 'filters'))->render();
            return response()->json(['success' => true, 'html' => $html], 200);
        } else {
            if ($loqcation){
                $metaTags = $loqcation;
            }else{
                $metaTags = getMetaTags('search_page');
            }
            return view('cars.search.index', compact('cars', 'filters', 'car_types', 'metaTags', 'loqcation'));
        }

    }

    public function show(Request $request, $slug)
    {
        $dates = explode(' ~ ', $this->date);
        $start = Carbon::parse($dates[0]);
        $end = Carbon::parse($dates[1]);
        $request['days'] = $days = $end->addMinutes(-31)->diffInDays($start) + 1;
        $start = Carbon::parse($dates[0]);
        $end = Carbon::parse($dates[1]);
        $request['start_day'] = $start;
        $request['end_day'] = $end;
        $car = Car::published()->where('slug', $slug)->firstOrFail();
        $tariffPlan = [];
        foreach ($car->filters as $filter) {
            if ($filter->pivot->price <= 0) {
                $tariffPlan[] = ['name' => $filter->translate()->title];
            }
        }
        foreach ($car->insurances as $insurances) {
            if ($insurances->pivot->price <= 0) {
                $tariffPlan[] = ['name' => $insurances->translate()->name, 'desc' => '<span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="' . str_replace(['%franchise_price%', '"'], [currency($insurances->pivot->franchise), "'"], $insurances->translate()->description) . '"><i class="fas fa-info-circle"></i></span>'];
            }
        }

        $green_card = json_decode($car->green_card ?? '[]', true);

        if (count($green_card)) {
            foreach ($green_card as $key => $item) {
                if ($item[getGreenCardDayKey(request()->input('days'))] <= 0) {
                    $tariffPlan[] = ['name' => 'Грин Карта' . config('car.green_card.country.' . $key)];
                }
            }
        }
        if ($car) {
            \Counter::showAndCount('car', $car->id);
            return view('cars.show', compact('car', 'tariffPlan'));
        }
        return redirect()->back();
    }


    public function getPrices(Request $request, Car $car)
    {
        $dates = CarOrderRepository::getDate($this->date);
        $request['days'] = CarOrderRepository::getDays(CarOrderRepository::getDate($this->date));
        $request['start_day'] = $dates['start'];
        $request['end_day'] = $dates['end'];
        if ($request->ajax() && $request->method() == 'GET') {
            $data = CarOrderRepository::getHtml($request, $car, $this->date);
            return response()->json($data, 200);
        }
        if (\Auth::guest() || !profileIsFull(auth()->user()->profile, true)) {
            return response()->json(['error' => 'У вас не праверений Профиль ']);
        }
        if (!$request->ajax()) {
            $order = CarOrderRepository::getOrder($request->barcode);

            CarOrderRepository::fillingOrder($request->all(), $order, $car, $dates, auth()->user());


            if ($order->status == 'requested') {

                $order->car->owner->notify(new CarRequestOwner($order));
                $users = User::where('role_id', 1)->get();
                Notification::send($users, new CarRequestAdmin($order));

                return back()->with([
                    'status' => __('car.request_to_car'),
                    'autoHideDisabled' => true
                ]);
            } elseif ($order->status != 'confirmed') {
                $result = json_decode(MpiService::sendPayment($order, 'reservation'), true);
                if ($result['errorCode'] == 0) {
                    $order->transaction_id = $result['orderId'];
                    $order->save();
                    return redirect($result['formUrl']);
                } else {
                    $order->status = 'failed';
                    $order->save();
                    return back()->with([
                        'status' => __('car.order_registration_failed'),
                        'alert_type' => 'warning',
                    ]);
                }
            }
        } else {
            return response()->json(['success' => true]);
        }


    }

    public function similarCars(Request $request, Car $car)
    {
        if ($request->ajax()) {
            $selected_car = $car;
            $dates = explode(' ~ ', $this->date);
            $start = Carbon::parse($dates[0])->format('Y-m-d H:i:s');
            $end = Carbon::parse($dates[1])->format('Y-m-d H:i:s');
            $cars = Car::where('id', '!=', $car->id)
                ->published()
                ->closeTo($car->getCoordinates()[0]['lat'], $car->getCoordinates()[0]['lng'], 50000)
                ->where(function ($query) use ($request, $start, $end) {

                    $query->whereNotIn('id', function ($query) use ($start, $end) {
                        $query->select('car_id')
                            ->from(with(new OrderDate())->getTable())
                            ->where(function ($query) use ($start, $end) {
                                $query->whereBetween('start', [$start, $end])
                                    ->orWhere(function ($q) use ($start, $end) {
                                        $q->whereBetween('end', [$start, $end]);
                                    })
                                    ->orWhere(function ($q) use ($start, $end) {
                                        $q->whereBetween('end', [$start, $end]);
                                    })
                                    ->orWhere(function ($q) use ($start, $end) {
                                        $q->whereDate('start', '<=', $start)->whereDate('end', '>=', $end);
                                    });

                            });
                    });
                })
                ->distance($car->getCoordinates()[0]['lat'], $car->getCoordinates()[0]['lng'])
                ->get();
            $html = view('cars.cars-map', compact('cars', 'selected_car'))->render();

            return response()->json(['html' => $html]);
        } else {
            return abort(403);
        }
    }

}
