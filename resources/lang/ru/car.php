<?php
return [
    'pick_date' => 'Дата подбора',

    'main' => 'Основные',
    'open_filters' => 'Открыть Фильтры',
    'closed_filters' => 'Закрыт Фильтры',
    'price' => 'Цена',
    'currency_can_be' => 'Валюту можно менять в подвале',
    'transmission' => 'Коробка передач',
    'year' => 'Год выпуска',
    'seats' => 'Количество мест',
    'door_count' => 'Количество Дверей',
    'doors' => 'Дверей',
    'consumption' => 'Расход топлива',
    'min' => 'мин. -  :i',
    'max' => 'Макс. -  :i',
    'fuel' => 'Топливо',
    'more' => 'Больше',
    'less' => 'меньше',
    'instantly' => 'Мгновенно',
    'request' => 'Запрос',
    'empty_result' => 'По вашему запросу ничего не найдено. Измените настройки фильтров.',

    'type' => 'Тип',
    'all' => 'Все',
    'sort_by' => 'Сортировать по',
    'open_map' => 'Открыть карту',
    'close_map' => 'Закрыть карту',

    'day' => 'День',
    'rent_it' => 'Арендовать',
    'similar' => 'Похожие',

    'receipt' => 'Получение и возврат',
    'receive' => 'Получить',
    'airport' => 'Аеропорт',
    'aero' => 'АИР аеро',
    'railway' => 'Вокзал',
    'return' => 'Вернуть',
    'days' => 'Дней',
    'day_more' => 'день и более',
    'spec' => 'Характеристики',
    'drive_unit' => 'Привод',
    'power' => 'Мощность',
    'add_eq' => 'Доп. оборудование',
    'option' => 'Опция',
    'driver_data' => 'Данные водителя',
    'last_name' => 'Имя',
    'first_name' => 'Фамилия',
    'birthday' => 'Дата Рождения',
    'email' => 'E-mail',
    'phone' => 'Телефон',
    'flight_number' => 'Номер рейса',
    'your_comment' => 'Ваш коментарий',
    'to_pay' => 'Оплатить',

    'blog' => 'БЛОГ',
    'rent' => 'Аренда',
    'total' => 'Тотал',
    'deposit' => 'Депозит',
    'total_pay_now' => 'К оплате сейчас',
    'to_request' => 'ЗАПРОСИТЬ',
    'enabled' => 'Включено',
    'insurance' => 'Страховка',
    'go_to_profile_fill' => 'Перейти к заполнению профиля',
    'login_as_tenant' => 'Для аренды нужно войти как арендатор',
    'waiting_for_an_answer' => 'в ожидании ответа',
    'busy' => 'Занят',
    'request_to_car' => 'Ваш запрос отправлен администратору скоро вы получите ответ по электронной почте.',
    'order_registration_failed' => 'регистрация заказа не удалась просьба попробовать еще раз',
    'mark' => 'Mарка',
    'model' => 'Модель',
    'class' => 'Класс',
    'year_of_issue' => 'Год выпуска',
    'color' => 'Цвет',
    'average_fuel_consumption' => 'Средний расход топливо',
    'engine_capacity' => 'Объем Двигателя',
    'trunk' => 'Багажник',
    'types_of_ody' => 'Типы Кузова',
    'petrol' => 'Бензин',
    'diesel' => 'Дизель',
    'gas' => 'Газ',
    'electric' => 'Электрический',
    'hybrid' => 'Гибрид',
    'green_card_rus' => 'Грин Карта Россия',
    'green_card_eur' => 'Грин Карта Европа',
    'green_card_ukr_mold' => 'Грин Карта Украина и Молдова',
    'mileage_limit' => 'Лимит на пробег',
    'do_you_really_want_to_cancel' => 'Вы действительно хотите отменить',
    'write_a_reason' => 'Write a reason',
    'read_the_cancellation_policy_and_agree' => 'прочитал <a href=":link"> условие отмены </a> и согласен cancellation policy',
    'to_cancel_your_reservation_you_must_agree_to_the_terms' => 'для отмены бронирования вы должны согласиться с условиями',
    'reviews' => 'отзывы',
    'no_reviews' => 'Нет отзывов',
    'urgency' => 'Засрочность',
    'car_book_desc' => 'Бронирование%20аренды%20:car_title%20c%20:start%20по%20:end',
    'car_full_pay_desc' => 'Полная%20оплата%20аренды%20:car_title%20c%20:start%20по%20:end',
    'in' => 'в',
    'description' => 'Описание',

    'expired' => '<p>Заказ снят с бронирования т.к. <br>
    в течении 2 часов не произведена оплата. <br>
    Повторите процедуру бронирования автомобиля.</p>',

    'error_status' => [
        'publish' => 'Опубликован',
        'moderation' => 'На модерации',
        'error' => 'Ошибка'
    ]

];
