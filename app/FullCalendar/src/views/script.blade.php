<script>
     options  =  {!! $options !!} ;

     options.customButtons ={
         makeOrder: {
            text: 'Добавить Занятость!',
            click: function() {
                $('#makeOrder').modal('show');
            }
        }
    };

    options.eventRender = function(info, element) {
        $(info.el).tooltip({
             title: info.event.extendedProps.description,
             placement: 'top',
             trigger: 'hover',
             container: 'body'
         });
    };


    var calID = document.getElementById('calendar-{{ $id }}');
     calendar = new FullCalendar.Calendar(calID, options);
    calendar.render()

</script>
