@extends('layouts.app')
{{--meta tags--}}
@section('meta')
        <title>{{$page->myTranslate()->meta_title}}</title>
        <meta name="keywords" content="{{$page->myTranslate()->meta_keyword}}">
        <meta name="description" content="{{$page->myTranslate()->meta_description}}">

        <meta property="og:locale" content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="{{$page->myTranslate()->meta_title}}"/>
        <meta property="og:description" content="{{$page->myTranslate()->meta_description}}"/>
        <meta property="og:image" content="{{Voyager::image('img/Logo_my_rent_auto.png')}}"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
        <meta name="twitter:title" content="{{$page->myTranslate()->meta_title}}">
        <meta name="twitter:description" content="{{$page->myTranslate()->meta_description}}">
        <meta name="twitter:image" content="{{Voyager::image('img/Logo_my_rent_auto.png')}}">
{{--        <link href="{{ LaravelLocalization::getNonLocalizedURL(route('for-whom')) }}" rel="canonical">--}}

@stop
{{--page title--}}
@section('title')
    {{--    {{$page->translate()->title}}--}}
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
@stop
@section('content')
    <div class="my-container my-5 p-3 p-md-5 bg-white border-radius-20">
        <h1 class="text-center">{{$page->myTranslate()->title}}</h1>
        <div class="d-flex flex-wrap">
            <div class="col-md-12 ff-ms">
                {!! $page->myTranslate()->announcement !!}
            </div>
            <div class="col ">
                <h2 class="text-center">{{$page->myTranslate()->tenant_title}}</h2>
               <div class="col-12 ff-ms">
                   {!! $page->myTranslate()->tenant_content !!}
               </div>
            </div>
            <div class="col-12 border-t d-flex w-100 d-md-none my-4">
                <div class="separator-4"></div>
                <div class="separator-5 mx-3"></div>
                <div class="separator-6"></div>
            </div>
            <div class="d-none flex-column d-md-flex ">
                <span class="separator-1 mx-auto"></span>
                <img src="{{ownMakeImage(60 , 60 ,85,'img/logo-arrow-left.png', 'png')}}" alt="MyRentAuto" >
                <span class="separator-2 mx-auto"></span>
                <img src="{{ownMakeImage(60 , 60 ,85,'img/logo-arrow-right.png', 'png')}}" alt="MyRentAuto" >
                <span class="separator-3 mx-auto"></span>
            </div>
            <div class="col">
                <h2 class="text-center">{{$page->myTranslate()->renter_title}}</h2>
               <div class="col-12 ff-ms">
                   {!! $page->myTranslate()->renter_content !!}
               </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@stop
