<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">
<head>
    <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="assets-path" content="{{ route('voyager.assets') }}"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/x-icon">


    <!-- App CSS -->
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-app.css') }}">
    {{--    <link rel="stylesheet" href="{{ asset('css/fullcalendar/timegrid/main.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/fullcalendar/daygrid/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fullcalendar/bootstrap/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fullcalendar/list/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/webui-popover/2.1.15/jquery.webui-popover.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>


    @yield('css')
    @if(config('voyager.multilingual.rtl'))
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/fullcalendar/main.min.css') }}">
    <!-- Few Dynamic Styles -->
    <style type="text/css">
        .voyager .side-menu .navbar-header {
            background: {{ config('voyager.primary_color','#22A7F0') }};
            border-color: {{ config('voyager.primary_color','#22A7F0') }};
        }

        .widget .btn-primary {
            border-color: {{ config('voyager.primary_color','#22A7F0') }};
        }

        .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus {
            background: {{ config('voyager.primary_color','#22A7F0') }};
        }

        .voyager .breadcrumb a {
            color: {{ config('voyager.primary_color','#22A7F0') }};
        }
    </style>

    @if(!empty(config('voyager.additional_css')))<!-- Additional CSS -->
    @foreach(config('voyager.additional_css') as $css)
        <link rel="stylesheet" type="text/css" href="{{ asset($css) }}">@endforeach
    @endif

    @yield('head')
</head>

<body class="voyager @if(isset($dataType) && isset($dataType->slug)){{ $dataType->slug }}@endif">

<div id="voyager-loader">
    <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
    @if($admin_loader_img == '')
        <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
    @else
        <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
    @endif
</div>

<?php
if (starts_with(app('VoyagerAuth')->user()->avatar, 'http://') || starts_with(app('VoyagerAuth')->user()->avatar, 'https://')) {
    $user_avatar = app('VoyagerAuth')->user()->avatar;
} else {
    $user_avatar = Voyager::image(app('VoyagerAuth')->user()->avatar);
}
?>

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
        @include('voyager::dashboard.navbar')
        @include('voyager::dashboard.sidebar')
        <script>
            (function () {
                var appContainer = document.querySelector('.app-container'),
                    sidebar = appContainer.querySelector('.side-menu'),
                    navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                    loader = document.getElementById('voyager-loader'),
                    hamburgerMenu = document.querySelector('.hamburger'),
                    sidebarTransition = sidebar.style.transition,
                    navbarTransition = navbar.style.transition,
                    containerTransition = appContainer.style.transition;

                sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                        navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                if (window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                    appContainer.className += ' expanded no-animation';
                    loader.style.left = (sidebar.clientWidth / 2) + 'px';
                    hamburgerMenu.className += ' is-active no-animation';
                }

                navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
            })();
        </script>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                @yield('page_header')
                <div id="voyager-notifications"></div>
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('voyager::partials.app-footer')

<!-- Javascript Libs -->


{{--<script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('vendor/voyager/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script>
            @if(Session::has('alerts'))
    let alerts = {!! json_encode(Session::get('alerts')) !!};
    helpers.displayAlerts(alerts, toastr);
    @endif

    @if(Session::has('message'))

    // TODO: change Controllers to use AlertsMessages trait... then remove this
    var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
    var alertMessage = {!! json_encode(Session::get('message')) !!};
    var alerter = toastr[alertType];

    if (alerter) {
        alerter(alertMessage);
    } else {
        toastr.error("toastr alert-type " + alertType + " is unknown");
    }
    @endif
</script>
@include('voyager::media.manager')
@include('voyager::menu.admin_menu')
<script>
    new Vue({
        el: '#adminmenu',
    });
    var userId = '{{auth()->id()?? true }}';
</script>
@include('sweetalert::alert')
<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
<script src="{{asset('js/fileinput/piexif.min.js')}}"></script>
<script src="{{asset('js/fileinput/purify.min.js')}}"></script>
<script src="{{asset('js/fileinput/sortable.min.js')}}"></script>
<script src="{{asset('js/fileinput/fileinput.min.js')}}"></script>
<script src="{{asset('js/fileinput/locales/ru.js')}}"></script>
{{--<script src="{{asset('js/fullcalendar.js')}}"></script>--}}
<script src="{{asset('js/fullcalendar/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/interaction/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/bootstrap/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/daygrid/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/timegrid/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/list/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/luxon/main.min.js')}}"></script>
<script src="{{asset('js/fullcalendar/locales/ru.js')}}"></script>
<script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>
{{--exif js--}}
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<script>
    var $uploadCrop, tempFilename, rawImg, imageId;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');
                $('#cropImagePop').modal('show');
                rawImg = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 400,
            height: 250,
        },
        // enforceBoundary: false,
        enableExif: true,

    });
    $('#cropImagePop').on('shown.bs.modal', function () {
        $uploadCrop.croppie('bind', {
            url: rawImg
        }).then(function () {
            console.log('jQuery bind complete');
        });
    });
    $('.item-img').on('change', function () {
        imageId = $(this).data('id')
        var ext = $(this).val().split('.').pop().toLowerCase();
        var error_message;
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
           error_message = 'Запрещенное расширение Только "gif, png, jpg, jpeg" разрешены.';
        } else {
            var picsize = (this.files[0].size);
            if (picsize > 3000000) {
                error_message = 'Файл превышает максимальный размер 5 МБ.';
            } else {
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                readFile(this);
            }
        }
        if (error_message){
            $('#' + imageId).find('.error_content').html('' +
                '<div class="alert alert-danger alert-dismissible fade in">\n' +
                '    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                error_message +
                '  </div>');
        }


    });

    $('#cropImageBtn').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: {width: 1500, height: 938}
        }).then(function (resp) {
            $('[data-status="' + imageId + '"]').attr('src', resp);
            $('#cropImagePop').modal('hide');
            $('.' + imageId).val(resp)
        });
    });
    // End upload preview image

    $('.file-image').fileinput({
        language: "ru",
        showUpload: false,
        maxFileSize: 5120,
        allowedFileExtensions: ['gif', 'png', 'jpg', 'jpeg'],
        dropZoneEnabled: false,
        showCaption: false,
    });
</script>
@yield('javascript')
@stack('javascript')
@if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
@foreach(config('voyager.additional_js') as $js)
    <script type="text/javascript" src="{{ asset($js) }}"></script>@endforeach
@endif

</body>
</html>
