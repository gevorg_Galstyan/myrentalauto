<section class="section-reviews">
    <h4 class="text-center text-uppercase">{{__('car.reviews')}}</h4>
    <div class="row p-3 {{!$car->reviews->count() ? 'justify-content-center' : ''}} ">
        @forelse($car->reviews as $review)
            <div class="col-md-3">
                <div class="fs-14 border p-2 shadow-lg p-3 bg-white rounded">
                    <div class="">
                        <div class="d-flex justify-content-start mb-1">
                            <div class="p-2 align-self-center">
                                <img src="{{Voyager::image($review->author->avatar??'users/default.png')}}"
                                     class="img rounded-circle img-fluid" width="50px" height="50px"/>
                            </div>

                            <div class="align-self-center">
                                <strong>{{$review->author->full_name}}</strong>
                                <div class="align-self-center">
                                    <span class="text-secondary ">{{$review->created_at->diffForHumans()}}</span>
                                </div>
                                <div class=" align-self-center">
                                    <span class=""><i class="color-orange {{$review->score >= 1 ? 'fas' : 'far'}} fa-star"></i></span>
                                    <span class=""><i class="color-orange {{$review->score >= 2 ? 'fas' : 'far'}} fa-star"></i></span>
                                    <span class=""><i class="color-orange {{$review->score >= 3 ? 'fas' : 'far'}} fa-star"></i></span>
                                    <span class=""><i class="color-orange {{$review->score >= 4 ? 'fas' : 'far'}} fa-star"></i></span>
                                    <span class=""><i class="color-orange {{$review->score >= 5 ? 'fas' : 'far'}} fa-star"></i></span>
                                </div>
                            </div>
                        </div>
                        <p class="mb-0 pl-2 ff-ms more">{{$review->body}}</p>

                    </div>
                </div>
            </div>
        @empty
            <p class="text-center">{{__('car.no_reviews')}} </p>
        @endforelse
    </div>
</section>

