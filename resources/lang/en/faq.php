<?php
return [
    'themes' => 'Themes',
    'questions' => 'Questions',
    'did_this_article_help_you' => 'Did this article help you',
    'yes' => 'Yes',
    'no' => 'No',
    'you_rated_this_article' => 'You rated this article. thank you for rating.',
    'may_be_useful' => 'May be useful',
    'info' => 'If you did not find the answer to your question, we will answer it by e-mail. Just write to  <a href="mailto:info@myrentauto.com">info@myrentauto.com</a>',
    'search_for_a_question_here' => 'Search for a question here',
    'thanks_for_rating_this_will_hel_us_to_become_better' => 'Thanks for rating this will help us to become better',
    'tenant' => 'Tenant',
    'owner' => 'Owner',
    'tenant_popular_question' => 'Hot issues tenants',
    'owner_popular_question' => 'Hot issues owners',
    'tenant_categories' => 'Hot Topics tenants',
    'owner_categories' => 'Hot Topics owners',
];
