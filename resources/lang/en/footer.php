<?php
return [
    'about_us' => 'Main',
    'start' => 'Go',
    'services' => 'Services',
    'faq' => 'Questions',
    'blog' => 'Blog',
    'contacts' => 'Contacts',
    'conditions' => 'Conditions',
    'important' => 'Important',
    'cars' => 'Cars',
    'for_car_owners' => 'For car owners',
    'for_tenants' => 'For tenants',
    'insurance' => 'Insurance',
    'lease_contract' => 'Lease contract',
    'currency' => 'Currency',
    'ip' => 'IP Pavlov O.V № 193279993 R/s: BY15OLMP30130001043200000933 в OAO «Belgazprombank» SWIFT OLMPBY2X',
];
