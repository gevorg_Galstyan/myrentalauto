{!! Robots::metaTag() !!}

<meta charset="utf-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="Content-Language" content="en"/>
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="theme-color" content="#4188c9">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<title>
    @section('title')
        {{ config('app.name', 'Laravel') }}
    @show
</title>
<link rel="icon" href="./favicon.ico" type="image/x-icon"/>

<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico"/>
<!-- Generated: 2019-04-04 16:57:42 +0200 -->
<title>Homepage - tabler.github.io - a responsive, flat and full featured admin template</title>
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

<link rel="stylesheet" href="{{ asset('css/fullcalendar/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/fullcalendar/bootstrap/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/fullcalendar/list/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/fullcalendar/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/fullcalendar/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('owner/css/style.css') }}">
{{--<link rel="stylesheet" href="{{ asset('css/main.css') }}">--}}
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/daterangepicker.min.css') }}">


<link href="{{asset('css/owner-app.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{ asset('css/owner.css') }}">
<link href="{{asset('owner/css/image_upload.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{ asset('css/fileinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/fileinput/themes/explorer/theme.css') }}">

<!-- c3.js Charts Plugin -->
{{--<link href="{{asset('owner/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />--}}

<!-- Google Maps Plugin -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


@section('meta')

@show
@section('script_top')
@show
@section('style')

@show
