<header class="bg-white {{request()->url() != route('home') ? 'position-relative bg-white search-page ' : ''}}">
    <nav class="navbar navbar-expand-lg navbar-light bg-transparent container-fluid py-0 own-nav">
        {{--        <a class="navbar-brand" href="{{route('home')}}">{{ config('app.name', 'My Rent Auto') }}</a>--}}
        {{--        <a class="navbar-brand" href="{{route('home')}}">--}}
        {{--            <img src="{{asset('storage/img/logo.png')}}" alt="MyRentAuto" width="60px">--}}
        {{--            <span class="own-blue">My</span><span class="own-orange">Rent</span><span class="own-blue">Auto</span>--}}
        {{--        </a>--}}
        {{--        <button class="navbar-toggler own-navbar" type="button" data-toggle="collapse"--}}
        {{--                data-target="#navbarSupportedContent"--}}
        {{--                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--}}
        {{--            <span class="navbar-toggler-icon"></span>--}}
        {{--        </button>--}}


        <a class="navbar-brand d-none d-lg-block pl-0 p-0 ml-4"
           href="{{request()->url() == route('home') ? '#' : route('home')}}">
            <img src="{{ownMakeImage(50 , 50 ,85,'img/logo.png', 'png')}}" alt="MyRentAuto" >
        </a>


        <div class="d-block d-lg-none row w-100">
            <div class="row align-items-center justify-content-between">
                <div class="col-3 d-flex  logo ">
                    <a class="navbar-brand p-0" href="{{route('home')}}">
                        <img src="{{ ownMakeImage(50 , 50 , 85,'img/logo.png', 'png') }}" alt="MyRentAuto">
                        {{--                        <span class="d-none d-md-inline-block">--}}
                        {{--                            <span class="own-blue">My</span><span class="own-orange">Rent</span><span class="own-blue">Auto</span>--}}
                        {{--                        </span>--}}
                    </a>
                </div>

                <div class="col-6 align-self-center row justify-content-center">
                    <a class="p-0 m-0 black call-num own-blue text-nowrap d-flex"
                       href="tel:{{str_replace(['(', ')', ' '], '', setting('site.header_phone_number'))}}">
                        <span class="d-none d-sm-inline-block phone-icon"></span>
{{--                        <img src="{{ownMakeImage(50, 0 , 85, 'img/icons/call.png')}}" class="d-none d-sm-inline-block"--}}
{{--                             alt="phone number" width="25"/>--}}
                        <span class="d-inline-block">{{setting('site.header_phone_number')}}</span>
                    </a>
                </div>
                <div class="col-3 pr-0 align-self-center row justify-content-end">
                    <button class="navbar-toggler own-navbar " type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </div>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="m-auto">
                {{--<a href="#0"
                   class="call fs-14 ml-2 black">--}}
                <a class="p-0 m-0 black call-num own-blue text-nowrap d-flex"
                   href="tel:{{str_replace(['(', ')', ' '], '', setting('site.header_phone_number'))}}">
                    <span class="pl-4 align-self-end phone-icon d-inline-block"></span>
{{--                    <img src="{{ownMakeImage(50, 0 , 85, 'img/icons/call.png')}}" class="pl-4 align-self-end" alt="phone number" width="50"/>--}}
                    {{--</a>

                    <a class="p-0 m-0 black call-num own-blue"
                       href="tel:{{str_replace(['(', ')', ' '], '', setting('site.header_phone_number'))}}">--}}
                    <span class="d-inline-block">{{setting('site.header_phone_number')}}</span>
                </a>
            </div>
            <div class=" d-flex">
                <ul class="navbar-nav mr-2 d-flex align-items-center">
                    <li class="nav-item active">
                        <a class="nav-link own-blue"
                           href="{{route('pages', ['slug' => 'o-service'])}}">{{__('header.services')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link own-blue" href="{{route('faq')}}">{{__('header.support')}}</a>
                    </li>
                    {{--<li class="nav-item">
                        <a class="nav-link own-blue" href="#">{{__('header.documentation')}}</a>
                    </li>--}}
                    <li class="nav-item dropdown ">
                        <a class="nav-link own-blue" href="#" id="navbardropa"
                           role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{__('header.documentation')}}
                        </a>
                        @if(count($documentPages) > 0)
                            <div class="dropdown-menu sm-menu mt-2 animate slideIn" aria-labelledby="navbardropa">
                                @foreach($documentPages as $page)
                                    <a class="dropdown-item own-dropdown-item own-blue fw-600"
                                       href="{{route('pages', ['slug' => $page->slug])}}">{{$page->myTranslate()->title}}</a>
                                @endforeach
                            </div>
                        @endif
                    </li>
                </ul>
                <div class=" d-flex flex-wrap align-items-center dropdown">
                    @guest
                        <a class="own-btn d-none d-xl-block" data-toggle="modal" href="javascript:void(0)" data-target="#loginModal">{{__('auth.login')}}</a>
                    @else
                        <a class="nav-link own-blue pr-1 mr-1 d-none d-xl-block nav-link"
                           title="{{__('header.personal_area')}}"
                           data-letters="{{mb_substr(auth()->user()->first_name,0, 1)??'O' }}"
                           @if (auth()->user()->role_id == 2)
                           id="profileDropdown" role="button" data-toggle="dropdown"
                           href="#"
                           @else
                           href="{{route((config('user-role.dashboard.'.auth()->user()->role->name)??'voyager.dashboard'))}}"
                            @endif>
                            @if (auth()->user()->role_id == 2)
                                @if (auth()->user()->profile->status == 'confirm')
                                    <span class="info-icon text-success">
                                   <i class="fas fa-check"></i>
                                </span>
                                @else
                                    <span class="info-icon text-warning">
                                    <i class="fas fa-exclamation"></i>
                                </span>
                                @endif
                            @endif
                        </a>
                        @if (auth()->user()->role_id == 2)
                            <div class="dropdown-menu mt-1 animate slideIn" aria-labelledby="profileDropdown">
                                <a class="dropdown-item own-dropdown-item own-blue fw-600"
                                   href="{{route((config('user-role.dashboard.'.auth()->user()->role->name)??'voyager.dashboard'))}}">
                                    {{__('header.profile')}}
                                </a>
                                <a class="dropdown-item own-dropdown-item own-blue fw-600"
                                   href="{{route('profile.history')}}">
                                    {{__('header.story')}}
                                </a>
                                <a class="dropdown-item own-dropdown-item own-blue fw-600"
                                   href="{{route('profile.favorites')}}">
                                    {{__('header.favorites')}}
                                </a>
                                @can ('post-read')
                                    <a class="dropdown-item own-dropdown-item own-blue fw-600"
                                       href="{{route('posts.index')}}">
                                        {{__('header.posts')}}
                                    </a>
                                @endcan
                                <a class="dropdown-item own-dropdown-item own-blue fw-600"
                                   href="{{route('logout')}}">
                                    <i class="fas fa-sign-out-alt"></i>
                                    {{__('auth.logout')}}
                                </a>

                            </div>
                        @endif
                    @endguest
                    <div class="">
                        <a class="mobile-menu-toggle js-toggle-menu hamburger-menu ml-4" href="#">
                            <span class="menu-item"></span>
                            <span class="menu-item"></span>
                            <span class="menu-item"></span>
                        </a>
                    </div>
                    <div class="position-relative lang-relative ml-3">
                        <form class="lang position-absolute" method="get">
                            @if (LaravelLocalization::getCurrentLocale() == 'en')
                                <button type="submit" class="en lang-block">en</button>
                                <a href="{{LaravelLocalization::getLocalizedURL('ru', request()->url())}}"
                                    data-href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == 'en' ? 'ru': 'en', null, request()->all(), true) }}"
                                   class="ru lang-block lang">ru</a>
                            @else
                                <button type="submit" class="ru lang-block">ru</button>
                                <a href="{{LaravelLocalization::getLocalizedURL('en', request()->url())}}"
                                    data-href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() == 'en' ? 'ru': 'en', null, request()->all(), true) }}"
                                   class="en lang-block lang">en</a>
                            @endif

                        </form>

                    </div>
                </div>
            </div>

        </div>
    </nav>
    <div class="bord d-flex flex-wrap align-items-center justify-content-start">
        <ul class="bord-menu m-0 mt-3 w-100">
            <li class="hide-md d-flex align-items-center justify-content-center mobile-lang-bar">
                <a href="{{LaravelLocalization::getLocalizedURL('ru', request()->url())}}"
                    data-href="{{LaravelLocalization::getLocalizedURL( 'ru', null, request()->all(), true) }}"
                   class="{{LaravelLocalization::getCurrentLocale() == 'ru' ? 'active':''}} hreflang">ru</a>
                <a href="{{LaravelLocalization::getLocalizedURL('en', request()->url())}}"
                    data-href="{{LaravelLocalization::getLocalizedURL( 'en', null, request()->all(), true) }}"
                   class="{{LaravelLocalization::getCurrentLocale() == 'en' ? 'active':''}} hreflang">en</a>
            </li>
            <li class="d-xl-none d-flex flex-column justify-content-start">
                @auth
                    <a class="nav-link own-blue pr-1 mr-1 text-left {{auth()->user()->role_id == 2 ? 'dropdownWithClick' : ''}}"
                       data-letters="{{auth()->user()->first_name[0]??'O' }}"
                       title="{{__('header.personal_area')}}"
                       @if (auth()->user()->role_id == 2)
                       data-dropdown="profile"
                       id="profileDropdown" role="button" data-toggle="dropdown"
                       href="#"
                       @else
                       href="{{route((config('user-role.dashboard.'.auth()->user()->role->name)??'voyager.dashboard'))}}"
                        @endif>
                        @if (auth()->user()->role_id == 2)
                            @if (auth()->user()->profile->status == 'confirm')
                                <span class="info-icon text-success">
                                   <i class="fas fa-check"></i>
                                </span>
                            @else
                                <span class="info-icon text-warning">
                                    <i class="fas fa-exclamation"></i>
                                </span>
                            @endif
                        @endif
                        {{auth()->user()->first_name}}
                    </a>
                    @if (auth()->user()->role_id == 2)
                        <div class="collapse-dropdown collapse ml-5 text-left" data-status="profile">
                            <ul class="m-0 p-0">
                                <li>
                                    <a class="fs-14"
                                       href="{{route((config('user-role.dashboard.'.auth()->user()->role->name)??'voyager.dashboard'))}}">
                                        {{__('header.profile')}}
                                    </a>
                                </li>
                                <li>
                                    <a class="fs-14"
                                       href="{{route('profile.history')}}">
                                        {{__('header.story')}}
                                    </a>
                                </li>
                                <li>
                                    <a class="fs-14"
                                       href="{{route('profile.favorites')}}">
                                        {{__('header.favorites')}}
                                    </a>
                                </li>
                                @can ('post-read')
                                    <li>
                                        <a class="fs-14"
                                           href="{{route('posts.index')}}">
                                            {{__('header.posts')}}
                                        </a>
                                    </li>
                                @endcan

                                <li>
                                    <a class="fs-14"
                                       href="{{route('logout')}}">
                                        <i class="fas fa-sign-out-alt"></i>
                                        {{__('auth.logout')}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @endif
                @else
                    <a class="own-btn d-xl-non px-3 py-2  right-bar-login-button" data-toggle="modal" href="javascript:void(0)"
                       data-target="#loginModal">{{__('auth.login')}}</a>
                @endauth


            </li>
            <li class="hide-md d-flex justify-content-start ml-3">
                <a href="{{route('pages', ['slug' => 'o-service'])}}">
                    {{__('header.services')}}
                </a>
            </li>
            <li class="hide-md d-flex justify-content-start ml-3"><a
                    href="{{route('faq')}}">{{__('header.support')}}</a></li>
            <li class="hide-md d-flex flex-wrap justify-content-start ml-3">
                <a href="#" class="dropdownWithClick" data-dropdown="documents">
                    {{__('header.documentation')}}

                </a>
                <div class="collapse-dropdown collapse ml-3 text-left" data-status="documents">
                    <ul class="m-0 p-0">

                        @if(count($documentPages) > 0)
                            @foreach($documentPages as $page)
                                <li>
                                    <a class="fs-14"
                                       href="{{route('pages', ['slug' => $page->slug])}}">{{$page->myTranslate()->title}}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </li>
            <li class="d-flex justify-content-start justify-content-lg-center ml-3"><a
                    href="{{route('for-whom')}}">{{__('header.for_whom')}}</a></li>
            <li class="d-flex justify-content-start justify-content-lg-center ml-3"><a
                    href="{{route('pages', ['slug' => 'team'])}}">{{__('header.team')}}</a></li>
            <li class="d-flex justify-content-start justify-content-lg-center ml-3"><a
                    href="{{route('posts')}}">{{__('header.blog')}}</a></li>
            <li class="d-flex justify-content-start justify-content-lg-center ml-3"><a
                    href="{{route('contact_us')}}">{{__('header.contacts')}}</a></li>
            @if (Auth::check() && auth()->user()->role_id != 2)
                <li><a href="{{route('logout')}}">{{__('auth.logout')}}</a></li>
            @endif
        </ul>
    </div>
</header>
