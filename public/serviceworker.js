var staticCacheName = "pwa-v" + new Date().getTime();
const CACHE = "pwabuilder-adv-cache";
const filesToCache = [
    '/offline',
    '/css/app.css',
    '/css/custom-style.css',
    '/js/app.js',
    '/js/custom.js',
    '/js/main.js',
    '/storage/img/logo-72x72.png',
    '/storage/img/logo-96x96.png',
    '/storage/img/logo-128x128.png',
    '/storage/img/logo-144x144.png',
    '/storage/img/logo-152x152.png',
    '/storage/img/logo-192x192.png',
    '/storage/img/logo-384x384.png',
    '/storage/img/logo.png',
];

// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match('offline');
            })
    )
});
self.addEventListener('push', function (e) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        //notifications aren't supported or permission not granted!
        return;
    }

    if (e.data) {
        var msg = e.data.json();
        e.waitUntil(self.registration.showNotification(
            msg.title, {
            body: msg.body,
            icon: msg.icon,
            actions: msg.actions
        }));
    }
});
