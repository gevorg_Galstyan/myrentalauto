@extends('owners.layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop

@section('content')

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Профиль пользователя - {{$customer->full_name}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="text-center">
                                <img src="{{Voyager::image($customer->avatar)}}" class="img-fluid" width="100%"
                                     alt="avatar">
                            </div>
                        </div>
                        <div class="col-md-9 personal-info ff-st d-flex flex-column">
                            <p class="row">
                                <b class="fw-700 col-lg-3">{{__('user.email')}}: </b>
                                <span class="fw-400 col-lg-9">{{$customer->email??''}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">{{__('user.phone')}}: </b>
                                <span class="fw-400 col-lg-9">{{$customer->number??''}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">{{__('user.birthday')}}: </b>
                                <span class="fw-400 col-lg-9">{{$customer->birthday??''}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">{{__('user.citizenship')}}: </b>
                                @php($citizenship = isset($customer->profile->citizenship)?$customer->profile->citizenship : null)
                                @php($citizenship = $citizenship ? getCountryName($citizenship) : '')
                                <span class="fw-400 col-lg-9">{{$citizenship}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">{{__('user.country')}}: </b>
                                {{--{{dd(getCountryName())}}--}}
                                @php($region = isset($customer->profile->region)?$customer->profile->region : null)
                                @php($country = isset($customer->profile->country)?$customer->profile->country : null)
                                @php($city = isset($customer->profile->city)?$customer->profile->city : null)
                                <span class="fw-400 col-lg-9">{{getCountryName($country)}}
                                    {{$region ? ', '.getStateName($region) : ''}}
                                    {{$city ? ', '.$city : ''}} </span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">{{__('user.driving_experience')}}: </b>
                                @php($experience = isset($customer->profile->experience)?$customer->profile->experience : null)
                                <span class="fw-400 col-lg-9">{{$experience ? date('Y') - $experience : '' }}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Картинки</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="all-border">
                                <legend><h6 class="mb-0"></h6></legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="profile-images">
                                            <a data-fancybox="gallery"
                                               href="{{Voyager::image($customer->profile->passport_one ?? 'img/icons/img.png')}}">
                                                <img src="{{Voyager::image($customer->profile->passport_one ?? 'img/icons/img.png')}}" alt="" class="img-fluid">
                                            </a>

                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="profile-images">
                                            <a data-fancybox="gallery"
                                               href="{{Voyager::image($customer->profile->passport_two ?? 'img/icons/img.png')}}">
                                                <img src="{{Voyager::image($customer->profile->passport_two ?? 'img/icons/img.png')}}" alt="" class="img-fluid">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="all-border text-right">
                                <legend><h6 class="mb-0"></h6></legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="profile-images">
                                            <a data-fancybox="gallery"
                                               href="{{Voyager::image($customer->profile->license_one ?? 'img/icons/img.png')}}">
                                                <img src="{{Voyager::image($customer->profile->license_one ?? 'img/icons/img.png')}}" alt="" class="img-fluid">
                                            </a>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="profile-images">
                                            <a data-fancybox="gallery"
                                               href="{{Voyager::image($customer->profile->license_two ?? 'img/icons/img.png')}}">
                                                <img src="{{Voyager::image($customer->profile->license_two ?? 'img/icons/img.png')}}" alt="" class="img-fluid">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(count($customerOrderedCars) > 0)
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">История</h3>

                        <div class="table-responsive">
                            <table class="table card-table table-vcenter text-nowrap datatable" id="ownerTables">
                                <thead>
                                <tr>
                                    {{--<th class="w-1">Id</th>--}}
                                    <th>Название</th>
                                    <th>Взята в аренду</th>
                                    <th>Изображение</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($customerOrderedCars as $car)
                                    <tr>
                                        {{--<td><span class="text-muted">{{$car->id}}</span></td>--}}
                                        <td>
                                            <a href="{{$car->slug ? route('car.single', $car->slug) : '#0'}}"
                                               class="text-inherit">{{$car->title}}</a>
                                        </td>
                                        <td>
                                            {{$car->orders[0]->start_date}}
                                        </td>
                                        <td>
                                            <img src="{{Voyager::image($car->image_1)}}" alt="" style="width: 100px;">

                                        </td>

                                        <td class="text-right">
                                            <a class=" ml-5 btn btn-primary getOrderInfo" href="#" data-id="{{$car->id}}">
                                                Инфо
                                            </a>
                                            <a class=" ml-5 btn btn-primary {{!$car->slug ? 'disabled' : ''}}" href="{{$car->slug ? route('car.single', $car->slug) : '#0'}}">
                                                Посмотреть
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-12" id="ordersInfo">

        </div>

    </div>


@stop

@section('script')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#ownerTables').DataTable({
                responsive: true,
            });

            $(".getOrderInfo").click(function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                $.ajax({
                    url: '{{route("customer.ajax", $customer)}}',
                    type: 'GET',
                    data: {'id': id},
                    success: function (data) {
                        $("#ordersInfo").html(data);
                       $("html, body").animate({scrollTop:  $("#ordersInfo").offset().top}, "1000")
                    }
                })
            })
        });


    </script>
    {{--<script>--}}

        {{--$('[data-toggle="tooltip" data-html="true"]').tooltip()--}}

        {{--$(document).ready(function () {--}}
            {{--$('#owner_carsTable').DataTable({--}}
                {{--responsive: true,--}}

                {{--"aoColumnDefs": [--}}
                    {{--{'bSortable': false, 'aTargets': [5, 6, 7, 8]}--}}
                {{--]--}}
            {{--});--}}
        {{--});--}}

    {{--</script>--}}

@stop
