<?php

namespace App\Widgets;

use App\Models\Car;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class CarDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        $cars = Car::where('deleted_at', null)->count();
        $deleted_cars = Car::where('deleted_at','!=', null)->count();
        $string = 'в проекте ' . ($cars + $deleted_cars) . ' автомобиля';
        $text = 'активные  '. $cars.'<br> удаленные  '. $deleted_cars;
        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'icon fas fa-car',
            'title' => "{$string}",
            'text' => $text,
            'button' => [
                'text' => 'Все автомобили',
                'link' => route('voyager.cars.index'),
            ],
            'image' => asset('storage/widget-backgrounds/cars.png'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return \Auth::user()->hasPermission('browse_cars');
    }
}
