<div class="my-container" id="faq-container">
    <div class="tab-content bg-white border-radius-20 p-3 p-md-5  mb-5" id="faq-content">
        <div class="tab-pane fade show active ">
            <div class="row ">
                <div class="col-lg-3 d-flex flex-wrap">
                    <div class="bg-white pb-2 col-12">
                        <h5 class="text-center fw-700 fs-30">{!! __('faq.themes') !!}</h5>
                        <div class="list-group list-group-flush">
                            @foreach($faqCategories as $category)
                                <a href="{{route('faq', ['for_whom' => $category->for_whom, 'cat' => $category->slug])}}"
                                   class="list-group-item list-group-item-action theme   {{($activeCat->slug == $category->slug) || ($loop->index == 0 && !$activeCat) ? 'active' : ''}}">
                                    {{$category->translate()->name}}
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12 align-self-end mt-3">
                        <p class="ff-ms d-none d-lg-block fs-14 mb-0">
                            {!! __('faq.info') !!}
                        </p>
                    </div>
                </div>
                @include('faqs.items')
                <div class="col-lg-12 d-block d-lg-none align-self-end mt-3">
                    <p class="ff-ms px-4 fs-12 mb-0">
                        {!! __('faq.info') !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
