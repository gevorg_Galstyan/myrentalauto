<div class="my-container" id="faq-container">
    <div class="bg-white border-radius-20 p-3 p-md-5  mb-5" id="faq-content">
        <ul class="nav nav-tabs border-0 w-100 row mx-0 justify-content-center mb-5" id="faq-general-tab" role="tablist">
            <li class="nav-item w-50 w-lg-25 w-md-200 w-sm-150 w-xs-130">
                <a class="nav-link left-border-radius-20 text-center py-2 fs-30 fw-500 fs-md-20 fs-lg-25 fs-sm-16" id="tenant-tab" data-toggle="pill"
                   href="#tenant" aria-selected="true">{{__('faq.tenant')}}</a>
            </li>
            <li class="nav-item w-50 w-lg-25 w-md-200 w-sm-150 w-xs-130">
                <a class="nav-link right-border-radius-20 text-center py-2 fs-30 fw-500 fs-md-20 fs-lg-25 fs-sm-16" id="car-owner-tab" data-toggle="pill"
                   href="#car-owner" aria-selected="false">{{__('faq.owner')}}</a>
            </li>
        </ul>

        <div class="tab-content shadow-none">
            <div class="tab-pane fade" id="tenant" role="tabpanel" aria-labelledby="tenant-tab">
                <div class="mb-5">
                    <h2 class="text-center h3">{{__('faq.owner_popular_question')}}</h2>
                    <ul class="">
                        @foreach ($categories['tenant']->faqPopular as $faqPopular)
                            <li class="py-1 border-bottom">
                                <a href="{{route('faq', ['for_whom' => 'tenant','cat' => $faqPopular->category->slug, 'question' => $faqPopular->slug])}}" class="">
                                    {{$faqPopular->translate()->title}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <h2 class="text-center h3 mb-4">{{__('faq.tenant_categories')}}</h2>
                <div class="d-flex flex-wrap">
                    @foreach($categories['tenant'] as $category)

                        <a href="{{route('faq', ['for_whom' => 'tenant','cat' => $category->slug])}}"
                           class="col row justify-content-center  rounded shadow-lg p-3 mb-5 mx-2 bg-white rounded w-100 mw-250">
                                        <span class="faq-icon mx-2  d-block"
                                              style="background: url({{$category->icon ? Voyager::image($category->icon) : asset('storage/img/icons/read.svg')}}) no-repeat"></span>
                            <span class="fs-sm-14 ff-st own-blue col-12 text-center">{{$category->translate()->name}}</span>
                        </a>

                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="car-owner" role="tabpanel" aria-labelledby="car-owner-tab">
                <div class="mb-5">
                    <h2 class="text-center h3">{{__('faq.owner_popular_question')}}</h2>
                    <ul class="">
                        @foreach ($categories['car-owner']->faqPopular as $faqPopular)
                            <li class="py-1 border-bottom">
                                <a href="{{route('faq', ['for_whom' => 'car-owner','cat' => $faqPopular->category->slug, 'question' => $faqPopular->slug])}}" class="">
                                    {{$faqPopular->translate()->title}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <h2 class="text-center h3 mb-4">{{__('faq.owner_categories')}}</h2>
                <div class="d-flex flex-wrap">
                    @foreach($categories['car-owner'] as $category)
                        <a href="{{route('faq', ['for_whom' => 'car-owner','cat' => $category->slug])}}"
                           class="col row justify-content-center  rounded shadow-lg p-3 mb-5 mx-2 bg-white rounded w-100 mw-250">
                                        <span class="faq-icon mx-2  d-block"
                                              style="background: url({{$category->icon ? Voyager::image($category->icon) : asset('storage/img/icons/read.svg')}}) no-repeat"></span>
                            <span class="fs-sm-14 ff-st own-blue col-12 text-center">{{$category->translate()->name}}</span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
