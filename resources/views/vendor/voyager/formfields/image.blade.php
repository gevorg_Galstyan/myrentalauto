@if($row->field == 'image_1' || $row->field == 'image_2' || $row->field == 'image_3' ||$row->field == 'image_4')
    <div class="col-xs-12" id="{{ $row->field }}">

        <div data-field-name="{{ $row->field }}"></div>
        <div class="error_content"></div>
        <label class="cabinet center-block">
            <figure data-field-name="{{ $row->field }}">
                @if($dataTypeContent->{$row->field})
                    <a href="#0" class=" remove-single-image-crop" style="position:absolute;">
                        <i class="far fa-times-circle fa-2x"></i>
                    </a>
                    <a class="pull-right" data-fancybox="gallery"
                       title="Увеличить"
                       href="{{Voyager::image($dataTypeContent->{$row->field})}}">
                        <i class="fas fa-search-plus  fa-2x"></i>
                    </a>
                @endif
                <img data-status="{{ $row->field }}"
                     src="@if( $dataTypeContent->{$row->field}){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{asset('storage/default/default.png')}}@endif"
                     class="gambar img-responsive img-thumbnail" id="item-img-output"
                     data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->id }}"/>
                <figcaption><i class="fa fa-camera"></i></figcaption>
            </figure>
            <input type="file" data-id="{{ $row->field }}" class="item-img hidden  center-block" name="file_photo"/>
            <input type="hidden" class="{{ $row->field }}" name="{{ $row->field }}">
        </label>

    </div>

    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Обрезать</h4>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
                </div>
            </div>
        </div>
    </div>
@else
    @if(isset($dataTypeContent->{$row->field}))
        <div data-field-name="{{ $row->field }}"  style="max-width:50%;">
            <a href="#0" class="remove-single-image" style="position:absolute; ">
                <i class="far fa-times-circle fa-2x"></i>
            </a>
            <a class="pull-right" data-fancybox="gallery"
               title="Увеличить"
               href="{{Voyager::image($dataTypeContent->{$row->field})}}">
                <i class="fas fa-search-plus fa-2x"></i>
            </a>
            <img src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif"
                 data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->id }}"
                 style="max-width:100%; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
        </div>
    @endif

        <input class="file-image"
               @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
               name="{{ $row->field }}" accept="image/*">

@endif
