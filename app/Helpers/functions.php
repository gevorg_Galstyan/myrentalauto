<?php

use http\Env\Request;

function distanceM($meter)
{
    if ($meter <= 1000) {
        return round($meter) . ' M';
    } else {
        return round(($meter / 1000), 2) . ' KM';
    }
}

function tooltips($toolips, $key, $param = null, $property = null)
{
    $toolip = $toolips->where('key', $key)->first();
    $text = $toolip ? $toolip->myTranslate()->value : "";
    $text = str_replace(["{params}", "%property%"], $param, $text);
//    $text = str_replace('"', "'", $text);

//    $text = str_replace("<p>", '', $text);
//    $text = str_replace("</p>", '', $text);
//    $text = str_replace('"', "'", $text);
//    if ($key == 'deposit_in_bank' || $key == 'deposit_in_cash'){
//        dd($text);
//    dump($text);
//}
//    $text = preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $text);

//    if ($key == 'deposit_in_bank' || $key == 'deposit_in_cash'){
//        dd($text);
//        dd($text);
//    }
    return $text;
}


function states($code)
{
    $states = null;
    if ($code) {
        try {
            $country = \MenaraSolutions\Geographer\Country::build($code);
            $states = $country->getStates()->setLocale(\Illuminate\Support\Facades\App::getLocale())->toArray();
        } catch (\Exception $exception) {
            $data = \YaGeo::setQuery($code)->load();
            if (isset($data->getResponse()->getData()['CountryNameCode'])) {
                $country = \MenaraSolutions\Geographer\Country::build($data->getResponse()->getData()['CountryNameCode']);
                $states = $country->getStates()->setLocale(\Illuminate\Support\Facades\App::getLocale())->toArray();
            }
        }
    }
    return $states;
}


function getStateName($code)
{

    try {
        $state = \MenaraSolutions\Geographer\State::build($code);
        $name = $state->setLocale(\Illuminate\Support\Facades\App::getLocale())->getName();
    } catch (\Exception $exception) {
        $name = $code;
    }
    return $name;
}

function profileIsFull($profile, $check_status = false)
{

    if (!$profile->experience || !$profile->country || !$profile->region || !$profile->city || !$profile->citizenship) {
        return false;
    }
    if ($check_status && $profile->status != 'confirm') {
        return false;
    }
    return true;
}

function profileIsDocExist($profile)
{

    if (!$profile->passport_one || !$profile->passport_two || !$profile->license_one || !$profile->license_two) {
        return false;
    }
    return true;
}


function getGreenCardDayKey($day)
{
    foreach (config('car.green_card.days') as $days_key => $days) {
        $day_array = explode('_', $days_key);
        if ($day >= $day_array[0] && $day <= $day_array[1]) {
            return $days_key;
        }

    }
    return end(config('car.green_card.days'));
}

function matchingDays($start, $end, $car)
{
    $data = [];
    $period = \Carbon\CarbonPeriod::create($start, $end);
    foreach ($period as $date) {
        if ($car->orderDates()->where('start', '<=', $date->format('Y-m-d H:i'))->where('end', '>=', $date->format('Y-m-d H:i'))->exists()) {
            $data[] = $date->format('d.m.Y');
        }

    }
    return $data;
}

function generateBarcodeNumber($increase = 1)
{

    $latestOrder = \App\Models\Order::orderBy('created_at', 'DESC')->first();
    $number = isset($latestOrder->id) && $latestOrder->id ? $latestOrder->id + $increase : 1;
    $number = str_pad($number, 8, "0", STR_PAD_LEFT);
    // call the same function if the barcode exists already
    if (barcodeNumberExists($number)) {
        $increase = ++$increase;
        return generateBarcodeNumber($increase);
    }

    // otherwise, it's valid and can be used
    return $number;
}

function barcodeNumberExists($number)
{
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return \App\Models\Order::where('barcode_number', $number)->exists();
}

function generateTicketNumber($increase = 1)
{

    $latestOrder = \App\Models\Ticket::orderBy('ticket_number', 'DESC')->first();
    $number = isset($latestOrder->id) && $latestOrder->id ? $latestOrder->id + $increase : 1;
    $number = str_pad($number, 8, "0", STR_PAD_LEFT);
    // call the same function if the barcode exists already
    if (ticketNumberExists($number)) {
        $increase = ++$increase;
        return generateTicketNumber($increase);
    }

    // otherwise, it's valid and can be used
    return $number;
}

function ticketNumberExists($number)
{
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return \App\Models\Ticket::where('ticket_number', $number)->exists();
}


function getMetaTags($key = '')
{
    $meta_tag = \Cache::remember('meta_tag_' . $key, setting('site.cache_time'), function () use ($key) {
        return \App\Models\MetaTag::where('name', $key)->first();
    });
    return $meta_tag;
}

function myTranslate($word, $target = false, $source = 'ru')
{
    if (!$target) $target = LaravelLocalization::getCurrentLocale();
    if (is_null($word)) $word = '';
    $tr = \YandexTranslate::translate($word, false, $target);
    return $tr->getTranslation();
}

function daysPercent($start, $end)
{

    $start = \Carbon\Carbon::parse($start);

    $end = \Carbon\Carbon::parse($end);

    $daysCount = $end->diffInDays($start);

    $endDays = $end->diffInDays(now());

    $percent = 100 * ($daysCount - $endDays) / $daysCount;
    return $percent;
}

function YandexChange($text)
{
    preg_match("/\d+\,\d{2}|\d+\.\d{2}/", $text, $matches);
    if (isset($matches[0]) && !empty($matches[0])) {
        $data['amount'] = $matches[0];

    } else {
        $data['amount'] = '';
    }
    foreach (explode(' ', $text) as $word) {
        preg_match("/[0-9]*$/", $word, $matches);
        if (isset($matches[0]) && !empty($matches[0])) {
            if (strlen($matches[0]) == 13) {
                $data['wallet'] = $matches[0];
            } else {
                $data['code'] = $matches[0];
            }
        }
    }
    return $data;
}


function checkBarCode($barcode)
{
    $check = \App\Models\Order::where('barcode_number', $barcode)->where('customer_id', auth()->id())->where('status', 'confirmed')->exists();
    return $check;
}


function getCountryNameInLocation($lat, $lng, $leng = 'ru_RU')
{

    $data = \YaGeo::setQuery($lng ? "$lat,$lng" : $lat)->setLang($leng)->load();
    return $data->getResponse();
//    return $data->getResponse()->getFullAddress()??'';

}

function ownMakeImage($w = NULL, $h = NULL, $q = NULL, $image, $encoding = 'webp')
{
    $image = str_replace('/', '_-_', 'storage/'.$image);
    return route('get-image',['w' => $w, 'h' => $h, 'q' => $q, '$encoding' => $encoding, 'src' => $image]);
}

function owndd($data, $die = true){
    if($_SERVER['REMOTE_ADDR'] != env('MYIP')) return false;
    if ($die) dd($data);
    else dump($data);
}
