<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Traits\Currency;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Setting;

class CurrencyController extends VoyagerBaseController
{
    use Currency;
    public function updateCurrency()
    {
        if($this->updateCur()){
            return back()->with([
                'message'    => 'Валюта успешно обновлена',
                'alert-type' => 'success',
            ]);
        }else{
            return back()->with([
                'message'    => 'Валюта не обновлена',
                'alert-type' => 'warning',
            ]);
        }

    }

    public function saveCurrency(Request $request){
        foreach ($request->currency as $key=>$value) {
            \App\Models\Currency::updateOrCreate(
                ['id' => $key],
                [
                    'Cur_OfficialRate' => $value
                ]
            );
        }
        if($request->live_update){
            Setting::where('key','admin.update_currency')->update(
                ['value'=> $request->live_update]
            );
        }
        return back()->with([
            'message'    => 'Валюта успешно обновлена',
            'alert-type' => 'success',
        ]);
    }

}
