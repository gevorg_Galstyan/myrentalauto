@php
    $face_data = new $dataType;
    $action = new $action($dataType,$face_data);
@endphp

@if ($action->shouldActionDisplayOnDataType())
    @if ($face_data)


        <a href="#"
           title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!}>
            <i class="{{ $action->getIcon() }}"></i> <span
                    class="hidden-xs hidden-sm">{{ $action->getTitle() }}</span>
        </a>

        @if($action->getTitle() == 'Удалить')
            <a href="#" title="Восстановить" class="btn btn-sm btn-success pull-right restore">
                <i class="voyager-trash"></i>
                Восстановить
            </a>

        @endif
    @endif
@endif
