@php($greenCard = json_decode($dataTypeContent->green_card, true))

<div id="accordion-greenCard">
    <div class="card shadow-sm  bg-light  rounded">
        <div class="card-header" id="heading-attribute">
            <h5 class="mb-0">
                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-greenCard"
                   aria-expanded="true" aria-controls="collapse-greenCard">
                    <i class="fa"></i>
                    Грин карта
                </a>
            </h5>
        </div>

        <div id="collapse-greenCard" class="collapse {{isset($greenCard) && count($greenCard) ? 'show' : ''}}" aria-labelledby="heading-attribute"
             data-parent="#accordion-greenCard">
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class=""></th>
                        <th class="">Страна:</th>
                        @foreach(config('car.green_card.days') as $days)
                            <th class="">{{$days}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(config('car.green_card.country') as $country_key => $country)
                        <tr>
                            <td>
                                <input class="form-check-input m-0 position-static change-greenCard-opt"
                                       type="checkbox"
                                       value=""
                                       data-target="greenCard-{{$country_key}}"
                                       name="green_card[{{$country_key}}]"
                                        {{isset($greenCard[$country_key])?'checked':''}}>
                            </td>
                            <td {{isset($greenCard[$country_key])?'':'disabled'}}>{{$country}}</td>
                            @foreach(config('car.green_card.days') as $days_key => $days)
                                <td>
                                    <input type="text" class="form-control "
                                           data-status="greenCard-{{$country_key}}"
                                           name="green_card[{{$country_key}}][{{$days_key}}]"
                                           value="{{ $greenCard[$country_key][$days_key]??''}}"
                                           placeholder="{{$days}} (BYN)"
                                           {{isset($greenCard[$country_key])?'':'disabled'}} required>
                                </td>
                            @endforeach
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



{{--<div class="panel relation-group">--}}
    {{--<div class="panel-heading" role="tab" id="heading4">--}}
        {{--<h4 class="panel-title">--}}
            {{--<a class="collapsed" role="button" data-toggle="collapse"--}}
               {{--data-parent="#accordion"--}}
               {{--href="#greenCard" aria-expanded="false"--}}
               {{--aria-controls="collapse3">--}}
                {{--@if(isset($greenCard) && count($greenCard))--}}
                    {{--<i class="more-less fas fa-minus"></i>--}}
                {{--@else--}}
                    {{--<i class="more-less fas fa-plus"></i>--}}
                {{--@endif--}}
                {{--Грин карта--}}
            {{--</a>--}}
        {{--</h4>--}}
    {{--</div>--}}
    {{--<div id="greenCard" class="panel-collapse collapse {{isset($greenCard) && count($greenCard) ? 'in' : ''}}" role="tabpanel"--}}
         {{--aria-labelledby="heading4">--}}
        {{--<div class="panel-body">--}}
            {{--<table class="table table-bordered table-hover">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th class=""></th>--}}
                    {{--<th class="">Страна:</th>--}}
                    {{--@foreach(config('car.green_card.days') as $days)--}}
                        {{--<th class="">{{$days}}</th>--}}
                    {{--@endforeach--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach(config('car.green_card.country') as $country_key => $country)--}}

                    {{--<tr>--}}
                        {{--<td>--}}
                            {{--<input class="form-check-input change-greenCard-opt"--}}
                                   {{--type="checkbox"--}}
                                   {{--value=""--}}
                                   {{--data-target="greenCard-{{$country_key}}"--}}
                                   {{--name="green_card[{{$country_key}}]"--}}
                                    {{--{{isset($greenCard[$country_key])?'checked':''}}>--}}
                        {{--</td>--}}
                        {{--<td {{isset($greenCard[$country_key])?'':'disabled'}}>{{$country}}</td>--}}

                        {{--@foreach(config('car.green_card.days') as $days_key => $days)--}}
                            {{--<td>--}}
                                {{--<input type="text" class="form-control "--}}
                                       {{--data-status="greenCard-{{$country_key}}"--}}
                                       {{--name="green_card[{{$country_key}}][{{$days_key}}]"--}}
                                       {{--value="{{ $greenCard[$country_key][$days_key]??''}}"--}}
                                       {{--placeholder="{{$days}}"--}}
                                       {{--{{isset($greenCard[$country_key])?'':'disabled'}} required>--}}
                            {{--</td>--}}
                        {{--@endforeach--}}
                    {{--</tr>--}}
                {{--@endforeach--}}

                {{--</tbody>--}}
            {{--</table>--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
