@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>

    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }}
        &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}"
                   title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore"
                   data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete"
                   data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card panel-bordered">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col text-center">
                            <strong>Ползователь</strong> :
                            <span>{{$dataTypeContent->target ? $dataTypeContent->target->full_name : ''}}</span>
                        </div>
                        <div class="col text-center">
                            <strong>Номер</strong> : <span>#{{$dataTypeContent->ticket_number}}</span>
                        </div>
                        <div class="col text-center">
                            <strong>Тип</strong> :
                            <span>{{$dataTypeContent->priority ? $dataTypeContent->priority->title : ''}}</span>
                        </div>
                        <div class="col text-center">
                            <strong>Статус</strong> :
                            @if ($dataTypeContent->status == 'open')
                                <span class="badge badge-success">Открыт</span>
                            @else
                                <span class="badge badge-danger">Закрыт</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card panel-bordered">
                    <div class="card-header">
                        <div class="d-flex justify-content-center">
                            <div class="col-3"></div>
                            <div class="col-6">
                                <p class="h4 text-center sub-heading">Тема </p>
                            </div>
                            <div class="col-3 text-right">
                                {{$dataTypeContent->created_at->format('d-m-Y H:i')}}
                            </div>
                        </div>
                        <div class="col-12">
                            <h3 class="text-center">
                                {{$dataTypeContent->title}}
                            </h3>
                        </div>

                    </div>
                    <?php $images = json_decode($dataTypeContent->images); ?>
                    <div class="card-body">
                        <div class="row mx-0">
                            <div class="col-{{$images != null ? 8 : 12}}">
                                <p class="ff-ms"> {!! $dataTypeContent->text !!} </p>
                            </div>
                            @if($images != null)
                                <div class="col-4 border-left d-flex flex-wrap overflow-auto">
                                    <div class="col-12 mb-4">
                                        <p class="h4 text-center sub-heading">Фото</p>
                                    </div>
                                    @foreach($images as $image)
                                        <div class="img_settings_container col-6">
                                            <a class="sl-slider-slide" data-fancybox="ticket">
                                                <img src="{{ Voyager::image( $image ) }}" class="w-100">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        @if ($dataTypeContent->comments()->count())
                            <div class="card panel-bordered">
{{--                                <div class="card-header">--}}
{{--                                    <p class="h5 text-center sub-heading">--}}
{{--                                        Ответы--}}
{{--                                    </p>--}}
{{--                                </div>--}}
                                <div class="card-body">
                                    @foreach ($dataTypeContent->comments as $item)

                                        <div class="row bg-light py-3 my-4 rounded">
                                            <div class="col-12 border-bottom d-flex justify-content-between">
                                                <div class="py-2"><strong>{{$item->creator->full_name}}</strong></div>
                                                <div class="py-2">{{$item->created_at->format('d-m-Y H:i')}}</div>
                                            </div>
                                            <?php $images = json_decode($item->images); ?>
                                            <div class="row mx-0">
                                                <div class="col-{{$images != null ? 8 : 12}}">
                                                    <p class="ff-ms"> {!! $item->text !!} </p>
                                                </div>
                                                @if($images != null)
                                                    <div class="col-4 border-left d-flex flex-wrap overflow-auto">
                                                        @foreach($images as $image)
                                                            <div class="img_settings_container col-6">
                                                                <a class="sl-slider-slide" data-fancybox="ticket">
                                                                    <img src="{{ Voyager::image( $image ) }}"
                                                                         class="w-100">
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <div class="" style="padding: 2rem">
                            @if ($dataTypeContent->status == 'open')
                                <form role="form"
                                      action="{{ route('voyager.ticket-comments.store', $dataTypeContent->ticket_number) }}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf

                                    <input type="hidden" name="creator_id" value="{{auth()->id()}}">
                                    <input type="hidden" name="target_id" value="{{auth()->id()}}">
                                    <input type="hidden" name="ticket_id" value="{{$dataTypeContent->id}}">
                                    <div class="form-group col-12">
                                        <label>Ответить</label>
                                        <textarea name="text" class="form-control " rows="6" placeholder="Ответить"
                                                  id="editor"></textarea>
                                    </div>
                                    <div class="d-flex">
                                        <div class="col text-left">
                                            <label for="input-24">Прикрепите файл</label>
                                            <input id="input-24" name="images[]" type="file" multiple>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="col-12 text-center">
                                            <button class="btn btn-success" type="submit">
                                                Отправить
                                            </button>
                                            <a class="btn btn-danger pull-right"
                                               href="{{route('voyager.tickets.close', $dataTypeContent)}}"
                                               type="button">
                                                Закрыть
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
            <script>
                $("#input-24").fileinput({
                    language: "ru",
                    theme: "fas",
                    quality: 0.75,
                    showUpload: false,
                    maxFileSize: 5120,
                    dropZoneEnabled: false,
                    showCaption: false,
                    showCancel: false,
                    showRemove: false,
                    // layoutTemplates: {footer: ''},
                });

            </script>
@stop
