<div class="mb-5 ">
    <div class="py-3 ">
        <form action="{{route('posts')}}" method="get" class="input-group " role="search" id="search-form">
            <div class="input-group">
                {{--<input type="text" class="form-control" placeholder="Поиск.." name="search">
                <span class="input-group-btn ml-2"><button type="submit" class="btn own-btn ora border-radius-5 px-3">Go!</button></span>--}}

                <input type="text" class="form-control brd-orange bw-2 py-4 border-radius-left-5 faq-search" name="search"
                       placeholder="{{__('post.search')}}.." aria-label="find answer" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn own-btn border-radius-0 py-2 px-3 border-radius-right-5 ora " type="submit"
                            id="button-addon2"><i class="fas fa-search fs-20"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card border-0 shadow mb-5 border-radius-20">
    <div class="card-header  py-3 own-btn top-border-radius-20 ">
        <span class="font-weight-bold small">{{__('post.tags')}}</span>
    </div>
    <div class="list-group list-group-flush">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-unstyled mb-0 d-flex flex-wrap">
                        @if(isset($selected_tag) && $selected_tag)
                            <li class="tag ">
                                <a href="{{route('posts', ['slug' => $selected_tag->slug])}}"
                                   class="badge badge-pill badge-secondary p-2 selected-tag clear-tag">
                                    {{$selected_tag->name}}
                                    <span class="clear-tag"><i class="fas fa-times-circle"></i></span>
                                </a>
                            </li>
                        @endif
                        @foreach($tags as $tag)
                            <li class="tag ">
                                <a href="{{route('posts', ['slug' => $tag->slug])}}"
                                   class="badge  badge-pill badge-secondary p-2 ">{{$tag->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
