<?php

namespace App\Http\Controllers\User;

use App\Models\Order;
use App\Notifications\BookingSuccessUserNotify;
use App\Notifications\NewBooking;
use App\Services\MpiService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class PaymentController extends Controller
{
    public function paymentResponse(Request $request)
    {

//        dd($request->hasValidSignature());
//        if (!$request->hasValidSignature()) {
//            abort(401);
//        }
        $order = Order::where('transaction_id', $request->orderId)->firstOrFail();
        if ($order->customer_id != auth()->id()) abort(403);
        $type = 'reservation';
        if ($order->parent) $type = 'full';
        $status = json_decode(MpiService::getOrderStatus($order, $type), true);

        if (isset($status['OrderStatus']) && $status['OrderStatus'] == 2 || $request->status != 'failed') {
            $data['title'] = __('payment.success.title');
            $data['text'] = __('payment.success.text');
            $data['text-color'] = 'text-success';
            $order->status = 'paid';
            $admins = User::where('role_id', 1)->get();

            if ($order->see == 0) {
                if ($order->parent){
                    Notification::send($admins, new NewBooking($order,'full'));
                    $order->customer->notify(new BookingSuccessUserNotify($order,'full'));
                }else{
                    Notification::send($admins, new NewBooking($order,'booking'));
                    $order->car->owner->notify(new NewBooking($order,'booking'));
                    $order->customer->notify(new BookingSuccessUserNotify($order,'booking'));
                }
                $order->payment_date = now();
            }
        } else {
            $data['title'] = __('payment.error.title');
            $data['text'] = __('payment.error.text');
            $data['text-color'] = 'text-danger';
            $order->status = 'no_paid';
        }

        $order->see = 1;
        $order->save();

        return view('payment.index', compact('data'));
    }

    public function orderStatus(Request $request, Order $order)
    {
        $status = json_decode(MpiService::getOrderStatus($order, $request->type), true);
        $html = view('user.order-status', compact('status', 'order'))->render();

        return response()->json(['html' => $html]);
    }

    public function fullPay(Request $request, Order $order)
    {
        if ($order->customer_id != auth()->id() || $order->status != 'paid' || !$order->car->full_payment || $order->fullPay) {
            abort(404);
        }
        $newOrder = new Order();
        $newOrder->car_id = $order->car->id;
        $newOrder->customer_id = auth()->id();
        $newOrder->parent_id = $order->id;
        $newOrder->price_to_pay = currency(($order->total_price - $order->price_to_pay), null, false);
        $newOrder->origin_amount = currency(($order->origin_total - $order->origin_amount), 'BYN', false);
        $newOrder->status = 'pending';
        $newOrder->barcode_number = generateBarcodeNumber();
        $newOrder->save();

        $result = json_decode(MpiService::sendPayment($newOrder, 'full'), true);
        if ($result['errorCode'] == 0) {
            $newOrder->transaction_id = $result['orderId'];
            $newOrder->save();
            return redirect($result['formUrl']);
        } else {
            $newOrder->status = 'failed';
            $newOrder->save();
            return back()->with([
                'status' => __('car.order_registration_failed'),
                'alert_type' => 'warning',
            ]);
        }
    }
}
