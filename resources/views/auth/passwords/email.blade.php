@extends('layouts.app')

@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop

@section('content')
    <div class="container">
        <div class="auth login m-5">
            <h1 class="text-center fw-600 fs-30 sub-heading text-uppercase m-0 mb-2">{{getMetaTags('reset_password_page') ? getMetaTags('reset_password_page')->translate()->h1:''}}</h1>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="modal-header text-center justify-content-center pb-1">
                            <h4 class="modal--title">{{__('auth.login_with')}}</h4>
                        </div>
                        <div class="modal-body py-1">
                            <div class="box">
                                <div class="content p-2">
                                    <div class="social d-flex align-items-center justify-content-center mb-3">
                                        <a class="facebook-login social-login mr-2" href="{{route('oauth',['facebook'])}}">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a class="vk-login social-login" href="{{route('oauth',['vkontakte'])}}">
                                            <i class="fab fa-vk"></i>
                                        </a>
                                    </div>
                                    <div class="division">
                                        <div class="line l"></div>
                                        <span>{{__('auth.or')}}</span>
                                        <div class="line r"></div>
                                    </div>
                                    <div class="error"></div>
                                    <div class="form pb-2">
                                        <form id="forgotPasswordForm" method="POST" action="{{ route('password.email') }}">
                                            @csrf

                                            <input id="email" type="email" class="form-control mb-2"
                                                   name="email" value="{{ old('email') }}" required
                                                   placeholder="{{__('auth.email')}}">
                                            <div class="text-center mt-3">
                                                <button type="submit"
                                                        class="own-btn text-capitalize fs-16 d-inline-block ora btn-submit-login">
                                                    смена пароля
                                                </button>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="modal-footer mt-3 pt-0">
                                        <div class="forgot rest-password-footer">
                                            Вспомнил, вернуться <a href="{{ route('login') }}">назад</a>.

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
