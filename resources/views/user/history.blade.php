@extends('layouts.app')
@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop
@section('css')
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop

@section('content')

    <div class="own-container ">
        <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center">

            <!-- edit form column -->
            <div class="col-md-12  ff-st row ">
                <div class="col-sm-4 mb-3">
                    <a href="{{url()->previous()}}" class="btn own-btn ora   d-inline-block  border-radius-20"
                       title="{{__('general.back')}}">
                        <i class="fas fa-arrow-left"></i>
                        <span class="">{{__('general.back')}}</span>
                    </a>
                </div>
                <div class="col-sm-4 mb-3">
                    <h1 class="text-center">{{__('user.story')}}</h1>
                </div>
                <div class="col-md-12">
                    @if (auth()->user()->orders()->whereDoesntHave('parent')->count())
                        <table id="payHistory" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>{{__('user.order.photo')}}</th>
                                <th class="d-none d-xl-table-cell">{{__('user.order.car')}}</th>
                                <th class="d-sm-none d-none d-md-table-cell">{{__('user.order.rental_date')}}</th>
                                <th class="d-none d-xl-table-cell">{{__('user.order.return_date')}}</th>
                                <th>{{__('user.order.transaction_date')}}</th>
                                <th>{{__('user.order.status')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (auth()->user()->orders()->where('status', '!=', 'failed')->whereDoesntHave('parent')->get()->sortByDesc('id') as $order)
                                @if(!$order->car) @continue @endif
                                <tr data-status="{{ $order->id}}"
                                    data-url="{{route('user.order.get', ['order' => $order])}}"
                                    class="k-pointer {{'show-details' }}">
                                    <td>
                                        <img src="{{Voyager::image($order->car->image_1)}}" alt="{{$order->car->title}}"
                                             width="100">
                                    </td>
                                    <td class="d-none d-xl-table-cell">{{$order->car->title}}</td>
                                    <td class="d-sm-none d-none d-md-table-cell">{{$order->dates->start->format('d:m:Y g:i')}}</td>
                                    <td class="d-none d-xl-table-cell">{{$order->dates->end->format('d:m:Y g:i')}}</td>
                                    <td>{{$order->created_at->format('d:m:Y g:i')}}</td>
                                    <td class="order-status">
                                        <a href="#" class="badge badge-{{config('car.order.class.'.$order->status)}}">
                                            {{($order->fullPay && $order->fullPay->status == 'paid') ? __('general.order.fully_paid') :__('general.order.'.$order->status)}}
                                        </a>
                                        @if ($order->status == 'canceled' && $order->fullPay)
                                            <br>
                                            <a href="#" class="badge badge-{{config('car.order.class.'.$order->status)}}">
                                                {{__('general.order.'.$order->status)}}
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-center">
                            У вас пока нет сделок
                        </p>
                    @endif
                </div>

            </div>

        </div>

    </div>
    <!-- Large modal -->

    <div class="modal fade show-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered mw-75 mw-sm-100">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="order-content"></div>
            </div>
        </div>
    </div>
    <div id="overlay">
        <div id="popup" class="popup-content">
            <div id="close">X</div>
            <div class="order-status-container"></div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>

        $(document).on('submit', '#review-form', function (form) {
            form.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (response) {
                    var review = $('textarea[name="review"]').val();
                    var star = $('.star-val:checked').val();
                    $('.review-title').text('ваш отзыв');
                    $('.review-container').html('' +
                        ' <div>\n' +
                        '        <div class="ff-st fs-14 row justify-content-center">\n' +
                        '            <div class="">\n' +
                        '                <div class="d-flex justify-content-center mb-1">\n' +
                        '\n' +
                        '                    <div class="align-self-center">\n' +
                        '                        <div class="align-self-center">\n' +
                        '                        <div class=" align-self-center">\n' +
                        '                            <span class=""><i class="color-orange ' + (star >= 1 ? 'fas' : 'far') + ' fa-star"></i></span>\n' +
                        '                            <span class=""><i class="color-orange ' + (star >= 2 ? 'fas' : 'far') + ' fa-star"></i></span>\n' +
                        '                            <span class=""><i class="color-orange ' + (star >= 3 ? 'fas' : 'far') + ' fa-star"></i></span>\n' +
                        '                            <span class=""><i class="color-orange ' + (star >= 4 ? 'fas' : 'far') + ' fa-star"></i></span>\n' +
                        '                            <span class=""><i class="color-orange ' + (star >= 5 ? 'fas' : 'far') + ' fa-star"></i></span>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '                <p class="mb-0 text-center p-2">' + review + '</p>\n' +
                        '        </div>\n' +
                        '    </div>')
                }
            })
        });


        $(document).ready(function () {
            $('#payHistory').DataTable({
                "ordering": false,
                "language": {
                    "url": locale == 'ru' ? "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json" : ''
                }
            });
        });
        $('#accordionHistory, #accordionFavorite').on('shown.bs.collapse', function () {
            $(this).find('.collapseHeader').addClass('color-orange')
        });
        $('#accordionHistory, #accordionFavorite').on('hidden.bs.collapse', function () {
            $(this).find('.collapseHeader').removeClass('color-orange')
        })

        $('.show-details').click(function () {
            $('.show-details-modal').modal('show');
            var url = $(this).data('url');
            var container = $('.order-content');
            container.html('').preloader();
            axios.get(url)
                .then(function (response) {
                    container.html(response.data.html).preloader('remove');
                    $('[data-toggle="tooltip"]').tooltip();
                })

        });


        $(document).on('click', '.order-cancel', function () {
            var cancel_url = $(this).data('url');
            var order = $(this).data('target');
            $('.show-details-modal').modal('hide');
            Swal.fire({
                title: '{{__('car.do_you_really_want_to_cancel')}}',
                input: 'checkbox',
                inputValue: 0,
                html: `<textarea id="order-cancel-note" style="width:75%" class="swal2-textarea" placeholder="{{__('car.write_a_reason')}}"></textarea>`,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                showCancelButton: true,
                cancelButtonText: '{{__('general.order.cancel')}} ',
                inputPlaceholder: '{!! __('car.read_the_cancellation_policy_and_agree', ['link' => '#0']) !!}',
                confirmButtonText: '{!! __('general.order.confirm') !!}',
                inputValidator: function (result) {
                    return !result && '{{__('car.to_cancel_your_reservation_you_must_agree_to_the_terms')}}'
                }
            }).then((result) => {
                if (result.value) {
                    $('.own-container ').preloader();
                    var note = $('#order-cancel-note').val();
                    var confirm = result.value

                    axios.post(cancel_url, {
                        note: note,
                        confirm: confirm
                    }).then(function (response) {
                        if (response.data.success) {
                            Swal.fire(
                                '{{__('general.cancel_booking')}}',
                            );
                            var order_status_container = $('[data-status="' + order + '"]');
                            order_status_container.removeClass('show-details');
                            order_status_container.find('.order-status').html('<a href="#" class="badge badge-{{config('car.order.class.canceled')}}">отменен</a>').data('url', ' ');
                        }
                        $('.own-container ').preloader('remove');
                    })
                } else {
                    $('.show-details-modal').modal('show');
                }
            })

        })

        $(document).on('click', '.order-info', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            var type = $(this).data('type');
            axios.get(url+'?type='+type).then(function (response) {
                $('.order-status-container').html(response.data.html);
                $('#overlay').fadeIn(300);
            });
        });
        $(document).on('click', '#close, #overlay', function (e) {
            if (e.target.id == 'close' || e.target.id == 'overlay') {
                $('#overlay').fadeOut(300);
                $('.order-status-container').html('');
            }

        })
    </script>
@stop

