<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class TicketComment extends Model
{
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }
}
