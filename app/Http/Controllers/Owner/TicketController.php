<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Models\TicketPriority;
use App\Notifications\NewTickatCreated;
use App\Notifications\TicketNotification;
use App\Notifications\TicketReplyNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;

class TicketController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        $tickets = auth()->user()->tickets()->orderBy('created_at', 'desc')->get();

        return view('owners.tickets.index', compact('tickets'));
    }

    public function edit(Request $request, $id)
    {
        return abort(403);
    }

    public function create(Request $request)
    {
        $priorities = TicketPriority::get();

        return view('owners.tickets.create', compact('priorities'));
    }

    public function store(Request $request)
    {
        $slug = 'tickets';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
//        $this->authorize('add', app($dataType->model_name));
        $request['creator_id'] = auth()->id();
        $request['ticket_number'] = generateTicketNumber();
        $request['status'] = 'open';
        // Validate fields with ajax
        $this->validate($request, [
            'title' => 'required|string',
            'text' => 'required|string',
            'priority_id' => 'required|string',
        ], [], [
            'title' => 'Заголовок',
            'text' => 'Опесание',
            'priority_id' => 'Приоритет',
        ]);
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        $admins = User::where('role_id', 1)->get();
        Notification::send($admins, new TicketNotification($data));

        event(new BreadDataAdded($dataType, $data));

        return redirect()
            ->route("tickets.index")
            ->with([
                'status' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function show(Request $request, $id)
    {
        $ticket = Ticket::where('ticket_number', $id)->where('target_id', auth()->id())->first();
        if (!$ticket) return abort(403);
        return view('owners.tickets.show', compact('ticket'));

    }

    public function update(Request $request, $id)
    {
        return abort(403);
    }

    public function createComment(Request $request, $ticketNumber)
    {
        $ticket = Ticket::where('ticket_number', $ticketNumber)->firstOrFail();

        $slug = 'ticket-comments';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
//        $this->authorize('add', app($dataType->model_name));
        $request['creator_id'] = auth()->id();
        $request['target_id'] = auth()->id();
        $request['ticket_id'] = $ticket->id;
        // Validate fields with ajax
//        $this->validate($request, [
//            'title' => 'required|string',
//            'text' => 'required|string',
//            'priority_id' => 'required|string',
//        ], [], [
//            'title' => 'Заголовок',
//            'text' => 'Опесание',
//            'priority_id' => 'Приоритет',
//        ]);
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        $admins = User::where('role_id', 1)->get();
        Notification::send($admins, new TicketReplyNotification($data));
        return back()
            ->with([
                'status' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }
}
