<?php

namespace App\Providers;

use App\Listeners\BreadDataAddedListener;
use App\Listeners\BreadDataUpdatedListener;
use Illuminate\Support\Facades\Event;
use App\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            'App\Listeners\SendEmailVerificationNotification',
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
        ],

        BreadDataAdded::class => [
            BreadDataAddedListener::class
        ],

        BreadDataUpdated::class => [
            BreadDataUpdatedListener::class
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
