<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function history()
    {
        return view('user.history');
    }

    public function favorites()
    {
        return view('user.favorites');
    }

}
