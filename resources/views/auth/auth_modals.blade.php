<div class="modal fade login" id="loginModal">
    <div class="modal-dialog login animated modal-dialog-centered">
        <div class="modal-content border-25">
            <div class="modal-header text-center justify-content-center pb-1">
                <h4 class="modal--title">{{__('auth.login_with')}}</h4>
            </div>
            <div class="modal-body py-1">
                <div class="box">
                    <div class="content">
                        <div class="social d-flex align-items-center justify-content-center mb-3">
                            <a class="facebook-login social-login mr-2" href="{{route('oauth',['facebook'])}}">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a class="vk-login social-login" href="{{route('oauth',['vkontakte'])}}">
                                <i class="fab fa-vk"></i>
                            </a>
                        </div>
                        <div class="division">
                            <div class="line l"></div>
                            <span>{{__('auth.or')}}</span>
                            <div class="line r"></div>
                        </div>
                        <div class="error"></div>
                        <div class="form loginBox">
                            <form id="loginForm" method="POST" action="{{ route('login') }}" accept-charset="UTF-8">
                                @csrf
                                <input id="email_l" type="email"
                                       class="form-control mb-2 border-25"
                                       name="email" value="{{ old('email') }}" required
                                       placeholder="{{__('auth.email')}}">

                                <input id="password_l" type="password"
                                       class="form-control  border-25"
                                       name="password" placeholder="{{__('auth.password')}}" required>
                                <div class="d-flex align-items-center justify-content-center">

                                    <label for="remember" class="d-block ml-2 mb-0 w-100 font-weight-normal">
                                        <input class="position-relative opacity-1" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{__('auth.remember_me')}}
                                    </label>
                                </div>

                                <div class="text-center mt-2">
                                    <button type="submit"
                                            class="own-btn text-capitalize fs-16 d-inline-block border-5 ora btn-submit-login">
                                        {{__('auth.login')}}
                                    </button>
                                </div>
                                <br>
                                @if (Route::has('password.request'))
                                    <div class="text-center mt-2">
                                        <a class="btn btn-link forgot-password" href="{{ route('password.request') }}">
                                            {{ __('auth.forgot_password') }}
                                        </a>
                                    </div>
                                @endif
                                <div class="alert-danger d-inline-block"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content registerBox" style="display:none;">
                        <div class="form">
                            <form id="registerForm" method="POST" action="{{route('register')}}"
                                  accept-charset="UTF-8">
                                @csrf
                                <div>
                                    <input id="first_name" type="text" class="form-control  border-25 mb-2"
                                           name="first_name" value="{{ old('first_name') }}" required
                                           placeholder="{{__('auth.first_name')}}">
                                </div>

                                <div>
                                    <input id="last_name" type="text" class="form-control  border-25 mb-2"
                                           name="last_name" value="{{ old('last_name') }}" required
                                           placeholder="{{__('auth.last_name')}}">
                                </div>

                                <div>
                                    <input type="email"
                                           class="form-control  border-25 mb-2"
                                           name="email" value="{{ old('email') }}" required
                                           placeholder="{{__('auth.email')}}">
                                </div>

                                <div>
                                    <input id="password" type="password"
                                           class="form-control  border-25 mb-2"
                                           name="password" required placeholder="{{__('auth.password')}}">
                                </div>

                                <div>
                                    <input id="password-confirm" type="password" class="form-control  border-25 mb-2"
                                           name="password_confirmation" placeholder="{{__('auth.repeat_password')}}"
                                           required>
                                </div>

                                <div class="text-center mt-2 mb-4 chose-role">
                                    <div class="btn-group btn-group-toggle d-flex" data-toggle="buttons">
                                        <label class="btn left-border-radius-20 fs-16 fw-600 active col">
                                            <input type="radio" name="role" value="2" checked> {{__('auth.role_user')}}
                                        </label>
                                        <label class="btn right-border-radius-20 fs-16 fw-600 col">
                                            <input type="radio" name="role"  value="3"> {{__('auth.role_owner')}}
                                        </label>
                                    </div>
                                </div>

                                <div class="text-center mt-2">
                                    <button type="submit"
                                            class="own-btn text-capitalize fs-16 d-inline-block border-5 ora  btn-submit-reg">
                                        {{__('auth.register')}}
                                    </button>
                                </div>
                                <div class="alert-danger d-inline-block"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content forgotPasswordBox" style="display:none;">
                        <div class="form">
                            <form id="forgotPasswordForm" method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <input  type="email" class="form-control  border-25 mb-2"
                                       name="email" value="{{ old('email') }}" required
                                       placeholder="{{__('auth.email')}}">
                                <div class="text-center mt-2">
                                    <button type="submit"
                                            class="own-btn text-capitalize fs-16 d-inline-block border-0 border-5 ora">
                                        смена пароля
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer mt-0 pt-0">
                <div class="forgot login-footer">
                    <a href="javascript: showRegisterForm();">{{__('auth.create_an_account')}}?</a>
                </div>
                <div class="forgot rest-password-footer" style="display:none">
                    Вспомнил, вернуться <a href="javascript: showLoginForm();">назад</a>.
                </div>
                <div class="forgot register-footer" style="display:none">
                    <span>{{__('auth.already_account')}}</span>
                    <a href="javascript: showLoginForm();">{{__('auth.login')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>

