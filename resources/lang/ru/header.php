<?php
return [
    'services' => 'О сервисе',
    'support' => 'Вопросы',
    'documentation' => 'Информация',
    'profile' => 'Профиль',
    'favorites' => 'Избранные',
    'for_whom' => 'Для кого',
    'team' => 'Команда',
    'blog' => 'Блог',
    'contacts' => 'Контакты',
    'personal_area' => 'Личный кабинет',
    'story' => 'История',
    'posts' => 'Посты',
];
