<?php

namespace App\Http\Middleware;

use Closure;

class CheckOwnerProfile
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && $request->method() == 'GET') {
            if (auth()->user()->profile->check() ) {
                return $next($request);

            }else{
                if ($request->url() != route('owner.profile')) return redirect()->route('owner.profile')->with('info-message', 'Для продолжения заполните свой профиль ');
            }
        }
        return $next($request);
    }
}
