<?php

namespace App\Http\Controllers\Owner;

use App\Models\Order;
use App\Models\OrderDate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;

class CustomersController extends Controller
{

    /*
     * get customers list
     */
    public function index()
    {

        $customers = auth()->user()->customers()->get();

        return view('owners.customers.index', [
            'customers' => $customers
        ]);
    }

    /*
     * customer single page
     */
    public function customer(Request $request, $slug)
    {

        $customer = User::where('slug', $slug)->firstOrFail();

        $customerOrderedCars = Car::whereHas('orders', function ($query) use ($customer) {
            $query->where('customer_id', $customer->id);
        })->with('orders')->where('owner_id', auth()->user()->id)->get();

//        $customerOrderedCars = Order::with(["car" => function($q){
//            $q->where('owner_id', auth()->user()->id);
//        }])->where('customer_id', $customer->id)->get();

        return view('owners.customers.customer', [
            'customer' => $customer,
            'customerOrderedCars' => $customerOrderedCars
        ]);
    }

    public function getCarOrdersInfo(Request $request, User $customer)
    {
        if ($request->ajax()) {
            $orders = Order::with('customer')->where('car_id', $request->id)->where('customer_id', $customer->id)->get();
            $car = Car::find($request->id);
            return view('owners.customers.orders_info', [
                'orders' => $orders,
                'customer' => $customer,
                'car' => $car,
            ]);
        }
    }

}
