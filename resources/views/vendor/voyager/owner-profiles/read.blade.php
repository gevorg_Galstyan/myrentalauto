@extends('voyager::master')
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop
@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }}
        &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}"
                   title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore"
                   data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete"
                   data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.(request()->input('redirect')?:$dataType->slug).'.index') }}"
           class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>

    </h1>
    @include('voyager::multilingual.language-selector')

@stop
@section('content')
    <div class="page-content read container-fluid bg-white">
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    <div class="col-md-9">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                @if ( $dataTypeContent->checked == 1)
                                    <span class=" label info-icon text-success">
                                        <i class="far fa-check-circle fa-3x"></i>
                                    </span>
                                @else
                                    <span class="label info-icon text-warning">
                                        <i class="fas fa-exclamation-triangle fa-3x"></i>
                                    </span>
                                @endif
                                <div class="mb-3 pb-3">
                                    <div class="form-group col-md-6">
                                        <img src="{{Voyager::image($dataTypeContent->user->avatar)}}"
                                             class=" img-fluid w-100"
                                             alt="avatar" width="100px"
                                             id="avatarProfile">
                                    </div>
                                    <div class="form-row-title col-lg-12">
                                        <h4>{{__('owner-profile.information')}}</h4>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="col-lg-10 control-label">{{__('owner-profile.name')}}</label>
                                        <div class="col-lg-10">
                                            <div class="form-control">{{$dataTypeContent->user->first_name}}</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="col-lg-10 control-label">{{__('owner-profile.sour_name')}}</label>
                                        <div class="col-lg-10">
                                            <div class="form-control">{{$dataTypeContent->user->last_name}}</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="col-lg-10 control-label">{{__('owner-profile.mail')}}</label>
                                        <div class="col-lg-10">
                                            <div class="form-control">{{$dataTypeContent->user->email}}</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="col-lg-10 control-label">{{__('owner-profile.phone_number')}}</label>
                                        <div class="col-lg-10">
                                            <div class="form-control">{{$dataTypeContent->user->number}}</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="col-lg-10 control-label">{{__('owner-profile.birthday')}}</label>
                                        <div class="col-lg-10">
                                            <div class="form-control">{{$dataTypeContent->user->birthday}}</div>
                                        </div>
                                    </div>
                                    <div class="form-row border-bottom mb-3">
                                        <div class="form-row-title col-lg-12">
                                            <h4>{{__('owner-profile.general_information')}}</h4>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.company')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->company}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label
                                                class="col-lg-10 control-label">{{__('owner-profile.reg_number')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->registration_number}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label
                                                class="col-lg-10 control-label">{{__('owner-profile.legal_address')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->legal_address}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.physical_address')}}</label>
                                            <div class="col-lg-10">
                                                <div
                                                    class="form-control">{{$dataTypeContent->physical_address}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.unp')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->unp}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.owner_name')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->owner}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.assigned')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->assigned}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.cont_number')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->cont_tel}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.cont_mail')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->cont_mail}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.bank')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->bank}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="col-lg-10 control-label">{{__('owner-profile.bank_reg_number')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->bank_reg_number}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label
                                                class="col-lg-10 control-label">{{__('owner-profile.bank_account_number')}}</label>
                                            <div class="col-lg-10">
                                                <div class="form-control">{{$dataTypeContent->bank_account_number}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="col-lg-12 control-label">{{__('owner-profile.additional_information')}}</label>
                                        <div class="form-control col-lg-12">{{$dataTypeContent->about_me}}</div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="col-md-10 control-label"></label>
                                        <div class="col-md-10 text-center">
                                            @if ($dataTypeContent->checked == 0)
                                                <a href="{{route('admin.check-owner-profile', $dataTypeContent)}}"
                                                       class="own-btn btn btn-primary ora rounded-0 border-0 m-auto fw-400">Проверено</a>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <a href="{{route('voyager.cars.index', ['key' => 'owner_id', 'filter' => 'equals', 's' => $dataTypeContent->user->id])}}">Все автомобили {{$dataTypeContent->user->full_name}}</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });
        $('.help').tooltip()
    </script>
@stop
