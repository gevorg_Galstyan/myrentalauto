<?php

namespace App\Widgets;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class UserDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $all_users = Voyager::model('User')->where('role_id', '!=', 1)->get();

        $deleted_users = $all_users->where('deleted_at', '!=', null)->count();
        $users = $all_users->where('deleted_at', null)->where('role_id', 2)->count();
        $owners = $all_users->where('deleted_at', null)->where('role_id', 3)->count();

        $user_cont = $all_users->count();

        $string = 'в проекте ' . $user_cont . ' пользователей ';
        $text = 'Арендаторы '. $users.'<br> Автовладельцы '. $owners .'<br> Удаленных '. $deleted_users;
        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-group',
            'title' => "{$string}",
            'text' => $text,
            'button' => [
                'text' => __('voyager::dimmer.user_link_text'),
                'link' => route('voyager.users.index'),
            ],
            'image' => asset('storage/widget-backgrounds/users.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return \Auth::user()->hasPermission('browse_users');
    }
}
