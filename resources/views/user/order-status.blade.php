<ul class="list-group list-group-flush">
    <li class="list-group-item">
        <span>{{__('general.order.servicing_bank')}} </span> : <span>{{__('general.order.belgazprombank')}}</span>
    </li>
{{--    <li class="list-group-item">--}}
{{--        <span>{{__('general.order.terminal_id')}}</span> : <span>{{$status['bindingId']??''}}</span>--}}
{{--    </li>--}}
    <li class="list-group-item">
        <span>{{__('general.order.order_id')}}</span> : <span>{{$status['OrderNumber']??''}}</span>
    </li>
    <li class="list-group-item">
        <span>{{__('general.order.transaction_status')}}</span> : <span>{{__('general.order.'.$order->status)}}</span>
    </li>
    <li class="list-group-item">
        <span>{{__('general.order.status_date_and_time_of_operation')}}</span> : <span>{{$order->payment_date}}</span>
    </li>
    <li class="list-group-item">
        <span>{{__('general.order.transaction_amount')}}</span> : <span>{{isset($status['Amount']) && $status['Amount'] ? number_format($status['Amount']/100, 2/*, ',' , ' '*/):''}} BYN</span>
    </li>
</ul>
