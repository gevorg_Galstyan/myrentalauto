<?php
/**
 * Created by PhpStorm.
 * User: Maximum Code
 * Date: 09.04.2019
 * Time: 14:30
 */

namespace App\Traits;
use Curl;

trait Currency
{
    public function updateCur(){
        try {
            $response = Curl::to('http://www.nbrb.by/API/ExRates/Rates?Periodicity=0')
                ->asJson()
                ->get();
            $currency_code = config('currency.needed');
            foreach ($response as $value){
                if(in_array($value->Cur_Abbreviation, $currency_code)){
//                $currency = \DB::table('currencies')
//                    ->where('Cur_Abbreviation', $value->Cur_Abbreviation)
//                    ->first();
                    \App\Models\Currency::updateOrCreate(
                        ['Cur_Abbreviation' => $value->Cur_Abbreviation],
                        [
                            'Cur_ID' => $value->Cur_ID,
                            'Cur_Abbreviation' => $value->Cur_Abbreviation,
                            'Cur_Scale' => $value->Cur_Scale,
                            'Cur_Name' => $value->Cur_Name,
                            'Cur_OfficialRate' => $value->Cur_OfficialRate,
                            'updated_at' => $value->Date,
                        ]
                    );
//                $currency = new Currency();
//                $currency->Cur_ID = $value->Cur_ID;
//                $currency->Cur_Abbreviation = $value->Cur_Abbreviation;
//                $currency->Cur_Scale = $value->Cur_Scale;
//                $currency->Cur_Name = $value->Cur_Name;
//                $currency->Cur_OfficialRate = $value->Cur_OfficialRate;
//                $currency->updated_at = $value->Date;
//                $currency->save();
                }
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }
}
