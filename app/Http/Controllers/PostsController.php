<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Tag;

class PostsController extends Controller
{
    /*
     * returns posts list
     */
    public function index(Request $request)
    {
        //get posts with relation
        $otherPosts = false;
        $posts = Post::with('tags')
            ->whereTranslation("title", "LIKE", "%" . $request->search . "%")
            ->where(function ($query) use ($request) {
                if ($request->slug) {
                    {
                        $query->whereHas('tags', function ($q) use ($request) {
                            $q->where('tags.slug', $request->slug);
                        });
                    }
                }
            })->orderBy('created_at', 'desc')->orderBy('updated_at', 'desc')->published()->paginate(10);
        $selected_tag = '';
        if ($request->slug) {
            {
                if ($selected_tag = Tag::where('slug', $request->slug)->first()) {
                    $selected_tag->increment('rating');
                }else{
                    return redirect()->route('posts');
                }
            }
        }

        if (!$posts->count()){
            $otherPosts = Post::with('tags')
                ->whereTranslation("title", "LIKE", "%" . $request->search . "%")
                ->limit(10)->get();
            if (!$otherPosts->count()){
                $otherPosts = Post::with('tags')
                    ->limit(10)->get();
            }
        }

        //get all tags
        $tags = Tag::orderBy('rating', 'desc')->where('id', '!=', ($selected_tag ? $selected_tag->id : ''))->get();
        //
        $posts->appends([
            'search' => $request->search,
            'slug' => $request->slug,
        ]);

        if ($request->ajax()) {
            $html = view('posts.post-list', compact('posts'))->render();
            return response()->json(['html' => $html], 200);
        }
        return view('posts.index', compact('posts', 'tags', 'selected_tag', 'otherPosts'));
    }

    /*
     * post single page
     *
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();
        if (!$post) abort(404);
        return view('posts.show', compact('post'));

    }

}
