<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TranslateController extends Controller
{
    public function getTranslate(Request $request)
    {
        $word = $request->word;
        return response()->json(['translate' => ($word ? myTranslate($word, 'en') : '')]);
    }
}
