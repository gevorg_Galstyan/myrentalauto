@extends('layouts.app')


@section('meta')
    <title>{{getMetaTags('home_page')->translate()->title}}</title>
    <meta name="keywords" content="{{getMetaTags('home_page')->translate()->Keyword}}"/>
    <meta name="description" content="{{getMetaTags('home_page')->translate()->description}}"/>
@stop

@section('content')
    <section class="slider">
        <div class="row mt-3">
            <div class="col-3 p-0 mt-3">
                <div class="slider-logo ">
                    <h1 class="slider-logo "><span class="own-blue">My</span><span class="own-orange">Rent</span><span
                            class="own-blue">Auto</span></h1>
                </div>
            </div>
        </div>

    </section>
    <section class="pt-3 translate-90">
        <div class="text-part col-lg-12 p-0">
            <div>
                <ul class="w-100 row justify-content-center textSlider list-unstyled">
                    <li class="{{$data['text-color']}}">
                        <h2 class="mb-3 text-center">{{$data['title']}}</h2>
                        <p class = "text-center">{{$data['text']}}</p>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <div class="translate-190">
        <div class="col-12 text-center">
            <a href="{{route('home')}}" class="own-btn text-uppercase d-inline-block btn-shadow ora">
                {{__('payment.return_home_page')}}
            </a>
        </div>
    </div>
@endsection


@section('script')

@stop


