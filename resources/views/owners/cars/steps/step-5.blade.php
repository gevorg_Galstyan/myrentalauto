<h3>Местоположение</h3>
<section>
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
    @forelse($dataTypeContent->getCoordinates() as $point)
        <input type="hidden" name="location[lat]" value="{{ $point['lat'] }}" id="lat"/>
        <input type="hidden" name="location[lng]" value="{{ $point['lng'] }}" id="lng"/>
    @empty
        <input type="hidden" name="location[lat]"
               value="{{ config('voyager.googlemaps.center.lat') }}" id="lat"/>
        <input type="hidden" name="location[lng]"
               value="{{ config('voyager.googlemaps.center.lng') }}" id="lng"/>
    @endforelse
    <div class="form-group">
        <input type="text" class="form-control" id="location">
    </div>
    <div id="map"></div>
</section>