<?php

namespace App;

use App\Models\Post;
use App\Models\{Order, Profile, OwnerProfile, Car, Favorite, Ticket};
use ChristianKuri\LaravelFavorite\Traits\Favoriteability;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use HappyDemon\UsrLastly\LastSeenTrait as LastSeen;
use NotificationChannels\WebPush\HasPushSubscriptions; //import the trait


use Laravelista\Comments\Commenter;
use TCG\Voyager\Models\Permission;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Notifiable, Sluggable, Favoriteability, Commenter , LastSeen , SoftDeletes, HasPushSubscriptions;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $additional_attributes = ['full_name'];

    protected $appends = ['fullName'];

    protected $fillable = [
        'role_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'avatar',
        'provider',
        'provider_id',
        'birthday',
        'number',
        'country',
        'city',
        'about_me',
        'slug',
        'default_password',
        'number'
    ];

    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function cars(){
//        return $this->hasMany(Car::class, 'owner_id', 'id');
//    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id');
    }

    public function profile()
    {
        return $this->hasOne($this->role_id == 3 ? OwnerProfile::class : Profile::class, 'user_id', 'id');
    }

//    public function favorites(){
//        return $this->hasMany(Favorite::class, 'user_id', 'id');
//    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'owner_id', 'id');
    }


    public function customers()
    {
        $customers = User::whereHas('orders', function ($query) {
            $query->whereHas('car', function ($query) {
                $query->where('cars.owner_id', $this->id);
            });
        });

        return $customers;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'fullName'
            ]
        ];
    }

    public function getTicketAdmins(){
        return $this->where('role_id', 1)->get();
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'target_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id', 'id');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_user');
    }

    public function markEmailAsVerified()
    {
        $this->default_password = '';
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }
}
