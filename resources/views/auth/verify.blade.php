@extends('layouts.app')
@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 m-5 p-5 text-center rounded">
                <div class="card">
                    <div class="card-header align-items-center ">
                        <p>
                            {{ __('auth.verify_title') }}
                        </p>
                        <p>
                            <a href="" class="change-email">Изменить электронный адрес?</a>
                        </p>
                        <form action="{{ route('verification.resend') }}" method="GET"
                              class="form-inline change-email-form  text-center{{ $errors->has('email') ? ' is-invalid' : '' }}" style="display: none">
                            <div class="form-group mx-sm-3 mb-2">
                                <input type="email" name="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       value="{{auth()->user()->email}}">
                                {{--                            <div class="form-group mx-sm-3 mb-2">--}}
                                {{--                                @if ($errors->has('email'))--}}
                                {{--                                    <span class="invalid-feedback" role="alert">--}}
                                {{--                                        <strong>{{ $errors->first('email') }}</strong>--}}
                                {{--                                    </span>--}}
                                {{--                                @endif--}}
                                {{--                            </div>--}}
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Отправить</button>
                        </form>
                    </div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('auth.verify_alert') }}
                            </div>
                        @endif

                        {{ __('auth.verify_content') }}
                        {{ __('auth.verify_info') }}, <a
                            href="{{ route('verification.resend') }}">{{ __('auth.verify_link') }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.change-email').click(function (e) {
            e.preventDefault();
            $('.change-email-form').slideToggle();
        })
    </script>
@stop
