@extends('owners.layouts.app')


@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">{{__('user.edit_profile')}}</h3>
                            <div class="row">
                                <div class="col-md-12 personal-info ff-st d-flex flex-column">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <img src="{{Voyager::image(auth()->user()->avatar)}}"
                                                     class="circle img-fluid"
                                                     alt="avatar" width="100px"
                                                     id="avatarProfile">
                                                <div class="unit mt-2">
                                                    <input name="avatar"
                                                           type="file"
                                                           data-target="#avatarProfile"
                                                           data-name="avatar"
                                                           class=" d-none choose-avatar-photo ajaxUpload">
                                                    <button type="button"
                                                            class="own-btn ora rounded-0 border-0 mt-2 m-auto px-3 btn btn-primary click-to-ajaxUpload">
                                                        {!! __('user.choose_avatar_photo') !!}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <form class="form-horizontal edit-profile"
                                          action="{{route('owner.profile.update')}}" role="form"
                                          id="editProfile"
                                          method="POST">
                                        @csrf
                                        <div class="form-row mb-3 pb-3">
                                            <div class="form-row-title col-lg-12">
                                                <h4>{{__('owner-profile.information')}}</h4>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.name')}}*:
                                                    @if (auth()->user()->provider)
                                                        <span class="k-pointer d-inline-block" data-toggle="tooltip"
                                                              data-html="true"
                                                              data-placement="top"
                                                              title="{{tooltips($toolips, ('note access_change_param'))}}">
                                                            <i class="fa fa-info-circle"></i>
                                                        </span>
                                                    @endif

                                                </label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                        name="{{!auth()->user()->provider ? 'first_name' : ''}}"
                                                        type="text" value="{{auth()->user()->first_name}}"
                                                        placeholder="{{__('owner-profile.name')}}"
                                                        {{auth()->user()->profile->status  == 'for_consideration' || auth()->user()->provider ? 'disabled' : ''}}>
                                                    @if ($errors->has('first_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('first_name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.sour_name')}}
                                                    *:
                                                    @if (auth()->user()->provider)
                                                        <span class="k-pointer d-inline-block" data-toggle="tooltip"
                                                              data-html="true"
                                                              data-placement="top"
                                                              title="{{tooltips($toolips, ('note access_change_param'))}}">
                                                            <i class="fa fa-info-circle"></i>
                                                        </span>
                                                    @endif
                                                </label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                        name="{{!auth()->user()->provider ? 'last_name' : ''}}"
                                                        type="text" value="{{auth()->user()->last_name}}"
                                                        placeholder="{{__('owner-profile.sour_name')}}"
                                                        {{auth()->user()->profile->status == 'for_consideration' || auth()->user()->provider ? 'disabled' : ''}}>
                                                    @if ($errors->has('last_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('last_name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.mail')}}*:
                                                    <span class="k-pointer d-inline-block" data-toggle="tooltip"
                                                          data-html="true"
                                                          data-placement="top"
                                                          title="{{tooltips($toolips, (!auth()->user()->provider ? 'change_user_email': 'note access_change_param'))}}">
                                                        <i class="fa fa-info-circle"></i>
                                                    </span>
                                                </label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                        type="text" value="{{auth()->user()->email ?? old('email')}}"
                                                        placeholder="{{auth()->user()->email??'example@example.com'}}"
                                                        name="{{auth()->user()->provider ? '' : 'email'}}"
                                                        {{auth()->user()->profile->status == 'for_consideration' || auth()->user()->provider ? 'disabled' : ''}}
                                                        required>
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">{{__('owner-profile.phone_number')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}"
                                                        id="number"
                                                        type="text" value="{{auth()->user()->number??old('number', null)}}" required>
                                                    @if ($errors->has('number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.birthday')}}
                                                    *:</label>
                                                <div class="col-lg-10">

                                                    <input
                                                        class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                                        name="birthday"
                                                        type="date"
                                                        id="bday"
                                                        value="{{date('Y-m-d', strtotime(auth()->user()->birthday))}}" required
                                                        {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                                    @if ($errors->has('birthday'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('birthday') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row border-bottom mb-3">
                                            <div class="form-row-title col-lg-12">
                                                <h4>{{__('owner-profile.general_information')}}</h4>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.company')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}"
                                                        name="company"
                                                        type="text"
                                                        placeholder="MyRentAuto"
                                                        value="{{auth()->user()->profile->company}}" required>
                                                    @if ($errors->has('company'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('company') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">{{__('owner-profile.reg_number')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('registration_number') ? ' is-invalid' : '' }} registration-number"
                                                        name="registration_number"
                                                        type="text"
                                                        value="{{auth()->user()->profile->registration_number??old('registration_number')}}"
                                                        placeholder="10226010/090617/0003344" required>
                                                    @if ($errors->has('registration_number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('registration_number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">{{__('owner-profile.legal_address')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('legal_address') ? ' is-invalid' : '' }}"
                                                        name="legal_address"
                                                        type="text"
                                                        value="{{auth()->user()->profile->legal_address??old('legal_address')}}"
                                                        placeholder="Б. Серпуховская, дом 43, офис 2" required>
                                                    @if ($errors->has('legal_address'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('legal_address') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">
                                                    {{__('owner-profile.physical_address')}}:
                                                    <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                                          data-placement="top"
                                                          title=" {!! tooltips($toolips, 'physical_address') !!}">
                                                            <i class="fas fa-info-circle"></i>
                                                    </span>
                                                </label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('physical_address') ? ' is-invalid' : '' }}"
                                                        name="physical_address"
                                                        type="text"
                                                        value="{{auth()->user()->profile->physical_address??old('physical_address')}}"
                                                        placeholder="Б. Серпуховская, дом 43, офис 2" required>
                                                    @if ($errors->has('physical_address'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('physical_address') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.unp')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('unp') ? ' is-invalid' : '' }} unp"
                                                        name="unp"
                                                        type="text"
                                                        value="{{auth()->user()->profile->unp??old('unp')}}"
                                                        placeholder="123456789" required>
                                                    @if ($errors->has('unp'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('unp') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">{{__('owner-profile.owner_name')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('owner') ? ' is-invalid' : '' }}"
                                                        name="owner"
                                                        type="text"
                                                        value="{{auth()->user()->profile->owner??old('owner')}}"
                                                        placeholder="Иван Иванов Иванович" required>
                                                    @if ($errors->has('owner'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('owner') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.assigned')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('assigned') ? ' is-invalid' : '' }}"
                                                        name="assigned"
                                                        type="text"
                                                        value="{{auth()->user()->profile->assigned??old('assigned')}}"
                                                        placeholder="На основании чего назначен" required>
                                                    @if ($errors->has('assigned'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('assigned') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.cont_number')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('cont_tel') ? ' is-invalid' : '' }}"
                                                        id="cont_tel"
                                                        type="text"
                                                        value="{{auth()->user()->profile->cont_tel??old('cont_tel')}}" required>
                                                    @if ($errors->has('cont_tel'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('cont_tel') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.cont_mail')}}*:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('cont_mail') ? ' is-invalid' : '' }}"
                                                        name="cont_mail"
                                                        type="email"
                                                        value="{{auth()->user()->profile->cont_mail??old('cont_mail')}}"
                                                        placeholder="example@example.com" required>
                                                    @if ($errors->has('cont_mail'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('cont_mail') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="col-lg-10 control-label">{{__('owner-profile.bank')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('bank') ? ' is-invalid' : '' }}"
                                                        name="bank"
                                                        type="text"
                                                        value="{{auth()->user()->profile->bank??old('bank')}}"
                                                        placeholder='ОАО "Белгазпромбанк"' required>
                                                    @if ($errors->has('bank'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('bank') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">{{__('owner-profile.bank_reg_number')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('bank') ? ' is-invalid' : '' }}"
                                                        name="bank_reg_number"
                                                        type="text"
                                                        value="{{auth()->user()->profile->bank_reg_number??old('bank')}}"
                                                        placeholder="БИК" required>
                                                    @if ($errors->has('bank'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('bank') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label
                                                    class="col-lg-10 control-label">{{__('owner-profile.bank_account_number')}}
                                                    *:</label>
                                                <div class="col-lg-10">
                                                    <input
                                                        class="form-control{{ $errors->has('bank') ? ' is-invalid' : '' }}"
                                                        name="bank_account_number"
                                                        type="text"
                                                        value="{{auth()->user()->profile->bank_account_number??old('bank_account_number')}}"
                                                        placeholder="{{__('owner-profile.bank_account_number')}}">
                                                    @if ($errors->has('bank_account_number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('bank_account_number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label
                                                class="col-lg-12 control-label">{{__('owner-profile.additional_information')}}</label>
                                            <textarea rows="5" name="about_me" class="form-control col-lg-12"
                                                      placeholder="{{__('owner-profile.additional_information')}}">{{auth()->user()->about_me}}</textarea>
                                        </div>
                                        <div class="form-group col-12">
                                            <label class="col-md-10 control-label"></label>
                                            <div class="col-md-10 text-center">
                                                <input type="submit"
                                                       class="own-btn btn btn-primary ora rounded-0 border-0 m-auto fw-400"
                                                       value="{{__('user.save')}}">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

<style>
    .has-error-inp {
        border-color: red !important;
        box-shadow: 0 0 0 0.2rem rgb(255, 0, 0);
    }
</style>

@section('script')

    <script>
        $(document).ready(function () {
            $('.select-withSearch').select2({
                placeholder: "Select a state"
            });

        });

        $(document).on('click', '.choose-avatar-photo-click', () => {
            $('.choose-avatar-photo').click()
        });
        $(document).on('click', '.click-to-ajaxUpload', function () {
            $(this).parents('.unit').find('.ajaxUpload').click();
        });

        $('#country').on('change', function () {
            var code = $(this).val();
            $.ajax({
                type: 'GET',
                url: '{{route('profile.states')}}/' + code,
                success: function (data) {
                    if (data.success) {
                        $('#region').html(data.view);
                    } else {
                        $('#region').html('');
                    }
                }
            });
        });

        var token = $('meta[name="csrf-token"]').attr('content');
        $(document).on('change', '.ajaxUpload', function (event) {
            event.preventDefault();
            var formData = new FormData;
            var img_id = $(this).data('target');
            var name = $(this).data('name');
            formData.append('img', $(this).prop('files')[0]);
            formData.append('type', name);
            formData.append('_token', token);
            $.ajax({
                type: 'POST',
                url: '{{route('owner.profile.ajax')}}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $(img_id).attr("src", data);
                }
            });
        });


        // $(".edit-profile").submit(function (e) {
        //     e.preventDefault();
        //     updateProfile(this);
        // });


        function sendToConfirm(send) {
            axios.get('{{route('profile.check_doc', auth()->user()->profile)}}')
                .then((response) => {
                    if (response.data.success) {
                        location.href = $(send).attr('href')
                    } else {
                        if (response.data.errors) {
                            $('.has-error-inp').removeClass('has-error-inp');
                            $.each(response.data.errors, function (t, n) {
                                var i = $("[name='" + t + "']"), r = i.first().parent().offset().top,
                                    o = $("nav.navbar").height();
                                if (i.attr('type') == 'file') i = i.parent('.unit').find('button');
                                0 === Object.keys(response.data.errors).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 70 + "px"}, "fast"), i.addClass("has-error-inp").find('.select2 ').addClass("has-error-inp");
                            })
                        }
                    }
                });
        }

        function updateProfile(form, send) {
            var t = $(form).attr("action"), n = $(form), i = new FormData(form);
            $.ajax({
                url: t,
                type: "POST",
                dataType: "json",
                data: i,
                processData: !1,
                contentType: !1,
                beforeSend: function () {
                    $("body").css("cursor", "progress"), $(".has-error").removeClass("has-error"), $(".help-block").remove()
                },
                success: function (e) {
                    $("body").css("cursor", "auto");
                    if (e.errors) {
                        $('.has-error-inp').removeClass('has-error-inp');
                        $.each(e.errors, function (t, n) {
                            var i = $("[name='" + t + "']"), r = i.first().parent().offset().top,
                                o = $("nav.navbar").height();
                            0 === Object.keys(e.errors).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 50 + "px"}, "fast"), i.addClass("has-error-inp").parent().find('.select2 ').addClass("has-error-inp")
                        })
                    } else {
                        if (send) {
                            if (e.check_email == '') {
                                sendToConfirm(send);
                            } else {
                                myAlert(e.message + e.check_email, 'success')
                            }
                        } else {
                            myAlert(e.message + e.check_email, 'success')
                        }

                    }

                },
            })
        }

        $('[data-toggle="tooltip"]').tooltip()

        $(document).ready(function () {
            $('#owner_carsTable').DataTable({
                responsive: true,

                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [5, 6, 7, 8]}
                ]
            });
        });
        $('.unp , .registration-number').mask('000000000', {reverse: true});

        var input = document.querySelector("#number");

        // initialise plugin
        var iti = window.intlTelInput(input, {
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get('https://ipinfo.io', function () {
                }, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "by";
                    callback(countryCode);
                });
            },
            separateDialCode: true,
            nationalMode: true,
            hiddenInput: "number",
            utilsScript: "/js/utils.js"
        });
        var cont_tel = document.querySelector("#cont_tel");

        // initialise plugin
        var iti_cont_tel = window.intlTelInput(cont_tel, {
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get('https://ipinfo.io', function () {
                }, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "by";
                    callback(countryCode);
                });
            },
            separateDialCode: true,
            nationalMode: true,
            hiddenInput: "cont_tel",
            utilsScript: "/js/utils.js"
        });
    </script>
@stop
