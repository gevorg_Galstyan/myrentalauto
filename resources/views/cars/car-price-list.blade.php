<div class="dey-price display-none  justify-content-be">
    <div class="form-group d-flex align-items-center justify-content-center"
         data-toggle="tooltip" data-html="true"
         title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') == 1 ? 'shadow-lg  to-day' : ''}} "
             data-days="1-1">
            <div class="days text-center"><span class="count-day-text">1 {{__('car.day')}} </span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency($car->price_1) !!}</span> <span class="day count-day-text"></span></div>
        </div>
    </div>
    <div class="form-group d-flex align-items-center justify-content-center"
             data-toggle="tooltip" data-html="true"
             title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') >= 2 && request()->input('days') <= 3 ? 'shadow-lg  to-day' : ''}}"
             data-days="2-3">
            <div class="days text-center"><span class="count-day-text">2-3 {{__('car.days')}}</span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency(carPriceInPercent($car, $car->price_2_3), false, false) !!} <span class="currency-symbol"></span></span><span class="day count-day-text"></span>
            </div>
        </div>
    </div>
    <div class="form-group d-flex align-items-center justify-content-center"
             data-toggle="tooltip" data-html="true"
             title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') >= 4 && request()->input('days') <= 6 ? 'shadow-lg  to-day' : ''}}"
             data-days="4-6">
            <div class="days text-center"><span class="count-day-text">4-6 {{__('car.days')}}</span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency(carPriceInPercent($car, $car->price_4_6), false, false) !!} <span class="currency-symbol"></span></span><span class="day count-day-text"></span>
            </div>
        </div>
    </div>
    <div class="form-group d-flex align-items-center justify-content-center"
             data-toggle="tooltip" data-html="true"
             title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') >= 7 && request()->input('days') <= 14 ? 'shadow-lg  to-day' : ''}}"
             data-days="7-14">
            <div class="days text-center"><span class="count-day-text">7-14 {{__('car.days')}}</span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency(carPriceInPercent($car, $car->price_7_14), false, false) !!} <span class="currency-symbol"></span></span><span class="day count-day-text"></span>
            </div>
        </div>
    </div>
    <div class="form-group d-flex align-items-center justify-content-center"
             data-toggle="tooltip" data-html="true"
             title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') >= 15 && request()->input('days') <= 22 ? 'shadow-lg  to-day' : ''}}"
             data-days="15-22">
            <div class="days text-center"><span class="count-day-text">15-22 {{__('car.days')}}</span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency(carPriceInPercent($car, $car->price_15_22), false, false) !!} <span class="currency-symbol"></span></span><span class="day count-day-text"></span>
            </div>
        </div>
    </div>
    <div class="form-group d-flex align-items-center justify-content-center"
             data-toggle="tooltip" data-html="true"
             title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') >= 23 && request()->input('days') <= 30 ? 'shadow-lg  to-day' : ''}}"
             data-days="23-30">
            <div class="days text-center"><span class="count-day-text">23-30 {{__('car.days')}}</span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency(carPriceInPercent($car, $car->price_23_30), false, false) !!} <span class="currency-symbol"></span></span><span class="day count-day-text"></span>
            </div>
        </div>
    </div>
    <div class="form-group d-flex align-items-center justify-content-center"
             data-toggle="tooltip" data-html="true"
             title='{!! tooltips($toolips, 'car_price_list_info') !!}'>
        <div class="days-count bg-white rounded {{request()->input('days') >= 31 ? 'shadow-lg  to-day' : ''}}"
             data-days="31-31">
            <div class="days text-center"><span class="count-day-text">31 {{__('car.day_more')}}</span></div>
            <div class="price-for-day text-center"><span class="ff-st">{!! currency(carPriceInPercent($car, $car->price_31), false, false) !!} <span class="currency-symbol"></span></span><span class="day count-day-text"></span>
            </div>
        </div>
    </div>
</div>
