@extends('owners.layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop

@section('content')

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Страница ордера</h3>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="text-center">
                                <img src="{{Voyager::image($order->customer->avatar)}}" class="img-fluid" width="100%"
                                     alt="avatar">
                            </div>
                        </div>
                        <div class="col-md-9 personal-info ff-st d-flex flex-column">
                            <p class="row">
                                <b class="fw-700 col-lg-3">Customer: </b>
                                <span class="fw-400 col-lg-9">{{$order->customer ? $order->customer->full_name : 'Пользователь не найден '}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Дата: </b>
                                <span class="fw-400 col-lg-9">{{$order->start_date}} - {{$order->end_date}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Валюта: </b>
                                <span class="fw-400 col-lg-9">{{$order->currency}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Цена за один день: </b>

                                <span class="fw-400 col-lg-9">{{$order->price_per_day}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Цена оплаты: </b>

                                <span class="fw-400 col-lg-9">
                                    {{$order->price_to_pay}}
                                </span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Заметка: </b>

                                <span class="fw-400 col-lg-9">{{$order->note}}</span>
                            </p>
                            <p class="row">
                                <b class="fw-700 col-lg-3">Окончательная цена: </b>

                                <span class="fw-400 col-lg-9">{{$order->total_price}}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>


    </script>
    {{--<script>--}}

    {{--$('[data-toggle="tooltip" data-html="true"]').tooltip()--}}

    {{--$(document).ready(function () {--}}
    {{--$('#owner_carsTable').DataTable({--}}
    {{--responsive: true,--}}

    {{--"aoColumnDefs": [--}}
    {{--{'bSortable': false, 'aTargets': [5, 6, 7, 8]}--}}
    {{--]--}}
    {{--});--}}
    {{--});--}}

    {{--</script>--}}

@stop
