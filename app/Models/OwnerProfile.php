<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class OwnerProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company',
        'legal_form',
        'legal_address',
        'car_address',
        'registration_certificate',
        'working_time',
        'tin',
        'bank_id_code',
        'correspondent_bill',
        'bank',
        'bank_address',
        'card_bill',
        'user_id',
    ];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function check()
    {
        $data = array_merge($this->toArray(), $this->user->toArray());
        $val_data = [
            'number' => 'required|string|max:16',
            'birthday' => 'required|date|before:13 years ago',
            'company' => 'required',
            'registration_number' => 'required',
            'legal_address' => 'required',
            'unp' => 'required',
            'owner' => 'required',
            'assigned' => 'required',
            'cont_tel' => 'required',
            'cont_mail' => 'required|email',
            'bank' => 'required',
            'bank_account_number' => 'required',

        ];
        if (!$this->user->provider){
            $val_data['last_name'] =  'required|string|max:255';
            $val_data['first_name'] =  'required|string|max:255';
            $val_data['email'] =  'required|string|email|max:255|unique:users,email,'.auth()->id();
        }

        $validator = Validator::make($data, [
            'number' => 'required|string|max:16',
            'birthday' => 'required|date|before:13 years ago',
            'company' => 'required',
            'registration_number' => 'required',
            'legal_address' => 'required',
            'unp' => 'required',
            'owner' => 'required',
            'assigned' => 'required',
            'cont_tel' => 'required',
            'cont_mail' => 'required|email',
            'bank' => 'required',
            'bank_account_number' => 'required',
        ]);
        if ($validator->fails()) return false;
        else return true;
    }
}
