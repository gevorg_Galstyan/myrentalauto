@extends('layouts.app')
{{--meta tags--}}
@section('meta')
    <title>{{$page->translate()->page_title}}</title>
    <meta name="keywords" content="{{$page->translate()->meta_keywords}}">
    <meta name="description" content="{{$page->translate()->meta_description}}">

    <meta property="og:locale" content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$page->translate()->page_title}}"/>
    <meta property="og:description" content="{{$page->translate()->meta_description}}"/>
    <meta property="og:image" content="{{Voyager::image('img/Logo_my_rent_auto.png')}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title" content="{{$page->translate()->page_title}}">
    <meta name="twitter:description" content="{{$page->translate()->meta_description}}">
    <meta name="twitter:image" content="{{Voyager::image('img/Logo_my_rent_auto.png')}}">
{{--    <link href="{{ LaravelLocalization::getNonLocalizedURL(route('pages', ['slug' => $page->slug])) }}" rel="canonical">--}}

@stop
{{--page title--}}
@section('title')
    {{$page->translate()->title}}
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
@stop
@section('content')
    <div class="my-container my-5 p-3 p-md-5 bg-white border-radius-20">
        <!-- Page Content Column -->

        <!-- Title -->
        @if ($page->title)
            <h1 class="text-center fw-700  sub-heading">{{$page->translate()->title}}</h1>
        @endif
    <!-- Preview Image -->
        <div class="p-3 p-md-5 ff-ms">{!! $page->translate()->body !!}</div>
        @isset($page->pdf)
            @php($pdf = json_decode($page->pdf, true))
            @php($pdf = isset($pdf[0]['download_link']) ? $pdf[0]['download_link'] : false)
            @if ($pdf)
                <div id="my_pdf_viewer"></div>

                <div class="row justify-content-center">
                    <div class="row col-12 justify-content-center">
                        <nav class="next-prev d-none">
                            <ul class="pagination m-0">
                                <li class="page-item align-self-center" id="go_previous">
                                    <a class="page-link" href="#0" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="fs-16 align-self-center">
                                    <span class="current-page">1 </span>
                                    <span class="">{{__('general.of')}}</span>
                                    <span class="page-count"></span>
                                </li>
                                <li class="page-item align-self-center" id="go_next">
                                    <a class="page-link" href="#0" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                            <input id="current_page" value="1" type="hidden"/>
                        </nav>
                    </div>
                    <div id="canvas_container" class="position-relative">
                        <canvas id="pdf_renderer" class="col-12"></canvas>
                        <div class="plus-minus-input btn-group d-none d-md-flex position-absolute mt-3">
                            <div class="input-group-button mr-3">
                                <button type="button" id="zoom_out" class="button hollow circle border-0"
                                        data-quantity="plus" data-field="quantity">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="input-group-button ">
                                <button type="button" id="zoom_in" class="button hollow circle border-0"
                                        data-quantity="minus" data-field="quantity">
                                    <i class="fa fa-plus" aria-hidden="true"></i>

                                </button>
                            </div>
                        </div>
                        <input id="current_page" value="1" type="hidden"/>
                    </div>
                </div>


            @endif
        @else
            @php($pdf = false)
        @endisset

    </div>

@endsection

@section('script')
    @if ($pdf)
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>

        <script>
            var myState = {
                pdf: null,
                currentPage: 1,
                zoom: 2
            };
            var page_cont = 1;
            // more code here
            pdfjsLib.getDocument('{{route('page.getDocument', ['page' => $page]) }}').then((pdf) => {
                page_cont = pdf._pdfInfo.numPages;
                myState.pdf = pdf;
                render();
                if (page_cont > 1) {
                    $('.page-count').text(' '+page_cont);
                    $('.next-prev').removeClass('d-none');
                    show_hide_arrow()
                }
            });

            function render() {
                myState.pdf.getPage(myState.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState.zoom);
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }

            function show_hide_arrow() {
                var previous = $('#go_previous');
                var next = $('#go_next');

                if (myState.currentPage == 1){
                    previous.css('visibility', 'hidden');
                }else{
                    previous.css('visibility', 'visible');
                }

                if ((myState.currentPage + 1) > myState.pdf._pdfInfo.numPages){
                    next.css('visibility', 'hidden');
                }else{
                    next.css('visibility', 'visible');
                }
            }

            document.getElementById('go_previous')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null
                        || myState.currentPage == 1) return;
                    myState.currentPage -= 1;
                    show_hide_arrow()
                    $('.current-page').text(myState.currentPage+' ')
                    document.getElementById("current_page")
                        .value = myState.currentPage;
                    render();
                });
            document.getElementById('go_next')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null
                        || (myState.currentPage + 1) > myState.pdf._pdfInfo.numPages)
                        return;

                    myState.currentPage += 1;
                    show_hide_arrow()
                    $('.current-page').text(myState.currentPage+' ')
                    document.getElementById("current_page")
                        .value = myState.currentPage;
                    render();
                });
            document.getElementById('zoom_in')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null) return;
                    myState.zoom += 0.5;
                    render();
                });
            document.getElementById('zoom_out')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null) return;
                    myState.zoom -= 0.5;
                    render();
                });
        </script>
    @endif
@stop
