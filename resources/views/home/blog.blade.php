
<div class="bg-white py-3 mt-5 ff-st">
    <div class="own-container mt-5">
        <h5 class="text-center mb-5 fw-700 sub-heading " id="section-blog">{{__('home.blog')}}</h5>
        <div id="blog">
            @if ($width > 991)
                <div class="d-flex justify-content-between">
                    @foreach($posts as $post)
                        <div class="col">
                            <h6 class="card-title h4">
                                <a href="{{route('post.single', ['slug' => $post->slug])}}">{!! strip_tags($post->myTranslate()->title) !!}</a>
                            </h6>
                        </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-between">
                    @foreach($posts as $post)
                        <div class="col">
                            @if ($post->updated_at->diffInHours($post->created_at) > 24)
                                <div class="small text-muted mb-3"><span>{{__('post.updated')}}:</span><span
                                        class="pl-2">{{$post->updated_at->format('Y-m-d')}}</span></div>
                            @else
                                <div class="small text-muted mb-3">{{$post->created_at->format('Y-m-d')}}</div>
                            @endif

                            <div class="author d-flex  col-12 p-0 mb-3">
                                <div class="avatar mr-3"
                                     style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                                </div>
                                <div>
                                <span
                                    class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</span>
                                    <small
                                        class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                                </div>
                            </div>


                            <div class="col-12 p-0 ff-ms">
                                <p class="">
                                    {{$post->myTranslate()->excerpt}}
                                    <a href="{{route('post.single', ['slug' => $post->slug])}}"
                                       class="d-inline-block read-post">
                                        {{__('post.read_article')}} →
                                    </a>
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class=" d-flex d-lg-none flex-wrap">
                    @foreach($posts as $post)


                        <div class="col-lg-4  border-0 p-5">

                            <div class="col-12 p-0">
                                <h6 class="card-title h4">
                                    <a href="{{route('post.single', ['slug' => $post->slug])}}">{!! strip_tags($post->myTranslate()->title) !!}</a>
                                </h6>
                                @if ($post->updated_at->diffInHours($post->created_at) > 24)
                                    <div class="small text-muted mb-3"><span>{{__('post.updated')}}:</span><span
                                            class="pl-2">{{$post->updated_at->format('Y-m-d')}}</span></div>
                                @else
                                    <div class="small text-muted mb-3">{{$post->created_at->format('Y-m-d')}}</div>
                                @endif
                            </div>

                            <div class="author d-flex  col-12 p-0 mb-3">
                                <div class="avatar mr-3"
                                     style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                                </div>
                                <div>
                                <span
                                    class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</span>
                                    <small
                                        class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                                </div>
                            </div>


                            <div class="col-12 p-0 ff-ms">
                                <p class="">
                                    {{$post->myTranslate()->excerpt}}
                                    <a href="{{route('post.single', ['slug' => $post->slug])}}"
                                       class="d-inline-block read-post">
                                        {{__('post.read_article')}} →
                                    </a>
                                </p>
                            </div>
                        </div>


                    @endforeach
                </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <a href="{{route('posts')}}"
               class="own-btn">
                {{__('post.see_more')}}
            </a>
        </div>
    </div>
</div>
