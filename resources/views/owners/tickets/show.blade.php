@extends('owners.layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop
@section('content')
    <style>
        .btn-file {
            text-align: center;
            width: auto;
            display: inline-block;
            min-width: auto;
            margin: 15px 0;
        }
    </style>
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card panel-bordered">
                        <div class="card-header d-flex justify-content-between">
                            <div class="col text-center">
                                <strong>Номер</strong> : <span>#{{$ticket->ticket_number}}</span>
                            </div>
                            <div class="col text-center">
                                <strong>Тип</strong> :
                                <span>{{$ticket->priority ? $ticket->priority->title : ''}}</span>
                            </div>
                            <div class="col text-center">
                                <strong>Статус</strong> :
                                @if ($ticket->status == 'open')
                                    <span class="badge badge-success">Открыт</span>
                                @else
                                    <span class="badge badge-danger">Закрыт</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card panel-bordered">
                        <div class="card-header d-block">
                            <div class="d-flex  justify-content-center">
                                <div class="col-3"></div>
                                <div class="col-6">
                                    <p class="h4 text-center sub-heading">Тема </p>
                                </div>
                                <div class="col-3 text-right">
                                    {{$ticket->created_at->format('d-m-Y H:i')}}
                                </div>
                            </div>
                            <div class="col-12">
                                <h3 class="text-center">
                                    {{$ticket->title}}
                                </h3>
                            </div>

                        </div>
                        <div class="card-body">
                            <?php $images = json_decode($ticket->images); ?>
                            <div class="row mx-0">
                                <div class="col-{{$images != null ? 8 : 12}}">
                                    <p class="ff-ms"> {!! $ticket->text !!} </p>
                                </div>
                                @if($images != null)
                                    <div class="col-4 border-left d-flex flex-wrap overflow-auto">
                                        <div class="col-12 mb-4">
                                            <p class="h4 text-center sub-heading">Фото</p>
                                        </div>
                                        @foreach($images as $image)
                                            <div class="img_settings_container col-6">
                                                <a class="sl-slider-slide" data-fancybox="ticket">
                                                    <img src="{{ Voyager::image( $image ) }}" class="w-100">
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>

                                @if ($ticket->comments()->count())
                                    <div class="card border-0">
                                        {{--                                <div class="card-header">--}}
                                        {{--                                    <p class="h5 text-center sub-heading">--}}
                                        {{--                                        Ответы--}}
                                        {{--                                    </p>--}}
                                        {{--                                </div>--}}
                                        <div class="card-body">
                                            @foreach ($ticket->comments as $item)

                                                <div class="row bg-light py-3 my-4 rounded">
                                                    <div class="col-12 border-bottom d-flex justify-content-between ">
                                                        <div class="py-2"><strong>{{$item->creator->full_name}}</strong></div>
                                                        <div class="py-2">{{$item->created_at->format('d-m-Y H:i')}}</div>
                                                    </div>
                                                    <?php $images = json_decode($item->images); ?>
                                                    <div class="row mx-0 my-5">
                                                        <div class="col-{{$images != null ? 8 : 12}}">
                                                            <p class="ff-ms"> {!! $item->text !!} </p>
                                                        </div>
                                                        @if($images != null)
                                                            <div class="col-4 border-left d-flex flex-wrap overflow-auto">
                                                                @foreach($images as $image)
                                                                    <div class="img_settings_container col-6">
                                                                        <a class="sl-slider-slide" data-fancybox="ticket">
                                                                            <img src="{{ Voyager::image( $image ) }}"
                                                                                 class="w-100">
                                                                        </a>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif

                            @if ($ticket->status == 'open')
                                <form role="form" action="{{ route('owner.tickets.comment', $ticket->ticket_number) }}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group col-12">
                                        <label>Ответить</label>
                                        <textarea name="text" class="form-control " rows="6" placeholder="Описание"
                                                  id="editor"></textarea>
                                    </div>
                                    <div class="d-flex">
                                        <div class="col text-left">
                                            <label for="input-24">Прикрепите файл</label>
                                            <input id="input-24" name="images[]" type="file" multiple>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <div class="col-12 align-self-center text-center">
                                            <button class="btn btn-success pull-right" type="submit">Отправить билет
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
        $("#input-24").fileinput({
            language: "ru",
            theme: "fas",
            quality: 0.75,
            showUpload: false,
            maxFileSize: 5120,
            dropZoneEnabled: false,
            showCaption: false,
            showCancel: false,
            showRemove: false,
            // layoutTemplates: {footer: ''},
        });

    </script>
@stop
