<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Notifications\TicketNotification;
use App\Notifications\TicketReply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;

class TicketController extends VoyagerBaseController
{
    public function reply(Request $request, Ticket $ticket)
    {
        $comment = new TicketComment();
        $comment->body = $request->reply;
        $comment->ticket_id = $ticket->id;
        $comment->save();

        $ticket->creator->notfy(new TicketReply($comment));
        return back()->with([
            'status' => __('voyager::generic.successfully_added_new') . " Ответить ",
            'alert-type' => 'success',
        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        $data->target->notify(new TicketNotification($data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function close(Ticket $ticket)
    {
        $ticket->status = 'closed';
        $ticket->save();
        return back()->with([
            'message'    => 'Тикет закрыт',
            'alert-type' => 'success',
        ]);
    }
}
