@extends('layouts.app')

@section('meta')
    @php($meta = getMetaTags('single_car_page'))
    <title>
        {{$car->meta_title? $car->myTranslate()->meta_title :str_replace(['%property%', '%location%'], [$car->title, $car->meta_location_title], $meta->translate()->title)}}
    </title>
    <meta name="keywords" content="{{$car->meta_keyword? $car->myTranslate()->meta_keyword :$meta->translate()->Keyword}}"/>
    <meta name="description" content="{{$car->meta_description? $car->myTranslate()->meta_description :$meta->translate()->description}}"/>

    <meta property="og:locale"
          content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title"
          content="{{$car->meta_title? $car->myTranslate()->meta_title :str_replace(['%property%', '%location%'], [$car->title, $car->meta_location_title], $meta->translate()->title)}}"/>
    <meta property="og:description" content="{{$car->meta_description? $car->myTranslate()->meta_description :$meta->translate()->description}}"/>
    <meta property="og:image" content="{{Voyager::image($car->image_1)}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title"
          content="{{$car->meta_title? $car->myTranslate()->meta_title :str_replace(['%property%', '%location%'], [$car->title, $car->meta_location_title], $meta->translate()->title)}}">
    <meta name="twitter:description" content="{{$car->meta_description? $car->myTranslate()->meta_description :$meta->translate()->description}}">
    <meta name="twitter:image" content="{{Voyager::image($car->image_1)}}">

{{--    <link href="{{ request()->url() }}" rel="canonical">--}}

@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
@stop
@section('content')
    <!-- PURCHESH CARS -->
    <section class="own-container single-car-page my-3">
        <div class="row">
            <div class="col-md-12  m-auto">
                <div class="bg-white shadow-lg  rounded p-3 mb-3">
                    <div class="row justify-content-start">
                        @if(!isset($preview))
                            <div class="col-4">
                                <a href="{{route('search',  request()->only('location', 'date', 'attributes', 'transmission', 'price', 'fuel_consumption_min', 'fuel_consumption_max', 'type', 'seats', 'age' , 'fuel'))}}"
                                   class="btn own-btn ora d-inline-block  border-5 py-0 px-1"
                                   rel="nofollow"
                                   title="Вернуться к поиску">
                                    <i class="fas fa-arrow-left"></i>
                                    <span class="">Поиск</span>
                                </a>
                            </div>
                        @endif
                        <div class="col-4  ">
                            <h1 class="text-center single-header fw-700 mb-4">{{$car->title}} {{$car->city ? __('car.in').' '.$car->translate()->city : ''}}</h1>
                        </div>
                    </div>

                    {{--<h5 class="text-center color-light fw-700">{{__('car.receipt')}}</h5>--}}
                    <form action="{{route('car.get_prices', ['car' => $car,'barcode' => request()->get('orderId')])}}"
                          id="single-car-params" method="POST">
                        @csrf
                        <div class="form-row justify-content-between">
                            <div class="form-group col-md-6" id="search-block">
                                <input type="hidden" class="date-input"
                                       name="date" autocomplete="off"
                                       value="{{request()->date}}"/>
                                <div
                                    class="form-control  date-tame-x text-center own-shadow date-block-single ">{{request()->date??__('car.pick_date')}}</div>

                            </div>
                            <div class="form-group col-md-3">
                                <select name="took" id="took"
                                        class="form-control text-truncate own-shadow car-attribute font-weight-bold">
                                    <option value="">{{__('car.receive')}}</option>
                                    @foreach ($car->deliveries as $delivery)
                                        <option class="text-truncate"
                                                value="{{$delivery->id}}">{{$delivery->translate()->location}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <select name="give" id="give"
                                        class="form-control text-truncate own-shadow car-attribute font-weight-bold">
                                    <option value="">{{__('car.return')}}</option>
                                    @foreach ($car->deliveries as $delivery)
                                        <option class="text-truncate"
                                                value="{{$delivery->id}}">{{$delivery->translate()->location}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                    {{-- CAR PRICE LIST --}}
                    @include('cars.car-price-list')
                </div>
                    <div class="bg-white shadow-lg  rounded p-3 ">
                    <div class="d-flex flex-wrap">
                        <div class="col-md-12 text-right">
                            <button class="map-button mb-3 mr-0">{{__('car.open_map')}}
                                <img src="{{asset('storage/img/icons/location.png')}}" class="img-fluid ml-2"
                                     alt="location">
                            </button>

                            <div id="map" class="pb-3"></div>
                        </div>

                        <div class="carousel col-md-6">
                            <div class="slick-slider-flex">
                                <div class="sl-slider">
                                    @foreach ($car->images as $image)
                                        @if (!$image) @continue @endif
                                        <a class="sl-slider-slide" data-fancybox="gallery"
                                           href="{{Voyager::image($image)}}">
                                            <img data-src="{{Voyager::image($image)}}"
                                                 data-srcset="{{Voyager::image($image)}}"
                                                 data-sizes="auto"
                                                 src="{{Voyager::image(str_replace('.jpg', '-tumble.jpg',$image))}}"
                                                 alt="{{$car->title}}"
                                                 class="img-fluid lazyload">
                                        </a>
                                    @endforeach
                                </div>
                                <div class="sl-slider-nav">
                                    @foreach ($car->images as $image)
                                        @if (!$image) @continue @endif
                                        <div class="sl-slider-slide">
                                            <img src="{{Voyager::image(str_replace('.jpg', '-tumble.jpg',$image))}}"
                                                 alt="{{$car->title}}" class="img-fluid w-100">
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="bg-white shadow-lg  rounded mt-2">
                                <div class="single-options">
                                    <div id="accordion-plan">
                                        <div class="">
                                            <button class="spec-button w-100 " data-toggle="collapse"
                                                    data-target="#collapse-plan" aria-expanded="true"
                                                    aria-controls="collapse-plan">
                                                {{__('car.enabled')}}
                                                <i class="fa" aria-hidden="true"></i>
                                            </button>
                                        </div>


                                        <div id="collapse-plan"
                                             class="collapse show fs-16 my-1  rounded "
                                             data-parent="#accordion-spec">
                                            <ul class="plan-enabled py-4">
                                                <li>
                                                    <span>{{__('car.mileage_limit')}}: </span>
                                                    <span>{{$car->mileage_limit?$car->mileage_limit . __('general.km') : __('general.no') }}</span>
                                                    @if ($car->mileage_limit)
                                                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                                              data-placement="top"
                                                              title="{!! tooltips($toolips, 'mileage_limit_desc', $car->mileage_limit_for_every, currency($car->mileage_limit_charged)) !!}">
                                                         <i class="fas fa-info-circle"></i>
                                                    </span>
                                                    @endif

                                                </li>
                                                @foreach ($tariffPlan as $item)

                                                    <li class="mt-2">
                                                        {{--                                                        {{dd($item['name'])}}--}}
                                                        {{$item['name']}}
                                                        {!! $item['desc'] ?? '' !!}
                                                    </li>

                                                    {{--                                                <div class="row m-0 align-items-center justify-content-between">--}}
                                                    {{--                                                    <span>--}}
                                                    {{--                                                        {{$item['name']}}--}}
                                                    {{--                                                        {!! $item['desc'] ?? '' !!}--}}
                                                    {{--                                                    </span>--}}
                                                    {{--                                                </div>--}}
                                                @endforeach
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @if ($car->note)
                                <div class="bg-white shadow-lg  rounded mt-2 mb-3 p-4">
                                    <h5 class="text-left">{{__('car.description')}} : </h5>
                                    <div class="ff-ms fs-15">
                                        {!! nl2br($car->translate()->note) !!}
                                    </div>
                                </div>
                            @endif

                        </div>

                        <div class="col-md-6  mob-padding-0">
                            <div class="total-block">
                                @include('cars.total-block')
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <hr class="w-100">
                    </div>
                    <div>
                        @include('cars.reviews')
                    </div>
                </div>
            </div>

        </div>

    </section>
    @if(isset($preview))
        <div class="row mr-5 my-5">
            <div class="col-lg-12 mr-5">
                @if(\Auth::user()->role_id == 1)
                    <a href="{{route('voyager.cars.status', ['car' => $car, 'status' => 'publish'])}}"
                       class="btn btn-success float-right mr-5">Опубликовать</a>
                    <a href="{{route('voyager.cars.edit', $car->id)}}" class="btn btn-primary float-right mr-5"><i
                            class="fas fa-edit"></i> Изменит</a>
                @endif
                @if(\Auth::user()->role_id == 3)
                    <a href="{{route('cars.edit', $car->id)}}" class="btn btn-primary float-right mr-5"><i
                            class="fas fa-edit"></i> Изменит</a>
                @endif
                <a href="{{url()->previous()}}" class="btn btn-secondary float-right mr-5"><i
                        class="fas fa-arrow-left"></i> Назад</a>
            </div>
        </div>
    @endif
    @php($all_dates = [])
    @foreach($car->busyDays as $item)
        <?php
        $startDate = new \Carbon\Carbon($item->start);
        $endDate = new \Carbon\Carbon($item->end);
        while ($startDate->lte($endDate)) {
            $a = $startDate;

            $all_dates[] = \Carbon\Carbon::parse($a)->format('d-m-Y');

            $startDate->addDay();
        }
        ?>
    @endforeach
    <input type="hidden" form="single-car-params" name="collap" value="attribute">
    @guest
        @include('cars.auth-booking-modal')
    @endguest
@endsection
@section('script')
    <div class="map-script"></div>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <script src="{{asset('js/validation/messages_'.LaravelLocalization::getCurrentLocale().'.js')}}"></script>

    <script src="{{asset('js/intl-tel-input.js')}}"></script>
    <script>
        // $('.own-container').preloader()
        var ymaps = false;
        $(document).on('click', '.collap', function () {
            $('input[name="collap"]').val($(this).data('col'));
        });
        order_date = jQuery.parseJSON('{!! json_encode($all_dates) !!}');
        $("#single-car-params").submit(function (e) {

            e.preventDefault();
            var t = $(this).attr("action"), n = $(this), i = new FormData(this);
            $('#single-car-params').preloader();
            // alert(1)
            $.ajax({
                url: t,
                type: "POST",
                dataType: "json",
                data: i,
                processData: !1,
                contentType: !1,
                beforeSend: function () {
                    $("body").css("cursor", "progress"), $(".has-error").removeClass("has-error"), $(".help-block").remove()
                },
                success: function (e) {
                    $("body").css("cursor", "auto");
                    if (e.error) {
                        $("#single-car-params").preloader('remove');
                        var i = n.find('input[type="submit"]'), r = i.first().parent().offset().top,
                            o = $("nav.navbar").height();
                        0 === Object.keys(e.errors).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 50 + "px"}, "fast"), i.parent().addClass("has-error").append("<span class='help-block' style='color:#f96868'>" + e.error + "</span>")

                    } else {
                        $(n).unbind("submit").submit()
                    }

                },
            })
        });
        $('.dey-price').on('init', function (evt, slick, direction) {
            $('.dey-price').removeClass('display-none')
        });
        $('.dey-price').slick({
            dots: true,
            infinite: false,
            // centerMode: true,
            speed: 300,
            slidesToShow: 7,
            slidesToScroll: 7,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $(".dey-price").slick('slickGoTo', parseInt($('.days-count.to-day').parents('.slick-slide').index()), false);

        $('.sl-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            slick: true,
            infinite: false,
            adaptiveHeight: true,
            asNavFor: '.sl-slider-nav'
        });
        $('.sl-slider-nav').slick({
            adaptiveHeight: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            vertical: false,
            asNavFor: '.sl-slider',
            focusOnSelect: true,
            verticalSwiping: true,
            arrows: false,
        });

        $('.date-tame-x').dateRangePicker({
            startOfWeek: 'monday',
            container: '#search-block',
            separator: ' ~ ',
            format: 'DD.MM.YYYY HH:mm',
            minDays: 1,
            autoClose: false,
            language: locale,
            swapTime: false,
            hoveringTooltip: function (e, t, a) {
                return "";
            },
            startDate: moment().format('DD-MM-YYYY'),
            beforeShowDay: function (t) {

                var myDate = moment(t);

                var _class = '';
                var _tooltip = '';
                if (!(order_date.indexOf(moment(t).format('DD-MM-YYYY')) == -1)) {
                    _class = 'red';
                    _tooltip = 'На этот день машина занята ';

                } else if (myDate.isBefore(moment().add(3, 'd'))) {
                    _class = 'urgency';
                    _tooltip = urgency_text;
                }
                var valid = (order_date.indexOf(moment(t).format('DD-MM-YYYY')) == -1);  //disable saturday and sunday
                return [valid, _class, _tooltip];
            },
            time: {
                enabled: true
            },
            getValue: function () {
                this.value = $('.date-input').val();
                return this.value;
            },
            setValue: function (s) {
                this.value = $('.date-input').val();
                this.innerHTML = date_dive_content_show(s);
            }
        }).bind('datepicker-apply', function (event, obj) {

            $('.date-input').val(obj.value)
            getCarPricis();
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = obj.date2.getTime() - obj.date1.getTime();
            var days = (millisBetween / millisecondsPerDay) + 1;
            $('.days-count').removeClass('shadow-lg  bg-white rounded  to-day');

            if (days >= 31) {
                $('.days-count[data-days="31-31"]').addClass('shadow-lg  bg-white rounded  to-day');
            } else {
                $('.days-count').each(function () {
                    var al = $(this).data('days');
                    var d = al.split('-');

                    if (parseInt(days) >= parseInt(d[0]) && parseInt(days) <= parseInt(d[1])) {
                        $('.days-count[data-days="' + al + '"]').addClass('shadow-lg  bg-white rounded  to-day');
                        return false;
                    }
                })
            }
            $(".dey-price").slick('slickGoTo', parseInt($('.days-count.to-day').parents('.slick-slide').index()), false);
            var date = $('[name="date"]').val();
            cars_map(date);
        });


        $(window).scroll(() => {

            $('.mob-price-pay').css('display', 'flex')

        });

        $(window).resize(() => {
            if ($(window).width() <= 768) {
                $('.date-tame-x').attr('readonly', "true");
            }
        });
        $(document).on('change', '.car-attribute', function () {
            if ($(this).is('checked')) {
                $(this).prop('checked', false)
            }
            getCarPricis();

        });
        $(document).on('mouseup', '.csr-green-card-label', function (event) {
            event.stopPropagation();
            var radio = $(this).find('input[type=radio]');
            if (radio.is(':checked')) {
                radio.prop('checked', false);
            } else {
                radio.prop('checked', true);
            }
            getCarPricis();
        });


        function getCarPricis() {
            var form = $('#single-car-params');
            var data = form.serialize();
            var url = form.attr('action');
            console.log(data);
            $.ajax({
                url: url,
                data: data,
                success: function (data) {
                    $('#mob-price').html(data.day_price);
                    $('.total-block').html(data.html);
                    $('[data-toggle="tooltip"]').tooltip()
                    $('.currency-symbol').text(currency_symbol);
                    $('.day').text(day_translate);
                }
            })
        }
        function date_dive_content_show(date) {
            var date_array = date.split(' ~ ');
            var date_1 = date_array[0].split(' ');
            var date_2 = date_array[1].split(' ');
            date_1 = '<span class="date-1-block"><strong>' + date_1[0] + '</strong></span> <span class="time-1-clock">' + date_1[1] + '</span>';
            date_2 = '<span class="date-2-block"><strong>' + date_2[0] + '</strong></span> <span class="time-2-clock">' + date_2[1] + '</span>';
            return date_1 + ' ~ ' + date_2;
        }

        var date = $('[name="date"]').val();
        cars_map(date);
        $('.date-tame-x').html(date_dive_content_show(date));

        var flag;
        $(document).on('click', '.map-button', () => {
            $('#map').html('');
            if (flag) {
                // $('.map-button').css('box-shadow', '0px 0px 11px rgba(0, 0, 0, 1.5)');
                $('.map-button').html("{{__('car.close_map')}} <img src='{{asset('storage/img/icons/location.png')}}' class='img-fluid ml-2' alt=''>")
                $('.map-button').addClass('activeBtn');
                if (!ymaps) {
                    script = document.createElement('script');
                    script.src = "https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=195a01c1-11df-40b3-bf48-96004f1783eb&mode=debug";
                    document.body.append(script); // (*)
                    script.async = true;
                    script.onload = function () {
                        ymaps.ready(init);
                    };
                }else{
                    ymaps.ready(init);
                }

                $('#map').slideDown();
                flag = false;
            } else {
                // $('.map-button').css('box-shadow', '0px 0px 11px rgba(0, 0, 0, 0.5)');
                $('.map-button').html("{{__('car.open_map')}} <img src='{{asset('storage/img/icons/location.png')}}' class='img-fluid ml-2' alt=''>")
                $('.map-button').removeClass('activeBtn');
                $('#map').slideUp();
                flag = true;
            }
            $('.map-button').blur();


        });

        function cars_map(date) {
            $('.map-button').html("{{__('car.open_map')}} <img src='{{asset('storage/img/icons/location.png')}}' class='img-fluid ml-2' alt=''>")
            $('.map-button').removeClass('activeBtn');
            $('.map-button').blur();
            flag = true;
            $('#map').slideUp();
            axios.get('{{route('car.similar', $car)}}?date=' + date)
                .then(function (response) {
                    $('.map-script').html(response.data.html)
                })
        }

        $(window).resize(() => {
            if ($(window).width() <= 991) {

            }
        });
@guest
        let input = document.querySelector("#phone");
        let tInp = false;
        $('#auth-booking-modal').on('show.bs.modal', function () {
            if (!tInp){
                tInp = window.intlTelInput(input, {
                    initialCountry: "auto",
                    customPlaceholder: function(selectedCountryPlaceholder) {
                        return selectedCountryPlaceholder.replace(/-/g, '');
                    },
                    separateDialCode : true,
                    geoIpLookup: function (callback) {
                        $.get('https://ipinfo.io', function () {
                        }, "jsonp").always(function (resp) {

                            var countryCode = (resp && resp.country) ? resp.country : "by";
                            callback(countryCode);
                        });
                    },
                    // separateDialCode: true,
                    nationalMode: true,
                    // hiddenInput: "phone",
                    utilsScript: "{{asset('js/utils.js')}}"
                });
            }

        });

        let telInput = $("#phone"),
            errorMsg = $("#error-msg"),
            validMsg = $("#valid-msg");

        telInput.focusout(function () {
            if ($.trim(telInput.val())) {
                if (tInp.isValidNumber()) {
                    validMsg.removeClass("hide");
                } else {
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                    validMsg.addClass("hide");
                }
            }
        });

        telInput.keydown(function () {
            telInput.removeClass("error");
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        });


        jQuery.validator.addMethod('validatePhone', function () {
            if (tInp.isValidNumber()) {
                return true;
            } else {

                return false;
            }
        });


        $(".guest-before-booking").validate({
            errorElement: 'small',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                // if (element.attr("name") == "aut_privacy") {
                    error.insertBefore(element.parent());
                // } else {
                //     error.insertAfter(element);
                // }
            },
            rules: {
                guestFirstName: {
                    required: true,
                    minlength: 2
                },
                guestLastName: {
                    required: true,
                    minlength: 2
                },
                guestEmail: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    validatePhone: true
                },
            },
                // messages: {
                //     phone: "Телефонный номер указан не верно ",
                // },
            highlight: function (element) {

                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                $(element).siblings().filter('.glyphicons').removeClass('ok_2').addClass('remove_2');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).siblings().filter('.glyphicons').removeClass('remove_2').addClass('ok_2');

            },
            submitHandler: function(form) {
                let order = $('#single-car-params').serializeFormJSON();
                let guest = $(form).serializeFormJSON();
                let $form = $(form);
                guest.phone = tInp.getNumber();
                $form.preloader();
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: {order : order, guest : guest, _token : $('input[name="_token"]').val()},
                    success: function(e) {
                        $form.find('.help-block').remove();
                        $form.preloader('remove');
                        if (e.errors) {
                            $.each(e.errors, function (t, n) {
                                if(t == 'phone') t = 'mobile';
                                var i = $form.find("[name='" + t + "']"), r = i.first().parent().offset().top,
                                    o = $("nav.navbar").height();
                                0 === Object.keys(t).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 50 + "px"}, "fast"), i.parents('.form-group').addClass("has-error").before(`<small id='${e.errors}-error' class='help-block'>${n}</small>`)
                            })
                        }else{
                            if(e.success){
                                $('#auth-booking-modal').modal('hide');
                                Swal.fire({
                                    title: e.title,
                                    html: e.message,
                                    icon: 'success'
                                }).then((result) => {
                                   window.location.href = '/';
                                })
                            }
                        }
                    }
                });
            }
        });
        (function ($) {
            $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        })(jQuery);
        @endguest
    </script>
@endsection
