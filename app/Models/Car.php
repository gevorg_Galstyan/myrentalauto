<?php

namespace App\Models;

use App\Traits\MyCache;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Traits\Spatial;
use Naoray\LaravelReviewable\Traits\HasReviews;
use TCG\Voyager\Traits\Translatable;

class Car extends Model
{
    use Spatial, Sluggable, Favoriteable, SoftDeletes, HasReviews, Translatable, MyCache;

    protected $translatable = ['location_title', 'meta_location_title', 'meta_title', 'meta_keyword', 'meta_description', 'city', 'note'];

    protected $spatial = ['location'];

    protected $dates = ['deleted_at'];

    protected $appends = ['title', 'images'];

    public $additional_attributes = ['title'];

    public function getTranslatedAttributeMeta($attribute, $locale = null, $fallback = true)
    {
        // Attribute is translatable
        //
        if (!in_array($attribute, $this->getTranslatableAttributes())) {
            return [$this->getAttribute($attribute), config('voyager.multilingual.default'), false];
        }

        if (!$this->relationLoaded('translations')) {
            $this->load('translations');
        }

        if (is_null($locale)) {
            $locale = app()->getLocale();
        }

        if ($fallback === true) {
            $fallback = config('app.fallback_locale', 'en');
        }

        $default = config('voyager.multilingual.default');

        $translations = $this->getRelation('translations')
            ->where('column_name', $attribute);

        if ($default == $locale) {
            return [$this->getAttribute($attribute), $default, true];
        }

        $localeTranslation = $translations->where('locale', $locale)->first();
        if ($localeTranslation && $localeTranslation->value) {
            return [$localeTranslation->value, $locale, true];
        }

        if ($fallback == $locale) {
            return [$this->getAttribute($attribute), $locale, false];
        }

        if ($fallback == $default) {
            return [$this->getAttribute($attribute), $locale, false];
        }

        $fallbackTranslation = $translations->where('locale', $fallback)->first();

        if ($fallbackTranslation && $fallback !== false) {
            return [$fallbackTranslation->value, $locale, true];
        }

        return [null, $locale, false];
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'car_filters',
            'car_id', 'filter_id', 'id', 'id')->withPivot('price', 'order');
    }

    public function insurances()
    {
        return $this->belongsToMany(Insurance::class, 'car_insurances',
            'car_id', 'insurance_id', 'id', 'id')
            ->withPivot('serial_number', 'start_date', 'end_date','franchise', 'price', 'order');
    }

    public function type()
    {
        return $this->belongsTo(CarType::class, 'car_type_id', 'id');
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'car_id', 'id');
    }

    public function sluggable()
    {
        return [];
    }

    public function scopeCloseTo(Builder $query, $latitude, $longitude, $delivery_max_range)
    {
        return $query->whereRaw("
       ST_Distance_Sphere(
            point(ST_X(location), ST_Y(location)),
            point(?, ?)
        )  < $delivery_max_range
    ", [
            $longitude,
            $latitude,
        ]);
    }

    public function scopeDistance(Builder $query, $latitude, $longitude)
    {
        return $query->select(DB::raw("*,
       ST_Distance_Sphere(
            point(ST_X(location), ST_Y(location)),
            point($longitude, $latitude)
        )  as distance
    "));
    }

    public function myFavorites()
    {
        return $this->morphMany(Favorite::class, 'favoriteable'/*, null, null, 'id'*/);
    }

    public function deliveries()
    {
        return $this->hasMany(DeliveryLocation::class);
    }

    public function scopeCurrentUser($query)
    {
        if (\Auth::user()->role_id == 1) {
            return $query;
        }
        if (\Auth::user()->role_id == 3) {
            return $query->where('owner_id', \Auth::user()->id);
        }

    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status',  1);
    }

    public function make()
    {
        return $this->belongsTo(CarMake::class, 'car_make_id', 'id');
    }

    public function model()
    {
        return $this->belongsTo(CarModel::class, 'car_model_id', 'id');
    }

    public function getTitleAttribute()
    {

        return  ($this->make->name??'').' '. ($this->model->name??'');
    }

    public function getImagesAttribute()
    {
        $data [] = $this->image_1;
        $data [] = $this->image_2;
        $data [] = $this->image_3;
        $data [] = $this->image_4;
        return  $data;
    }

    public function orderDates()
    {
        return $this->hasMany(OrderDate::class, 'car_id', 'id');
    }

    public function bodyType()
    {
        return $this->belongsTo(CarBodyType::class, 'body_type_id', 'id');
    }

    public function busy ($start, $end)
    {
        return $this->orderDates()->where(function ($query) use ($start, $end) {
            $query->whereBetween('start', [$start, $end])
                ->orWhere(function ($q) use ($start, $end) {
                    $q->whereBetween('end', [$start, $end]);
                })
                ->orWhere(function ($q) use ($start, $end) {
                    $q->whereBetween('end', [$start, $end]);
                })
                ->orWhere(function ($q) use ($start, $end) {
                    $q->whereDate('start', '<=', $start)->whereDate('end', '>=', $end);
                });

        })->whereHas('order', function ($query) {
            $query->where('status', 'pending')->orWhere('status', 'paid')->orWhere('status', 'confirmed')->orWhere('status', 'completed');
        })->orWhere(function ($query) use ($start, $end){
            $query->whereDoesntHave('order')->where('car_id', $this->id)->where(function ($query) use ($start, $end) {
                $query->whereBetween('start', [$start, $end])
                    ->orWhere(function ($q) use ($start, $end) {
                        $q->whereBetween('end', [$start, $end]);
                    })
                    ->orWhere(function ($q) use ($start, $end) {
                        $q->whereBetween('end', [$start, $end]);
                    })
                    ->orWhere(function ($q) use ($start, $end) {
                        $q->whereDate('start', '<=', $start)->whereDate('end', '>=', $end);
                    });

            });

        })->exists();
    }

    public function color()
    {
        return $this->belongsTo(CarColor::class);
    }

    public function scopeOfSort($query, $sort)
    {
        foreach ($sort as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }

    public static function destroy($ids)
    {
        return parent::destroy($ids); // TODO: Change the autogenerated stub
    }

    public function busyDays()
    {
//dd($this->orderDates);
        $dates = $this->orderDates()->whereDate('end', '>', \Carbon\Carbon::now())->whereHas('order', function ($query) {
            $query->where('status', 'pending')->orWhere('status', 'paid')->orWhere('status', 'confirmed')->orWhere('status', 'completed');
        })->orWhere(function ($query){
            $query->doesnthave('order')->where('car_id', $this->id);

        });
        return $dates;
    }

    public function wrongFills()
    {
        return $this->hasMany(WrongFillCar::class);
    }

    public function logs()
    {
        return $this->morphMany(Log::class, 'logable');
    }

    public function scopeWithAll($query)
    {
        $query->with('color', 'make', 'model', 'filters', 'insurances', 'type');
    }
}
