<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CarRequestAdmin extends Notification
{
    use Queueable;

    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.car-request-admin.title', [
                'name' => $notifiable->first_name,
            ]))
            ->line(__('notification.car-request-owner.text-1', [
                'car' => $this->order->car->title . ' ' . route('voyager.cars.show', $this->order->car),
            ]))
            ->line(__('notification.car-request-admin.text-2', [
                'order' => route('voyager.orders.show', $this->order),
            ]))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => __('car.rent'),
            'message' => '<a href="' . route('voyager.orders.show', $this->order) . '">' . __('notification.car-request-admin.text-2', ['order' => '']) . '</a>',

        ]);
    }
}
