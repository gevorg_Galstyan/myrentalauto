<h3>Локация</h3>
<section>
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
    @forelse($dataTypeContent->getCoordinates() as $point)
        <input type="hidden" name="location[lat]" value="{{ $point['lat'] }}" id="lat"/>
        <input type="hidden" name="location[lng]" value="{{ $point['lng'] }}" id="lng"/>
    @empty
        <input type="hidden" name="location[lat]"
               value="{{ config('voyager.googlemaps.center.lat') }}" id="lat"/>
        <input type="hidden" name="location[lng]"
               value="{{ config('voyager.googlemaps.center.lng') }}" id="lng"/>
    @endforelse
    <div class="form-group">
        <div class="form-label">
            Местонахождение автомобиля
            <span class="k-pointer" data-toggle="tooltip" data-html="true"
                  title=" {{ tooltips($toolips, 'owner_help_vehicle_location') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
        </div>
        <input type="text" class="form-control" id="location" value="{{$dataTypeContent->location_title}}">
    </div>
    <div id="map"></div>
</section>
