<div class="bg-white py-3 mb-5 mt-lg-5" id="advantage">

    <div class="own-container mt-5">
        <h3 class="text-center fw-700 mb-5 sub-heading">{{__('home.adv')}}</h3>
        <div class="row align-items-center">
            <div class="col-md-6">
                <img src="{{ownMakeImage(682,0,70,'img/advantages/cover-1.png')}}" alt="{{config('app.name')}}"
                     class="img-fluid">
            </div>
            <div class="col-md-6">
                <div class="advantage">
                    @foreach($advantages as $key=>$value)
                        <div class="card">
                            <div class="card-header {{$loop->last?'border-0' :''}}" id="heading-{{$value->id}}">
                                <h6 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                            data-target="#collapse-{{$value->id}}"
                                            aria-expanded="false" aria-controls="collapse-{{$value->id}}">
                                        {{$value->myTranslate()->title}}
                                    </button>
                                </h6>
                            </div>

                            <div id="collapse-{{$value->id}}"
                                 class="collapse {{$loop->last?'border-top' :''}} {{--{{$advantages[0]->id === $value->id ? 'show' : ''}}--}}"
                                 aria-labelledby="heading-{{$value->id}}"
                                 data-parent="#advantage">
                                <div class="card-body ff-ms">

                                    {!! $value->myTranslate()->description !!}


                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
