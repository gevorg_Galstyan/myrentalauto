<?php

namespace App\Providers;


use App\Models\Page;
use App\Models\Toolip;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use MadWeb\Robots\Robots;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        config([
            'laravellocalization.supportedLocales' => [
                'ru' => ['name' => 'Russian', 'script' => 'Cyrl', 'native' => 'русский', 'regional' => 'ru_RU'],
                'en' => ['name' => 'English', 'script' => 'Latn', 'native' => 'English', 'regional' => 'en_GB']
            ],

            'laravellocalization.useAcceptLanguageHeader' => true,

            'laravellocalization.hideDefaultLocaleInURL' => true
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $toolips = Toolip::get();
        View::share(compact('toolips'));
        View::composer('partials.header', function ($view) {
            $documentPages = \Cache::remember('pages:in_documents', setting('site.cache_time'), function () {
                return Page::where('in_documents', 1)->get();
            });

            $view->with([
                'documentPages' => $documentPages,
            ]);
        });

        Schema::defaultStringLength(191);
        Voyager::addAction(\App\Actions\ShowProfile::class);
        Voyager::addAction(\App\Actions\ShowCalendar::class);
        Voyager::addAction(\App\Actions\BlockUser::class);
        Voyager::addAction(\App\Actions\ShowAdmin::class);
        Voyager::addAction(\App\Actions\ShowModerator::class);
        Voyager::addAction(\App\Actions\ShowUser::class);
        Voyager::addAction(\App\Actions\ShowOwner::class);
        Voyager::addAction(\App\Actions\TicketAnswer::class);
        Voyager::addAction(\App\Actions\MakeUserTicket::class);
        $robots = new Robots();
        $robots->setShouldIndexCallback(function () {
            return app()->environment('production');
        });
        \Blade::directive('svg', function ($arguments) {
            // Funky madness to accept multiple arguments into the directive
            list($path, $class) = array_pad(explode(',', trim($arguments, "() ")), 2, '');
            $path = trim($path, "' ");
            $class = trim($class, "' ");

            // Create the dom document as per the other answers
            $svg = new \DOMDocument();
            $svg->load(public_path($path));
            $svg->documentElement->setAttribute("class", $class);
            $output = $svg->saveXML($svg->documentElement);

            return $output;
        });
        VerifyEmail::toMailUsing(function ($notifiable) {
            $verifyUrl = URL::temporarySignedRoute(
                'verification.verify', Carbon::now()->addMinutes(60), ['id' => $notifiable->getKey()]
            );
            if ($notifiable->default_password) {
                return (new MailMessage)
                    ->subject(__('verify.text_1'))
                    ->line(__('verify.text_2'))
                    ->action(__('verify.text_3'), $verifyUrl)
                    ->line(__('verify.text_5',['password' => $notifiable->default_password]))
                    ->line(__('verify.text_6'))
                    ->line(__('verify.text_4'));

            } else {
                return (new MailMessage)
                    ->subject(__('verify.text_1'))
                    ->line(__('verify.text_2'))
                    ->action(__('verify.text_3'), $verifyUrl)
                    ->line(__('verify.text_4'));
            }

        });

    }
}
