@section('css')

@stop

<h3 class="text-center">Изменить пароль</h3>


<form action="{{route('owner.settings.security.change-password')}}" class="needs-validation" novalidate method="POST">
    <fieldset class="form-fieldset">
        @csrf
        {{ method_field("PUT") }}
        <div class="form-group">
            <label class="form-label">Текущий пароль<span class="form-required">*</span></label>
            <input type="password" name="old_password"
                   class="form-control {{ $errors->has('current_password') ? ' is-invalid' : '' }}" min="8" required>
            @if ($errors->has('current_password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('current_password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label class="form-label">Новый пароль<span class="form-required">*</span></label>
            <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label class="form-label">Повторить пароль<span class="form-required">*</span></label>
            <input type="password" name="password_confirmation" class="form-control" required>
        </div>
        <div class="d-flex justify-content-center mt-3 rounded">
            <input type="submit" class="own-btn btn btn-primary ora rounded-0 border-0 m-auto fw-400 btnSerialize"
                   value="Сохранить">
        </div>
    </fieldset>
</form>



@section('script')


    <script>
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@stop
