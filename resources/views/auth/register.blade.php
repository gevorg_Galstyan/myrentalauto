@extends('layouts.app')

@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop

@section('content')
<div class="container">
    <div class="auth login m-5">
        <h1 class="text-center fw-600 fs-30 sub-heading text-uppercase m-0 mb-2">{{getMetaTags('register_page') ? getMetaTags('register_page')->translate()->h1 : ''}}</h1>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="modal-header text-center justify-content-center pb-1">
                        <h4 class="modal--title">{{__('auth.login_with')}}</h4>
                    </div>
                    <div class="modal-body py-1">
                        <div class="box">
                            <div class="content p-2">
                                <div class="social d-flex align-items-center justify-content-center mb-3">
                                    <a class="facebook-login social-login mr-2" href="{{route('oauth',['facebook'])}}">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a class="vk-login social-login" href="{{route('oauth',['vkontakte'])}}">
                                        <i class="fab fa-vk"></i>
                                    </a>
                                </div>
                                <div class="division">
                                    <div class="line l"></div>
                                    <span>{{__('auth.or')}}</span>
                                    <div class="line r"></div>
                                </div>
                                <div class="error"></div>
                                <div class="form pb-2">
                                    <form id="registerForm" method="POST" action="{{route('register')}}"
                                          accept-charset="UTF-8">
                                        @csrf
                                        <div>
                                            <input id="first_name" type="text" class="form-control mb-2"
                                                   name="first_name" value="{{ old('first_name') }}" required
                                                   placeholder="{{__('auth.first_name')}}">
                                        </div>

                                        <div>
                                            <input id="last_name" type="text" class="form-control mb-2"
                                                   name="last_name" value="{{ old('last_name') }}" required
                                                   placeholder="{{__('auth.last_name')}}">
                                        </div>

                                        <div>
                                            <input id="email" type="email"
                                                   class="form-control mb-2"
                                                   name="email" value="{{ old('email') }}" required
                                                   placeholder="{{__('auth.email')}}">
                                        </div>

                                        <div>
                                            <input id="password" type="password"
                                                   class="form-control mb-2"
                                                   name="password" required placeholder="{{__('auth.password')}}">
                                        </div>

                                        <div>
                                            <input id="password-confirm" type="password" class="form-control mb-2"
                                                   name="password_confirmation" placeholder="{{__('auth.repeat_password')}}"
                                                   required>
                                        </div>

                                        <div class="text-center mt-2 mb-4 chose-role">
                                            <div class="btn-group btn-group-toggle d-flex" data-toggle="buttons">
                                                <label class="btn left-border-radius-20 fs-16 fw-600 active col">
                                                    <input type="radio" name="role" value="2" checked> {{__('auth.role_user')}}
                                                </label>
                                                <label class="btn right-border-radius-20 fs-16 fw-600 col">
                                                    <input type="radio" name="role"  value="3"> {{__('auth.role_owner')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="text-center mt-2">
                                            <button type="submit"
                                                    class="own-btn text-capitalize fs-16 d-inline-block btn-shadow ora border-0">
                                                {{__('auth.register')}}
                                            </button>
                                        </div>
                                        <div class="alert-danger d-inline-block"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt-0 pt-0">
                        <div class="forgot login-footer">
                            <span>{{__('auth.already_account')}}</span>
                            <a href="{{ route('login') }}">{{__('auth.login')}}</a>
                        </div>
                        <div class="forgot register-footer" style="display:none">
                            <span>{{__('auth.already_account')}}</span>
                            <a href="{{ route('login') }}">{{__('auth.login')}}</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
