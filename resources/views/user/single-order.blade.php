<section class="own-container single-car-page my-3">
    <div class="row">
        <div class="col-md-12  m-auto">
            <div class="bg-white shadow-lg  rounded p-3 mb-3">
                <div class="row justify-content-center">
                    <div class="py-2 col ">
                        <h2 class="text-center single-header fw-700 p-0 text-center">{{$order->car->title}}</h2>
                    </div>
                    @if($order->status == 'paid' && \Carbon\Carbon::parse($order->dates->start->addDays(-1))->gte(now()))
                        <div class="py-2 col text-right">
                            <button class="btn btn-danger order-cancel" data-target="{{$order->id}}"
                                    data-url="{{route('user.order.cancel', ['order' => $order])}}">
                                @lang('general.order.cancel')
                            </button>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class=" col-md-6">
                        <div class="customer-order-image-container">
                            <img src="{{Voyager::image($order->car->image_1)}}" alt="Avatar" class="image img-fluid"
                                 style="width:100%">
                            <div class="middle">
                                <a href="{{route('car.single', ['slug' => $order->car->slug])}}"
                                   class="own-btn rounded d-inline-block ora px-2 py-1 fs-15">
                                    <span class="ff-st">{!! currency($order->car->price_1) !!}</span>/{{__('car.day')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pr-0">

                        <div class="d-flex justify-content-center">
                            <div>
                                <span>{{$order->dates->start->format('d:m:Y g:i')}}</span><span> - </span><span>{{$order->dates->end->format('d:m:Y g:i')}}</span>
                            </div>

                        </div>
                        <div class="row m-0 mt-2 fs-14 fw-700 align-items-center justify-content-center">
                            <span class="align-items-center px-2">
                                <span>{{__('car.deposit')}}</span> <span> : </span>
                                <span class="px-2 price  fs-16" id="total">
                                    {!! $order->deposit ? $order->deposit.' '.currency_symbol($order->currency) : 'НЕТ' !!}
                                </span>
                            </span>
                        </div>

                        <div class="p-2 d-flex justify-content-center align-items-center">
                            <div class="row m-0  fs-16 fw-700">
                            <span class=" shadow   bg-white rounded ">
                                 <span class="px-2 ">{{__('car.total')}}</span>
                                    :
                                <span class="px-2 price  fs-22"
                                      id="total">
                                     {!!  $order->total_price  !!} {{ currency_symbol($order->currency) }}
                                </span>
                            </span>
                            </div>
                        </div>
                        <hr>
                        <div class="col-12 text-right d-flex justify-content-end d-flex flex-wrap">
                            @if($order->status == 'completed' || $order->status == 'paid')
                                <div class="col mt-sm-2">
                                    <a href="{{route('payment.order_status', ['order' => $order])}}"
                                       class="badge border-0 color-white p-3 badge-info order-info" data-type="reservation">
                                        <span><i class="fas fa-file-invoice"></i></span>
                                    </a>
                                </div>
                                <div class="col badge border-0 px-2 py-3 mt-sm-2 badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.'.$order->status)}}</div>
                                @if ($order->status == 'paid' && $order->car->full_payment && !$order->fullPay && \Carbon\Carbon::parse($order->dates->start)->gte(now()))
                                    <div class="col-12 mt-sm-2 px-0 mx-0">
                                        <a href="{{route('user.full_pay', ['order' => $order])}}"
                                           class="badge border-0 color-white p-3 badge-info">
                                            {{__('general.order.go_full_payment')}}
                                            <span
                                                class="fs-20">{{($order->total_price - $order->price_to_pay) . ' ' .currency_symbol($order->currency) }}</span>
                                        </a>
                                    </div>
                                @endif
                            @elseif ($order->status == 'confirmed')
                                @php($attributes = array_column(json_decode($order->attributes, true), 'id'))
                                @php($insurances = array_column(json_decode($order->insurances, true), 'id'))
                                @php($green_card = isset(json_decode($order->green_card, true)['name']) ? json_decode($order->green_card, true)['name'] : '')
                                @php($date = \Carbon\Carbon::parse($order->dates->start) .' ~ '. \Carbon\Carbon::parse($order->dates->end))
                                <div class="col mt-sm-2">
                                    {{--                                    @if(\Carbon\Carbon::parse($order->updated_at->addHours(2))->gte(now()))--}}
                                    <a href="{{route('car.single', ['slug' => $order->car->slug, 'date' => $date,'attributes' => $attributes, 'insurances' => $insurances, 'green_card' => $green_card, 'orderId' => $order->barcode_number])}}"
                                       class="badge border-0 color-white p-3 badge-info ora">
                                        {{__('car.rent_it')}}
                                    </a>
                                </div>
                                <div
                                    class="col badge border-0 px-2 py-3 mt-sm-2 badge-{{config('car.order.class.'.$order->status)}}">
                                    {{__('general.order.'.$order->status)}}
                                </div>
                            @elseif ($order->status == 'expired')
                                @php($attributes = array_column(json_decode($order->attributes, true), 'id'))
                                @php($insurances = array_column(json_decode($order->insurances, true), 'id'))
                                @php($green_card = isset(json_decode($order->green_card, true)['name']) ? json_decode($order->green_card, true)['name'] : '')
                                @php($date = \Carbon\Carbon::parse($order->dates->start)->format('d.m.Y H:i') .' ~ '. \Carbon\Carbon::parse($order->dates->end)->format('d.m.Y H:i'))
                                <div class="col mt-sm-2">
                                    <a href="{{route('car.single', ['slug' => $order->car->slug, 'date' => $date,'attributes' => $attributes, 'insurances' => $insurances, 'green_card' => $green_card])}}"
                                       class="badge border-0 color-white p-3 badge-info ora">
                                        {{__('car.to_request')}}
                                    </a>

                                </div>
                                <div
                                    class="col badge border-0 px-2 py-3 mt-sm-2 badge-{{config('car.order.class.'.$order->status)}}">
                                    {{__('general.order.'.$order->status)}}
                                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                          title=" {!! __('car.expired') !!}">
                                            <i class="fas fa-info-circle"></i>
                                    </span>
                                </div>
                            @elseif ($order->status == 'rejected')
                                @php($date = \Carbon\Carbon::parse($order->dates->start)->format('d.m.Y H:i') .' ~ '. \Carbon\Carbon::parse($order->dates->end)->format('d.m.Y H:i'))
                                <div class="col mt-sm-2">
                                    <a href="{{route('search', ['date' => $date])}}"
                                       class="badge border-0 color-white p-3 badge-info ora">
                                        {{__('car.similar')}}
                                    </a>
                                </div>
                                <div class="col align-self-center fs-22 text-center d-block text-danger">
                                    {{__('car.busy')}}
                                </div>
                            @elseif ($order->status == 'no_paid')
                                @php($attributes = array_column(json_decode($order->attributes, true), 'id'))
                                @php($insurances = array_column(json_decode($order->insurances, true), 'id'))
                                @php($green_card = isset(json_decode($order->green_card, true)['name']) ? json_decode($order->green_card, true)['name'] : '')
                                @php($date = \Carbon\Carbon::parse($order->dates->start)->format('d.m.Y H:i') .' ~ '. \Carbon\Carbon::parse($order->dates->end)->format('d.m.Y H:i'))
                                <div class="col mt-sm-2">
                                    <a href="{{route('car.single', ['slug' => $order->car->slug, 'date' => $date,'attributes' => $attributes, 'insurances' => $insurances, 'green_card' => $green_card])}}"
                                       class="badge border-0 color-white p-3 badge-info ora">{{__('general.order.book_again')}}</a>
                                </div>
                                <div
                                    class="col badge border-0 px-2 py-3 mt-sm-2 badge-{{config('car.order.class.'.$order->status)}}">
                                    {{__('general.order.'.$order->status)}}
                                </div>

                                @elseif ($order->status == 'canceled' || $order->transaction_id)
                                <div class="badge border-0 col-12 px-2 py-3 badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.'.$order->status)}}</div>
                                <div class="col mt-sm-2">
                                    <a href="{{route('payment.order_status', ['order' => $order])}}"
                                       class="badge border-0 color-white p-3 badge-info order-info" data-type="reservation">
                                        <span><i class="fas fa-file-invoice"></i></span>
                                    </a>
                                </div>
                                <div class="col badge border-0 px-2 py-3 mt-sm-2 badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.paid')}}</div>
                            @else
                                <div class="badge border-0 px-2 py-3 badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.'.$order->status)}}</div>
                            @endif
                        </div>

                        @if ($order->fullPay && $order->fullPay->status == 'paid')
                            <div class="col-12 text-right d-flex justify-content-end d-flex flex-wrap">
                                <div class="col mt-sm-2">
                                    <a href="{{route('payment.order_status', ['order' => $order->fullPay])}}"
                                       class="badge border-0 color-white p-3 badge-info order-info" data-type="full">
                                        <span><i class="fas fa-file-invoice"></i></span>
                                    </a>
                                </div>
                                <div class="col badge border-0 px-2 py-3 mt-sm-2 badge-{{config('car.order.class.'.$order->status)}}">{{__('general.order.fully_paid')}}</div>
                            </div>
                        @endif
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="single-options my-2">
        <div id="accordion-spec">
            <div class="" id="headingOne">
                <button class="spec-button w-100 collapsed" data-toggle="collapse"
                        data-target="#collapse-spec" aria-expanded="true"
                        aria-controls="collapse-spec">
                    @lang('general.order.details')
                    <i class="fa" aria-hidden="true"></i>
                </button>
            </div>

            <div id="collapse-spec"
                 class="collapse fs-14 my-1 p-2 rounded border border-secondary"
                 aria-labelledby="headingOne"
                 data-parent="#accordion-spec">

                <div class="mx-3  fs-14 fw-700">
                    <div class="total-block-info color-red">
                        @php($green_card = json_decode($order->green_card, true))
                        @if (count($green_card))
                            <div>
                                <hr>
                            </div>

                            <div class="total-block-info">
                                <div class="row m-0 mt-2 align-items-center justify-content-between">
                                    <span class="col"> {{__('car.green_card_'.$green_card['name'])}}</span>

                                    <span class="col text-right">
                                    {!!  $green_card['price']  !!} {{ currency_symbol($order->currency) }}
                                </span>

                                </div>

                            </div>
                            <div>
                                <hr>
                            </div>

                        @endif

                        @php($other_details = json_decode($order->other_details, true))

                        @if ($other_details['take_no_work_time'])
                            <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                Взять не рабочее время
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title=" {{ tooltips($toolips, 'ne_rabochie_vremya', $other_details['take_no_work_time'] . currency_symbol($order->currency)) }}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </span>
                                <span
                                    class="col text-right">{!! $other_details['take_no_work_time'] . currency_symbol($order->currency) !!}</span>
                            </div>
                        @endif

                        @if ($other_details['return_in_non_working_time'])
                            <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                Вернуть в не рабочее время
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title=" {{ tooltips($toolips, 'ne_rabochie_vremya', $other_details['return_in_non_working_time'] . currency_symbol($order->currency)) }}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </span>
                                <span
                                    class="col text-right">{!! $other_details['return_in_non_working_time'] . currency_symbol($order->currency) !!}</span>
                            </div>
                        @endif

                        @if ($other_details['urgency'])
                            <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                Вернуть в не рабочее время
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title=" {!! tooltips($toolips, 'ne_rabochie_vremya', $other_details['urgency'] . currency_symbol($order->currency)) !!}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </span>
                                <span
                                    class="col text-right">{!! $other_details['urgency'] . currency_symbol($order->currency) !!}</span>
                            </div>

                        @endif

                        @if ($other_details['urgency'] || $other_details['return_in_non_working_time'] || $other_details['take_no_work_time'])
                            <hr>
                        @endif


                        @if ($other_details['took'])
                            <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                {{$other_details['took']['name']}}
                            </span>
                                <span
                                    class="col text-right">{!!  $other_details['took']['price'] . currency_symbol($order->currency)!!}</span>
                            </div>
                        @endif

                        @if ($other_details['give'])
                            <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                {{$other_details['give']['name']}}
                            </span>
                                <span
                                    class="col text-right">{!!  $other_details['give']['price'] . currency_symbol($order->currency)!!}</span>
                            </div>
                        @endif

                        @if ($other_details['give'] || $other_details['took'])
                            <hr>
                        @endif


                        @php($orderInsurances = json_decode($order->insurances, true))
                        @if (count($orderInsurances))
                            @foreach($orderInsurances as $item)
                                @php($insurances = \App\Models\Insurance::get())

                                <div>
                                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                                        <span class="col ">
                                            {{$insurances->where('id', $item['id'])->first()->translate()->name}}
                                        </span>
                                        <span class="col text-right">
                                            {!!  $item['price']  !!} {{ currency_symbol($order->currency) }}
                                        </span>
                                    </div>
                                    <div>
                                        <hr>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        @php($orderAttributes = json_decode($order->attributes, true))
                        @if (count($orderAttributes))
                            @foreach($orderAttributes as $item)
                                @php($attributes = \App\Models\Filter::get())
                                <div>
                                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                                        <span class="col ">
                                            {{$attributes->where('id', $item['id'])->first()->translate()->title}}
                                        </span>
                                        @if(!$item['disposable'])
                                            <span class="col text-center">
                                                {{$order->days}} Дней х {!! $item['price'] !!} {{currency_symbol($order->currency)}}
                                                <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                                      data-placement="top"
                                                      title=" Цена зависит от количество выбранных дней !">
                                                        <i class="fas fa-info-circle"></i>
                                                </span>
                                            </span>
                                            <span class="col text-right">
                                                {!! $item['price'] * $order->days !!} {{currency_symbol($order->currency)}}
                                            </span>
                                        @else
                                            <span
                                                class="col text-center">{!! $item['price'] .' '.currency_symbol($order->currency) !!}</span>
                                            <span
                                                class="col text-right">{!! $item['price'] .' '.currency_symbol($order->currency) !!}</span>
                                        @endif
                                    </div>
                                    <div>
                                        <hr>
                                    </div>
                                </div>
                            @endforeach
                        @endif


                    </div>
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                        <span class="col">{{__('car.rent')}}</span>
                        <span class="col text-center">
                            {{$order->days}} {{__('car.days')}} х  {!!  $order->price_per_day  !!} {{ currency_symbol($order->currency) }}</span>
                        <span class="col text-right">
                            {!! $order->days * $order->price_per_day !!} {{ currency_symbol($order->currency) }}
                        </span>
                    </div>
                    <hr/>


                </div>
            </div>
        </div>
    </div>
    @if(now()->gte(\Carbon\Carbon::parse($order->dates->end)))
        <div class="bg-white shadow-lg  rounded p-3 mb-3">
            @php($review = $order->car->reviews->where('author_id', auth()->id())->first())
            <div class="p-2 align-items-center">
                <h2 class="text-center single-header fw-700 mb-4 review-title">
                    @if ($review)
                        ваш отзыв
                    @else
                        Оставить Отзыв
                    @endif
                </h2>
            </div>
            <div class="review-container">
                @if ($review)
                    <div class="ff-st fs-14 row justify-content-center">
                        <div class="">
                            <div class="d-flex justify-content-center mb-1">


                                <div class="align-self-center">
                                    <div class=" align-self-center">
                                    <span class=""><i
                                            class="color-orange {{$review->score >= 1 ? 'fas' : 'far'}} fa-star"></i></span>
                                        <span class=""><i
                                                class="color-orange {{$review->score >= 2 ? 'fas' : 'far'}} fa-star"></i></span>
                                        <span class=""><i
                                                class="color-orange {{$review->score >= 3 ? 'fas' : 'far'}} fa-star"></i></span>
                                        <span class=""><i
                                                class="color-orange {{$review->score >= 4 ? 'fas' : 'far'}} fa-star"></i></span>
                                        <span class=""><i
                                                class="color-orange {{$review->score >= 5 ? 'fas' : 'far'}} fa-star"></i></span>
                                        <span
                                            class="text-secondary ml-2">{{$review->created_at->diffForHumans()}}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="mb-0 text-center p-2">{{$review->body}}</p>

                        </div>
                    </div>
                @else
                    <form action="{{route('user.review.make', ['car' => $order->car])}}" method="POST" id="review-form">
                        @csrf
                        <div class="row justify-content-center form-group">
                            <div class="rating rating-content">
                                <input type="radio" id="star5" name="rating" class="star-val" value="5"/><label
                                    for="star5"
                                    class="star k-pointer"></label>
                                <input type="radio" id="star4" name="rating" class="star-val" value="4"/><label
                                    for="star4"
                                    class="star k-pointer"></label>
                                <input type="radio" id="star3" name="rating" class="star-val" value="3"/><label
                                    for="star3"
                                    class="star k-pointer"></label>
                                <input type="radio" id="star2" name="rating" class="star-val" value="2"/><label
                                    for="star2"
                                    class="star k-pointer"></label>
                                <input type="radio" id="star1" name="rating" class="star-val" value="1"/><label
                                    for="star1"
                                    class="star k-pointer"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="review" placeholder="Отзыв" rows="3"></textarea>
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button type="submit"
                                    class="own-btn text-uppercase d-inline-block btn-shadow ora border-5 border-0 ">
                                Сохранить
                            </button>
                        </div>
                    </form>
                @endif

            </div>

        </div>
    @endif
</section>
