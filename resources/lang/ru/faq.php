<?php
return [
    'themes' => 'Темы',
    'questions' => 'Вопросы',
    'did_this_article_help_you' => 'Помогло ли вам эта статья',
    'yes' => 'Да',
    'no' => 'Нет',
    'you_rated_this_article' => 'Вы оценили эту статью . Спасибо за оценку.',
    'may_be_useful' => 'Может быть полезен',
    'info' => 'Если вы не нашли ответ на свой вопрос, мы ответим на него по электронной почте. Просто напишите на <a href="mailto:info@myrentauto.com">info@myrentauto.com</a>',
    'search_for_a_question_here' => 'Искать вопрос здесь',
    'thanks_for_rating_this_will_hel_us_to_become_better' => 'Спасибо за оценку, это поможет нам стать лучше.',
    'tenant' => 'Арендатор',
    'owner' => 'Владелец',
    'tenant_popular_question' => 'Актуальные вопросы арендаторов',
    'owner_popular_question' => 'Актуальные вопросы владельцев',
    'tenant_categories' => 'Актуальные темы арендаторов',
    'owner_categories' => 'Актуальные темы владельцев',
];
