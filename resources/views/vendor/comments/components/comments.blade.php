@if($model->comments->count() < 1)
    <div class="alert alert-warning comment-empty">{{__('comment.no_comments_yet')}}.</div>
@endif
@php
    $comments  = $model->comments()->where('child_id', null)->orderBy('id', 'desc')->paginate(1);
    $comments->setPath(route('comments',['model' => class_basename($model), 'item' => $model->id]));

@endphp
@if ($comments->nextPageUrl())
    <div class="ml-5 load-mor-container">
        <a href="{{$comments->nextPageUrl()}}"
           class="load-comment">
            Загрузи больше
        </a>
    </div>
@endif
@if (!isset($load))
    <div class="comment-container">
@endif
        @foreach($comments as $comment)
            @include('comments::_comment')
        @endforeach
@if (!isset($load))
    </div>
@endif

@if (!isset($load))
    @auth
        @include('comments::_form')
    @else
        <div class="card mx-2 mx-md-5">
            <div class="card-body">
                <h5 class="card-title">{{__('comment.authorization_required')}}</h5>
                <p class="card-text">{{__('comment.you_must_be_logged_in_to_leave_a_comment')}}</p>
                <button data-toggle="modal" data-target="#loginModal" class="own-btn border-0 ora">{{__('auth.login')}}</button>
            </div>
        </div>
    @endauth

@endif
