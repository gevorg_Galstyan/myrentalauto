<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Laravelista\Comments\Comment;

class CommentsController extends Controller
{
    use ValidatesRequests, AuthorizesRequests;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = ('App\Models\\' . $request->model)::find($request->item);
            $load = true;
            $html = view('comments::components.comments', compact('model', 'load'))->render();
            return response()->json(['html' => $html]);
        } else {
            return abort(403);
        }

    }

    /**
     * Creates a new comment for given model.
     */
    public function store(Request $request)
    {
        $this->authorize('create-comment', Comment::class);

        $this->validate($request, [
            'commentable_type' => 'required|string',
            'commentable_id' => 'required|integer|min:1',
            'message' => 'required|string'
        ]);
        if ($request->ajax()) {
            $model = $request->commentable_type::findOrFail($request->commentable_id);

            $comment = new Comment;
            $comment->commenter()->associate(auth()->user());
            $comment->commentable()->associate($model);
            $comment->comment = $request->message;
            $comment->save();
            $html = view('comments::_comment', compact('comment'))->render();
            return response()->json(['html' => $html]);
        } else {
            return abort(403);
        }
    }

    /**
     * Updates the message of the comment.
     */
    public function update(Request $request, Comment $comment)
    {
        $this->authorize('edit-comment', $comment);

        $this->validate($request, [
            'message' => 'required|string'
        ]);
        if ($request->ajax()) {
            $comment->update([
                'comment' => $request->message
            ]);

            return response()->json(['message' => $request->message]);
        } else {
            return abort(403);
        }
    }

    /**
     * Deletes a comment.
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete-comment', $comment);

        $comment->delete();

        return redirect()->back();
    }

    /**
     * Creates a reply "comment" to a comment.
     */
    public function reply(Request $request, Comment $comment)
    {
        $this->authorize('reply-to-comment', $comment);

        $this->validate($request, [
            'message' => 'required|string'
        ]);
        if ($request->ajax()) {
            $reply = new Comment;
            $reply->commenter()->associate(auth()->user());
            $reply->commentable()->associate($comment->commentable);
            $reply->parent()->associate($comment);
            $reply->comment = $request->message;
            $reply->save();

            $html = view('comments::_comment', ['comment' => $reply, '$reply' => true])->render();
            return response()->json(['html' => $html]);
        } else {
            return abort(403);
        }
    }
}
