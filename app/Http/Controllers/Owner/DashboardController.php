<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Order;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /*
     * owners main page
     */
    public function index()
    {
//        YandexChange('Пароль: Перевод на 4155 Спише45тся 50,26р. Перевод на счет4100175017397');
        $owner = \Auth::user();

        $currentDate = Carbon::now()->toDateTimeString();
        //get owner cars
        $ownerCars = auth()->user()->cars;
        $orders = Order::whereIn('car_id', $owner->cars()->pluck('id'))->whereHas('dates', function ($query) use ($currentDate) {
            $query->where('end', '>=', $currentDate)->where('start', '<=', $currentDate);
        })->get();

        $month_orders = Order::whereIn('car_id', $owner->cars()->pluck('id'))
            ->whereMonth('created_at', Carbon::now()
                ->subMonth()
                ->format('m'))
            ->whereHas('dates', function ($query) use ($currentDate) {
                $query->where('end', '>=', $currentDate)
                    ->where('start', '<=', $currentDate);
            })->get();

        $month_orders_success = Order::whereIn('car_id', $owner->cars()->pluck('id'))
            ->whereMonth('created_at', Carbon::now()
                ->subMonth()
                ->format('m'))
            ->whereHas('dates', function ($query) use ($currentDate) {
                $query->where('end', '>=', $currentDate)
                    ->where('start', '<=', $currentDate);
            })
            ->where(function ($query){
                $query->whereIn('status',['paid', 'canceled'] );
            })
            ->get();
        $month_orders_count = $month_orders->count();
        $month_orders_success_count = $month_orders_success->count();

        $income = $month_orders_success->sum('origin_total');

//        dd($month_orders_count);
//        $currentOrderedCars = auth()->user()->cars()->whereHas('orders', function ($query) use ($currentDate) {
//            $query->whereHas('dates', function ($query) use ($currentDate) {
//                $query->where('end', '>=', $currentDate)->where('start', '<=', $currentDate);
//            });
////            $query->where('end_date', '>=', $currentDate)->where('start_date', '<=', $currentDate);
//        })->get();
        //get percent of ordered cars

//        $percentOrderedCars = $ownerCars ? ($orders->count() / $ownerCars->count()) * 100 : 0;


        return view('owners.dashboard.index', compact('orders', 'month_orders_count', 'month_orders_success_count', 'income'));
    }
}
