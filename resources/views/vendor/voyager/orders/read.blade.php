@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }}
        &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}"
                   title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore"
                   data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete"
                   data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    @php($order = $dataTypeContent)
    <div class="page-content read container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-center">
                        Статус : {{__('general.order.'.$order->status)}}
                    </h2>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="text-center">
                                <img src="{{$order->customer ? Voyager::image($order->customer->avatar) : 'Пользователь не найден '}}" class="img-fluid" width="250px"
                                     alt="avatar">
                            </div>
                        </div>
                        <div class="col-md-6 personal-info ff-st d-flex flex-column">
                            <ul class="p-0" style="padding: 0;">
                                <li class="list-group-item">
                                    <b class="fw-700">Арендаватель: </b>
                                    <a href="{{route('voyager.users.edit',$order->customer)}}" target="_blank"><span
                                            class="fw-400">{{$order->customer ? $order->customer->full_name : 'Пользователь не найден '}}</span></a>
                                </li>
                                <li class="list-group-item">
                                    <b class="fw-700">Дата: </b>
                                    <span class="fw-400">
                                    <span>{{$order->dates->start->format('Y:m:d g:i')}}</span><span> - </span><span>{{$order->dates->end->format('Y:m:d g:i')}}</span>
                                </span>
                                </li>
                                <li class="list-group-item">
                                    <b class="fw-700">Валюта: </b>
                                    <span class="fw-400">{{$order->currency}}</span>
                                </li>
                                <li class="list-group-item">
                                    <b class="fw-700">Цена за один день: </b>

                                    <span class="fw-400">{{$order->price_per_day}}</span>
                                </li>
                                <li class="list-group-item">
                                    <b class="fw-700">количество дней :</b>

                                    <span class="fw-400">{{$order->days}}</span>
                                </li>
                                @if ($order->note)
                                    <li class="list-group-item">
                                        <b class="fw-700">Заметка: </b>

                                        <span class="fw-400">{{$order->note}}</span>
                                    </li>
                                @endif

                                <li class="list-group-item">
                                    <b class="fw-700">Окончательная цена: </b>

                                    <span
                                        class="fw-400">{!!  $order->total_price  !!} {{ currency_symbol($order->currency) }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="customer-order-image-container">
                                        <img src="{{Voyager::image($order->car->image_1)}}" alt="Avatar"
                                             class="image img-fluid"
                                             style="width:100%">
                                        <div class="middle">
                                            <a href="{{route('car.single', ['slug' => $order->car->slug])}}"
                                               class="own-btn rounded d-inline-block ora px-2 py-1 fs-15">
                                                {!! $order->car->title !!}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mt-5">
                                    @if ($order->status == 'requested')
                                        <div class="" role="group" aria-label="Basic example">
                                            <a href="{{route('admin.orders.send_response', ['order' => $order, 'act' => 'confirmed'])}}"
                                               class="btn btn-success">Подтвердить</a>
                                            <a href="{{route('admin.orders.send_response', ['order' => $order, 'act' => 'rejected'])}}"
                                               class="btn btn-warning">Отклонить </a>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel relation-group">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#DeliveryLocation"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        <i class="more-less fas fa-plus"></i>
                                        Детали
                                    </a>
                                </h4>
                            </div>
                            <div id="DeliveryLocation"
                                 class="panel-collapse  collapse"
                                 role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="delivery-content">
                                        <div class="d-flex removed">
                                            <ul class="p-0" style="padding: 0; width: 100%">
                                                @php($green_card = json_decode($order->green_card, true))
                                                @if (count($green_card))

                                                    <li class="list-group-item px-3">
                                                        <div class="">
                                                            <span
                                                                class="col "> <strong>Грин Карта {{config('car.green_card.country.'.($green_card['name']))}}</strong></span>
                                                            <strong> | </strong>
                                                            <span
                                                                class="col text-right">{!!  $green_card['price']  !!} {{ currency_symbol($order->currency) }}</span>
                                                        </div>
                                                    </li>
                                                @endif

                                                @php($other_details = json_decode($order->other_details, true))

                                                @if ($other_details['take_no_work_time'])
                                                    <li class="list-group-item px-3">
                                                        <span class="col">
                                                            <strong>Взять не рабочее время</strong>
                                                            <span class="k-pointer" data-toggle="tooltip"
                                                                  data-html="true" data-placement="top"
                                                                  title=" {{ tooltips($toolips, 'ne_rabochie_vremya', $other_details['take_no_work_time'] . currency_symbol($order->currency)) }}">
                                                                <i class="fas fa-info-circle"></i>
                                                            </span>
                                                        </span>
                                                        <strong> | </strong>
                                                        <span
                                                            class="col text-right">{!! $other_details['take_no_work_time'] . currency_symbol($order->currency) !!}</span>
                                                    </li>
                                                @endif

                                                @if ($other_details['return_in_non_working_time'])
                                                    <li class="list-group-item px-3">
                                                        <span class="col">
                                                            <strong>Вернуть в не рабочее время</strong>
                                                        </span>
                                                        <strong> | </strong>
                                                        <span
                                                            class="col text-right">{!! $other_details['return_in_non_working_time'] . currency_symbol($order->currency) !!}</span>
                                                    </li>
                                                @endif

                                                @if ($other_details['urgency'])
                                                    <li class="list-group-item px-3">
                                                        <span class="col">
                                                            <strong>Вернуть в не рабочее время</strong>
                                                        </span>
                                                        <strong> | </strong>
                                                        <span
                                                            class="col text-right">{!! $other_details['urgency'] . currency_symbol($order->currency) !!}</span>
                                                    </li>

                                                @endif

                                                @if ($other_details['took'])
                                                    <li class="list-group-item px-3">
                                                        <span class="col">
                                                            <strong>{{$other_details['took']['name']}}</strong>
                                                        </span>
                                                        <strong> | </strong>
                                                        <span
                                                            class="col text-right">{!!  $other_details['took']['price'] . currency_symbol($order->currency)!!}</span>
                                                    </li>
                                                @endif

                                                @if ($other_details['give'])
                                                    <li class="list-group-item px-3">
                                                        <span class="col">
                                                            <strong>{{$other_details['give']['name']}}</strong>
                                                        </span>
                                                        <strong> | </strong>
                                                        <span
                                                            class="col text-right">{!!  $other_details['give']['price'] . currency_symbol($order->currency)!!}</span>
                                                    </li>
                                                @endif

                                                @php($orderInsurances = json_decode($order->insurances, true))
                                                @if (count($orderInsurances))
                                                    @foreach($orderInsurances as $item)
                                                        @php($insurances = \App\Models\Insurance::get())

                                                        <li class="list-group-item px-3">
                                                            <div class="">
                                                                <span class="col ">
                                                                    <strong>{{$insurances->where('id', $item['id'])->first()->translate()->name}}</strong>
                                                                </span>
                                                                <strong> | </strong>
                                                                <span class="col text-right">
                                                                    {!!  $item['price']  !!} {{ currency_symbol($order->currency) }}
                                                                </span>
                                                            </div>
                                                            @if(!$loop->last)
                                                                <hr style="margin:0;">
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                @endif

                                                @php($orderAttributes = json_decode($order->attributes, true))
                                                @if (count($orderAttributes))
                                                    @foreach($orderAttributes as $item)
                                                        @php($attributes = \App\Models\Filter::get())
                                                        <li class="list-group-item px-3">
                                                            <div class="">
                                                                <span class="col ">
                                                                    <strong>{{$attributes->where('id', $item['id'])->first()->translate()->title}}</strong>
                                                                </span>
                                                                <strong> | </strong>
                                                                @if(!$item['disposable'])
                                                                    <span class="col text-center">
                                                                        {{$order->days}} Дней х {!! $item['price'] !!} {{currency_symbol($order->currency)}}
                                                                    </span>
                                                                    <strong> | </strong>
                                                                    <span class="col text-right">
                                                                        {!! $item['price'] * $order->days !!} {{currency_symbol($order->currency)}}
                                                                    </span>
                                                                @else
                                                                    <span
                                                                        class="col text-center">{!! $item['price'] .' '.currency_symbol($order->currency) !!}</span>
                                                                    <span
                                                                        class="col text-right">{!! $item['price'] .' '.currency_symbol($order->currency) !!}</span>
                                                                @endif
                                                            </div>
                                                            @if(!$loop->last)
                                                                <hr style="margin:0;">
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                @endif


                                                <li class="list-group-item px-3">
                                                    <span class="col">{{__('car.rent')}}</span>
                                                    <span class="col text-center">
                                                        <strong>{{$order->days}} {{__('car.days')}} х  {!!  $order->price_per_day  !!} {{ currency_symbol($order->currency) }}</strong>
                                                    </span>
                                                    <strong> | </strong>
                                                    <span class="col text-right">
                                                        {!! $order->days * $order->price_per_day !!} {{ currency_symbol($order->currency) }}
                                                    </span>
                                                </li>
                                            </ul>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($order->histories()->count())
                        <div class="col-md-6 panel relation-group">
                            <h3 class="text-center">История </h3>
                            <div class="d-flex removed">
                                <ul class="py-3" style=" width: 100%">
                                    @foreach ($order->histories->sortByDesc('created_at') as $history)
                                        <li class="list-group-item">
                                            <ul class="p-0" style="padding: 0; width: 100%">
                                                @php($details = json_decode($history->details, true))
                                                <li class="list-group-item">
                                                    <span><strong>Статус:</strong> </span><span>{{__('general.order.'.$details['type'])}}</span>
                                                </li>

                                                <li class="list-group-item">
                                                    <span><strong>Автор:</strong> </span><span>{{__('general.order.'.$details['author'])}}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    <span><strong>Действие:</strong> </span><span>{{__('general.order.'.$details['act'])}}</span>
                                                </li>
                                                <li class="list-group-item">
                                                    <span><strong>Дата:</strong> </span><span>{{$history->created_at->format('Y:m:d H:i')}}</span>
                                                </li>
                                                @isset($details['note'])
                                                    <li class="list-group-item">
                                                        <span><strong>Заметка:</strong> </span><span>{{$details['note']}}</span>
                                                    </li>
                                                @endisset
                                            </ul>
                                        </li>

                                    @endforeach
                                </ul>
                            </div>
                            @endif

                        </div>
                </div>
            </div>
        </div>

        {{-- Single delete modal --}}
        <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title"><i
                                class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}
                            ?</h4>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger pull-right delete-confirm"
                                   value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                        </form>
                        <button type="button" class="btn btn-default pull-right"
                                data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        @stop

        @section('javascript')
            @if ($isModelTranslatable)
                <script>
                    $(document).ready(function () {
                        $('.side-body').multilingual();
                    });
                </script>
                <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
            @endif
            <script>
                var deleteFormAction;
                $('.delete').on('click', function (e) {
                    var form = $('#delete_form')[0];

                    if (!deleteFormAction) {
                        // Save form action initial value
                        deleteFormAction = form.action;
                    }

                    form.action = deleteFormAction.match(/\/[0-9]+$/)
                        ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                        : deleteFormAction + '/' + $(this).data('id');

                    $('#delete_modal').modal('show');
                });

            </script>
@stop
