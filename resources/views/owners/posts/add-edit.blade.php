@extends('owners.layouts.app')

@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop

@section('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>

@stop

@section('content')
    <div class="page-header d-flex justify-content-lg-between">
        <div class="col">
            <h1 class="text-center">{{__('user_post.'.($post->getKey() ? 'edit' : 'add'))}}</h1>
        </div>
        <div class="col text-right">
            <a href="{{route('owners-posts.create')}}" class="btn btn-primary   d-inline-block  border-radius-20"
               title="{{__('user_post.add')}}">
                <i class="fas fa-plus"></i>
                <span class="">{{__('user_post.add')}}</span>
            </a>
        </div>
    </div>

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">
                <div class="own-container bg-white border-radius-20 p-5">
                    <div class="row">
                        <!-- Blog Entries Column -->
                        <div class="col-md-12 order-md-1 order-2">
                            <form method="post" id="edit-add" action="{{route('owners-posts.'.($post->getKey()?'update':'store'), $post)}}">
                                @if(isset($post->id))
                                    {{ method_field("PUT") }}
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="title">{{__('user_post.title')}}</label>
                                    <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}" placeholder="{{__('user_post.title')}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="body">{{__('user_post.content')}}</label>
                                    <textarea class="form-control" id="body" name="body" rows="10"  placeholder="{{__('user_post.content')}}" >{!! $post->body !!}</textarea>
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">{{__('user_post.save')}}</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.container -->
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.16/tinymce.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script src="{{asset('js/ru_RU.js')}}"></script>
    <script>
        tinymce.init({
            selector: '#body',
            language_url: '/admin/langs/ru.js',
            language: 'ru',
            plugins: 'image',
            height : 500,
            menubar: false,
            statusbar: false,
            image_dimensions: false,
            image_title: false,
            images_upload_url: '{{ route('posts.upload') }}',
            setup: function(editor) {
                editor.on('keyup', function(e) {
                    // Revalidate the hobbies field
                    $('#edit-add').bootstrapValidator('revalidateField', 'body');
                });
            },
            images_upload_handler: function (blobInfo, success, failure) {
                // console.log(blobInfo)
                // console.log(failure)
                // console.log(success)
                // setTimeout(function () {
                //     /* no matter what you upload, we will turn it into TinyMCE logo :)*/
                //     success('http://moxiecode.cachefly.net/tinymce/v9/images/logo.png');
                // }, 2000);
                var xhr, formData;

                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '{{ route('posts.upload') }}');

                xhr.onload = function() {
                    var json;

                    if (xhr.status != 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }

                    success(JSON.parse(xhr.responseText).path);
                };

                formData = new FormData();
                formData.append('image', blobInfo.blob(), blobInfo.filename());
                formData.append('type_slug', 'posts');
                formData.append('_token', '{{csrf_token()}}');

                xhr.send(formData);

            },
            // file_picker_callback: function (callback, value, meta) {
            //     if (meta.filetype == 'image') {
            //         $('#upload_file').trigger('click')
            //     }
            // },
            toolbar: ' bold italic |  image ',
            content_css: [
                'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
            ],
            content_style: [
                'body{width: 100%;padding-right: 15px;padding-left: 15px; margin-right: auto;margin-left: auto; color: #504d4d !important;} img {max-width: 100%;}'
            ],
            convert_urls: false,
            // style_formats: [
            //     {title: 'Image Left', selector: 'img', styles: {
            //             'float' : 'left',
            //             'margin': '0 10px 0 10px'
            //         }},
            //     {title: 'Image Right', selector: 'img', styles: {
            //             'float' : 'right',
            //             'margin': '0 10px 0 10px'
            //         }}
            // ]

        });
        $('#edit-add').bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                body: {
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                                // Get the plain text without HTML
                                var text = tinyMCE.activeEditor.getContent({
                                    format: 'text'
                                });
                                return text.length >= 5;
                            }
                        }
                    }
                }
            }
        });
        $(document).on('submit', '#my_form', function (form) {
            form.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    console.log(response)
                    $('.tox-control-wrap').find('input[type="text"]').val(response.path);
                }
            })
        });
    </script>
@stop
