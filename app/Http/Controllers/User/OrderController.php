<?php

namespace App\Http\Controllers\User;

use App\Models\Order;
use App\Models\OrderHitory;
use App\Notifications\CanceledBooking;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    public function getOrder(Request $request, Order $order)
    {
        $html = view('user.single-order', compact('order'))->render();

        return response()->json(['html' => $html]);
    }

    public function cancel(Request $request, Order $order)
    {
        $this->validate($request, [
            'confirm' => 'required|integer|min:1'
        ]);
        $details = [
            'author' => 'user',
            'user_name' => auth()->user()->full_name,
            'user_id' => auth()->id(),
            'act' => 'canceled',
            'type' => 'canceled',
            'note' => $request->note,
            'date' => Carbon::now(),
        ];

        $order->status = 'canceled';
        $order->save();

        $histort = new OrderHitory();
        $histort->order_id = $order->id;
        $histort->details = json_encode($details);
        $histort->save();
        $admins = User::where('role_id', 1)->get();
        Notification::send($admins, new CanceledBooking($order));
        $order->car->owner->notify(new CanceledBooking($order));

        if ($histort) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false, 'warning' => true]);
        }
    }



}
