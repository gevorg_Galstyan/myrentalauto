<?php

namespace App\Notifications;

use App\Models\TicketComment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketReplyNotification extends Notification
{
    use Queueable;

    private $comment;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(TicketComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return  $notifiable->role_id == 1 ? ['mail', 'broadcast'] : ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $comment = $this->comment;
        $images = @json_decode($comment->images);
        $url = $notifiable->role_id == 1 ? route('voyager.tickets.show', $comment->ticket->id) : route('tickets.show',$comment->ticket->ticket_number);

        $mail = (new MailMessage)
            ->subject('На ваш тикет ответили ')
            ->greeting($comment->ticket->title)
            ->line($comment->text)
            ->action('Открыть Тикет ', $url);
        if ($images) {
            foreach ($images as $image) {
                $mail->attach(storage_path('app/public/'.$image));
            }
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'На ваш тикет ответили ',
            'message' => $this->comment->ticket->title,
            'url' => route('tickets.show',$this->comment->ticket->ticket_number),
        ];
    }

    public function toBroadcast($notifiable)
    {
        $ticket = $this->comment->ticket;
        $url = $notifiable->role_id == 1 ? route('voyager.tickets.show', $ticket->id) : route('tickets.show',$ticket->ticket_number);

        return new BroadcastMessage([
            'title' => 'На ваш тикет ответили ',
            'message' => '<a href="'.$url.'">Страница тикета</a>',

        ]);
    }

}
