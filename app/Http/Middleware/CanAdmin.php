<?php

namespace App\Http\Middleware;

use Closure;

class CanAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::guest() || auth()->user()->role_id == 2 || auth()->user()->role_id == 3){
            return abort(403);
        }
        return $next($request);
    }
}
