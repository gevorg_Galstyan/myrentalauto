<div class="col-lg-{{request()->input('keyword') ? '12  my-5': '9'}} border-radius-20 bg-white ">
    @if ($items && !$item)
        <h1 class="text-center fw-700 fs-30">{!! $activeCat->translate()->name !!}</h1>
        <ul class=" ">
            @foreach($items as $faq)
                <li class="border-bottom">
                    <a href="{{route('faq', ['for_whom' => $faq->category->for_whom,'cat' => $faq->category->slug, 'question' => $faq->slug])}}"
                       class="">
                        <span class="pl-2">{{$faq->translate()->title}}</span></a>
                </li>
            @endforeach
        </ul>
    @else
        <div class="d-flex justify-content-center">
            <h1 class="text-center fw-700 fs-20"> {{$item->translate()->title}} </h1>
        </div>

        <div class="ff-ms">
            {!! $item->translate()->body !!}
        </div>
        <div class="help-question mt-4" id="faq{{$item->id}}">
            @guest
                <h6 class="fw-700">{!! __('faq.did_this_article_help_you') !!} ?</h6>
                <button class="btn yes fw-700" data-toggle="modal"
                        data-target="#loginModal">
                    {!! __('faq.yes') !!}
                </button>
                <button class="btn no fw-700" data-toggle="modal" data-target="#loginModal">
                    {!! __('faq.no') !!}
                </button>
            @else
                @if ($item->liked || $item->disliked )
                    <h6 class="fw-700">{!! __('faq.you_rated_this_article') !!}</h6>
                @else
                    <h6 class="fw-700">{!! __('faq.did_this_article_help_you') !!} ?</h6>
                    <button data-target="#faq{{$item->id}}"
                            data-href="{{route('faq.like', ['faq' => $item, 'like'])}}"
                            class="btn yes fw-700 faq-like">
                        {!! __('faq.yes') !!}
                    </button>
                    <button data-target="#faq{{$item->id}}"
                            data-href="{{route('faq.like', ['faq' => $item, 'dislike'])}}"
                            class="btn no fw-700 faq-like">
                        {!! __('faq.no') !!}
                    </button>
                @endif
            @endguest
        </div>
        @if ($item)
            @php
            $key = $items->search(function($i) use ($item) {
    return $item->id == $i->id;
});
            $collection = $items->getIterator();

$next = $collection[$key + 1]??null;
$prev = $collection[$key - 1]??null;
@endphp
            <ul class="">
                <h6 class="fw-700 mb-2 mt-4">{!! __('faq.may_be_useful') !!}</h6>
                @if ($prev)
                    <li class="border-bottom">
                        <a href="{{route('faq', ['for_whom' => $prev->category->for_whom,'cat' => $prev->category->slug, 'question' => $prev->slug])}}"
                           class="">
                            <span class="pl-2">{{$prev->translate()->title}}</span></a>
                    </li>
                @endif
                @if ($next)
                    <li class="border-bottom">
                        <a href="{{route('faq', ['for_whom' => $next->category->for_whom,'cat' => $next->category->slug, 'question' => $next->slug])}}"
                           class="">
                            <span class="pl-2">{{$next->translate()->title}}</span></a>
                    </li>
                @endif
            </ul>
        @endif
    @endif

</div>

{{--<div class="col-md-{{request()->input('keyword') ? '12  my-5': '9'}}">--}}
{{--    <div class="advantage bg-white" id="faq">--}}
{{--        @foreach($items as $faq)--}}
{{--            <div class="card " data-status="#faq{{$faq->id}}">--}}
{{--                <div class="card-header">--}}
{{--                    <h2 class="mb-0">--}}
{{--                        <button class="btn btn-link" type="button" data-toggle="collapse"--}}
{{--                                data-target="#collapse-{{$faq->id}}"--}}
{{--                                aria-expanded="false" aria-controls="collapse-{{$faq->id}}">--}}
{{--                            {{$faq->translate()->title}}--}}
{{--                        </button>--}}
{{--                    </h2>--}}
{{--                </div>--}}
{{--                <div id="collapse-{{$faq->id}}" class="collapse" aria-labelledby="heading-{{$faq->id}}"--}}
{{--                     data-parent="#faq">--}}
{{--                    <div class="card-body">--}}
{{--                        <p>--}}
{{--                            {!! $faq->translate()->body !!}--}}
{{--                        </p>--}}
{{--                        <div class="help-question mt-4" id="faq{{$faq->id}}">--}}
{{--                            @guest--}}
{{--                                <h6 class="fw-700">Помогло ли вам эта статья ?</h6>--}}
{{--                                <button class="btn yes fw-700" data-toggle="modal"--}}
{{--                                        data-target="#loginModal">Да--}}
{{--                                </button>--}}
{{--                                <button class="btn no fw-700" data-toggle="modal" data-target="#loginModal">--}}
{{--                                    Нет--}}
{{--                                </button>--}}
{{--                            @else--}}
{{--                                @if ($faq->liked || $faq->disliked )--}}
{{--                                    <h6 class="fw-700">Вы оценили эту статью . Спасибо за оценку .</h6>--}}
{{--                                @else--}}
{{--                                    <h6 class="fw-700">Помогло ли вам эта статья ?</h6>--}}
{{--                                    <button data-target="#faq{{$faq->id}}"--}}
{{--                                            data-href="{{route('faq.like', ['faq' => $faq, 'like'])}}"--}}
{{--                                            class="btn yes fw-700 faq-like">Да--}}
{{--                                    </button>--}}
{{--                                    <button data-target="#faq{{$faq->id}}"--}}
{{--                                            data-href="{{route('faq.like', ['faq' => $faq, 'dislike'])}}"--}}
{{--                                            class="btn no fw-700 faq-like">Нет--}}
{{--                                    </button>--}}
{{--                                @endif--}}
{{--                            @endguest--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}
{{--    </div>--}}
{{--</div>--}}
