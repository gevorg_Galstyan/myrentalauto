<div class="modal fade" id="wrong-fill-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                @if ($lists->count())
                    <h4 class="text-center">История</h4>
                    <ul class="list-group list-group-flush" style="max-height: 200px; overflow-y: auto">
                        @foreach ($lists as $item)
                            <li class="list-group-item">{{$item->text}} <span class="pull-right"> {{$item->created_at}}</span></li>
                        @endforeach
                    </ul>

                @endif
                <form id="wrong-fill-form" action="{{route('voyager.cars.wrong-fill', $dataTypeContent->id )}}"
                      method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Текст:</label>
                        <textarea class="form-control" rows="6" id="message-text" name="text"
                                  required>{{$lists->last() ? $lists->last()->text:''}}</textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="submit" form="wrong-fill-form" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>
