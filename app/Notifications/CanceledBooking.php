<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CanceledBooking extends Notification
{
    use Queueable;

    public $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($notifiable->role_id == 1) return ['mail', 'broadcast'];
        else return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.car-booking.owner.title', [
                'name' => $notifiable->first_name,
            ]))
            ->line(__('notification.canceled-booking-admin-owner.text', [
                'car' => $this->order->car->title,
                'start' => $this->order->dates->start->format('d:m:Y g:i'),
                'end' => $this->order->dates->end->format('d:m:Y g:i'),
                'customer' => $this->order->customer->full_name,
            ]))
            ->action(__('notification.canceled-booking-admin-owner.link_title'), $notifiable->role_id == 1 ? route('voyager.orders.show', $this->order) : route('owner.orders', ['status' => 'canceled', 'barcode_number' => $this->order->barcode_number]))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if ($notifiable->role_id != 1) {
            return [
                'type' => 'canceled',
                'car_slug' => $this->order->car->slug,
                'user' => [
                    'name' => $this->order->customer->first_name,
                    'id' => $this->order->customer->id,
                ],
                'title' => __('general.order.canceled'),
                'message' => __('notification.canceled-booking-admin-owner.link_title'), $notifiable->role_id == 1 ? route('voyager.orders.show', $this->order) : route('owner.orders', ['status' => 'canceled', 'barcode_number' => $this->order->barcode_number]),
                'url' => route('owner.orders', ['status' => 'canceled', 'barcode_number' => $this->order->barcode_number]),
                'order_id' => $this->order->id,
            ];
        } else {
            return [
                //
            ];
        }
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => __('general.order.canceled'),
            'message' => __('notification.canceled-booking-admin-owner.link_title'), $notifiable->role_id == 1 ? route('voyager.orders.show', $this->order) : route('owner.orders', ['status' => 'canceled', 'barcode_number' => $this->order->barcode_number]),
            'url' => $notifiable->role_id == 1 ? route('voyager.orders.show', $this->order) : route('owner.orders', ['status' => 'canceled', 'barcode_number' => $this->order->barcode_number]),
        ]);

    }
}
