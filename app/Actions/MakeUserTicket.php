<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class MakeUserTicket extends AbstractAction
{
    public function getTitle()
    {
        return 'Открыть тикет ';
    }
//    public function getPolicy()
//    {
//        return 'make_user_ticket';
//    }

    public function getIcon()
    {
        return 'far fa-file-alt';
    }

    public function getAttributes()
    {
        return [

            'class' => 'btn btn-sm btn-success pull-right ',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.tickets.create', ['user='.$this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }


}
