<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey={{ config('voyager.yandexmap.key') }}"></script>
<script>
    var locale = '{{LaravelLocalization::getCurrentLocale()}}';
    ymaps.ready(init);

    function init() {

            @forelse($dataTypeContent->getCoordinates() as $point)
        var center = [{{ $point['lat'] }}, {{$point['lng']}}];
            @empty
        var center = [{{ config('voyager.yandexmap.center.lat') }}, {{config('voyager.yandexmap.center.lng')}}];

            @endforelse
        var suggestView = new ymaps.SuggestView('location', {
                provider: {
                    suggest: function (request, options) {
                        var parseItems = ymaps.suggest((locale == 'en' ? "Belarus, " : "Беларусь, ") + request).then(function (items) {
                            for (var i = 0; i < items.length; i++) {
                                var displayNameArr = items[i].displayName.split(',');

                                var newDisplayName = [];
                                for (var j = 0; j < displayNameArr.length; j++) {
                                    if (displayNameArr[j].indexOf('район') == -1) {
                                        newDisplayName.push(displayNameArr[j]);
                                    }
                                }
                                items[i].displayName = newDisplayName.join();
                            }
                            return items;
                        });
                        return parseItems;
                    }
                }
            });

        suggestView.events.add('select', function (e) {
            ymaps.geocode(e.get('item').value).then(function (res) {
                myMap.geoObjects.remove(myPlacemark);
                myPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {}, {
                    preset: 'islands#blueAutoCircleIcon',
                    draggable: true
                });
                $('#lat').val(res.geoObjects.get(0).geometry.getCoordinates()[0]);
                $('#lng').val(res.geoObjects.get(0).geometry.getCoordinates()[1]);
                myMap.geoObjects.add(myPlacemark);
                myMap.setBounds(res.geoObjects.get(0).properties.get('boundedBy'));
                myPlacemark.events.add('drag', changeCoordinate).add('dragend', changeCoordinate);
            });
        });

        var myMap = new ymaps.Map('map', {
            center: center,
            zoom: {{config('voyager.yandexmap.zoom')}},
            controls: []
        });

        var myPlacemark = new ymaps.Placemark(center, {}, {
            preset: 'islands#blueAutoCircleIcon',
            draggable: true
        });
        myMap.geoObjects.add(myPlacemark);

        myPlacemark.events.add('drag', changeCoordinate).add('dragend', changeCoordinate);

        function changeCoordinate(e) {
            let target = e.get('target');
            let cord = target.geometry.getCoordinates();
            $('#lat').val(target.geometry.getCoordinates()[0]);
            $('#lng').val(target.geometry.getCoordinates()[1]);
            ymaps.geocode(cord).then(function (res) {
                let data = res.geoObjects.get(0).properties.getAll();
                $('#location').val(data.text);
            });
        }

    }
</script>


{{--<script type="application/javascript">--}}
{{--function initMap() {--}}
{{--@forelse($dataTypeContent->getCoordinates() as $point)--}}
{{--var center = {lat: {{ $point['lat'] }}, lng: {{ $point['lng'] }}};--}}
{{--@empty--}}
{{--var center = {--}}
{{--lat: {{ config('voyager.googlemaps.center.lat') }},--}}
{{--lng: {{ config('voyager.googlemaps.center.lng') }}};--}}
{{--@endforelse--}}
{{--var map = new google.maps.Map(document.getElementById('map'), {--}}
{{--zoom: {{ config('voyager.googlemaps.zoom') }},--}}
{{--center: center--}}
{{--});--}}
{{--var markers = [];--}}
{{--@forelse($dataTypeContent->getCoordinates() as $point)--}}
{{--var marker = new google.maps.Marker({--}}
{{--position: {lat: {{ $point['lat'] }}, lng: {{ $point['lng'] }}},--}}
{{--map: map,--}}
{{--draggable: true--}}
{{--});--}}
{{--markers.push(marker);--}}
{{--@empty--}}
{{--var marker = new google.maps.Marker({--}}
{{--position: center,--}}
{{--map: map,--}}
{{--draggable: true--}}
{{--});--}}
{{--@endforelse--}}

{{--google.maps.event.addListener(marker, 'dragend', function (event) {--}}
{{--document.getElementById('lat').value = this.position.lat();--}}
{{--document.getElementById('lng').value = this.position.lng();--}}
{{--});--}}
{{--}--}}
{{--</script>--}}

{{--<script async defer--}}
{{--src="https://maps.googleapis.com/maps/api/js?key={{ config('voyager.googlemaps.key') }}&callback=initMap"></script>--}}
