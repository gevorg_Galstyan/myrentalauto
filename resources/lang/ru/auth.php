<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, повторите попытку через :seconds секунд.',
    'login_with' => 'Войти через',
    'or' => 'или',
    'email' => 'Эл. адрес',
    'password' => 'Пароль',
    'repeat_password' => 'Повторите пароль',
    'remember_me' => 'Запомнить меня',
    'login' => 'Войти',
    'login_before_order' => 'Арендовать',
    'logout' => 'Выход',
    'name' => 'Имя',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'select_role' => 'Пожалуйста, выберите вашу роль',
    'role_user' => 'Пользователь',
    'role_owner' => 'Владелец',
    'register' => 'Регистрация',
    'looking_to' => 'Ищу в',
    'create_an_account' => 'Зарегистрироваться',
    'already_account' => 'Уже есть аккаунт?',
    'forgot_password' => 'Забыли пароль?',
    'reset_password' => 'Сброс пароля',
    'confirm_password' => 'Подтвердите Пароль',
    'reset_link' => 'Подтвердите Пароль',

    'verify_title' => 'Проверьте свой адрес электронной почты',
    'verify_alert' => 'На ваш адрес электронной почты была отправлена новая ссылка для подтверждения.',
    'verify_content' => 'Прежде чем продолжить, проверьте свою электронную почту на наличие ссылки для подтверждения.',
    'verify_info' => 'Если вы не получили письмо',
    'verify_link' => 'нажмите здесь, чтобы запросить другой',


    'email_exist' => 'пользователь с вашим электронным адресом уже зарегистрирован пожалуста зайди на с этим именем или смените акунт',

    'email_not_exist' => 'Социальная сеть :provider Не предоставил нам ваш емайл адрес пожалуста исправьте это или войдите с другова акунта',
    'accountIsBlocked' => 'Ваш алинт заблокирован для получения информации можете обратиться <a href=""> сюда </a> .'

];
