<?php
return [
    'cars_for_rent' => 'Cars for rent',
    'diesel' => 'Diesel',
    'auto' => 'Automatic',

    'adv' => 'ADVANTAGES',
    'blog' => 'BLOG',
    'how_it_work' => 'HOW IT WORKS',

    'register' => 'Sign up',
    'register_p' => 'Sign up for free to use all the features.',
    'fill_in' => 'Fill in the vehicle data',
    'fill_in_p' => 'We will show your car to more than 200,000 users of our platform.',
    'wait_call' => 'Wait for a call',
    'wait_call_p' => 'Our users will soon see your car and will surely call you.',

    'location' => 'Choose a place',
    'pick_date' => 'Pick date',
    'basic_filters' => 'Basic Filters',
    'body_type' => 'Body Type',
    'fuel' => 'Fuel',
    'class' => 'Class',
    'seats' => 'Number of seats',
    'clear' => 'Clear',

    'feel_auto' => 'Feel Auto',
    'mobility' => 'Mobility',
    'in_any_city' => 'In any city',

];
