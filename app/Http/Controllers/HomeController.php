<?php

namespace App\Http\Controllers;

use App\Models\Advantage;
use App\Models\Car;
use App\Models\CarType;
use App\Models\ContactUs;
use App\Models\HomeSlider;
use App\Models\HowItWork;
use App\Models\Location;
use App\Models\MetaTag;
use App\Models\Post;
use Illuminate\Http\Request;
use TimeHunter\LaravelGoogleReCaptchaV3\Facades\GoogleReCaptchaV3;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        if ($request->C || $request->D || $request->P || $request->P || $request->F || $request->C) return redirect()->route('home');
//        SitemapGenerator::create('http://127.0.0.1:8000')->writeToFile('site-map.xml');

        $advantages = Advantage::where('publish', 1)->get();

        $cars = Car::published()->where('in_home_slider', 1)->orderBy('order', 'asc')->limit(4)->get();

        $meta_tags = MetaTag::where('name', 'home_page')->first();

        $car_types = CarType::get();

        $texts = HomeSlider::orderBy('sort', 'asc')->get();

        $how_its_works = HowItWork::get();

        $how_its_works = $how_its_works->groupBy('for_whom');

        foreach ($texts as $text) {
            $adjs[] = '<h2 class="mb-3 text-center">' . $text->myTranslate()->title . '</h2> <p class = "text-center" >' . $text->myTranslate()->tagline . '</p>';
        }

        $locations = Location::orderBy('sort', 'asc')->get();

        return view('home.index', compact('advantages', 'car_types', 'meta_tags', 'cars', 'texts', 'how_its_works', 'adjs', 'locations'));

    }

    public function getPost(Request $request)
    {
        if ($request->ajax()) {
            $posts = Post::where('status', 'PUBLISHED')->orderBy('created_at', 'desc')->limit(3)->get();
            $width = $request->width;
            $html = view('home.blog', compact('posts', 'width'))->render();
            return response()->json(['html' => $html]);
        } else {
            return abort(403);
        }
    }

    public function home()
    {
        return view('user.index');
    }


    public function contact()
    {
        return view('contact-us');
    }

    public function contactUsPost(Request $request)
    {

        $response = GoogleReCaptchaV3::setAction('contact_us')->verifyResponse($request->input('g-recaptcha-response'), $request->ip());
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        if (!$response->isSuccess()) {
            if ($response->getMessage() == 'timeout-or-duplicate') {
                $message = "GoogleReCaptchaV3::messages.ERROR_TIMEOUT";
            } else {
                $message = $response->getMessage();
            }

            return back()->with([
                'message' => trans($message),
                'alert-type' => 'warning'
            ]);
        }
        ContactUs::create($request->all());


        Mail::to('info@myrentauto.com')
            ->send(new \App\Mail\ContactUs($request->all()));

        return back()->with('status', __('contact.thanks'));
    }


}
