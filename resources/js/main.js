$('.js-toggle-menu').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $('.bord').toggleClass('open');
    $('.lang-block ').toggleClass('bl');
});

$(window).scroll(function () {
    if ($(window).scrollTop() > 150) {
        $('header').addClass('position-fixed bg-white');
        // $('header').addClass('position-fixed');
        $('.own-nav').css('padding', '.2rem 1.5rem');
        $('.hamburger-menu .menu-item').css('background', '#000');
        $('.lang-block').css('color', '#000');
    } else {
        $('header').removeClass('position-fixed');
        // $('header').removeClass('position-fixed');
        $('.own-nav').css('padding', '1rem 1.5rem');
        // $('.hamburger-menu .menu-item').css('background', '#fff');
        // $('.lang-block').css('color', '#fff');
    }
});
$('.own-navbar').click(function (e) {
    e.stopPropagation();
    $('.navbar-collapse').collapse({
        toggle: false
    });
    $('.js-toggle-menu').toggleClass('open');
    $('.bord').toggleClass('open');

});
$('.tag-close').on('click', function() {
    let dataTag = $(this).data('tag');
    $('.badge[data-tagParent="'+dataTag+'"]').hide(500).remove()
});


$('body').on('click', '.stage', function() {
    //get tags in svg
    var animTags = $(this).children().find("g[id]");
    var animIds = animTags.map(function() { return this.id; }).get();

    for(var i = 0; i < animTags.length; i++) {
        //
        var animClasses = animIds[i] + '-animate';
        $(animTags[i]).toggleClass(animClasses)
    }
    $(this).toggleClass('activeBtn')

});

// rotate arrow with click
$('.dropdown-spec').on('show.bs.dropdown', function () {
    $(this).find('.icon-show-hide').html('<i class="fas fa-chevron-up"></i>')
})
$('.dropdown-spec').on('hide.bs.dropdown', function () {
    $(this).find('.icon-show-hide').html('<i class="fas fa-chevron-down"></i>')
})



