<div class="position-fixed w-100 d-flex flex-column p-4 ">
    <div class="toast ml-auto" role="alert" data-delay="20000" data-autohide="true">
        <div class="toast-header">
            <div class="rounded mr-2  text-primary"> <i class="fas fa-square primary"></i></div>
            <strong class="mr-auto notif-title"></strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="toast-body notif-message"></div>
    </div>
</div>