<?php
return [
    'updated' => 'updated',
    'read_article' => 'read article',
    'see_more' => 'See more',
    'search' => 'Search',
    'tags' => 'Tags',
    'no_search_result' => 'No results found',
    'can_read' => 'Can read',
];
