<div id="accordion-insurance">
    <div class="card shadow-sm  bg-light  rounded">
        <div class="card-header" id="heading-insurance">
            <h5 class="mb-0">
                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-insurance"
                   aria-expanded="true" aria-controls="collapse-insurance">
                    <i class="fa"></i>
                    Страховки
                </a>
            </h5>
        </div>

        <div id="collapse-insurance" class="collapse {{$dataTypeContent->insurances()->count()?'show':''}}"
             aria-labelledby="heading-insurance"
             data-parent="#accordion-insurance">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                        <tr>
                            <th></th>
                            <th>
                                Номер серии
                                <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title="{{tooltips($toolips, 'insurances_serial_number')}}">
                                    <i class="fa fa-question-circle"></i>
                                </span>
                            </th>
                            <th>
                                Дата с
                                <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title="{{tooltips($toolips, 'insurances_start_date')}}">
                                    <i class="fa fa-question-circle"></i>
                                </span>
                            </th>
                            <th>
                                Дата до
                                <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title="{{tooltips($toolips, 'insurances_end_date')}}">
                                    <i class="fa fa-question-circle"></i>
                                </span>
                            </th>
                            <th>
                                Цена (BYN)
                                <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title="{{tooltips($toolips, 'insurances_price')}}">
                                    <i class="fa fa-question-circle"></i>
                                </span>
                            </th>
                            <th>
                                Франшиза
                                <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title="{{tooltips($toolips, 'insurances_franchise')}}">
                                    <i class="fa fa-question-circle"></i>
                                </span>
                            </th>
                        </tr>
                        </thead>
                        @foreach(\App\Models\Insurance::all() as $item)

                            <tbody>
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <label class="form-check-label " for="checkbox-{{$item->id}}">
                                            <input class="form-check-input change-insurances"
                                                   type="checkbox" value="{{$item->id}}"
                                                   data-target="insurance-{{$item->id}}"
                                                   {{$dataTypeContent->insurances->contains($item->id)?'checked': ''}}
                                                   name="insurances[{{$item->id}}]"
                                                   id="checkbox-{{$item->id}}">
                                            {{$item->translate()->name}}
                                        </label>
                                    </div>
                                </td>

                                <td>
                                    <input type="text"
                                           class="form-control insurance-number insurance-{{$item->id}}"
                                           name="insurances[{{$item->id}}][serial_number]"
                                           value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->serial_number : ''}}"
                                           placeholder="ВВВ 1234567890"
                                           {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                        {{!$item->obligatory?'required':''}}>
                                </td>

                                <td>
                                    <input type="text"
                                           class="form-control own-date insurance-{{$item->id}}"
                                           name="insurances[{{$item->id}}][start_date]"
                                           autocomplete="off"  placeholder="ДД-ММ-ГГГГ"
                                           value="{{$dataTypeContent->insurances->contains($item->id)? \Carbon\Carbon::parse($dataTypeContent->insurances->where('id', $item->id)->first()->pivot->start_date)->format('d-m-Y') : ''}}"
                                           {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                        {{!$item->obligatory?'required':''}}>
                                <td>
                                    <input type="text"
                                           class="form-control own-date insurance-{{$item->id}}"
                                           name="insurances[{{$item->id}}][end_date]"
                                           value="{{$dataTypeContent->insurances->contains($item->id)? \Carbon\Carbon::parse($dataTypeContent->insurances->where('id', $item->id)->first()->pivot->end_date)->format('d-m-Y') : ''}}"
                                           autocomplete="off" placeholder="ДД-ММ-ГГГГ"
                                           {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                        {{!$item->obligatory?'required':''}}>
                                </td>

                                <td>
                                    <input type="text"
                                           class="form-control insurance-{{$item->id}}"
                                           name="insurances[{{$item->id}}][price]"
                                           value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->price : ''}}"
                                           placeholder="Цена (BYN)"
                                           {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                        {{!$item->obligatory?'required':''}}>
                                </td>
                                <td>
                                    @if($item->franchise === 1)

                                        <input type="text"
                                               class="form-control insurance-{{$item->id}}"
                                               name="insurances[{{$item->id}}][franchise]"
                                               value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->franchise : ''}}"
                                               placeholder="Франшиза"
                                               {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                            {{!$item->obligatory?'required':''}}>
                                    @else
                                        <span>НЕ ИМЕЕТ </span>

                                    @endif
                                </td>
                            </tr>
                            </tbody>

                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
