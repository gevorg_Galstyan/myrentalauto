<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Models\Order;
use App\Models\OrderHitory;
use App\Notifications\UserRequestAnswer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderController extends VoyagerBaseController
{
    public function sendResponse(Request $request, Order $order)
    {
        $data['act'] = $request->act;
        Validator::make($data, [
            'act' => [
                'required', Rule::in(['confirmed', 'rejected']),
            ]
        ])->validate();
        $order->status = $request->act;
        $order->save();

        $history['author'] = 'admin';
        $history['user_name'] = auth()->user()->full_name;
        $history['user_id'] = auth()->id();
        $history['act'] = $request->act;
        $history['type'] = 'response';
        $history['note'] = $request->note;
        $history['date'] = Carbon::now();


        $orderHistory = new OrderHitory();
        $orderHistory->order_id = $order->id;
        $orderHistory->details = json_encode($history);
        $orderHistory->save();

        $order->customer->notify(new UserRequestAnswer($order));

        return back()
            ->with([
                'message' => __('voyager::generic.successfully_updated') . "Заказ",
                'alert-type' => 'success',
            ]);
    }
}
