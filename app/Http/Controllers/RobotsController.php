<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MadWeb\Robots\Robots;

class RobotsController extends Controller
{
    public function __invoke(Request $request, Robots $robots)
    {
        if (request()->getHost() == 'myrentauto.com'){
            $robots->addUserAgent('*');

            $robots->addDisallow('/admin*');
            $robots->addDisallow('/public*');
            $robots->addDisallow('/images*');
            $robots->addDisallow('/profile*');
            $robots->addDisallow('/c-owner*');
            $robots->addDisallow('/currency*');
            $robots->addDisallow('/auth*');
            $robots->addDisallow('/login');
            $robots->addDisallow('/register');
            $robots->addDisallow('/password*');
            $robots->addDisallow('*?slug=');
            $robots->addDisallow('*?date=');
            $robots->addDisallow('/search?*');
            $robots->addDisallow('*?C=*');
            $robots->addDisallow('*?D=*');
            $robots->addDisallow('*?O=*');
            $robots->addDisallow('*?F=*');
            $robots->addDisallow('*?V=*');
            $robots->addDisallow('*?P=*');


            // Yandex
            $robots->addSpacer();
            $robots->addUserAgent('Yandex');

            $robots->addDisallow('/admin*');
            $robots->addDisallow('/public*');
            $robots->addDisallow('/profile*');
            $robots->addDisallow('/c-owner*');
            $robots->addDisallow('/currency*');
            $robots->addDisallow('/auth*');
            $robots->addDisallow('/login');
            $robots->addDisallow('/register');
            $robots->addDisallow('/password*');
            $robots->addDisallow('*?slug=');
            $robots->addDisallow('*?date=');
            $robots->addDisallow('/search?*');
            $robots->addDisallow('*?C=*');
            $robots->addDisallow('*?D=*');
            $robots->addDisallow('*?O=*');
            $robots->addDisallow('*?F=*');
            $robots->addDisallow('*?V=*');
            $robots->addDisallow('*?P=*');
            $robots->addHost(config('app.url'));


            // Googlebot
            $robots->addSpacer();
            $robots->addUserAgent('Googlebot');

//        $robots->addDisallow('*.css');
        $robots->addAllow('*.js');
            $robots->addDisallow('/admin*');
            $robots->addDisallow('/public*');
            $robots->addDisallow('/profile*');
            $robots->addDisallow('/c-owner*');
            $robots->addDisallow('/currency*');
            $robots->addDisallow('/auth*');
            $robots->addDisallow('/login');
            $robots->addDisallow('/register');
            $robots->addDisallow('/password*');
            $robots->addDisallow('*?slug=');
            $robots->addDisallow('*?date=');
            $robots->addDisallow('/search?*');
            $robots->addDisallow('*?C=*');
            $robots->addDisallow('*?D=*');
            $robots->addDisallow('*?O=*');
            $robots->addDisallow('*?F=*');
            $robots->addDisallow('*?V=*');
            $robots->addDisallow('*?P=*');
            $robots->addSitemap(config('app.url') . '/sitemap.xml');
        } else{
            $robots->addUserAgent('*');
            $robots->addDisallow('/');
        }




        return response($robots->generate(), 200, ['Content-Type' => 'text/plain']);
    }
}
