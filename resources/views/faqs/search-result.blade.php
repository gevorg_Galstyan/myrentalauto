<div class="own-container mt-5">
    <div class="tab-content bg-white border-radius-20 p-5 mb-5" id="faq-content">
        <div class="tab-pane fade show active ">

            <h5 class="text-center fw-700">Вопросы </h5>
            <ul class="cd-faq-categories ">
                @foreach($items as $faq)
                    <li class="border-bottom shadow p-3 mb-2 bg-white rounded">
                        <a href="{{route('faq', ['for_whom' =>$faq->category->for_whom, 'cat' => $faq->category->slug, 'question' => $faq->slug])}}"
                           class="py-1 align-items-center d-block">
                            <h5>{{--<span class="own-blue">{{(($loop->index + 1).' ) ')}}</span>--}}<span class="">{{$faq->translate()->title}}</span></h5>
                            <div class="col-12 pl-4 short-desc ff-ms">
                                {!! \Str::limit(strip_tags($faq->translate()->body, '<style>'), 200, '...') !!}
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
