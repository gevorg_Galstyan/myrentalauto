<?php

namespace App\Models;

use Gerardojbaez\Vehicle\Models\VehicleModel;

class CarModel extends VehicleModel
{
    protected $table = 'vehicle_models';
}
