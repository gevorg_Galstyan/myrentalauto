<?php


Route::get('/', 'DashboardController@index')->name('owner.dashboard');
Route::get('/cars/calendar/{car}', 'CarController@calendar')->name('cars.calendar');
Route::get('/customers/', 'CustomersController@index')->name('customers');
Route::get('/customers/{slug}', 'CustomersController@customer')->name('customer');
//Route::get('/orders', 'OrdersController@index')->name('orders');
Route::get('/orders/{slug}', 'OrdersController@transaction')->name('orders.transaction');
Route::get('/orders_info/{customer}', 'CustomersController@getCarOrdersInfo')->name('customer.ajax');

Route::get('order/{status}', 'OrdersController@index')->name('owner.orders.index');
Route::get('order/{status}/{barcode_number}', 'OrdersController@show')->name('owner.orders');
//Route::get('order/booking /{barcode_number}', 'OrdersController@getBooking ')->name('orders.booking');
//Route::get('order/canceled /{barcode_number}', 'OrdersController@getCanceled ')->name('orders.canceled ');

Route::match(['GET', 'POST'], '/request/{barcode_number}/{act}', 'OrdersController@setResponse')->name('orders.request.answer');

Route::get('/cars/deleted', 'CarController@deleted')->name('cars.deleted');
Route::get('/cars/restore/{id}', 'CarController@restore')->name('cars.restore');
Route::resource('cars', 'CarController');
Route::get('notify/{notify?}', 'OrdersController@readNotify')->name('owner.notify.read');


//owner profile routes
Route::group(['prefix' => 'profile'], function () {
    Route::get('/', 'ProfileController@edit')->name('owner.profile');
    Route::post('/update', 'ProfileController@update')->name('owner.profile.update');
    Route::post('/ajax-upload', 'ProfileController@ajaxUpload')->name('owner.profile.ajax');
});

//owner settings routes
Route::group(['prefix' => 'settings'], function () {
    Route::get('/', 'SettingsController@index')->name('owner.settings.index');
    Route::get('contract', 'SettingsController@contract')->name('owner.settings.contract');
    Route::get('working-times', 'SettingsController@workingTimes')->name('owner.settings.working_times');
    Route::post('working-times', 'SettingsController@workingTimes')->name('owner.settings.working_times.save');
    Route::get('security', 'SettingsController@security')->name('owner.settings.security');
    Route::put('change-password', 'SettingsController@changePassword')->name('owner.settings.security.change-password');

//    Route::post('/update', 'ProfileController@update')->name('owner.profile.update');
//    Route::post('/ajax-upload', 'ProfileController@ajaxUpload')->name('owner.profile.ajax');
});

// route posts
Route::resource('/owners-posts', 'PostController');

// TICKET ROUTE

Route::resource('tickets', 'TicketController');
Route::post('tickets/{ticketNumber}/comment', 'TicketController@createComment')->name('owner.tickets.comment');

