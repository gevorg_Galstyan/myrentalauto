<section class="slider">
    <div class="mt-3 w-100">
        <div class="d-flex p-0 mt-0 mt-sm-3 justify-content-center">
            <div class="slider-logo position-absolute">
               <div class="slider-logo "><span class="own-blue">My</span><span class="own-orange">Rent</span><span class="own-blue">Auto</span></div>
            </div>
            <div class="align-self-center home-title w-100">
                <h1 class="text-center">{{getMetaTags('home_page')->myTranslate()->h1}}</h1>
            </div>
        </div>

    </div>


{{--    <div class="row justify-content-center">--}}
{{--        <div class="  --}}{{--col-lg-6--}}{{-- text-center">--}}
{{--            <img src="{{asset('storage/img/slider-image-2.png')}}" alt="" class="img-fluid" width="50%">--}}
{{--            <!--<img src="img/cover2.png" alt="" class="img-fluid">-->--}}
{{--            <!--<img src="img/cover3.png" alt="" class="img-fluid">-->--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="d-none d-lg-block align-self-end flex-100">--}}
{{--        <div class="text-part col-lg-12 p-0">--}}
{{--            <div>--}}
{{--                <ul  class="w-100 row justify-content-center own-light-gray  textSlider"></ul>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <!--SEARCH-->--}}
{{--        @include('home.search')--}}
{{--    </div>--}}

</section>



    <!--SEARCH-->
    @include('home.search')


