@extends('layouts.app')

@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">
@endsection
@section('content')
    <div class="own-container">

        <div class="row bg-white shadow py-4 mb-4 mt-4 ">

            <div class="w-100 personal-infoHead ">

                <div class="d-flex flex-wrap justify-content-start pt-2 justify-content-around mb-3">
                    <div class="col-md-4 align-self-center">
                        <a href="{{route('profile')}}" class="btn own-btn ora   d-inline-block  border-radius-20"
                           title="Вернуться к поиску">
                            <i class="fas fa-arrow-left"></i>
                            <span class="">{{__('user.personal_area')}}</span>
                        </a>
                    </div>
                    <div class="align-self-center justify-content-center d-flex col-md-4 ">
                        <h1 class="px-3 edit-profile-title m-0 align-middle">
                            {{__('user.edit_profile')}}
                            @if (auth()->user()->provider)
                                <span class="d-inline-block soc-iconInfo m-0 ml-4 align-middle fs-15"
                                      data-toggle="tooltip" data-html="true"
                                      data-placement="top"
                                      title="{{tooltips($toolips, 'via_social_network', auth()->user()->provider)}}">

                                    <i class="fab {{auth()->user()->provider == 'vkontakte' ? 'fa-vk' : 'fa-facebook-f'}}"></i>
                                </span>
                            @endif
                        </h1>
                    </div>

                    <div class="text-center col-md-4 align-self-start">
                        <div class=" align-self-center align-middle">
                            <img src="{{Voyager::image(auth()->user()->avatar)}}" class="avatar rounded-circle"
                                 width="50px"
                                 height="50px" alt="avatar"
                                 id="avatarProfile">
                        </div>

                        <div class="unit mt-2 align-middle">
                            @if (auth()->user()->profile->status != 'for_consideration')
                                <input name="avatar"
                                       type="file"
                                       data-target="#avatarProfile"
                                       data-name="avatar"
                                       class=" d-none choose-avatar-photo ajaxUpload">
                                <button type="button"
                                        class="own-btn ora rounded-0 border-0 mt-2 m-auto px-3  click-to-ajaxUpload border-radius-20">
                                    {!! __('user.choose_avatar_photo') !!}
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
                <hr>

            </div>
            @if(auth()->user()->profile->status == 'for_consideration')

                <p class="text-center fw-700 w-100 text-info shadow-lg p-3 mb-5 bg-white rounded">
                    Отправлен на потверждение
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                          title="{{tooltips($toolips, 'profile_for_consideration')}}">
                            <i class="fas fa-info-circle"></i>
                        </span>
                </p>
        @endif

        {{--            <div class="col-md-12">--}}
        {{--                <div class="text-center">--}}
        {{--                    <img src="{{Voyager::image(auth()->user()->avatar)}}" class="avatar rounded-circle" width="100px"--}}
        {{--                         height="100px" alt="avatar"--}}
        {{--                         id="avatarProfile">--}}
        {{--                    <div class="unit mt-2">--}}
        {{--                        @if (auth()->user()->profile->status != 'for_consideration')--}}
        {{--                            <input name="avatar"--}}
        {{--                                   type="file"--}}
        {{--                                   data-target="#avatarProfile"--}}
        {{--                                   data-name="avatar"--}}
        {{--                                   class=" d-none choose-avatar-photo ajaxUpload">--}}
        {{--                            <button type="button"--}}
        {{--                                    class="own-btn ora rounded-0 border-0 mt-2 m-auto px-3  click-to-ajaxUpload">--}}
        {{--                                {{__('user.choose_avatar_photo')}}--}}
        {{--                            </button>--}}
        {{--                        @endif--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--                <hr>--}}
        {{--            </div>--}}

        <!-- edit form column -->
            <div class="col-md-12 personal-info ff-st d-flex flex-column">
                <form class="form-horizontal edit-profile" action="{{route('profile.update')}}" role="form"
                      id="editProfile"
                      method="post">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.first_name')}}*:
                                <span class="k-pointer d-inline-block" data-toggle="tooltip" data-html="true"
                                      data-placement="top"
                                      title="{{tooltips($toolips,  'real_data', __('user.first_name'))}}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </label>
                            <div class="col-lg-10">
                                <input class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                       name="first_name"
                                       type="text" value="{{auth()->user()->first_name}}"
                                       placeholder="{{__('user.first_name')}}"
                                    {{auth()->user()->profile->status  == 'for_consideration'  ? 'disabled' : ''}}>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.last_name')}}*:
                                <span class="k-pointer d-inline-block" data-toggle="tooltip" data-html="true"
                                      data-placement="top"
                                      title="{{tooltips($toolips,  'real_data', __('user.last_name'))}}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </label>
                            <div class="col-lg-10">
                                <input class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                       name="last_name"
                                       type="text" value="{{auth()->user()->last_name}}"
                                       placeholder="{{__('user.last_name')}}"
                                    {{auth()->user()->profile->status == 'for_consideration'  ? 'disabled' : ''}}>
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.patronymic')}}:</label>
                            <div class="col-lg-10">
                                <input class="form-control{{ $errors->has('patronymic') ? ' is-invalid' : '' }}"
                                       name="patronymic"
                                       type="text" value="{{auth()->user()->patronymic}}"
                                       placeholder="{{__('user.patronymic')}}"
                                    {{auth()->user()->profile->status == 'for_consideration'  ? 'disabled' : ''}}>
                                @if ($errors->has('patronymic'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('patronymic') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.email')}}*:
                                <span class="k-pointer d-inline-block" data-toggle="tooltip" data-html="true"
                                      data-placement="top"
                                      title="{{tooltips($toolips, (!auth()->user()->provider ? 'change_user_email': 'note access_change_param'))}}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </label>
                            <div class="col-lg-10">
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       type="text" value="{{auth()->user()->email ?? old('email')}}"
                                       placeholder="{{auth()->user()->email??'example@example.com'}}"
                                       name="{{auth()->user()->provider ? '' : 'email'}}"
                                    {{auth()->user()->profile->status == 'for_consideration' || auth()->user()->provider ? 'disabled' : ''}}>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.phone')}}*:</label>
                            <div class="col-lg-10">
                                <input id="phone" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}"
                                       name="phone"
                                       type="tel" value="{{auth()->user()->number??old('number')}}"
                                    {{--                                       placeholder="+123456789"--}}
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                <span id="error-msg" class="hide"></span>
                                @if ($errors->has('number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.birthday')}}*:</label>
                            <div class="col-lg-10">

                                <input class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                       name="birthday"
                                       type="date"
                                       id="bday"
                                       value="{{date('Y-m-d', strtotime(auth()->user()->birthday))}}"
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                @if ($errors->has('birthday'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.driving_experience')}}*:</label>
                            <div class="col-lg-10">
                                <select class="form-control{{ $errors->has('experience') ? ' is-invalid' : '' }}"
                                        name="experience"
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                    @php($ex = auth()->user()->profile->experience ?? null)
                                    @for ($i = date('Y'); $i >= date('Y')-50; $i--)
                                        <option value="{{ $i }}"
                                            {{($ex != null) && ($ex == $i) ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>

                                @if ($errors->has('experience'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.citizenship')}}*:</label>
                            <div class="col-lg-10">

                                <select
                                    class="form-control{{ $errors->has('citizenship') ? ' is-invalid' : '' }} select-withSearch"
                                    name="citizenship"
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                    @php($ct = isset(auth()->user()->profile->citizenship) ? auth()->user()->profile->citizenship : null)
                                    @foreach($countries as $value)
                                        <option
                                            value="{{$value['code']}}" {{($ct != null) && ($ct == $value['code']) ? 'selected' : '' }}>{{str_replace('Белоруссия','Беларусь',$value['name'])}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('citizenship'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('citizenship') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.country')}}*:</label>
                            <div class="col-lg-10">
                                <select
                                    class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }} select-withSearch"
                                    name="country" id="country"
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                    <option></option>
                                    @php($country = auth()->user()->profile->country ?? null)
                                    @foreach($countries as $value)
                                        <option
                                            value="{{$value['code']}}" {{$country  && ($country == $value['code'] || $country == $value['name']) ? 'selected' : '' }}>{{str_replace('Белоруссия','Беларусь',$value['name'])}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('country'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--@php($code = auth()->user()->profile->region ?? '')--}}

                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.region')}}*:</label>
                            <div class="col-lg-10">
                                <select
                                    class="form-control{{ $errors->has('region') ? ' is-invalid' : '' }} select-withSearch"
                                    name="region" id="region"
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                    @php($code = isset(auth()->user()->profile->region) ? auth()->user()->profile->region : null)
                                    @php($states = states($country))
                                    @if($states)
                                        @foreach($states as $value)
                                            <option
                                                value="{{$value['isoCode']}}" {{($code != null) && ($code == $value['isoCode']) ? 'selected' : '' }}>{{$value['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                @if ($errors->has('region'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('region') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="col-lg-10 control-label">{{__('user.city')}}*:</label>
                            <div class="col-lg-10">
                                @php($city = isset(auth()->user()->profile->city) ? auth()->user()->profile->city : null)
                                <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
                                       name="city"
                                       type="text" value="{{$city??old('city')}}"
                                       placeholder="{{__('user.city')}}"
                                    {{auth()->user()->profile->status == 'for_consideration' ? 'disabled' : ''}}>
                                @if ($errors->has('city'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label class="col-md-12 control-label"></label>
                            @if (auth()->user()->profile->status != 'for_consideration')
                                <div class="col-md-12 text-center">
                                    <input type="submit" class="own-btn ora rounded-0 border-0 m-auto fw-400 border-radius-20"
                                           value="{{__('user.save')}}">
                                </div>
                            @endif

                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center pasport-images-block">
            @include('user.profile.confirmation', ['edit' => false, 'profile' =>  auth()->user()->profile])
        </div>
        @if (!auth()->user()->provider)
            <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center ">
                <div class="w-100">
                    <div id="accordion" class="">

                        <div class="" id="headingOne">
                            <h4 class="mb-0 text-center fw-700 k-pointer" data-toggle="collapse"
                                data-target="#changePassword"
                                aria-expanded="true" aria-controls="collapseOne">
                                {{auth()->user()->password ? __('user.change_password') : __('user.add_password')}}

                            </h4>
                        </div>
                        <div id="changePassword" class="collapse cp-personal-info" aria-labelledby="headingOne"
                             data-parent="#accordion">
                            <div class="">
                                <form action="{{route('profile.password')}}" class="password_chagne " role="form"
                                      method="post">

                                    @csrf
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">{{__('user.currently_password')}}
                                            :</label>
                                        <div class="col-md-12">
                                            <input
                                                class="form-control{{ $errors->has('currently_password') ? ' is-invalid' : '' }}"
                                                name="currently_password" type="password" placeholder="********">
                                            @if ($errors->has('currently_password'))
                                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('currently_password') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">{{__('user.new_password')}}:</label>
                                        <div class="col-md-12">
                                            <input
                                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                name="password" type="password" placeholder="********">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">{{__('user.confirm_password')}}:</label>
                                        <div class="col-md-12">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" placeholder="********" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12 control-label"></label>
                                        <div class="col-md-12">
                                            <input type="submit" id="user_passowrdSave"
                                                   class="own-btn ora rounded-0 border-0 m-auto fw-400 border-radius-20"
                                                   value="{{__('user.save')}}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{--DELETE PROFILE--}}
        <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center ">
            <div class="w-100">
                <div id="delete-profile" class="">

                    <div class="" id="headingOne">
                        <h4 class="mb-0 text-center fw-700 k-pointer" data-toggle="collapse"
                            data-target="#delete-profile-collapse"
                            aria-expanded="true" aria-controls="collapseOne">
                            {{__('user.delete_profile')}}
                        </h4>
                    </div>
                    <div id="delete-profile-collapse" class="collapse cp-personal-info" aria-labelledby="headingOne"
                         data-parent="#delete-profile">
                        <div class="p-3">
                            <div id="delete-profile" class="ff-ms">

                                {!! tooltips($toolips, 'delete_profile') !!}
                            </div>
                            <form action="{{route('user.delete_profile', ['user' => auth()->user()])}}" method="POST"
                                  id="delete-form">
                                @csrf
                                @method('delete')
                                <div class="d-flex justify-content-center align-items-center">
                                    <button class="btn btn-danger border-radius-20" type="submit">
                                        {{__('general.delete')}}
                                    </button>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/intl-tel-input.js')}}"></script>
    <script>

        var input = document.querySelector("#phone");

        // initialise plugin
        var iti = window.intlTelInput(input, {
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get('https://ipinfo.io', function () {
                }, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            customPlaceholder: function(selectedCountryPlaceholder) {
                return selectedCountryPlaceholder.replace(/-/g, '');
            },
            separateDialCode : true,
            nationalMode: true,
            utilsScript: "/js/utils.js"
        });

        var reset = function () {
            input.classList.remove("has-error-inp");
        };

        // on blur: validate
        input.addEventListener('blur', function () {
            reset();
            if (input.value.trim()) {
                if (!iti.isValidNumber()) {
                    input.classList.add("has-error-inp");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);

        $(document).ready(function () {
            $('.select-withSearch').select2({
                placeholder: "Select a state"
            });

        });

        $(document).on('click', '.choose-avatar-photo-click', () => {
            $('.choose-avatar-photo').click()
        });
        $(document).on('click', '.click-to-ajaxUpload', function () {
            $(this).parents('.unit').find('.ajaxUpload').click();
        });

        $('#country').on('change', function () {
            var code = $(this).val();
            $.ajax({
                type: 'GET',
                url: '{{route('profile.states')}}/' + code,
                success: function (data) {
                    if (data.success) {
                        $('#region').html(data.view);
                    } else {
                        $('#region').html('');
                    }
                }
            });
        });

        var token = $('meta[name="csrf-token"]').attr('content');
        $(document).on('change', '.ajaxUpload', function (event) {
            event.preventDefault();
            var formData = new FormData;
            var img_id = $(this).data('target');
            var name = $(this).data('name');
            formData.append('img', $(this).prop('files')[0]);
            formData.append('type', name);
            formData.append('_token', token);
            $(img_id).parent().preloader();
            $.ajax({
                type: 'POST',
                url: '{{route('profile.ajax')}}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $(img_id).attr("src", data).parent().preloader('remove');
                }
            });
        });


        $(".edit-profile").submit(function (e) {
            e.preventDefault();
            updateProfile(this);
        });

        $(".password_chagne").submit(function (e) {
            e.preventDefault();
            changePassword(this);
        });

        function changePassword(form, send) {
            var t = $(form).attr("action"), n = $(form), i = new FormData(form);
            $('.cp-personal-info').preloader({
                setRelative: true

            });


            $.ajax({
                url: "{{route('profile.password')}}",
                type: "POST",
                dataType: "json",
                data: i,
                processData: !1,
                contentType: !1,
                beforeSend: function () {

                    $("body").css("cursor", "progress"), $(".has-error").removeClass("has-error"), $(".help-block").remove()
                },
                success: function (e) {

                    $("body").css("cursor", "auto");
                    $('.has-error-inp').removeClass('has-error-inp');
                    $('.cp-personal-info').preloader('remove');
                    myAlert(e.message, e.alert)
                },
                error: function (e, json) {
                    $(".cp-personal-info").preloader('remove');
                    $('.has-error-inp').removeClass('has-error-inp');
                    $("body").css("cursor", "auto");
                    var errors = e.responseJSON;
                    $.each(errors['errors'], function (t, n) {
                        myAlert(errors['errors'].password[0], 'warning')
                        var i = $("[name='" + t + "']"), r = i.first().parent().offset().top,
                            o = $("nav.navbar").height();
                        0 === Object.keys(errors['errors']).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 50 + "px"}, "fast"), i.addClass("has-error-inp")
                    });
                }
            })
        }

        $(document).on('click', '.edit-profile-document', function () {
            var block = $('.pasport-images-block');
            // block.preloader();
            $.ajax({
                url: '{{route('profile.posport_images', auth()->user()->profile)}}',
                success: function (data) {
                    block.html(data.html);
                    $('.edit-profile-document').addClass('activeBtn')
                    // $('[data-toggle="tooltip"]').tooltip();
                }
            });

        });
        $(document).on('click', '.send-document', function (e) {
            e.preventDefault();
            updateProfile($(".edit-profile")[0], this, true);
        });


        function sendToConfirm(send) {
            axios.get('{{route('profile.check_doc', auth()->user()->profile)}}')
                .then((response) => {
                    if (response.data.success) {
                        location.href = $(send).attr('href')
                    } else {
                        if (response.data.errors) {
                            $('.has-error-inp').removeClass('has-error-inp');
                            $.each(response.data.errors, function (t, n) {
                                var i = $("[name='" + t + "']"), r = i.first().parent().offset().top,
                                    o = $("nav.navbar").height();
                                if (i.attr('type') == 'file') i = i.parent('.unit').find('button');
                                0 === Object.keys(response.data.errors).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 70 + "px"}, "fast"), i.addClass("has-error-inp").find('.select2 ').addClass("has-error-inp");
                            })
                        }
                    }
                });
        }

        function updateProfile(form, send, terms) {
            var t = $(form).attr("action"), n = $(form), i = new FormData(form);
            reset();
            if (terms != undefined) {
                $('.terms').attr('required', true);
                i.append('terms', true);
            } else {
                $('.terms').attr('required', false);
            }
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    i.append('phone', iti.getNumber());
                    $.ajax({
                        url: t,
                        type: "POST",
                        dataType: "json",
                        data: i,
                        processData: !1,
                        contentType: !1,
                        beforeSend: function () {
                            $("body").css("cursor", "progress"), $(".has-error").removeClass("has-error"), $(".help-block").remove();
                            $('.personal-info').preloader();
                        },
                        success: function (e) {
                            $("body").css("cursor", "auto");
                            $(".personal-info").preloader('remove');
                            if (e.errors) {
                                $(".personal-info").preloader('remove');
                                $('.has-error-inp').removeClass('has-error-inp');
                                $.each(e.errors, function (t, n) {
                                    var i = $("[name='" + t + "']"), r = i.first().parent().offset().top,
                                        o = $("nav.navbar").height();
                                    0 === Object.keys(e.errors).indexOf(t) && $("html, body").animate({scrollTop: (r - o) - 100 + "px"}, "fast");
                                    if (t == 'terms_1' || t == 'terms_2') {
                                        i.parents('.terms-block').addClass("has-error-inp")
                                    } else {
                                        i.addClass("has-error-inp").parent().find('.select2 ').addClass("has-error-inp")
                                    }
                                })
                            } else {
                                if (send) {
                                    if (e.check_email == '') {
                                        sendToConfirm(send);
                                    } else {
                                        $('.personal-info').preloader('remove');
                                        myAlert(e.message + e.check_email, 'success')
                                    }
                                } else {
                                    $('.personal-info').preloader('remove');
                                    $('.has-error-inp').removeClass('has-error-inp');
                                    myAlert(e.message + e.check_email, 'success')
                                }

                            }

                        },
                    })
                } else {
                    input.classList.add("has-error-inp");
                }
            } else {
                input.classList.add("has-error-inp");
            }

        }

        $('#delete-form').submit(function (form) {
            form.preventDefault();
            Swal.fire({
                title: '{{__('user.delete.are_you_sure')}}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{__('user.delete.yes_delete')}}',
                cancelButtonText: '{{__('user.delete.cancel')}}',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url:  $('#delete-form').attr('action'),
                        type:  $('#delete-form').attr('method'),
                        data:  $('#delete-form').serialize(),
                        success:function (data) {
                            location.reload();
                        }
                    })
                }
            })
        })
    </script>
@endsection
