@extends('layouts.app')

@section('meta')
    <title>{{$item ? $item->translate()->title : ($activeCat ? $activeCat->translate()->meta_title : getMetaTags('faq_page')->translate()->title)  }}</title>
    <meta name="keywords"
          content="{{$activeCat ? $activeCat->translate()->meta_keywords : getMetaTags('faq_page')->translate()->Keyword}}"/>
    <meta name="description" content="{{
        $item ?Str::words(str_replace(["\n", "/\s/", "\xc2\xa0", "&nbsp;"],'', strip_tags($item->translate()->body) ), 40, '') :
        ($activeCat ? $activeCat->translate()->meta_description : getMetaTags('faq_page')->translate()->description)}}"/>

    <meta property="og:locale"
          content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title"
          content="{{$item ? $item->translate()->title : ($activeCat ? $activeCat->translate()->meta_title : getMetaTags('faq_page')->translate()->title)  }}"/>
    <meta property="og:description" content="{{
        $item ?Str::words(str_replace(["\n", "/\s/", "\xc2\xa0", "&nbsp;"],'', strip_tags($item->translate()->body) ), 40, '') :
        ($activeCat ? $activeCat->translate()->meta_description : getMetaTags('faq_page')->translate()->description)}}"/>
    <meta property="og:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name"
          content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site"
          content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title"
          content="{{$item ? $item->translate()->title : ($activeCat ? $activeCat->translate()->meta_title : getMetaTags('faq_page')->translate()->title)  }}">
    <meta name="twitter:description" content="{{
        $item ?Str::words(str_replace(["\n", "/\s/", "\xc2\xa0", "&nbsp;"],'', strip_tags($item->translate()->body) ), 40, '') :
        ($activeCat ? $activeCat->translate()->meta_description : getMetaTags('faq_page')->translate()->description)}}">
    <meta name="twitter:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}">
    {{--    <link href="{{ LaravelLocalization::getNonLocalizedURL(request()->url()).(request()->get('ex') ? '?ex='.request()->get('ex') : '') }}" rel="canonical">--}}

@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/faq.css')}}">
@stop
@section('content')
    <div class="faq-search-container">
        <div class="row">
            <div class="slider-logo ">
                <div class="slider-logo mt-2"><span class="own-blue">My</span><span class="own-orange">Rent</span><span
                        class="own-blue">Auto</span></div>
            </div>
        </div>
        <div class="my-container  d-flex flex-wrap justify-content-center mb-5 mt-md-0 mt-5 mt-sm-5 px-0">
            <div class="col-md-12 mb-md-0 mb-4">
{{--                {{dd($categories)}}--}}
                @if ($item || $items )
                    <div
                        class="text-center fw-600 fs-30 sub-heading text-uppercase m-0">{{getMetaTags('faq_page')->translate()->h1}}</div>
                @else
                    <h1
                        class="text-center fw-600 fs-30 sub-heading text-uppercase m-0">{{getMetaTags('faq_page')->translate()->h1}}</h1>
                @endif
            </div>
            <div class="col-md-6 align-self-center mt-4">
                <form class="input-group faq-search " action="{{{route('faq', ['search' => true])}}}">
                    <input type="text" class="form-control brd-color bw-2 py-4 border-radius-left-5 faq-search"
                           name="keyword"
                           placeholder="{!! __('faq.search_for_a_question_here') !!}" aria-label="find answer"
                           aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button
                            class="btn own-btn border-radius-0 py-2 px-3 border-radius-right-5 ora faq-search-button"
                            type="button"
                            id="button-addon2"><i class="fas fa-search fs-20"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="faq-content">
        @if ($categories)
            @include('faqs.faq')
        @else
            @include('faqs.content')
        @endif
    </div>


@endsection
@section('script')
    <script src="{{asset('js/faq.js')}}"></script>
    <script>
        $(document).on('click', '.faq-like', function (event) {
            event.preventDefault();
            var url = $(this).data('href');
            var id = $(this).data('target');
            var container = $('[data-status="' + id + '"]');
            container.preloader();
            axios.get(url).then((response) => {
                if (response.data.success) {
                    container.preloader('remove');
                    $(id).html('<p class="text-center">{!! __('faq.thanks_for_rating_this_will_hel_us_to_become_better') !!}</p>')
                }
            })
        });

        $('.faq-search').keyup(function () {
            faqSearch();
        });
        $('.faq-search-button').click(function () {
            faqSearch();
        });

        function faqSearch() {
            var form = $('.faq-search');
            var param = form.serialize();
            axios.get('?' + param).then((response) => {
                if (response.data.success) {
                    $('.faq-content').html(response.data.html);
                }
                if ($('[name="keyword"]').val()) {
                    $('.faq-cat').hide()
                } else {
                    $('.faq-cat').show()
                }
                if (($("#faq-general-tab").length > 0)) {
                    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
                    else $('a[href="#tenant"]').tab('show');
                }
            })
        }

        $(document).ready(function () {
            if (($("#faq-general-tab").length > 0)) {
                if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
                else $('a[href="#tenant"]').tab('show');
                $(document).on('show.bs.tab', '#faq-general-tab', function (e) {
                    location.href = e.target;
                });
            }
        });

    </script>
@stop
