<h3>Тарифы, услуги</h3>

<section>

    <ul class="list-group card-list-group">
        <li class="list-group-item py-2">
            <div class="row">
                <div class="form-group col-md-3  text-center">
                    <div class="form-label">
                        Цена за один день (BYN)
                            <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_one_day') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </div>
                    <input type="number" min="1" class="form-control" name="price_1" placeholder="Цена за один день (BYN)"
                           value="{{ old('price_1', $dataTypeContent->price_1??0) }}" required>
                </div>
                <div class="form-group col-md-3  text-center price-day">
                    <div class="form-label">Цена за 2 - 3 дней
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_percent') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </div>
                    <div class="input-group mb-3">

                        <input type="number" max="100" min="0" name="price_2_3" class="form-control price"
                               value="{{ $dataTypeContent->price_2_3 }}"
                               placeholder="%" aria-describedby="price_2_3" required>
                        <span class="input-group-append " id="price_2_3">
                            <span class="input-group-text disabled-price"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-3 text-center price-day">
                    <div class="form-label">Цена за 4 - 6 дней
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_percent') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </div>
                    <div class="input-group mb-3">

                        <input type="number" max="100" min="0" name="price_4_6" class="form-control price"
                               value="{{ $dataTypeContent->price_4_6 }}"
                               placeholder="%" aria-describedby="price_4_6" required>
                        <span class="input-group-append " id="price_4_6">
                            <span class="input-group-text disabled-price"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-3  text-center price-day">
                    <div class="form-label">Цена за 7 - 14 дней
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_percent') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </div>
                    <div class="input-group mb-3">

                        <input type="number" max="100" min="0" name="price_7_14" class="form-control price"
                               value="{{ $dataTypeContent->price_7_14 }}"
                               placeholder="%" aria-describedby="price_7_14" required>
                        <span class="input-group-append " id="price_7_14">
                            <span class="input-group-text disabled-price"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-3  text-center price-day">
                    <div class="form-label">Цена за 15 - 22 дней
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_percent') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </div>
                    <div class="input-group mb-4">
                        <input type="number" max="100" min="0" name="price_15_22" class="form-control price"
                               value="{{ $dataTypeContent->price_15_22 }}"
                               placeholder="%" aria-describedby="price_15_22" required>
                        <span class="input-group-append " id="price_15_22">
                            <span class="input-group-text disabled-price"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-4  text-center price-day">
                    <div class="form-label">Цена за 23 - 30 дней
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_percent') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </div>
                    <div class="input-group mb-4">
                        <input type="number" max="100" min="0" name="price_23_30" class="form-control price"
                               value="{{ $dataTypeContent->price_23_30 }}"
                               placeholder="%" aria-describedby="price_23_30" required>
                        <span class="input-group-append " id="price_23_30">
                            <span class="input-group-text disabled-price"></span>
                        </span>
                    </div>

                </div>
                <div class="form-group col-md-3  text-center price-day">
                    <div class="form-label">Цена за 31 дней и больше
                        <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'owner_help_car_price_percent') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </div>
                    <div class="input-group mb-3">

                        <input type="number" max="100" min="0" name="price_31" class="form-control price"
                               value="{{ $dataTypeContent->price_31 }}"
                               placeholder="%" aria-describedby="price_31" required>
                        <span class="input-group-append " id="price_31">
                            <span class="input-group-text disabled-price"></span>
                        </span>
                    </div>

                </div>
            </div>
        </li>
        <li class="list-group-item py-2">
            <div class="row">
                <div class="col-md-3 text-center" id="on_request">
                    <div class="form-group">
                        <div class="form-label">
                            По Запросу
                            <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                  title=" {{ tooltips($toolips, 'owner_help_on_request') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                        </div>
                        <div class="selectgroup ">
                            <label class="selectgroup-item">
                                <input type="checkbox" name="on_request" class="selectgroup-input">
                                <span class="selectgroup-button">По Запросу</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 text-center" id="deposit">
                    <div class="form-group">
                        <div class="form-label">
                            Депозит
                            <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                  title=" {{ tooltips($toolips, 'owner_help_deposit') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                        </div>
                        <div class="selectgroup ">
                            <label class="selectgroup-item">
                                <input type="checkbox" name="deposit" {{ $dataTypeContent->deposit ? 'checked' : ''}} class="selectgroup-input">
                                <span class="selectgroup-button">Депозит</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 text-center" id="deposit_type">
                    <div class="form-label">
                        Тип Депозита
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_deposit_type') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </div>
                    <div class="selectgroup w-100">
                        <label class="">
                            <input type="radio" name="deposit_type" value="bank" class="selectgroup-input"
                                   {{ $dataTypeContent->deposit_type == 'bank' ? 'checked' : '' }}>
                            <span class="selectgroup-button">По банку</span>
                        </label>
                        <label class="">
                            <input type="radio" name="deposit_type"
                                   {{ $dataTypeContent->deposit_type == 'in_cash' ? 'checked' : '' }}
                                   value="in_cash"
                                   class="selectgroup-input">
                            <span class="selectgroup-button">Наличными</span>
                        </label>
                    </div>
                </div>

                <div class="form-group col-md-3 text-center" id="deposit_price">
                    <div class="form-label">
                        Депозит Цена
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_price') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </div>
                    <input type="number" class="form-control" name="deposit_price" step="any"
                           placeholder="Депозит Цена" value="{{ $dataTypeContent->deposit_price    }}">
                </div>
            </div>
        </li>
        <li class="list-group-item py-2">
            {{-- green card --}}

            @include('owners.cars.steps.collapse.green_card')
           {{-- INSURANCE --}}

            @include('owners.cars.steps.collapse.insurance')

          {{-- DOSTAVKA --}}

            @include('owners.cars.steps.collapse.dostavka')

            {{-- ATTRIBUTE --}}

            @include('owners.cars.steps.collapse.attribute')




        </li>
    </ul>

</section>
