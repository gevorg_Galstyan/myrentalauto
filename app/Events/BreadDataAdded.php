<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use TCG\Voyager\Events\BreadDataChanged;
use TCG\Voyager\Models\DataType;

class BreadDataAdded implements ShouldBroadcast
{
    use SerializesModels;

    public $dataType;

    public $data;

    public $type;

    public function __construct(DataType $dataType, $data)
    {
        $this->dataType = $dataType;

        $this->data = $data;
        $this->type = 'Added';
        event(new BreadDataChanged($dataType, $data, 'Added'));
    }

    public function broadcastOn()
    {
        return new PrivateChannel('BreadDataAdded.' . auth()->id());
    }

}
