<?php
return [
    'services' => 'Services',
    'support' => 'Questions',
    'documentation' => 'Information',
    'profile' => 'Profile',
    'favorites' => 'Favorites',
    'for_whom' => 'For whom',
    'team' => 'Team',
    'blog' => 'Blog',
    'contacts' => 'Contacts',
    'personal_area' => 'Personal Area',
    'story' => 'Story',
    'posts' => 'Posts',
];
