<h3>Основная информация</h3>
<section>
    <ul class="list-group card-list-group">
        <li class="list-group-item py-5">
            <div class="row">
                <div class="col-lg-3">
                    <lable class="form-label">
                        Mарка *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_mark') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2-ajax-make select2" data-width="100%" name="car_make_id"
                            data-placeholder="Mарка" required>
                        @if($dataTypeContent->make)
                            <option value="{{$dataTypeContent->make->id}}">{{$dataTypeContent->make->name}}</option>
                        @else
                            <option></option>
                        @endif
                    </select>

                </div>
                <div class=" col-lg-3">
                    <lable class="form-label">
                        Модель *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_model') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2-ajax-model select2" data-width="100%" name="car_model_id"
                            data-placeholder="Модель" required>
                        @if($dataTypeContent->model)
                            <option value="{{$dataTypeContent->model->id}}">{{$dataTypeContent->model->name}}</option>
                        @else
                            <option></option>
                        @endif
                    </select>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Класс Автомобиля *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_class') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2" name="car_type_id" data-width="100%" data-placeholder="Класс Автомобиля" required>
                        <option></option>
                        @foreach($car_types as $type)
                            <option value="{{$type->id}}" {{$dataTypeContent->type &&$dataTypeContent->type->id == $type->id ? 'selected="selected"' : ''}}>{{$type->translate()->name}}</option>
                        @endforeach
                    </select>
                </div>
{{--                {{dd($dataTypeContent->bodyType)}}--}}
                <div class="col-lg-3">
                    <lable class="form-label">
                        Тип Кузова *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_body_type') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2" name="body_type_id" data-width="100%" data-placeholder="Тип Кузова " required>
                        <option></option>
                        @foreach($bodyTypes as $type)
                            <option value="{{$type->id}}" {{$dataTypeContent->bodyType && $dataTypeContent->bodyType->id == $type->id ? 'selected="selected"' : ''}}>{{$type->translate()->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </li>
        <li class="list-group-item py-5">
            <div class="row">
                <div class="col-lg-3">
                    <lable class="form-label">
                        Коробка передач *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_transmission') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2" name="transmission" data-width="100%" data-placeholder="Коробка передач" required>
                        <option></option>
                        @foreach($dataType->addRows->where('field', 'transmission')->first()->details->options  as $k => $item)
                            <option value="{{$k}}" @if($dataTypeContent->transmission === $k){{ 'selected="selected"' }}@endif>
                                {{\Lang::has('general.'.$k)? __('general.'.$k): $item}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Год выпуска *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_year') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="form-control select2" name="age" data-placeholder="Год выпуска" data-width="100%" required>
                        <option></option>
                        @php($years = array_combine(range(date("Y"), 1990), range(date("Y"), 1990)))
                        @foreach($years as $year)
                            <option value="{{ $year }}" {{  $dataTypeContent->age == $year ? 'selected="selected"' : '' }} >
                                {{\Lang::has('general.'.$year)? __('general.'.$year): $year}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Цвет *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_color') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2 colors" name="color_id" data-width="100%"
                            data-placeholder="Цвет" required>
                        <option></option>
                        @foreach($colors as $color)
                            <option value="{{$color->id}}"
                                    {{  $dataTypeContent->color_id == $color->id ? 'selected="selected"' : '' }} data-val="{{$color->color}}">
                                {{$color->translate()->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Топливо *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_fuel') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2" name="fuel" data-width="100%" data-placeholder="Топливо" required>
                        <option></option>
                        @foreach($dataType->addRows->where('field', 'fuel')->first()->details->options  as $k => $fuel)
                            <option value="{{$k}}" @if($dataTypeContent->fuel == $k){{ 'selected="selected"' }}@endif>{{__('general.'.strtolower($fuel))}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </li>
        <li class="list-group-item py-5">
            <div class="row">
                <div class="col-lg-3">
                    <lable class="form-label">
                        Ср. расход топлива *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_fuel_consumption') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <div class="form-group">
                        <input type="text" class="form-control fuel-consumption" name="fuel_consumption" step="any"
                               placeholder="Средний расход топливо (7.5)" value="{{ $dataTypeContent->fuel_consumption }}" required>
                    </div>
                </div>

                <div class="col-lg-3">
                    <lable class="form-label">
                        Объем двигателя *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_motor') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <div class="form-group">
                        <input type="text" class="form-control" name="motor" step="any"
                               data-mask="0.4" data-mask-clearifnotmatch="true"
                               placeholder="Объем Двигателя (2.4)" value="{{ $dataTypeContent->motor }}" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Количество сидений*
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_number_of_seats') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2" name="number_of_seats" data-width="100%"
                            data-placeholder="Количество Сидений" required>
                        <option></option>
                        @foreach($dataType->addRows->where('field', 'number_of_seats')->first()->details->options  as $k => $fuel)
                            <option value="{{$k}}" @if($dataTypeContent->number_of_seats == $k){{ 'selected="selected"' }}@endif>{{$fuel}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Количество дверей *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_number_of_doors') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <select class="select2" name="number_of_doors" data-width="100%"
                            data-placeholder="Количество дверей" required>
                        <option></option>
                        @foreach($dataType->addRows->where('field', 'number_of_doors')->first()->details->options  as $k => $fuel)
                            <option value="{{$k}}" @if($dataTypeContent->number_of_doors == $k){{ 'selected="selected"' }}@endif>{{$fuel}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Объем Багажника *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_trunk') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <div class="input-group">
                        <input type="text" class="form-control number-of-seats-trunk" name="number_of_seats_trunk"
                               placeholder="Объем Багажника" value="{{ $dataTypeContent->number_of_seats_trunk }}"
                               aria-label="Amount (to the nearest dollar)" required>
                        <span class="input-group-append">
                            <span class="input-group-text">Л</span>
                        </span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <lable class="form-label">
                        Рег. номер (гос.номер) *
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_registration_number') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <div class="form-group">
                        <input type="text" class="form-control serial-number" name="registration_number" step="any"
                               data-mask="0000 SS-0" data-mask-clearifnotmatch="true"
                               autocomplete="off"
                               placeholder="1234 АВ-7" value="{{ $dataTypeContent->registration_number }}" required>
                    </div>
                </div>
                <div class="col-lg-2">
                    <lable class="form-label">Лимит пробега</lable>
                    <div class="form-group">
                        <input type="text" class="form-control mileage-limit" name="mileage_limit" step="any"
                               autocomplete="off"
                               placeholder="Лимит пробега" value="{{ $dataTypeContent->mileage_limit }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <lable class="form-label">
                        За каждые ___ км.
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_mileage_limit_for_every') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <div class="form-group">
                        <input type="text" class="form-control mileage-limit-for-every" name="mileage_limit_for_every"
                                  placeholder="За каждые ___ км."  value="{{ $dataTypeContent->mileage_limit_for_every }}">
                    </div>
                </div>
                <div class="col-lg-2">
                    <lable class="form-label">
                        сверх лимита (BYN)
                        <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                              title=" {{ tooltips($toolips, 'mileage_limit_charged') }}">
                                <i class="fas fa-info-circle"></i>
                        </span>
                    </lable>
                    <div class="form-group">
                        <input type="number" class="form-control" name="mileage_limit_charged"
                                  placeholder="сверх лимита взимается"  value="{{ $dataTypeContent->mileage_limit_charged }}">
                    </div>
                </div>

                <div class="col-lg-12">
                    <lable class="form-label">
                        Дополнительная информация
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title=" {{ tooltips($toolips, 'owner_help_note') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                    </lable>
                    <div class="form-group">
                        <textarea class="form-control" name="note" placeholder="Дополнительная информация"
                                  rows="5">{{ $dataTypeContent->note }}</textarea>
                    </div>
                </div>
            </div>
        </li>
        <li class="list-group-item py-2"></li>
    </ul>
</section>
