<?php
return [
    /*
    |
    |
    | ROLE USER DASHBOARD
    |
    |
    */
    'dashboard' => [
        'admin' => 'voyager.dashboard',
        'moderator' => 'voyager.dashboard',
        'owner' => 'owner.dashboard',
        'user' => 'profile',
    ]

];
