<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index()
    {
        return view('owners.settings.index', ['view' => 'contract']);
    }

    public function contract()
    {
        $html = view('owners.settings.contract')->render();
        return response()->json(['success' => true, 'html' => $html]);
    }

    public function workingTimes(Request $request)
    {
        $profile = auth()->user()->profile;
        if ($request->method() == 'GET' && !$request->ajax()) {
            return view('owners.settings.index', [
                'view' => 'working-times',
                'working_times' => $profile->working_time
            ]);
        } elseif ($request->ajax()) {
            $profile->working_time = $request->data;
            $profile->save();
            return response()->json(['message' => 'Время работы обновлен ']);
        }
    }

    public function security()
    {
        return view('owners.settings.index', ['view' => 'security']);
    }


    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:8|confirmed',
        ], [], [
            'current_password' => 'Текущий пароль',
            'password' => 'Новый пароль'
        ]);
        if (!(Hash::check($request->get('current_password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with([
                "status" => "Ваш текущий пароль не совпадает с указанным вами паролем. Пожалуйста, попробуйте еще раз.",
                'alert_type' => 'error'
            ]);
        }
        if (strcmp($request->get('current_password'), $request->get('password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with([
                "status" => "Новый пароль не может совпадать с вашим текущим паролем. Пожалуйста, выберите другой пароль.",
                'alert_type' => 'error'
            ]);
        }

        //Change Password
        $user = auth()->user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back()->with("status", "успешно изменен пароль !");
    }
}
