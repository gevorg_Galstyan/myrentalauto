<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'passport_one',
        'passport_two',
        'license_one',
        'license_two',
        'experience',
        'citizenship',
        'country',
        'region',
        'city',
        'user_id',
        'status',
    ];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
