<?php

namespace App\Models;


use Gerardojbaez\Vehicle\Models\VehicleMake;

class CarMake extends VehicleMake
{
    protected $table = 'vehicle_makes';
}
