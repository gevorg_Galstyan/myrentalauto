@extends('layouts.app')

@section('meta')
    @php($meta = getMetaTags('blog'))
    <title>{{$meta->translate()->title}}</title>
    <meta name="keywords" content="{{$meta->translate()->Keyword}}"/>
    <meta name="description" content="{{$meta->translate()->description}}"/>

    <meta property="og:locale" content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$meta->translate()->title}}"/>
    <meta property="og:description" content="{{$meta->translate()->description}}"/>
    <meta property="og:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title" content="{{$meta->translate()->title}}">
    <meta name="twitter:description" content="{{$meta->translate()->description}}">
    <meta name="twitter:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}">
{{--    <link href="{{ LaravelLocalization::getNonLocalizedURL(route('posts')) }}" rel="canonical">--}}

@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
@stop



@section('content')
    <!-- Page Content -->
    <h1 class="fw-600 sub-heading text-uppercase text-center mt-3 post-title">{{$meta->translate()->h1}}</h1>
    <div class="posts">
        <div class="own-container bg-white border-radius-20 p-5">
            <div class="row">
                <!-- Blog Entries Column -->
                <div class="col-md-8 order-md-1 order-2">
                    <div class="post-content row" id="blog">
                        @include('posts.post-list')
                    </div>
                    <!-- Blog Post -->
                </div>
                <div class="col-md-4 order-1 order-md-2">
                    @include('posts.right-bar', ['tags' => $tags])
                </div>

            </div>
            <!-- /.container -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('click', '.post-paginate .pagination a', function (event) {
            event.preventDefault();
            var url = $(this).attr('href');
            var container = $('.post-content');
            container.html('').preloader();
            $.ajax({
                url: url,
                success: function (data) {
                    container.preloader('remove').html(data.html);
                }
            })

        });
        $('#search-form').submit(function (form) {
            form.preventDefault();
            if ($(this).find('[name="search"]').val()) {
                var url = $(this).attr('action');
                var container = $('.post-content');
                container.html('').preloader();
                $.ajax({
                    url: url,
                    data: $('#search-form').serialize(),
                    success: function (data) {
                        container.preloader('remove').html(data.html);
                    }
                })
            }
        });
        $('.clear-tag').click(function (e) {
            e.preventDefault();
            location.href = '{{route('posts')}}';
        })

    </script>
@endsection
