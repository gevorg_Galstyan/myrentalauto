<div class="bg-white shadow-lg  rounded p-2">
    <div class="single-options">
        @include('cars.characteristic')
    </div>
    <div class="single-options mt-3">
        <div id=myGroup>
            <ul class="nav nav-tabs character-tab-nav d-flex flex-no-wrap" id="myTab"
                role="tablist">
                <li class="nav-item flex-fill text-truncate w-50"
                    title="{{__('car.add_eq')}}">
                    <a class="nav-link text-truncate collap {{request()->input('collap') != 'insurance' ? 'active' :''}}"
                       data-col="attribute" id="attribute-tab" data-toggle="tab"
                       href="#attribute-cont"
                       role="tab" aria-controls="attribute-cont"
                       aria-selected="true">  {{__('car.add_eq')}}</a>
                </li>

                <li class="nav-item flex-fill" title="{{__('car.insurance')}}">
                    <a class="nav-link collap {{request()->input('collap') == 'insurance' ? 'active' :''}}"
                       id="insurance-tab" data-col="insurance" data-toggle="tab" href="#insurance-cont"
                       role="tab" aria-controls="insurance-cont"
                       aria-selected="false">{{__('car.insurance')}}</a>
                </li>
            </ul>
            <div class="tab-content character-tab-content" id="myTabContent">
                <div
                    class="tab-pane mobile-resp fade  rounded-bottom {{request()->input('collap') != 'insurance' ? 'show active' :''}}"
                    id="attribute-cont"
                    role="tabpanel"
                    aria-labelledby="attribute-tab">
                    {{--<h6 class="text-center fs-16 fw-700">{{__('car.add_eq')}}</h6>--}}
                    @foreach ($car->filters()->orderBy('car_filters.order', 'asc')->orderBy('order', 'asc')->get() as $filter)
                        @if ($filter->pivot->price <= 0) @continue @endif
                        <div
                            class="row m-0 mt-2 fs-13 fw-700 align-items-center justify-content-between mob-tooltip-box">
                            <div class="row m-0 p-0 col-lg-5 col-sm-9 col-10 align-items-center justify-content-start ">
                                <div class="col-1 px-0">
                                    <input type="checkbox" name="attributes[]"
                                           value="{{$filter->id}}" form="single-car-params"
                                           data-target="#attribute-{{$filter->id}}"
                                           class="car-attribute" {{in_array($filter->id,array_values( request()->input('attributes')??[])) ? 'checked' : ''}}/>
                                </div>
                                <div class="col-11 px-0">
{{--                                    {{dump($filter->translate(app()->getLocale(), 'ru')->title??'2')}}--}}
{{--                                    {{dump($filter->getTranslatedAttribute('title'))}}--}}
                                    {{$filter->translate()->title}}
                                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                          title=" {!! tooltips($toolips, ($filter->disposable ? 'attribute_price_disposable' : 'attribute_price_repeatedly'))    !!}">
                                        <i class="fas fa-info-circle"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-7 col-sm-3 col-2 text-right px-0">
                                <span class="px-2">
                                    <span class="ff-st">{!! ceil(getAttributePricePerPercent($car,$filter->pivot->price,request()->input('days')) ) !!} <span class="currency-symbol"></span></span>
                                </span>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div
                    class="tab-pane fade rounded-bottom mobile-resp {{request()->input('collap') == 'insurance' ? 'show active' :''}}"
                    id="insurance-cont"
                    role="tabpanel">
                    {{--<h6 class="text-center fs-16 fw-700">Страховка</h6>--}}
                    @foreach ($car->insurances()->orderBy('car_insurances.order', 'asc')->orderBy('order', 'asc')->get() as $insurances)
                        @if ($insurances->pivot->price <= 0) @continue @endif
                        <div
                            class="row m-0 mt-2 fs-13 fw-700 align-items-center justify-content-between mob-tooltip-box">
                            <div class="row m-0 p-0 col-lg-5 col-sm-9 col-10 align-items-center justify-content-start ">
                                <div class="col-1 px-0">
                                    <input type="checkbox" name="insurances[]"
                                           value="{{$insurances->id}}"
                                           form="single-car-params"
                                           data-target="#insurance-{{$insurances->id}}"
                                           {{in_array($insurances->id,array_values( request()->input('insurances')??[])) ? 'checked' : ''}}
                                           class="car-attribute"/>
                                </div>
                                <div class="col-11 px-0">
                                    <span>{{$insurances->translate()->name}}</span>
                                    <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                          data-placement="top"
                                          title=" {{str_replace(['%franchise_price%', '"'], [currency($insurances->pivot->franchise), "'"], $insurances->translate()->description)}}">
                                        <i class="fas fa-info-circle"></i>
                                    </span>

                                </div>
                            </div>
                            <div class="col-lg-7 col-sm-3 col-2 text-right px-0">
                                <span class="px-2"><span class="ff-st">{{currency($insurances->pivot->price, false, false)}} <span class="currency-symbol"></span></span></span>
                            </div>
                        </div>
                    @endforeach
                    {{--GREEN CARD--}}
                    @php($green_card = json_decode($car->green_card??'[]', true))
                    @if (count($green_card))

                        @foreach ($green_card as $key => $item)
                            @if ($item[getGreenCardDayKey(request()->input('days'))] <= 0) @continue  @endif
                            <div
                                class="row m-0 mt-2 fs-13 fw-700 align-items-center justify-content-between mob-tooltip-box">
                                <div
                                    class="row m-0 p-0 col-lg-5 col-sm-9 col-10 align-items-center justify-content-start csr-green-card-label k-pointer ">
                                    <div class="col-1 px-0">

                                        <input type="radio" name="green_card"
                                               value="{{$key}}" form="single-car-params"
                                               data-target="#green_card-{{$key}}"
                                               class="csr-green-card" {{request()->input('green_card') == $key ? 'checked' : ''}}/>

                                    </div>
                                    <div class="col-11 px-0">
                                        <span>{{__('car.green_card_'.$key)}}</span>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-sm-3 col-2 text-right px-0">
                                    <span
                                        class="px-2"><span class="ff-st">{!! currency($item[getGreenCardDayKey(request()->input('days'))], false, false) !!} <span class="currency-symbol"></span></span></span>
                                </div>
                            </div>
                        @endforeach

                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="single-options total mt-3">

        <div class="mx-3  fs-14 fw-700">
            @if (request()->input('green_card'))
                <div>
                    <hr>
                </div>

                <div class="total-block-info">
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                        <span class="col "> {{__('car.green_card_'.request()->input('green_card'))}}</span>

                        <span class="col text-right">
                            <span class="ff-st">{!!  currency($green_card[request()->input('green_card')][getGreenCardDayKey(request()->input('days'))], false, false) !!} <span class="currency-symbol"></span></span>
                                </span>

                    </div>

                </div>

            @endif
            <div class="total-block-info color-red">
                @if(request()->input('start_day')->format('H') < 9 || request()->input('start_day')->format('H') > 19)
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                Взять не рабочее время
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title=" {!! tooltips($toolips, 'ne_rabochie_vremya', currency(setting('site.during_off_hours'))) !!}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </span>
                        <span class="col text-right"><span class="ff-st">{!! currency(setting('site.during_off_hours'), false, false) !!} <span class="currency-symbol"></span></span></span>
                    </div>
                    <input type="hidden" name="other_details[take_no_work_time]" value="true" form="single-car-params">
                @endif
                @if(request()->input('end_day')->format('H') < 9 || request()->input('end_day')->format('H') > 19)
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                Вернуть в не рабочее время
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title=" {!! tooltips($toolips, 'ne_rabochie_vremya', currency(setting('site.during_off_hours'))) !!}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </span>
                        <span class="col text-right"><span class="ff-st">{!! currency(setting('site.during_off_hours'),false, false) !!} <span class="currency-symbol"></span></span></span>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <input type="hidden" name="other_details[return_in_non_working_time]" value="true" form="single-car-params">
                @endif


                @if(request()->input('took'))
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                               Взять {{$car->deliveries()->find(request()->input('took'))->location}}
                            </span>
                        <span class="col text-right">
                            <span class="ff-st">{!! currency($car->deliveries()->find(request()->input('took'))->price, false, false) !!} <span class="currency-symbol"></span></span>
                            </span>
                    </div>
                @endif
                @if(request()->input('give'))
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                Вернуть {{$car->deliveries()->find(request()->input('give'))->location}}
                            </span>
                        <span class="col text-right">
                            <span class="ff-st"> {!! currency($car->deliveries()->find(request()->input('give'))->price, false, false) !!} <span class="currency-symbol"></span></span>
                            </span>
                    </div>
                @endif

                <div>
                    <hr>
                </div>
                @foreach($car->filters as $item)
                    @if(!in_array($item->id,array_values( request()->input('attributes')??[]))) @continue @endif
                    <div id="attribute-{{$item->id}}">
                        <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col ">{{$item->translate()->title}}</span>
                            @if(!$item->disposable)
                                <span class="col ff-st text-center">
                                        {{request()->input('days')}}
                                    Дней х {!! ceil(getAttributePricePerPercent($car,$item->pivot->price,request()->input('days')) ) !!} <span class="currency-symbol"></span>
                                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                          title=" {!! tooltips($toolips,  'attribute_price_repeatedly')   !!}">
                                            <i class="fas fa-info-circle"></i>
                                    </span>
                                    </span>
                                <span class="col text-right">
                                        <span class="ff-st">{!! ceil (getAttributePricePerPercent($car,$item->pivot->price,request()->input('days')))* request()->input('days') !!}
                                           <span class="currency-symbol"></span></span>
                                    </span>
                            @else
                                <span class="col text-center"><span class="ff-st">{!! currency($item->pivot->price, false, false) !!} <span class="currency-symbol"></span></span></span>
                                <span class="col text-right"><span class="ff-st">{!! currency($item->pivot->price, false, false) !!} <span class="currency-symbol"></span></span></span>
                            @endif
                        </div>
                        <div>
                            <hr>
                        </div>
                    </div>
                @endforeach

                @foreach($car->insurances as $insurance)

                    @if(!in_array($insurance->id,array_values( request()->input('insurances')??[]))) @continue @endif

                    <div id="attribute-{{$insurance->id}}">
                        <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col ">{{$insurance->translate()->name}}</span>
                            <span class="col text-center">
                                {{request()->input('days')}}
                                Дней х <span class="ff-st">{{currency($insurance->pivot->price, false, false)}} <span class="currency-symbol"></span></span>
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title=" {!! tooltips($toolips, 'attribute_price_repeatedly')  !!}">
                                        <i class="fas fa-info-circle"></i>
                                    </span>
                                </span>
                            <span class="col text-right">
                                    <span class="ff-st">{{ currency($insurance->pivot->price, false, false) * request()->input('days') }}
                                        <span class="currency-symbol"></span></span>
                                </span>
                        </div>
                        <div>
                            <hr>
                        </div>
                    </div>
                @endforeach
                @if(strtotime(request()->input('start_day')) < strtotime(\Carbon\Carbon::now()->addDay(3)))
                    <div class="row m-0 mt-2 align-items-center justify-content-between">
                            <span class="col">
                                {{__('car.urgency')}}
                                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                                      title="{!! tooltips($toolips, 'urgency') !!}">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </span>
                        <span class="col text-right"><span class="ff-st">{!! currency(setting('site.urgency'), false, false) !!} <span class="currency-symbol"></span></span></span>

                    </div>
                    <div>
                        <hr>
                    </div>
                    <input type="hidden" name="other_details[urgency]" value="true" form="single-car-params">
                @endif
            </div>
            <div class="row m-0 mt-2 align-items-center justify-content-between">
                <span class="col">{{__('car.rent')}}</span>
                <span class="col text-center">
            {{request()->input('days')}} {{__('car.days')}}
                    х <span class="ff-st">{!! currency(getCarPrice($car, request()->input('days')), false, false) !!} <span class="currency-symbol"></span></span></span>
                <span class="col text-right">
           <span class="ff-st"> {!! request()->input('days') * currency( getCarPrice($car, request()->input('days')),'',false) !!}
               <span class="currency-symbol"></span></span>
        </span>
            </div>
            <hr/>

            <div class="row m-0 mt-2 fs-14 fw-700 align-items-center justify-content-end">
                    <span class="align-items-center px-2">
                        <span>{{__('car.deposit')}}</span>
                        @if ($car->deposit)
                            <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                  title=' {{tooltips($toolips, ($car->deposit_type == 'bank' ? 'deposit_in_bank': 'deposit_in_cash'), currency($car->deposit_price,null,true) ) }}'>
                            <i class="fas fa-info-circle"></i>
                        </span>
                        @endif
                        <span> : </span>
                        <span class="px-2 price  fs-16">{!! $car->deposit ? '<span class="ff-st">'.currency($car->deposit_price,false,false).'<span class="currency-symbol"></span></span>' : __('general.no') !!}</span>
                    </span>
            </div>

            <div class="row m-0 mt-2 fs-16 fw-700 align-items-center justify-content-end">
                    <span class=" shadow   bg-white rounded align-items-center">
                         <span class="px-2 ">{{__('car.total')}}</span>
                            :
                        <span class="px-2 price  fs-22 ff-st">{!! carTotalPrice($car, request()->input('days'), (request()->input('attributes')??[]), (request()->input('green_card')??[]), (request()->input('insurances')??[]))['total'] !!} <span class="currency-symbol"></span></span>
                    </span>
            </div>

            <div class="row m-0 mt-2 fs-16 fw-700 align-items-center justify-content-end">
                    <span class="shadow bg-white rounded">
                        <span class="px-2">{{__('car.total_pay_now')}}</span>
                            :
{{--                        @if ($car->deposit && $car->deposit_type == 'bank')--}}
                        {{--                            <span class="px-2 price  fs-22"--}}
                        {{--                                  id="total">{!! currency($car->deposit_price,null,true) !!}</span>--}}
                        {{--                            <span> + </span>--}}
                        {{--                        @endif--}}
                        <span class="px-2 color-orange  fs-22 ff-st">
                            {!! ceil(carTotalPrice($car, request()->input('days'), (request()->input('attributes')??[]), (request()->input('green_card')), (request()->input('insurances')??[]))['to_pay']) !!}
                            <span class="currency-symbol"></span>
                        </span>
                    </span>
            </div>
            <hr/>
            @if(!isset($preview))
                @include('cars.order')
            @endif

        </div>


    </div>
</div>

