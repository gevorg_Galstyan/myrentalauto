<?php

function currency($amount = false, $to = false, $symbol = true)
{
    if (!$to) {
        if (session('currency')) {
            $to = session('currency');
        } else {
            $to = config('currency.default');
        }
    }

//    $currency =  \Cache::remember('currencies', setting('site.cache_time'), function () use ($to) {
//        return \App\Models\Currency::where('Cur_Abbreviation', $to)->get();
//    });
    $currency = \App\Models\Currency::where('Cur_Abbreviation', $to)->get();
    $currency = $currency->first();
//    dd($currency);
    $value = $currency->Cur_OfficialRate / $currency->Cur_Scale;
    $total = ceil(round(round(($amount / $value) / 10, 2) * 10, 2));
//dump($amount);
//dd($total);
//        return $currency->symbol.$total.'('.$to.')';
    if ($symbol) {
//        return $total . ' <span class="fs-14">' . currency_symbol(null,'symbol').'</span>';
        return $total . ' ' . currency_symbol(null, 'symbol')/*.'</span>'*/
            ;
//        if ($currency->float == 'LEFT') return $currency->symbol . ' ' . $total;
//        if ($currency->float == 'RIGHT') return $total . ' ' . $currency->symbol . '.';
    } else {
        return $total;
    }
}


function getBackCurrency($amount = null, $to = null)
{
    if (!$to) {
        if (session('currency')) {
            $to = session('currency');
        } else {
            $to = config('currency.default');
        }
    }
    $currency = \App\Models\Currency::where('Cur_Abbreviation', $to)->first();
    $value = $currency->Cur_OfficialRate / $currency->Cur_Scale;
    $total = ceil(round(round(($amount * $value) / 10) * 10, 2));

    return $total;
}


function currency_symbol($currency = null, $symbol = 'symbol')
{
    return config('currency.currencies.' . ($currency ?? (session('currency'))??'BYN' ) . '.' . $symbol);
}


function getCarPercent($car, $days)
{
    $percent = $car->price_1;
    if ($days == 1) {
        $percent = $car->price_1;
    } elseif ($days >= 2 && $days <= 3) {
        $percent = $car->price_2_3;
    } elseif ($days >= 4 && $days <= 6) {
        $percent = $car->price_4_6;
    } elseif ($days >= 7 && $days <= 14) {
        $percent = $car->price_7_14;
    } elseif ($days >= 15 && $days <= 22) {
        $percent = $car->price_15_22;
    } elseif ($days >= 23 && $days <= 30) {
        $percent = $car->price_23_30;
    } elseif ($days >= 31) {
        $percent = $car->price_31;
    }

    return $percent;
}

function getCarPrice($car, $days)
{
    $price = $car->price_1;
    if ($days == 1) {
        $price = $car->price_1;
    } elseif ($days >= 2 && $days <= 3) {
        $price = carPriceInPercent($car, $car->price_2_3);
    } elseif ($days >= 4 && $days <= 6) {
        $price = carPriceInPercent($car, $car->price_4_6);
    } elseif ($days >= 7 && $days <= 14) {
        $price = carPriceInPercent($car, $car->price_7_14);
    } elseif ($days >= 15 && $days <= 22) {
        $price = carPriceInPercent($car, $car->price_15_22);
    } elseif ($days >= 23 && $days <= 30) {
        $price = carPriceInPercent($car, $car->price_23_30);
    } elseif ($days >= 31) {
        $price = carPriceInPercent($car, $car->price_31);
    }

    return $price;
}

function getAttributePricePerPercent($car, $price, $days, $to = null)
{
    if (!$to) {
        if (session('currency')) {
            $to = session('currency');
        } else {
            $to = config('currency.default');
        }
    }
    if ($days > 1) {
        $percent = getCarPercent($car, $days);
        return pricePercent(currency($price, $to, false), $percent);
    } else {
        return $price;
    }
}

function carPriceInPercent($car, $percent)
{
    $price = $car->price_1;
    return pricePercent($price, $percent);
}


function pricePercent($price, $percent)
{
    return $price - ($price * ($percent / 100));
}


function carTotalPrice($car, $days, array $attributes, $greenCard, array $insurances, $to = null)
{
    if (!$to) {
        if (session('currency')) {
            $to = session('currency');
        } else {
            $to = config('currency.default');
        }
    }
    $carGreenCard = json_decode($car->green_card ?? '[]', true);
    $price = getCarPrice($car, $days);

    $total = currency($price, $to, false) * $days;

    $filters = $car->filters()->whereIn('filters.id', $attributes)->get();
    $insurances = $car->insurances()->whereIn('insurances.id', $insurances)->get();


    if (count($carGreenCard) && $greenCard) {
        $total += currency(($carGreenCard[$greenCard][getGreenCardDayKey($days)]), $to, false);
    }


    foreach ($insurances as $insurance) {

        $total += (currency($insurance->pivot->price, $to, false) * $days);

    }
    foreach ($filters as $filter) {
        if ($filter->disposable) {

            $total += $filter->pivot->price;
        } else {
            $total += (ceil(getAttributePricePerPercent($car, $filter->pivot->price, $days, $to)) * $days);
        }
    }

    if (strtotime(request()->input('start_day')) < strtotime(\Carbon\Carbon::now()->addDay(3))) {
        $total += setting('site.urgency');
    }

    if (request()->input('start_day')->format('H') < 9 || request()->input('start_day')->format('H') > 19) $total += setting('site.during_off_hours');

    if (request()->input('end_day')->format('H') < 9 || request()->input('end_day')->format('H') > 19) $total += setting('site.during_off_hours');
    if (request()->input('took')) $total += $car->deliveries()->find(request()->input('took'))->price;
    if (request()->input('give')) $total += $car->deliveries()->find(request()->input('give'))->price;
    if (isset($car->percent_to_total) && ($car->percent_to_total > 0)) {
        $totalPercent = $car->percent_to_total;
    } elseif (isset($car->owner->percent_to_total)) {
        $totalPercent = $car->owner->percent_to_total;
    } else {
        $totalPercent = 0;
    }

    $to_pay = $total * ($totalPercent / 100);
    return ['total' => $total, 'to_pay' => $to_pay];
}


function getCountryName($code)
{
    if ($code) {
        try {
            $country = \MenaraSolutions\Geographer\Country::build($code);
        } catch (\Exception $exception) {
            $data = \YaGeo::setQuery($code)->load();
            if (isset($data->getResponse()->getData()['CountryNameCode'])) {
                $country = \MenaraSolutions\Geographer\Country::build($data->getResponse()->getData()['CountryNameCode']);
            }
        }
        return $country->setLocale(\Illuminate\Support\Facades\App::getLocale())->getLongName();
    }
    return $code;
}


