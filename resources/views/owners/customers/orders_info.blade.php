<div class="card">
    <div class="card-body">
        <h3 class="card-title">Ареды {{$customer->full_name??''}}  на {{$car->title}} </h3>


        <div class="table-responsive">
            <table class="table card-table table-vcenter text-nowrap datatable" id="ownerTables">
                <thead>
                <tr>
                    {{--<th class="w-1">Id</th>--}}
{{--                    <th>Арендатор</th>--}}
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Валюта</th>
                    <th>Цена за один день</th>
                    <th>Цена оплаты</th>
                    <th>Заметка</th>
                    <th>Окончательная цена</th>
                    <th>Просмотр</th>
                </tr>
                </thead>
                <tbody>

                @foreach($orders as $order)
                    <tr>
                        <td>
                            {{$order->start_date??''}}
                            -
                            {{$order->end_date??''}}
                        </td>
                        <td>
                            {{$order->status??''}}
                        </td>
                        <td>
                            {{$order->currency??''}}
                        </td>
                        <td>
                            {{$order->price_per_day??''}}
                        </td>
                        <td>
                            {{$order->price_to_pay??''}}
                        </td>
                        <td class="mw-50">
                            {{$order->note??''}}
                        </td>
                        <td>
                            {{$order->total_price??''}}
                        </td>
                        <td>
{{--                            <a href="{{route('orders.transaction', $order->transaction_id)}}" class="icon text-center"><i class="fa fa-eye"></i></a>--}}
                            <a href="" class="icon text-center"><i class="fa fa-eye"></i></a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

