@extends('owners.layouts.app')


@section('content')
    <style>
        .btn-file {
             text-align: center;
             width: auto;
             display: inline-block;
             min-width: auto;
             margin: 15px 0;
        }
    </style>
    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card p-5">
                <form role="form" action="{{ route('tickets.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="d-flex">


                        <div class="col-md-8 align-self-center">
                            <div class="form-group">
                                <label>Введите тему для вашего билета</label>
                                <input type="text" name="title"
                                       class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" max="255"
                                       value="{{ old('title') }}"
                                       placeholder="Введите тему для вашего билета">
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 align-self-center">
                            <div class="form-group">
                                <label>Приоритет</label>
                                <select class="form-control {{ $errors->has('priority_id') ? ' is-invalid' : '' }}"
                                        name="priority_id">
                                    <option value="" disabled selected>Выберите приоритетне</option>
                                    @forelse($priorities as $priority)
                                        <option
                                            value="{{$priority->id}}" {{$priority->id == old('priority_id') ? 'selected' : ''}}>
                                            {{ $priority->title }}
                                        </option>
                                    @empty
                                    @endforelse
                                </select>
                                @if ($errors->has('priority_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('priority_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="form-group col-12">
                            <label>Описание</label>
                            <textarea name="text" class="form-control {{ $errors->has('text') ? ' is-invalid' : '' }}"
                                      rows="6" placeholder="Описание"
                                      id="editor">{!! old('text') !!}</textarea>
                            @if ($errors->has('text'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('text') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="d-flex align-items-start">
                        <div class="col">
                            <label for="input-24">Прикрепите файл</label>
                            <input id="input-24" name="images[]" type="file" multiple>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="col-12 align-self-center text-center">
                            <button class="btn btn-success pull-right" type="submit">Отправить билет</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $("#input-24").fileinput({
            language: "ru",
            theme: "fas",
            quality: 0.75,
            showUpload: false,
            maxFileSize: 5120,
            dropZoneEnabled: false,
            showCaption: false,
            showCancel: false,
            showRemove: false,
            // layoutTemplates: {footer: ''},
        });

    </script>
@stop

