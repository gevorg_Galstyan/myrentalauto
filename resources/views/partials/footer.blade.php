<footer>
    <div class="own-container">
        <div class="row justify-content-center">
            <div class="col-md-3 footer-about">
                <h4 class="mb-3 text-uppercase">{{__('footer.about_us')}}</h4>
                <div class="m-0 fs-13 footer-about-services ff-ms">
                    {!! tooltips($toolips, 'about_services') !!}
                </div>
            </div>
            <div class="col-md-3  col-6  d-flex align-items-center flex-column footer-about">
                <h4 class="mb-3 text-uppercase">{{__('footer.start')}}</h4>
                <ul class="footer-nav">
                    <li><a href="{{route('pages', ['slug' => 'o-service'])}}">{{__('footer.services')}}</a></li>
                    <li><a href="{{route('search')}}">{{__('footer.cars')}}</a></li>
                    <li><a href="{{route('faq')}}">{{__('footer.faq')}}</a></li>
                    <li><a href="{{route('posts')}}">{{__('footer.blog')}}</a></li>
                    <li><a href="{{route('contact_us')}}">{{__('footer.contacts')}}</a></li>
                </ul>
            </div>
            <div class="col-md-3  col-6 d-flex align-items-center flex-column footer-about">
                <h4 class="mb-3 text-uppercase">{{__('footer.important')}}</h4>
                <ul class="footer-nav text-right text-sm-left  ">
                    <li><a href="{{route('pages.for_whom_owner')}}">{{__('footer.for_car_owners')}}</a></li>
                    <li><a href="{{route('pages.for_tenants')}}">{{__('footer.for_tenants')}}</a></li>
                    <li><a href="{{route('pages.insurance')}}">{{__('footer.insurance')}}</a></li>
                    <li><a href="{{route('pages', ['slug' => 'lease-contract'])}}">{{__('footer.lease_contract')}}</a>
                    </li>
                    <li><a href="{{route('pages', ['slug' => 'conditions'])}}">{{__('footer.conditions')}}</a></li>
                </ul>
            </div>

            <div class="col-md-3  col-12 d-flex align-items-center flex-column px-0">
                <h4 class="mb-3 text-uppercase">{{__('footer.currency')}}</h4>
                <ul class="p-0">
                    <li class="nav-item dropdown list-group ff-st">
                        <a class="nav-link dropdown-toggle color-white" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{session('currency')??(request()->cookie('currency')??'BYN')}}
                        </a>
                        <ul class="dropdown-menu currency-footer" aria-labelledby="navbarDropdown">
                            @foreach(\Arr::except(config('currency.currencies'), session('currency')) as $currency)
                                <li class="dropdown-item currency-item k-pointer"
                                    data-href="{{route('currency.change', ['currency' => $currency['iso_code']])}}">{{$currency['iso_code']}}</li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                <div class="d-flex flex-wrap justify-content-center col mb-5 mt-2 mb-sm-0 ">
                    <div class="align-self-center text-center col px-0">
                        <img src="{{ownMakeImage(70, 0 , 0, 'img/icons/MC_SC.png', 'png')}}" alt="MC_SC">
                    </div>
                    <div class="align-self-center text-center col px-0">
                        <img src="{{ownMakeImage(70, 0 , 0, 'img/icons/MC_Visa.png', 'png')}}" alt="MC_Visa">
                    </div>
                    <div class="align-self-center text-center col px-0">
                        <img src="{{ownMakeImage(70, 0 , 0, 'img/icons/VbV.png', 'png')}}" alt="VbV">
                    </div>
                </div>
                <div class="d-flex justify-content-center col mt-4">
                    <div class=" px-1 align-self-end text-center">
                        <a href="https://www.facebook.com/MyRentAuto/" target="_blank" title="Facebook" rel="noopener">
                            <img src="{{ownMakeImage(35, 0 , 75, 'img/icons/facebook-icon.png', 'png')}}"
                                 alt="Facebook">
                        </a>
                    </div>
                    <div class=" px-1 align-self-end text-center">
                        <a href="https://vk.com/myrentauto" target="_blank" title="Vkontakte">
                            <img src="{{ownMakeImage(35, 0 , 75, 'img/icons/vk-icon.png', 'png')}}" alt="Vkontakte"
                                 rel="noopener">
                        </a>
                    </div>
                    <div class=" px-1 align-self-end text-center">
                        <a href="https://www.instagram.com/myrentautocom/" target="_blank" title="Instagram"
                           rel="noopener">
                            <img src="{{ownMakeImage(35, 0 , 75, 'img/icons/instagram-icon.png', 'png')}}"
                                 alt="Instagram">
                        </a>
                    </div>
                    <div class=" px-1 align-self-end text-center">
                        <a href="https://telegram.me/MyRentAuto" target="_blank" title="Telegram" rel="noopener">
                            <img src="{{ownMakeImage(35, 0 , 75, 'img/icons/telegram-icon.png', 'png')}}"
                                 alt="Telegram">
                        </a>
                    </div>
                    <div class=" px-1 align-self-end text-center">
                        <a href="https://twitter.com/myrentauto" target="_blank" title="Twitter" rel="noopener">
                            <img src="{{ownMakeImage(35, 0 , 75, 'img/icons/twitter-icon.png', 'png')}}" alt="Twitter">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {{--    <hr class="my-2"/>--}}
    {{--    <div class="own-container pb-2">--}}
    {{--        <div class="row justify-content-center align-items-center">--}}
    {{--            <div class="pl-3">--}}
    {{--                <a href="https://www.facebook.com/MyRentAuto/" target="_blank">--}}
    {{--                    <img src="{{asset('storage/img/icons/facebook-icon.png')}}" width="35" alt="">--}}
    {{--                </a>--}}
    {{--            </div>--}}
    {{--            <div class="pl-3">--}}
    {{--                <a href="https://www.instagram.com/myrentautocom/" target="_blank">--}}
    {{--                    <img src="{{asset('storage/img/icons/insta-icon.png')}}" width="35" alt="">--}}
    {{--                </a>--}}
    {{--            </div>--}}
    {{--            <div class="pl-3">--}}
    {{--                <a href="">--}}
    {{--                    <img src="{{asset('storage/img/icons/youtube-icon.png')}}" width="35" alt="">--}}
    {{--                </a>--}}
    {{--            </div>--}}
    {{--            <div class="pl-3">--}}
    {{--                <a href="">--}}
    {{--                    <img src="{{asset('storage/img/icons/gmail-icon.png')}}" width="35" alt="">--}}
    {{--                </a>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    <hr class="mt-2 mb-1"/>
    <div>
        <div class="own-container d-flex justify-content-center align-items-center px-5">
            <p class="ff-ms fs-12 text-white m-0 text-center px-xl-0 px-lg-5 ip-footer">
                {{__('footer.ip')}}
            </p>
        </div>
    </div>
</footer>
