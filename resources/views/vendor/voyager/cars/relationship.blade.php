@if(isset($options->model) && isset($options->type))

    @if(class_exists($options->model))

        @php $relationshipField = $row->field; @endphp

        @if($options->type == 'belongsTo')

            @if(isset($view) && ($view == 'browse' || $view == 'read'))

                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
                    $model = app($options->model);
                    $query = $model::where($options->key,$relationshipData->{$options->column})->first();
                @endphp

                @if(isset($query))
                    <p>{{ $query->{$options->label} }}</p>
                @else
                    <p>{{ __('voyager::generic.no_results') }}</p>
                @endif

            @else
                <select
                    class="form-control select2-ajax{{$options->model == "App\Models\CarModel" ? '-model':''}}"
                    name="{{ $options->column }}"
                    data-get-items-route="{{route('voyager.' . $dataType->slug.'.relation')}}"
                    data-get-items-field="{{$row->field}}"
                >
                    @php
                        $model = app($options->model);
                        $query = $model::where($options->key, $dataTypeContent->{$options->column})->get();
                    @endphp

                    @if(!$row->required)
                        <option value="">{{__('voyager::generic.none')}}</option>
                    @endif
                    @if ($options->model == "App\\User")
                        <?php $query = $query->where('role_id', 3) ?>
                    @endif
                    @foreach($query as $relationshipData)
                        <option
                            value="{{ $relationshipData->{$options->key} }}" @if($dataTypeContent->{$options->column} == $relationshipData->{$options->key}){{ 'selected="selected"' }}@endif>
                            {!! $relationshipData->{$options->label} !!}
                            <span style='display:inline-block;width: 20px; height: 20px; background: #000000'></span>
                        </option>
                    @endforeach
                </select>

            @endif

        @elseif($options->type == 'hasOne')

            @php

                $relationshipData = (isset($data)) ? $data : $dataTypeContent;

                $model = app($options->model);
                $query = $model::where($options->column, '=', $relationshipData->id)->first();

            @endphp

            @if(isset($query))
                <p>{{ $query->{$options->label} }}</p>
            @else
                <p>{{ __('voyager::generic.no_results') }}</p>
            @endif

        @elseif($options->type == 'hasMany')



            @if(isset($options->model) && $options->model && $options->model == 'App\Models\DeliveryLocation')
                <div class="panel relation-group">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#DeliveryLocation"
                               aria-expanded="true" aria-controls="collapseOne">
                                @if($dataTypeContent->deliveries()->count())
                                    <i class="more-less fas fa-minus"></i>
                                @else
                                    <i class="more-less fas fa-plus"></i>
                                @endif
                                Место доставки
                            </a>
                        </h4>
                    </div>
                    <div id="DeliveryLocation"
                         class="panel-collapse {{$dataTypeContent->deliveries()->count()?'in':''}} collapse"
                         role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="delivery-content">
                                <div class="d-flex removed">
                                    <div class="form-check col-2 m-2">
                                        <input type="text" id="{{'suggest-'.time()}}"
                                               class="form-control input-delivery_location"
                                               value="Местонахождение Автомобиля"
                                               placeholder="Место " disabled/>
                                    </div>

                                    <div class="form-check col-2 m-2">

                                        <input type="text" class="form-control"
                                               value="0"
                                               placeholder="Цена " disabled/>
                                    </div>

                                    <div class="form-check col-1 m-2">
                                        <a href="javascript:void(0);" class="add_button" title="Add field">
                                            <i class="fas fa-plus-square fa-2x"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12 input-delivery_location_en"></div>
                                </div>
                                @foreach($dataTypeContent->deliveries as $delivery)
                                    @if ($delivery->location == 'Местонахождение Автомобиля') @continue @endif
                                    <div class="d-flex removed">

                                        <div class="form-check col-2 m-2">
                                            <input type="text" id="{{'suggest-'.time().'-'.$loop->index}}"
                                                   class="form-control input-delivery_location"
                                                   name="delivery_locations[]"
                                                   value="{{$delivery->location}}"
                                                   placeholder="Место "/>
                                        </div>

                                        <div class="form-check col-2 m-2">

                                            <input type="text" class="form-control" name="delivery_prices[]"
                                                   value="{{$delivery->price}}"
                                                   placeholder="Цена "/>
                                        </div>

                                        <div class="form-check col-1 m-2">
                                            <a href="javascript:void(0);" class="remove_button" title="Add field">
                                                <i class="fas fa-minus-circle fa-2x"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-12 input-delivery_location_en">
                                            {{$delivery->translate('en')->location}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            @endif

            @if(isset($view) && ($view == 'browse' || $view == 'read'))

                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
                    $model = app($options->model);

            		$selected_values = $model::where($options->column, '=', $relationshipData->id)->get()->map(function ($item, $key) use ($options) {
            			return $item->{$options->label};
            		})->all();
                @endphp

                @if($view == 'browse')
                    @php
                        $string_values = implode(", ", $selected_values);
                        if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
                    @endphp
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <p>{{ $string_values }}</p>
                    @endif
                @else
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <ul>
                            @foreach($selected_values as $selected_value)
                                <li>{{ $selected_value }}</li>
                            @endforeach
                        </ul>
                    @endif
                @endif

            @else

                @php
                    $model = app($options->model);
                    $query = $model::where($options->column, '=', $dataTypeContent->id)->get();
                @endphp

                @if($options->model != 'App\Models\DeliveryLocation')
                    @if(isset($query))
                        <ul>
                            @foreach($query as $query_res)
                                <li>{{ $query_res->{$options->label} }}</li>
                            @endforeach
                        </ul>

                    @else
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @endif

                @endif
            @endif

        @elseif($options->type == 'belongsToMany')

            @if(isset($view) && ($view == 'browse' || $view == 'read'))

                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;

                    $selected_values = isset($relationshipData) ? $relationshipData->belongsToMany($options->model, $options->pivot_table)->get()->map(function ($item, $key) use ($options) {
            			return $item->{$options->label};
            		})->all() : array();
                @endphp

                @if($view == 'browse')
                    @php
                        $string_values = implode(", ", $selected_values);
                        if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
                    @endphp
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <p>{{ $string_values }}</p>
                    @endif
                @else
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <ul>
                            @foreach($selected_values as $selected_value)
                                <li>{{ $selected_value }}</li>
                            @endforeach
                        </ul>
                    @endif
                @endif

            @else
                @if(isset($options->model) && $options->model && $options->model == 'App\Models\Filter')

                    <div class="panel relation-group">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#attribute" aria-expanded="false" aria-controls="collapseTwo">
                                    @if($dataTypeContent->filters()->count())
                                        <i class="more-less fas fa-minus"></i>
                                    @else
                                        <i class="more-less fas fa-plus"></i>
                                    @endif
                                    Доп. оборудования
                                </a>
                            </h4>
                        </div>
                        <div id="attribute"
                             class="panel-collapse {{$dataTypeContent->filters()->count()?'in':''}} collapse"
                             role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div class="filter-content">
                                    @foreach(app($options->model)->orderBy('order', 'asc')->get() as $item)
                                        <div class="d-flex">
                                            <div class="form-check col-2">
                                                <label class="form-check-label ">
                                                    <input class="form-check-input  change-price" type="checkbox"
                                                           value="{{$item->id}}"
                                                           data-target="#price-{{$item->id}}"
                                                           {{$dataTypeContent->filters->contains($item->id)?'checked': ''}}
                                                           name="filter[]" id="checkbox-{{$item->id}}">
                                                    {{$item->title}}
                                                </label>
                                            </div>
                                            <div class="col-3 mt-2">
                                                <input type="number" id="price-{{$item->id}}" class="form-control"
                                                       name="filter_price[]"
                                                       value="{{$dataTypeContent->filters->contains($item->id)? $dataTypeContent->filters->where('id', $item->id)->first()->pivot->price : ''}}"
                                                       placeholder="Цена"
                                                       {{$dataTypeContent->filters->contains($item->id)?'': 'disabled'}} required>
                                            </div>
                                            <div class="ml-2 mt-2">
                                                <input type="number" id="price-{{$item->id}}-order" class="form-control"
                                                       name="filter_order[]"
                                                       value="{{$dataTypeContent->filters->contains($item->id)? $dataTypeContent->filters->where('id', $item->id)->first()->pivot->order : ''}}"
                                                       placeholder="Порядок"
                                                       {{$dataTypeContent->filters->contains($item->id)?'': 'disabled'}} required>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                                <a href="{{route('voyager.filters.create')}}"
                                   class="m-2 pull-right"
                                   target="_blank">
                                    <i class="more-less fas fa-plus"></i>
                                    Добавить атрибут
                                </a>
                            </div>
                        </div>
                    </div>

                @elseif(isset($options->model) && $options->model && $options->model == 'App\Models\Insurance')

                    <div class="panel relation-group">
                        <div class="panel-heading" role="tab" id="heading3">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#insurances" aria-expanded="false" aria-controls="collapse3">
                                    @if($dataTypeContent->insurances()->count())
                                        <i class="more-less fas fa-minus"></i>
                                    @else
                                        <i class="more-less fas fa-plus"></i>
                                    @endif
                                    Страховки
                                </a>
                            </h4>
                        </div>
                        <div id="insurances"
                             class="panel-collapse {{$dataTypeContent->insurances()->count()?'in':''}} collapse"
                             role="tabpanel"
                             aria-labelledby="heading3">
                            <div class="panel-body">
                                <div class="">
                                    @foreach(app($options->model)->orderBy('order', 'asc')->get() as  $item)
                                        <div class="d-flex mx-2">
                                            <div class="form-group col-lg-2">
                                                <div class="form-check">
                                                    <label class="form-check-label ">
                                                        <input class="form-check-input change-insurances"
                                                               type="checkbox"
                                                               value="{{$item->id}}"
                                                               data-target="insurance-{{$item->id}}"
                                                               {{$dataTypeContent->insurances->contains($item->id)?'checked': ''}}
                                                               name="insurances[{{$item->id}}]"
                                                               id="checkbox-{{$item->id}}">
                                                        {{$item->translate()->name}}
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-2">
                                                <div class="input-group d-flex flex-nowrap">
                                                    <input type="text"
                                                           class="form-control insurance-{{$item->id}}"
                                                           name="insurances[{{$item->id}}][serial_number]"
                                                           value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->serial_number : ''}}"
                                                           placeholder="Номер серии"
                                                        {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                                        {{!$item->obligatory?'required':''}}>
                                                    <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true"
                                                          data-placement="top"
                                                          title="{{tooltips($toolips, 'insurances_serial_number')}}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                </div>


                                            </div>

                                            <div class="form-group col-lg-2">
                                                <div class="input-group d-flex flex-nowrap">

                                                    <input type="datetime"
                                                           class="form-control datepicker insurance-{{$item->id}}"
                                                           name="insurances[{{$item->id}}][start_date]"

                                                           value="{{$dataTypeContent->insurances->contains($item->id)? \Carbon\Carbon::parse($dataTypeContent->insurances->where('id', $item->id)->first()->pivot->start_date)->format('m/d/Y g:i A') : ''}}"
                                                           placeholder="Дата с"
                                                        {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                                        {{!$item->obligatory?'required':''}}>
                                                    <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true"
                                                          data-placement="top"
                                                          title="{{tooltips($toolips, 'insurances_start_date')}}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-2">
                                                <div class="input-group d-flex flex-nowrap">
                                                    <input type="datetime"
                                                           class="form-control datepicker insurance-{{$item->id}}"
                                                           name="insurances[{{$item->id}}][end_date]"
                                                           value="{{$dataTypeContent->insurances->contains($item->id)? \Carbon\Carbon::parse($dataTypeContent->insurances->where('id', $item->id)->first()->pivot->end_date)->format('m/d/Y g:i A') : ''}}"
                                                           placeholder="Дата до"
                                                        {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                                        {{!$item->obligatory?'required':''}}>
                                                    <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true"
                                                          data-placement="top"
                                                          title="{{tooltips($toolips, 'insurances_end_date')}}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-1">
                                                <div class="input-group d-flex flex-nowrap">
                                                    <input type="text"
                                                           class="form-control insurance-{{$item->id}}"
                                                           name="insurances[{{$item->id}}][price]"
                                                           value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->price : ''}}"
                                                           placeholder="Цена"
                                                        {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                                        {{!$item->obligatory?'required':''}}>
                                                    <span class="k-pointer ml-2" data-toggle="tooltip" data-html="true"
                                                          data-placement="top"
                                                          title="{{tooltips($toolips, 'insurances_price')}}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-1">
                                                <div class="input-group d-flex flex-nowrap">
                                                    <input type="text"
                                                           class="form-control insurance-{{$item->id}}"
                                                           name="insurances[{{$item->id}}][order]"
                                                           value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->order : ''}}"
                                                           placeholder="Порядок"
                                                        {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                                        {{!$item->obligatory?'required':''}}>
                                                    {{--<span class="k-pointer ml-2" data-toggle="tooltip" data-html="true"
                                                          data-placement="top"
                                                          title="{{tooltips($toolips, 'insurances_price')}}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>--}}
                                                </div>
                                            </div>

                                            @if($item->franchise === 1)
                                                <div class="form-group col-lg-2">
                                                    <div class="input-group d-flex flex-nowrap">
                                                        <input type="text"
                                                               class="form-control insurance-{{$item->id}}"
                                                               name="insurances[{{$item->id}}][franchise]"
                                                               value="{{$dataTypeContent->insurances->contains($item->id)? $dataTypeContent->insurances->where('id', $item->id)->first()->pivot->franchise : ''}}"
                                                               placeholder="Франшиза"
                                                            {{$dataTypeContent->insurances->contains($item->id)?'': 'disabled'}}
                                                            {{!$item->obligatory?'required':''}}>
                                                        <span class="k-pointer ml-2" data-toggle="tooltip"
                                                              data-html="true" data-placement="top"
                                                              title="{{tooltips($toolips, 'insurances_franchise')}}">
                                                            <i class="fas fa-info-circle"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif


                                        </div>

                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                @else
                    <select
                        class="form-control @if(isset($options->taggable) && $options->taggable == 'on') select2-taggable @else select2-ajax @endif"
                        name="{{ $relationshipField }}[]" multiple
                        data-get-items-route="{{route('voyager.' . $dataType->slug.'.relation')}}"
                        data-get-items-field="{{$row->field}}"
                        @if(isset($options->taggable) && $options->taggable == 'on')
                        data-route="{{ route('voyager.'.\Illuminate\Support\Str::slug($options->table).'.store') }}"
                        data-label="{{$options->label}}"
                        data-error-message="{{__('voyager::bread.error_tagging')}}"
                        @endif
                    >

                        @php
                            $selected_values = isset($dataTypeContent) ? $dataTypeContent->belongsToMany($options->model, $options->pivot_table)->get()->map(function ($item, $key) use ($options) {
                                return $item->{$options->key};
                            })->all() : array();
                            $relationshipOptions = app($options->model)->all();
                        @endphp

                        @if(!$row->required)
                            <option value="">{{__('voyager::generic.none')}}</option>
                        @endif

                        @foreach($relationshipOptions as $relationshipOption)
                            <option
                                value="{{ $relationshipOption->{$options->key} }}" @if(in_array($relationshipOption->{$options->key}, $selected_values)){{ 'selected="selected"' }}@endif>
                                {{ $relationshipOption->{$options->label} }}
                            </option>
                        @endforeach

                    </select>
                @endif

            @endif

        @endif

    @else

        cannot make relationship because {{ $options->model }} does not exist.

    @endif

@endif
