<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OwnerProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $data = [
            'number' => 'required|string|max:16',
            'birthday' => 'required|date|before:13 years ago',
            'company' => 'required',
            'registration_number' => 'required',
            'legal_address' => 'required',
            'unp' => 'required',
            'owner' => 'required',
            'assigned' => 'required',
            'cont_tel' => 'required',
            'cont_mail' => 'required|email',
            'bank' => 'required',
            'bank_account_number' => 'required',

        ];

        if (!auth()->user()->provider){
            $data['last_name'] =  'required|string|max:255';
            $data['first_name'] =  'required|string|max:255';
            $data['email'] =  'required|string|email|max:255|unique:users,email,'.auth()->id();
        }

        return $data;
    }

    public function messages()
    {
        return [
            'number.required' => __('validation.required', ['attribute' => __('owner-profile.phone_number')]),
            'birthday.required' => __('validation.required', ['attribute' => __('owner-profile.birthday')]),
            'company.required' => __('validation.required', ['attribute' => __('owner-profile.company')]),
            'registration_number.required' => __('validation.required', ['attribute' => __('owner-profile.reg_number')]),
            'legal_address.required' => __('validation.required', ['attribute' => __('owner-profile.legal_address')]),
            'unp.required' => __('validation.required', ['attribute' => __('owner-profile.unp')]),
            'owner.required' => __('validation.required', ['attribute' => __('owner-profile.owner_name')]),
            'assigned.required' => __('validation.required', ['attribute' => __('owner-profile.assigned')]),
            'cont_tel.required' => __('validation.required', ['attribute' => __('owner-profile.cont_number')]),
            'cont_mail.required' => __('validation.required', ['attribute' => __('owner-profile.cont_mail')]),
            'bank.required' => __('validation.required', ['attribute' => __('owner-profile.phone_number')]),
            'bank_account_number.required' => __('validation.required', ['attribute' => __('owner-profile.bank_account_number')]),
        ];
    }
}
