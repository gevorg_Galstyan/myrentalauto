@inject('markdown', 'Parsedown')


<div class="card {{isset($reply) && $reply === true ? 'card-inner ml-5 mt-2' : 'mx-2 mx-md-5 my-2'}} " id="comment-{{ $comment->id }}">

    <div class="card-body" id="reply-content-{{$comment->id}}">
        <div class="">
            <div class="fs-14">
            <div class="">
                <div class="d-flex justify-content-start mb-1">
                    <div class="p-2 align-self-center">
                        <img src="{{Voyager::image($comment->commenter->avatar)}}"
                             class="img rounded-circle img-fluid" width="50px" height="50px"/>
                    </div>

                    <div class="align-self-center">
                        <strong>{{$comment->commenter->full_name}}</strong>
                        <div class="align-self-center">
                            <span class="text-secondary ">{{$comment->created_at->diffForHumans()}}</span>
                        </div>

                    </div>
                </div>
                <div class="d-flex justify-content-between w-100">
                    <div class="ml-5 pl-5">
                        <p class="mb-0 ff-ms"
                           id="comment-text-{{ $comment->id }}">{{$markdown->line($comment->comment)}}</p>
                    </div>
                    <div class="">
                        <p>
                            {{--<a class="float-right btn btn-outline-primary ml-2"> <i class="fa fa-reply"></i>--}}
                            {{--Reply</a>--}}
                            @can('reply-to-comment', $comment)
                                <button data-toggle="modal" data-target="#reply-modal-{{ $comment->id }}"
                                        class="float-right btn btn-outline-primary ml-2 text-uppercase">
                                    <i class="fa fa-reply"></i>
                                    {{__('comment.cancel')}}
                                </button>
                            @endcan
                            {{--<a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>--}}
                            @can('edit-comment', $comment)
                                <button data-toggle="modal" data-target="#comment-modal-{{ $comment->id }}"
                                        class="float-right btn text-white btn-danger text-uppercase">{{__('comment.edit')}}
                                </button>
                            @endcan
                        </p>
                    </div>
                </div>


            </div>
        </div>

{{--            <div class="col-md-2">--}}
{{--                <img src="{{Voyager::image($comment->commenter->avatar)}}" alt="{{ $comment->commenter->name }} Avatar" class="img rounded-circle img-fluid" width="50px" height="50px"/>--}}
{{--                <p class="text-secondary text-center">{{ $comment->created_at->diffForHumans() }}</p>--}}
{{--            </div>--}}
{{--            <div class="col-md-10">--}}
{{--                <p>--}}
{{--                    <a class="float-left"--}}
{{--                       href="#">--}}
{{--                        <strong>{{ $comment->commenter->first_name }}</strong></a>--}}

{{--                </p>--}}
{{--                <div class="clearfix"></div>--}}
{{--                <p id="comment-text-{{ $comment->id }}">{{$markdown->line($comment->comment)}}</p>--}}
{{--                <p>--}}
{{--                    --}}{{--<a class="float-right btn btn-outline-primary ml-2"> <i class="fa fa-reply"></i>--}}
{{--                    --}}{{--Reply</a>--}}
{{--                    @can('reply-to-comment', $comment)--}}
{{--                        <button data-toggle="modal" data-target="#reply-modal-{{ $comment->id }}"--}}
{{--                                class="float-right btn btn-outline-primary ml-2 text-uppercase">--}}
{{--                            <i class="fa fa-reply"></i>--}}
{{--                            Ответить--}}
{{--                        </button>--}}
{{--                    @endcan--}}
{{--                    --}}{{--<a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>--}}
{{--                    @can('edit-comment', $comment)--}}
{{--                        <button data-toggle="modal" data-target="#comment-modal-{{ $comment->id }}"--}}
{{--                                class="float-right btn text-white btn-danger text-uppercase">редактировать--}}
{{--                        </button>--}}
{{--                    @endcan--}}
{{--                </p>--}}
{{--            </div>--}}
        </div>
        @can('edit-comment', $comment)
            <div class="modal fade" id="comment-modal-{{ $comment->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form method="POST" data-id="{{$comment->id}}" data-target="#comment-modal-{{ $comment->id }}" class="edit-comment-form" action="{{ url('comments/' . $comment->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">{{__('comment.edit_comment')}}</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="message">{{__('comment.update_your_message_here')}}</label>
                                    <textarea required class="form-control comment" name="message"
                                              rows="3">{{ $comment->comment }}</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-outline-secondary text-uppercase"
                                        data-dismiss="modal">{{__('comment.cancel')}}
                                </button>
                                <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">{{__('comment.update')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

        @can('reply-to-comment', $comment)
            <div class="modal fade" id="reply-modal-{{ $comment->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form method="POST" class="comment-reply-form" data-id="{{$comment->id}}" data-target="#reply-modal-{{ $comment->id }}" action="{{ url('comments/' . $comment->id) }}">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">{{__('comment.reply_to_comment')}}</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="message">{{__('comment.enter_your_message_here')}}</label>
                                    <textarea required class="form-control comment" name="message" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-outline-secondary text-uppercase"
                                        data-dismiss="modal">{{__('comment.cancel')}}
                                </button>
                                <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">{{__('comment.send')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan
        @foreach($comment->children as $child)
            @include('comments::_comment', [
                'comment' => $child,
                'reply' => true
            ])
        @endforeach
    </div>

</div>
