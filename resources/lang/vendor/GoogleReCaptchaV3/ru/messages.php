<?php

return [
    'ERROR_MISSING_INPUT' => 'Отсутствует входной ответ.',
    'ERROR_UNABLE_TO_VERIFY' => 'Невозможно проверить.',
    'ERROR_HOSTNAME' => 'Имя хоста не совпадает.',
    'ERROR_ACTION' => 'Действие не совпадает.',
    'ERROR_SCORE_THRESHOLD' => 'Оценка не соответствует порогу.',
    'ERROR_TIMEOUT' => 'Тайм-аут',
    'SUCCESS' => 'Успешно пройдено.',
];
