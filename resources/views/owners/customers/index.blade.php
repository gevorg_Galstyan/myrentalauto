@extends('owners.layouts.app')


@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Арендаторы
        </h1>
    </div>

    <div class="row row-cards row-deck">
        <div class="col-12">
            <div class="card">

                {{--<div class="card-header">--}}
                    {{--<h3 class="card-title"></h3>--}}
                {{--</div>--}}
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable" id="ownerTables">
                        <thead>
                        <tr>
                            <th>Арендатор</th>
                            <th>Эл. адрес</th>
                            <th>Номер телефона</th>
                            <th>День рождения</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                                @if (!$customer)
                                    @continue
                                @endif
                                <tr>
                                    <td><a href="{{route('customer', ['slug' => $customer->slug])}}" class="text-inherit">{{$customer->full_name??''}}</a></td>
                                    <td>
                                        {{$customer->email}}
                                    </td>
                                    <td>
                                        {{$customer->number}}
                                    </td>
                                    <td>
                                        {{$customer->birthday ? Carbon\Carbon::parse($customer->birthday)->format('Y-m-d') : 'не заполнен'}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script>

        $('[data-toggle="tooltip"]').tooltip()

        $(document).ready(function () {
            $('#ownerTables').DataTable({
                responsive: true,
            });
        });

    </script>
@stop
