<?php

namespace App\Actions;

use App\User;
use TCG\Voyager\Actions\AbstractAction;

class ShowCalendar extends AbstractAction
{
    public function getTitle()
    {
        return 'Календарь';
    }

    public function getIcon()
    {
        return 'far fa-calendar-alt';
    }

    public function getPolicy()
    {
//        return 'cars';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-default pull-right ',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.cars.calendar', ['car' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'cars';
    }


}