<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class OrderDate extends Model implements \MaddHatter\LaravelFullcalendar\Event
{

    protected $dates = ['start', 'end'];

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        if ($this->note) {
            return $this->note;
        }
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function dateString()
    {
        $start = Carbon::parse($this->start)->locale(\LaravelLocalization::getCurrentLocale());
        $end = Carbon::parse($this->end)->locale('ru');
        if (!$start->diffInMonths($end)) {
            return $start->day . ' - ' . $end->day . ' ' . $start->monthName . ' ' . $start->year;
        } elseif (!$start->diffInYears($end)) {
            return "$start->day  $start->monthName $start->year - $end->day $end->monthName $end->year";
        } else {
            return "$start->dayName $start->monthName - $end->day $end->monthName $start->year";
        }
    }
}
