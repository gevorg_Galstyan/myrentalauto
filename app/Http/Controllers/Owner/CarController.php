<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use App\Models\CarBodyType;
use App\Models\CarColor;
use App\Models\DeliveryLocation;
use App\Models\Insurance;
use App\User;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Car;
use App\Models\CarType;
use App\Notifications\ChangeOwnerCar;
use App\Notifications\RestoreOwnerCar;
use App\Models\CarMake;
use App\Models\CarModel;

class CarController extends VoyagerBaseController
{


    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = 'cars';

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        // Check permission
        $this->authorize('browse', app(Car::class));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object)['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && \Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        return view('owners.cars.index', [
//            'dataType' => $dataType,
            'cars' => $dataTypeContent,
//            'isModelTranslatable' => $isModelTranslatable,
//            'search' => $search,
//            'orderBy' => $orderBy,
//            'orderColumn' => $orderColumn,
//            'sortOrder' => $sortOrder,
//            'searchable' => $searchable,
//            'isServerSide' => $isServerSide,
//            'defaultSearchKey' => $defaultSearchKey,
//            'usesSoftDeletes' => $usesSoftDeletes,
//            'showSoftDeleted' => $showSoftDeleted

        ]);
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************
    public function show(Request $request, $id)
    {
        if ($car = Car::find($id)) {
            if ($car->owner->id != auth()->id()) return abort(403);
        }

        $request['date'] = $request->date ?? Carbon::now()->setTimeFromTimeString('09:00')->addDay(5)->format('d.m.Y H:00') . ' ~ ' . Carbon::now()->setTimeFromTimeString('09:00')->addDay(7)->format('d.m.Y H:00');
        $dates = explode(' ~ ', $request->date);
        $start = Carbon::parse($dates[0]);
        $end = Carbon::parse($dates[1]);
        $request['days'] = $end->diffInDays($start) + 1;
        $request['start_day'] = $start;
        $request['end_day'] = $end;

        $slug = 'cars';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', app(Car::class));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $tariffPlan = [];
        foreach ($dataTypeContent->filters as $filter) {
            if ($filter->pivot->price <= 0) {
                $tariffPlan[] = ['name' => $filter->translate()->title];
            }
        }
        foreach ($dataTypeContent->insurances as $insurances) {
            if ($insurances->pivot->price <= 0) {
                $tariffPlan[] = ['name' => $insurances->translate()->name, 'desc' => '<span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="' . $insurances->translate()->description . '"><i class="fas fa-info-circle"></i></span>'];
            }
        }

        $green_card = json_decode($dataTypeContent->green_card ?? '[]', true);

        if (count($green_card)) {
            foreach ($green_card as $key => $item) {
                if ($item[getGreenCardDayKey(request()->input('days'))] <= 0) {
                    $tariffPlan[] = ['name' => 'Грин Карта' . config('car.green_card.country.' . $key)];
                }
            }
        }


        return view('owners.cars.read', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'tariffPlan'));
    }

    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************
    public function edit(Request $request, $id)
    {
        if ($car = Car::find($id)) {
            if ($car->owner->id != auth()->id()) return abort(403);
        }
        $slug = 'cars';

        $car_types = CarType::get();

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', app(Car::class));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $numberOfSeats = $dataType->addRows->where('field', 'number_of_seats')->first();
        $carFuel = $dataType->addRows->where('field', 'fuel')->first();
        $colors = CarColor::get();
        $bodyTypes = CarBodyType::get();
        return view('owners.cars.create', compact('dataType', 'numberOfSeats', 'carFuel', 'car_types', 'dataTypeContent', 'isModelTranslatable', 'colors', 'bodyTypes'));
    }

    public function update(Request $request, $id)
    {
        if ($car = Car::find($id)) {
            if ($car->owner->id != auth()->id()) return abort(403);
        }
        $slug = 'cars';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', app(Car::class));

        // location_title
        $ruLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'ru_RU');
        $enLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'en_GB');
        $location_title = [
            'ru' => $ruLocation ? $ruLocation->getFullAddress() : $data->translate('ru')->location_title,
            'en' => $enLocation ? $enLocation->getFullAddress() : $data->translate('en')->location_title,
        ];
        $metaLocationTitle = [
            'ru' => $ruLocation ? ($ruLocation->getLocality() ?? $ruLocation->getRegion()) : $data->translate('ru')->meta_location_title,
            'en' => $enLocation ? ($enLocation->getLocality() ?? $enLocation->getRegion()) : $data->translate('ru')->meta_location_title,
        ];

//        dd($title_location);

        $request['meta_title_i18n'] = $request['meta_title'] = json_encode([
            'en' => $data->translate('en')->meta_title,
            'ru' => $data->translate('ru')->meta_title
        ]);
        $request['meta_keyword_i18n'] = $request['meta_keyword'] = json_encode([
            'en' => $data->translate('en')->meta_keyword,
            'ru' => $data->translate('ru')->meta_keyword
        ]);
        $request['meta_description_i18n'] = $request['meta_description'] = json_encode([
            'en' => $data->translate('en')->meta_description,
            'ru' => $data->translate('ru')->meta_description
        ]);


        $request['meta_location_title'] = json_encode($metaLocationTitle);
        $request['meta_location_title_i18n'] = json_encode($metaLocationTitle);

        $request['location_title'] = json_encode($location_title);
        $request['location_title_i18n'] = json_encode($location_title);

        //green card data
        $request['green_card'] = json_encode($request->green_card && is_array($request->green_card) ? $request->green_card : []);

        //default values
        $request['owner_id'] = \Auth::user()->id;


        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $request['percent_to_total'] = $data->percent_to_total;
        $request['order'] = $data->order;
        $notifyText = 'Владелец сделал обновление машины. ';
        if ($data->in_home_slider) {
            $notifyText .= '<br> Машина снята из главного слайдера.';
        }
        if ($request->image_1 ||
            $request->age != $data->age ||
            $request->motor != $data->motor ||
            $request->transmission != $data->transmission ||
            $request->car_make_id != $data->car_make_id ||
            $request->car_model_id != $data->car_model_id ||
            $request->on_request != $request->on_request ||
            $request->price_1 != $data->price_1) {
            $request['status'] = 0;
            $notifyText .= '<br> Машина снята с публикации "поиска".';
        } else {
            $request['status'] = $data->status;
        }
        if (!$data->slug) {
            $make = CarMake::find($request->car_make_id);
            $model = CarModel::find($request->car_model_id);
            $make = $make->name ?? null;
            $model = $model->name ?? null;
            $request['slug'] = SlugService::createSlug(Car::class, 'slug', $make . '-' . $model, ['unique' => true]);
        } else {
            $request['slug'] = $data->slug;
        }

//      CITY
        $en_city = $data->translate('en')->city;
        $ru_city = $data->translate('ru')->city;
        $city = [
            'en' => ($request->city ? myTranslate($request->city, 'en') : ($en_city ? $en_city : "")),
            'ru' => ($request->city ? $request->city : ($ru_city ? $ru_city : "")),
        ];
        $request['city_i18n'] = json_encode($city);

//      NOTE
        $en_note = $data->translate('en')->note;
        $ru_note = $data->translate('ru')->note;
        $note = [
            'en' => ($request->note ? myTranslate($request->note, 'en') : ($en_note ? $en_note : "")),
            'ru' => ($request->note ? $request->note : ($ru_note ? $ru_note : "")),
        ];
        $request['note_i18n'] = json_encode($note);

        $data = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        $data->error_status = '';
        $data->save();
        //Relationship
        $data->filters()->delete();
        $data->deliveries()->delete();
        $data->insurances()->delete();


        $delivery = new DeliveryLocation();
        $delivery->car_id = $data->id;
        $delivery->location = 'Местонахождение Автомобиля';
        $delivery->price = 0;
        $delivery->save();
        $delivery = $delivery->translate('en');
        $delivery->location = myTranslate('Местонахождение Автомобиля', 'en');
        $delivery->save();
        if (is_array($request->delivery_locations) && !empty($request->delivery_locations)) {
            foreach ($request->delivery_locations as $index => $location) {
                if ($location) {
                    if (is_null($location)) continue;
                    $delivery = new DeliveryLocation();
                    $delivery->car_id = $data->id;
                    $delivery->location = $location;
                    $delivery->price = $request->delivery_prices[$index] ?? null;
                    $delivery->save();
                    $delivery = $delivery->translate('en');
                    $delivery->location = myTranslate($location, 'en');
                    $delivery->save();
                }

            }

        }

        if (is_array($request->filter)) {
            foreach ($request->filter as $index => $filter) {
                $data->filters()->attach($filter, [
                    'price' => $request->filter_price[$index],
                    'order' => 1,
                ]);
            }
        }

        $obligatoryInsurances = Insurance::where('obligatory', 1)->pluck('id')->toArray();
        if (is_array($request->insurances)) {
            foreach ($request->insurances as $insurance_id => $insurance) {
                $data->insurances()->attach($insurance_id, [
                    'serial_number' => $insurance['serial_number'],
                    'start_date' => Carbon::parse($insurance['start_date'])->format('Y-m-d H:i:s'),
                    'end_date' => Carbon::parse($insurance['end_date'])->format('Y-m-d H:i:s'),
                    'price' => $insurance['price'],
                    'order' => 1,
                    'franchise' => isset($insurance['franchise']) ? $insurance['franchise'] : 0,
                ]);
            }
            foreach ($obligatoryInsurances as $obligatory) {
                if (!in_array($obligatory, array_keys($request->insurances))) {
                    $data->insurances()->attach($obligatory, [
                        'price' => 0,
                    ]);
                }
            }
        } else {
            foreach ($obligatoryInsurances as $obligatory) {
                $data->insurances()->attach($obligatory, [
                    'price' => 0,
                ]);
            }
        }

        $data->save();

        if ($data) {
            $car = Car::where('id', $id)->first();
            $users = User::where('role_id', 1)->get();
            Notification::send($users, new ChangeOwnerCar(auth()->user(), $car, $notifyText));
        }

        event(new BreadDataUpdated($dataType, $data));
        return redirect()
            ->route("cars.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    //***************************************
    //
    //                   /\
    //                  /  \
    //                 / /\ \
    //                / ____ \
    //               /_/    \_\
    //
    //
    // Add a new item of our Data Type BRE(A)D
    //
    //****************************************
    public function create(Request $request)
    {
        $slug = 'cars';

        $car_types = CarType::get();
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if ($request->ajax()) {
            $term = trim($request->search);

            if ($request->make_id) {
                $data = CarModel::select('id', 'name as text')->where('make_id', $request->make_id)->where('name', 'like', '%' . $term . '%')->take(6)->get()->toArray();
            } else {
                $data = CarMake::select('id', 'name as text')->where('name', 'like', '%' . $term . '%')->take(6)->get()->toArray();
            }

            return response()->json($data);


        }

        // Check permission
        $this->authorize('add', app(Car::class));


        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $numberOfSeats = $dataType->addRows->where('field', 'number_of_seats')->first();
        $carFuel = $dataType->addRows->where('field', 'fuel')->first();
        $colors = CarColor::get();
        $bodyTypes = CarBodyType::get();
        return view('owners.cars.create', compact('dataType', 'carFuel', 'numberOfSeats', 'car_types', 'dataTypeContent', 'isModelTranslatable', 'colors', 'bodyTypes'));
    }

    public function store(Request $request)
    {
        $slug = 'cars';
        $request['owner_id'] = \Auth::user()->id;
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app(Car::class));

        $carMake = CarMake::find($request->car_make_id);
        $carModel = CarModel::find($request->car_model_id);
        $carMake = $carMake->name ?? null;
        $carModel = $carModel->name ?? null;
        $request['slug'] = SlugService::createSlug(Car::class, 'slug', $carMake . '-' . $carModel, ['unique' => true]);

        // location_title
        $ruLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'ru_RU');
        $enLocation = getCountryNameInLocation($request->location['lng'], $request->location['lat'], 'en_GB');
        $location_title = [
            'ru' => $ruLocation->getFullAddress(),
            'en' => $enLocation->getFullAddress(),
        ];
        $metaLocationTitle = [
            'ru' => $ruLocation->getLocality() ?? $ruLocation->getRegion(),
            'en' => $enLocation->getLocality() ?? $enLocation->getRegion(),
        ];

        $request['meta_title_i18n'] = $request['meta_title'] = json_encode([
            'en' => '',
            'ru' => ''
        ]);
        $request['meta_keyword_i18n'] = $request['meta_keyword'] = json_encode([
            'en' => '',
            'ru' => ''
        ]);
        $request['meta_description_i18n'] = $request['meta_description'] = json_encode([
            'en' => '',
            'ru' => ''
        ]);

//        dd($title_location);
        $request['meta_location_title'] = json_encode($metaLocationTitle);
        $request['meta_location_title_i18n'] = json_encode($metaLocationTitle);

        $request['location_title'] = json_encode($location_title);
        $request['location_title_i18n'] = json_encode($location_title);

        //green card data
        $request['green_card'] = json_encode($request->green_card && is_array($request->green_card) ? $request->green_card : []);

//      CITY
        $city = [
            'en' => ($request->city ? myTranslate($request->city, 'en') : ""),
            'ru' => ($request->city ? $request->city : ""),
        ];
        $request['city_i18n'] = json_encode($city);

//      NOTE
        $note = [
            'en' => ($request->note ? myTranslate($request->note, 'en') : ""),
            'ru' => ($request->note ? $request->note : ""),
        ];
        $request['note_i18n'] = json_encode($note);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        //Relationship
        $data->filters()->delete();
        $data->deliveries()->delete();
        $data->insurances()->delete();


        $delivery = new DeliveryLocation();
        $delivery->car_id = $data->id;
        $delivery->location = 'Местонахождение Автомобиля';
        $delivery->price = 0;
        $delivery->save();
        $delivery = $delivery->translate('en');
        $delivery->location = myTranslate('Местонахождение Автомобиля', 'en');
        $delivery->save();
        if (is_array($request->delivery_locations) && !empty($request->delivery_locations)) {
            foreach ($request->delivery_locations as $index => $location) {
                if (is_null($location)) continue;
                $delivery = new DeliveryLocation();
                $delivery->car_id = $data->id;
                $delivery->location = $location;
                $delivery->price = $request->delivery_prices[$index] ?? null;
                $delivery->save();
                $delivery = $delivery->translate('en');
                $delivery->location = myTranslate($location, 'en');
                $delivery->save();
            }
        }
        if (is_array($request->filter)) {
            foreach ($request->filter as $index => $filter) {
                $data->filters()->attach($filter, [
                    'price' => $request->filter_price[$index],
                    'order' => 1,
                ]);
            }
        }

        $obligatoryInsurances = Insurance::where('obligatory', 1)->pluck('id')->toArray();
        if (is_array($request->insurances)) {
            foreach ($request->insurances as $insurance_id => $insurance) {
                $data->insurances()->attach($insurance_id, [
                    'serial_number' => $insurance['serial_number'],
                    'start_date' => Carbon::parse($insurance['start_date'])->format('Y-m-d H:i:s'),
                    'end_date' => Carbon::parse($insurance['end_date'])->format('Y-m-d H:i:s'),
                    'price' => $insurance['price'],
                    'order' => 1,
                    'franchise' => isset($insurance['franchise']) ? $insurance['franchise'] : 0,
                ]);
            }
            foreach ($obligatoryInsurances as $obligatory) {
                if (!in_array($obligatory, array_keys($request->insurances))) {
                    $data->insurances()->attach($obligatory, [
                        'price' => 0,
                    ]);
                }
            }
        } else {
            foreach ($obligatoryInsurances as $obligatory) {
                $data->insurances()->attach($obligatory, [
                    'price' => 0,
                ]);
            }
        }

        $data->save();

        event(new BreadDataAdded($dataType, $data));

        return redirect()
            ->route("cars.index")
            ->with([
                'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }


    //***************************************
    //                _____
    //               |  __ \
    //               | |  | |
    //               | |  | |
    //               | |__| |
    //               |_____/
    //
    //         Delete an item BREA(D)
    //
    //****************************************
    public function destroy(Request $request, $id)
    {
        if ($car = Car::find($id)) {
            if ($car->owner->id != auth()->id()) return abort(403);
        }
        $slug = 'cars';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app(Car::class));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message' => __('voyager::generic.successfully_deleted') . " {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message' => __('voyager::generic.error_deleting') . " {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("cars.index")->with($data);
    }

    /**
     * get deleted cars list
     */
    public function deleted()
    {
        $deletedCars = Car::onlyTrashed()->where('owner_id', auth()->user()->id)->get();
        return view('owners.cars.index', [
            'cars' => $deletedCars,
            'carsDeletedPage' => true,
        ]);
    }

    /**
     * restore deleted car
     * @param Request $request
     * @param $id
     * @return void
     */
    public function restore(Request $request, $id)
    {
        if ($car = Car::find($id)) {
            if ($car->owner->id != auth()->id()) return abort(403);
        }
        $slug = 'cars';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app(Car::class));
        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->display_name_singular;

        //send message admins
        $users = User::where('role_id', 1)->get();
        Notification::send($users, new RestoreOwnerCar(auth()->user(), $data));

        $res = $data->restore($id);


        $data = $res
            ? [
                'message' => __('voyager::generic.successfully_restored') . " {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message' => __('voyager::generic.error_restoring') . " {$displayName}",
                'alert-type' => 'error',
            ];

//        if ($res) {
//            event(new BreadDataRestored($dataType, $data));
//        }

        return redirect()->back()->with('status', __('voyager::generic.successfully_restored') . " {$displayName}");
    }

    public function calendar(Request $request, Car $car)
    {

        if ($car->owner->id != auth()->id()) return abort(403);

        $dataTypeContent = $car;
        $dataType = Voyager::model('DataType')->where('slug', '=', 'cars')->first();

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $events = [];
        $colors = [
            'orders' => [
                '4ebf22',
                '82e25d'
            ],
            'added' => [
                '32A0DA',
            ]
        ];

        foreach ($car->busyDays as $index => $dates) {

            if ($dates->order) {
                $color = $colors['orders'][$index % 2 ? 1 : 0];
            } else {
                $color = $colors['added'][0];
            }
            $events[] = \Calendar::event(

                $dates->getTitle(),
                true,
                new \DateTime($dates->getStart()),
                new \DateTime(Carbon::parse($dates->getEnd())->addDays(1)),
                null,
                [
                    'color' => '#' . $color,
                    "overlap" => false,
                    'className' => 'busy-day',
                    'description' => $dates->note ?? '',
                ]
            );
        }
        $calendar = \Calendar::addEvents($events, [

            'defaultView' => 'timeGridWeek',
            'rendering' => 'background',
            'allDay' => true,
            'editable' => true,
            'eventDurationEditable' => true,
        ])//add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'plugins' => ['dayGrid', 'bootstrap'],
            'themeSystem' => 'bootstrap',
            'header' => ["center" => 'makeOrder'],

            "views" => [
                "dayGridMonth" => [ // name of view
                    "titleFormat" => ["year" => 'numeric', "month" => '2-digit', "day" => '2-digit'],
                    "duration" => ["month" => 2]
                ],

            ],

            "locale" => \LaravelLocalization::getCurrentLocale(),
            'showNonCurrentDates' => false,
            'fixedWeekCount' => false,
            'bootstrap' => true,
            'gotoDate' => '2019-07-05',

        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)

        ]);
        return view('owners.cars.calendar', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'calendar'));
    }
}
