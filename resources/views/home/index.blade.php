@extends('layouts.app')


@section('meta')
    <title>{{$meta_tags->myTranslate()->title}}</title>
    <meta name="keywords" content="{{$meta_tags->myTranslate()->Keyword}}"/>
    <meta name="description" content="{{$meta_tags->myTranslate()->description}}"/>

    <meta property="og:locale"
          content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$meta_tags->myTranslate()->title}}"/>
    <meta property="og:description" content="{{$meta_tags->myTranslate()->description}}"/>
    <meta property="og:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">
    <meta name="twitter:title" content="{{$meta_tags->myTranslate()->title}}">
    <meta name="twitter:description" content="{{$meta_tags->myTranslate()->description}}">
    <meta name="twitter:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}">

{{--    <link href="{{request()->url()}}" rel="canonical">--}}
@stop

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    <style>.slick-slide{margin:0 10px!important}.slick-list{margin:0 -10px!important}.location-icon-container{position:absolute;right:60px;top:calc(50% - 10px);display:none}.location-loader{position:relative;height:20px;width:20px;display:inline-block;animation:around 5.4s infinite}@keyframes around{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.location-loader::after,.location-loader::before{content:"";background:#fff;position:absolute;display:inline-block;width:100%;height:100%;border-width:2px;border-color:#384c79 #f60 #384c79 #f60;border-style:solid;border-radius:20px;box-sizing:border-box;top:0;left:0;animation:around .7s ease-in-out infinite}.location-loader::after{animation:around .7s ease-in-out .1s infinite;background:0 0}</style>
@stop

@section('content')
    <!--SLIDER-->
    @include('home.slider')
    <!--SEARCH-->
    {{--        @include('home.search')--}}
    <!--ACTIVE CARS-->
    @include('home.active_cars')
    <!--ПРЕИМУЩЕСТВА-->
    @include('home.advantage')
    <!--HOW IT WORKS-->
    @include('home.how_it_work')
    <!--BLOG-->
    {{--    @include('home.blog')--}}
    <section class="post-content"></section>
@endsection


@section('script')
{{--    <script  src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=195a01c1-11df-40b3-bf48-96004f1783eb&mode=release&load=SuggestView,suggest"></script>--}}
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
        var adjs = {!! json_encode($adjs) !!},
                sentence = $('#sentence'),
            textSlider = $('.textSlider'),
            stop = false,
            post_in_home = '{{route('post_in_home')}}/',
            home_slider_on_off = {{setting('site.home_slider_on_off')}},
            home_slider_speed = '{{setting('site.home_slider_speed')}}';
        // ymaps = false,
    </script>
    <script src="{{asset('js/home.js')}}"></script>
@stop
@section('schema')
    {!! Spatie\SchemaOrg\Schema::organization()
        ->name(config('app.name'))
        ->url(route('home'))
        ->logo(ownMakeImage(50 , 50 ,85,'img/logo.png', 'png'))
        ->contactPoint(Spatie\SchemaOrg\Schema::contactPoint()
            ->telephone('+375-44-575-999-1')
            ->email('info@myrentauto.com')
        )
        ->sameAs([
            'https://www.facebook.com/MyRentAuto/',
            'https://vk.com/myrentauto',
            'https://www.instagram.com/myrentautocom/',
            'https://telegram.me/MyRentAuto',
            'https://twitter.com/myrentauto'
        ])
        ->toScript() !!}
@endsection
