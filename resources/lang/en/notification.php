<?php
return [
    'car-request-owner' => [
        'title' => 'Hello dear :name',
        'text-1' => 'Received an application for booking your car :car',
        'text-2' => 'To confirm or cancel the reservation (required with justification), you must go to your Personal Account by the link :link',
        'text-3' => 'Regards and best wishes. Site administration :name',
    ],
    'car-request-admin' => [
        'title' => 'Hello dear :name',
        'text-1' => 'on the  car came a request for rent  :car',
        'text-2' => 'order page :order',
    ],
    'car-response-owner' => [
        'title' => 'Hello dear :name',
        'text-1' => 'Request Response',
        'text-2' => 'The owner responded to the request',
        'text-3' => 'order page :order',
    ],
    'car-booking' => [
        'owner' => [
            'title' => 'Hello dear :name',
            'text' => 'Car :car is booked 
            from :start to :end . 
            Please ensure that the vehicle is ready for the specified rental period. 
            All information on renting in the owner’s personal account.
            Regards, MyRentAuto Service.',
            'link_title' => 'Booking Page'
        ],

        'admin' => [
            'title' => 'Hello dear :name',
            'text_booking' => ':customer made payment for the reservation ',
            'text_full_pay' => ':customer made the full payment of the lease ',
            'text' => ' :car 
            from :start to :end  owner :owner .',
            'link_title' => 'Booking Page'
        ],
        'customer' => [
            'title' => 'Hello dear :name',
            'text_booking' => 'You have booked a car :car for rental',
            'text_full_pay' => 'You have paid car rental :car',
            'text' => ' from :start to :end . 
            All information and a receipt for paying for your reservation in your personal account, 
            section "History". 
            Thank you for your cooperation',
            'link_title' => 'Personal Area',
        ]

    ],
    'answer-request' => [
        'confirmed' => [
            'title' => 'Hello dear :name',
            'text' => 'Your request for a car reservation :car CONFIRMED.',
            'link_title' => 'complete reservation',
        ],
        'rejected' => [
            'title' => 'Hello dear :name',
            'text' => 'Your request for a car reservation :car is NOT CONFIRMED.
             Change the search parameters and select the current car by :link.',
        ]

    ],
    'canceled-booking-admin-owner' => [
        'title' => 'Hello dear :name',
        'text' => 'We hereby inform you that on the 
         car :car reserved from :start to :end the 
         client :customer canceled the payment of the reservation.',
        'link_title' => 'Booking Page'
    ],
    'rent' => 'Rent',
    'booking' => 'Booking',
    'hello' => 'Hello',
    'whoops' => 'Whoops',
    'regards' => 'Regards',
    'footer_text' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n" .
        'into your web browser: [:actionURL](:actionURL)',
];
