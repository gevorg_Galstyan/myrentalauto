<?php



Voyager::routes();

Route::get('/cars/calendar/{car}', 'CarController@calendar')->name('voyager.cars.calendar');

Route::get('/update/currency', 'CurrencyController@updateCurrency')->name('currency.update');
Route::get('/update/currency', 'CurrencyController@updateCurrency')->name('currency.update');
Route::get('/profile/confirmation', 'CurrencyController@saveCurrency')->name('currency.save');

Route::post('/profile/status/{id}', 'ProfileController@updateStatus')->name('profile.status');

Route::get('/cars-status/{car}/{status}', 'CarController@updateStatus')->name('voyager.cars.status');

Route::get('/user-block/{user}/{status}', 'UserController@block')->name('voyager.users.block');

Route::post('car-wrong-fill/{car}', 'CarController@wrongFill')->name('voyager.cars.wrong-fill');

//Route::get('/templates/{template}', function ($template){
//    return view('templates.'.$template);
//});

Route::get('orders/{order}/{act}', 'OrderController@sendResponse')->name('admin.orders.send_response');


// REOLY TICKETS

Route::post('ticket-reply/{ticket}', 'TicketController@reply')->name('admin.ticket.reply');

// NOTIFY

Route::get('car-notify', 'NotificationController@carChange')->name('admin.car-change.notify');
Route::get('owner-profile-notify', 'NotificationController@ownerProfileChange')->name('admin.owner-profile-change.notify');

// OWNER PROFILE

Route::get('check-owner-profile/{profile}', 'OwnerProfileController@checkProfile')->name('admin.check-owner-profile');

Route::post('send-user-email', 'UserController@sendUserEmail')->name('admin.send_user_email');

Route::get('tickets/close/{ticket}', 'ticketController@close')->name('voyager.tickets.close');

Route::get('car/history/{car}', 'CarController@showHistory')->name('voyager.cars.history.show');

//Route::post('cars', function()
//{
//    dd(($_SERVER["CONTENT_LENGTH"]));
//    if (isset($_SERVER["CONTENT_LENGTH"])
//        && ($_SERVER["CONTENT_LENGTH"]>((int)ini_get('post_max_size')*1024*1024)))
//    {
//        throw new PostSizeExceededException();
//    }
//})->name('voyager.cars.store');
