$(document).ready(function () {
    // $('#location').keyup(function () {
    //     if ($(this).val() && !ymaps) {
    //         $('.location-icon-container').css('display', 'block');
    //     }
    // });

    // $('#location').focus(function () {
    //     if (!ymaps) {
    //         script = document.createElement('script');
    //         script.src = "https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=195a01c1-11df-40b3-bf48-96004f1783eb&mode=debug";
    //         document.body.append(script); // (*)
    //         script.async = true;
    //         script.onload = function () {
    //             yMapIni();
    //         };
    //     }
    // });
    // function yMapIni() {
    // ymaps.ready(function () {
    //     // $('.location-icon-container').css('display', 'block');
    //     var suggestView = new ymaps.SuggestView('location', {
    //         offset: [0, 8],
    //         provider: {
    //             suggest: function (request, options) {
    //                 var y = $('#location').height() + 250;
    //                 if ($(window).scrollTop() < y) {
    //                     $('html, body').animate({scrollTop: y + 'px'}, 800)
    //                 }
    //                 var parseItems = ymaps.suggest((locale == 'en' ? "Belarus, " : "Беларусь, ") + request).then(function (items) {
    //                     for (var i = 0; i < items.length; i++) {
    //                         var displayNameArr = items[i].displayName.split(',');
    //
    //                         var newDisplayName = [];
    //                         for (var j = 0; j < displayNameArr.length; j++) {
    //                             if (displayNameArr[j].indexOf('район') == -1) {
    //                                 newDisplayName.push(displayNameArr[j]);
    //                             }
    //                         }
    //                         items[i].displayName = newDisplayName.join();
    //                     }
    //                     $('.location-icon-container').css('display', 'none');
    //                     return items;
    //                 });
    //                 return parseItems;
    //             }
    //         }
    //     });
    //     suggestView.events.add('select', function (event) {
    //         $('.currentLocationBtn').removeClass('currentLocActive')
    //     });
    // });
    // }

    function getList() {
        return $('.adj');
    }

    function build(arr) {
        for (i = 0; i < arr.length; i++) {
            var item = "<li class='adj'>";
            $(textSlider).append(item + arr[i] + "</li>");
        }
    }

    function slideIn(el) {

        var hold = home_slider_speed ;
        $(el).addClass('slide-in');
        if (home_slider_on_off) {
            setTimeout(function () {
                slideOut(el);
                slideIn($(el).next());
            }, hold);
        }
    }

    function slideOut(el) {
        $(el).removeClass('slide-in').addClass('slide-out');
        setTimeout(function () {
            change();
        }, 200);
    }

    function change() {
        var i = adjs.length - 1;
        $('.adj:eq(' + i + ')').after($('.slide-out').detach());
        $('.adj').removeClass('slide-out');
    }

    build(adjs);
    slideIn(getList()[0]);

    $('#stop').click(function () {
        // stop/start loop
        if ($(this).html() === "Start") {
            $(this).html("Stop");
            slideIn(getList()[0]);
        } else {
            stop = true;
            $(this).html("Start");
        }
    });
    let selector;
    let alGeoWarning = false;
    let datePicker = $('.search-date');

    $('.currentLocationBtn').click(function (e) {
        let location = ymaps.geolocation.get({provider: 'browser'});
        location.then(
            function (result) {
                let address = result.geoObjects.get(0).properties.get('text');
                $("#location").val(address);
                $('.currentLocationBtn').addClass('currentLocActive');
                datePicker.data('dateRangePicker').open();
                selector = 2;
            },
            function (err) {
                if (err.code == 1 && alGeoWarning) {
                    alert('Вы должны изменить настройки конфиденциальности вашего браузера, чтобы предоставить доступ к геолокации на веб-странице MyRentAuto!');
                } else {

                }
            }
        );

    });

    $('.open-home-filters').click(() => {
        $('.search-filter').slideToggle(function () {
            if (!$(this).is(':hidden')) {
                $('html, body').animate({
                    scrollTop: $(".search-filter").offset().top - 200
                }, 500);
            }
            $('.stage').css('color', 'red !important')
        })
    });

    $(document).on('click', '.ymaps-2-1-73-suggest-item', function (e) {
        e.stopPropagation();
        selector = 2;
    });

    $(document).on('click', function () {
        if (selector == 2) {
            selector = 0;
        }
    });
    $(document).on('keydown', function (e) {
        if (e.target.nodeName == 'BODY'){

            if (selector == 2) {
                selector = 3;
                $('#transmission-dropdown').dropdown("show");
                datePicker.data('dateRangePicker').close();
            }else if (selector == 3){
                $('#transmission-dropdown').dropdown("hide");
                $('#homeSearchForm').submit();
            }
        }
    });

    datePicker.click(function () {
        selector = 2;
    });

    $('#transmission-dropdown').on('show.bs.dropdown', function () {
        datePicker.data('dateRangePicker').close();
        if ($(window).scrollTop() < 250) {
            $('html, body').animate({scrollTop: '250px'})
        }
        if ($('.search-filter').is(':visible')) {
            $(".search-filter").slideUp();

            var animTags = $('.open-home-filters').children().find("g[id]");
            var animIds = animTags.map(function () {
                return this.id;
            }).get();

            for (var i = 0; i < animTags.length; i++) {
                //
                var animClasses = animIds[i] + '-animate';
                $(animTags[i]).removeClass(animClasses)
            }

            $('.open-home-filters').removeClass('activeBtn')
        }
    });

    $('.transmission-item').click(function () {
        let value = $(this).data('value');
        let text = $(this).text();
        $('input[name="transmission"]').val(value);
        $('.transmission-parent').text(text);
        $('#search-transmission-dropdown').dropdown('dispose');
    });

    $('#location-dropdown').on('show.bs.dropdown', function () {
        datePicker.data('dateRangePicker').close();
        if ($(window).scrollTop() < 250) {
            $('html, body').animate({scrollTop: '250px'})
        }
        if ($('.search-filter').is(':visible')) {
            $(".search-filter").slideUp();
            var animTags = $('.open-home-filters').children().find("g[id]");
            var animIds = animTags.map(function () {
                return this.id;
            }).get();

            for (var i = 0; i < animTags.length; i++) {
                //
                var animClasses = animIds[i] + '-animate';
                $(animTags[i]).removeClass(animClasses)
            }

            $('.open-home-filters').removeClass('activeBtn')
        }
    });

    $('.transmission-item').click(function () {
        let value = $(this).data('value');
        let text = $(this).text();
        $('input[name="transmission"]').val(value);
        $('.transmission-parent').text(text);
        $('#search-transmission-dropdown').dropdown('dispose');
    });

    $('#homeSearchForm').on('keydown', function (event) {
        if (event.which == 13 || event.key == "Enter") {
            event.stopPropagation();
            if (selector == 1) {
                event.preventDefault();
                datePicker.data('dateRangePicker').open();
                selector = 2;
            } else if (selector == 2) {
                datePicker.data('dateRangePicker').close();
                selector = 3;
                $('#search-transmission-dropdown').dropdown('show');
            } else {
                $('.transmission').blur();
                $('#homeSearchForm').submit()
            }
        }
    });

    $('#how-it-work-accordion .collapse, #how-it-work-accordion-owner .collapse').on('show.bs.collapse', function () {
        var content = $('.how-it-work-image-content');
        content.preloader();
        var img = $(this).data('image');
        content.find('a').attr('href', img.replace("647_0_75", "0_0_100"))
        content.find('img').attr('src', img)
        content.preloader('remove')
    });
    setTimeout(function () {
        axios.get(post_in_home + $(window).width()).
        then(function (response) {
            $('.post-content').html(response.data.html);
        });
    }, 9000);

    $('#homeSearchForm').submit(function (event) {
        event.preventDefault();
        let params = $("#homeSearchForm :input[value!=''][value!='.'][name!='hour']").serialize();
        let url = $('#homeSearchForm').attr('action');
        if (params){
            url += `?${params}`;
        }
        window.location.href = url;
    });

    $('#location-dropdown').on('show.bs.dropdown', function () {
        selector = 1;
        datePicker.data('dateRangePicker').close();
        if ($(window).scrollTop() < 250) {
            $('html, body').animate({scrollTop: '250px'})
        }
    });

    // $('#location-dropdown').on('hidden.bs.dropdown', function (event) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //     selector = 2;
    //     datePicker.data('dateRangePicker').open();
    //     return false;
    // });

    $('.location-item').click(function () {
        let value = $(this).data('value');
        let text = $(this).text();
        $('.location-parent').text(text);
        $('#homeSearchForm').attr('action', value);
        $('#search-location-dropdown').dropdown('dispose');
    });
});
$('.car-container').on('init', function (evt, slick, direction) {
    $('.car-container').removeClass('display-none')
});
$('.car-container').slick({
    dots: true,
    infinite: false,
    // centerMode: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: false,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 870,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 590,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$(document).on('click', function (e) {
    if (!$(e.target).parents('.search-filter').length &&
        !$(e.target).parents('.open-home-filters').length &&
        !$(e.target).parents('.search-date').length &&
        !$(e.target).hasClass('open-home-filters') &&
        $('.search-filter').is(':visible')) {
        $(".search-filter").slideUp();
        var animTags = $('.open-home-filters').children().find("g[id]");
        var animIds = animTags.map(function() { return this.id; }).get();

        for(var i = 0; i < animTags.length; i++) {
            //
            var animClasses = animIds[i] + '-animate';
            $(animTags[i]).toggleClass(animClasses)
        }
        $('.open-home-filters').removeClass('activeBtn');
    }
});
