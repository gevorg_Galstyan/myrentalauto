<?php

return [
    /*
    |--------------------------------------------------------------------------
    | User config
    |--------------------------------------------------------------------------
    |
    | Here you can specify voyager user configs
    |
    */

    'user' => [
        'add_default_role_on_register' => true,
        'default_role'                 => 'user',
        // Set `namespace` to `null` to use `config('auth.providers.users.model')` value
        // Set `namespace` to a class to override auth user model.
        // However make sure the appointed class must ready to use before installing voyager.
        // Otherwise `php artisan voyager:install` will fail with class not found error.
        'namespace'                    => null,
        'default_avatar'               => 'users/default.png',
        'redirect'                     => '/admin',
    ],

    /*
    |--------------------------------------------------------------------------
    | Controllers config
    |--------------------------------------------------------------------------
    |
    | Here you can specify voyager controller settings
    |
    */

    'controllers' => [
        'namespace' => 'App\Http\Controllers\Admin\Voyager',
    ],

    /*
    |--------------------------------------------------------------------------
    | Models config
    |--------------------------------------------------------------------------
    |
    | Here you can specify default model namespace when creating BREAD.
    | Must include trailing backslashes. If not defined the default application
    | namespace will be used.
    |
    */

    'models' => [
        'namespace' => 'App\\Models\\',
    ],

    /*
    |--------------------------------------------------------------------------
    | Storage Config
    |--------------------------------------------------------------------------
    |
    | Here you can specify attributes related to your application file system
    |
    */

    'storage' => [
        'disk' => env('FILESYSTEM_DRIVER', 'public'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Media Manager
    |--------------------------------------------------------------------------
    |
    | Here you can specify if media manager can show hidden files like(.gitignore)
    |
    */

    'hidden_files' => false,

    /*
    |--------------------------------------------------------------------------
    | Database Config
    |--------------------------------------------------------------------------
    |
    | Here you can specify voyager database settings
    |
    */

    'database' => [
        'tables' => [
            'hidden' => ['migrations', 'data_rows', 'data_types', 'menu_items', 'password_resets', 'permission_role', 'settings'],
        ],
        'autoload_migrations' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Multilingual configuration
    |--------------------------------------------------------------------------
    |
    | Here you can specify if you want Voyager to ship with support for
    | multilingual and what locales are enabled.
    |
    */

    'multilingual' => [
        /*
         * Set whether or not the multilingual is supported by the BREAD input.
         */
        'enabled' => true,

        /*
         * Select default language
         */
        'default' => 'ru',

        /*
         * Select languages that are supported.
         */
        'locales' => [
            'ru',
            'en',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Dashboard config
    |--------------------------------------------------------------------------
    |
    | Here you can modify some aspects of your dashboard
    |
    */

    'dashboard' => [
        // Add custom list items to navbar's dropdown
        'navbar_items' => [
            'voyager::generic.profile' => [
                'route'      => 'voyager.profile',
                'classes'    => 'class-full-of-rum',
                'icon_class' => 'voyager-person',
            ],
            'voyager::generic.home' => [
                'route'        => '/',
                'icon_class'   => 'voyager-home',
                'target_blank' => true,
            ],
            'voyager::generic.logout' => [
                'route'      => 'voyager.logout',
                'icon_class' => 'voyager-power',
            ],
        ],

        'widgets' => [
            'App\\Widgets\\UserDimmer',
            'App\\Widgets\\CarDimmer',

        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Automatic Procedures
    |--------------------------------------------------------------------------
    |
    | When a change happens on Voyager, we can automate some routines.
    |
    */

    'bread' => [
        // When a BREAD is added, create the Menu item using the BREAD properties.
        'add_menu_item' => true,

        // which menu add item to
        'default_menu' => 'admin',

        // When a BREAD is added, create the related Permission.
        'add_permission' => true,

        // which role add premissions to
        'default_role' => 'admin',
    ],

    /*
    |--------------------------------------------------------------------------
    | UI Generic Config
    |--------------------------------------------------------------------------
    |
    | Here you change some of the Voyager UI settings.
    |
    */

    'primary_color' => '#22A7F0',

    'show_dev_tips' => true, // Show development tip "How To Use:" in Menu and Settings

    // Here you can specify additional assets you would like to be included in the master.blade
    'additional_css' => [
        'https://use.fontawesome.com/releases/v5.8.1/css/all.css',
        'css/admin.css',
        'css/admin-app.css',
        'css/fullcalendar/daygrid/main.min.css',
        'css/fullcalendar/bootstrap/main.min.css',
        'css/fullcalendar/list/main.min.css',
        'css/daterangepicker.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/webui-popover/2.1.15/jquery.webui-popover.min.css',
        'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css',
        'css/fonts.css',
        'css/fullcalendar/main.min.css',
        'https://cdn.jsdelivr.net/npm/jspanel4@4.10.1/dist/jspanel.css',
//        'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
//        'css/style.css',
    ],

    'additional_js' => [
        'js/admin.js',
        'js/fileinput/piexif.min.js',
        'js/fileinput/purify.min.js',
        'js/fileinput/sortable.min.js',
        'js/fileinput/fileinput.min.js',
        'js/fileinput/locales/ru.js',
        'js/fullcalendar/main.min.js',
        'js/fullcalendar/interaction/main.min.js',
        'js/fullcalendar/bootstrap/main.min.js',
        'js/fullcalendar/daygrid/main.min.js',
        'js/fullcalendar/timegrid/main.min.js',
        'js/fullcalendar/list/main.min.js',
        'js/fullcalendar/luxon/main.min.js',
        'js/fullcalendar/locales/ru.js',
        'js/tinymce.js',
        'js/bootstrap-maxlength.js',
        'https://cdn.jsdelivr.net/npm/jspanel4@4.10.1/dist/jspanel.js',
    ],

    'googlemaps' => [
         'key'    => env('GOOGLE_MAPS_KEY', 'AIzaSyAJBEC8dYy9Ygzs9561-pBL04t5keLi0bQ'),
         'center' => [
             'lat' => env('GOOGLE_MAPS_DEFAULT_CENTER_LAT', '53.9'),
             'lng' => env('GOOGLE_MAPS_DEFAULT_CENTER_LNG', '27.56667'),
         ],
         'zoom' => env('GOOGLE_MAPS_DEFAULT_ZOOM', 13),
     ],

    'yandexmap' => [
         'key'    => env('YANDEX_MAPS_KEY', '195a01c1-11df-40b3-bf48-96004f1783eb'),
         'center' => [
             'lat' => env('YANDEX_MAPS_DEFAULT_CENTER_LAT', '53.9'),
             'lng' => env('YANDEX_MAPS_DEFAULT_CENTER_LNG', '27.56667'),
         ],
         'zoom' => env('YANDEX_MAPS_DEFAULT_ZOOM', 14),
     ],

    /*
    |--------------------------------------------------------------------------
    | Model specific settings
    |--------------------------------------------------------------------------
    |
    | Here you change some model specific settings
    |
    */

    'settings' => [
        // Enables Laravel cache method for
        // storing cache values between requests
        'cache' => false,
    ],

    // Activate compass when environment is NOT local
    'compass_in_production' => true,

    'media' => [
        // The allowed mimetypes to be uploaded through the media-manager.
        'allowed_mimetypes' => '*', //All types can be uploaded
        /*
        'allowed_mimetypes' => [
          'image/jpeg',
          'image/png',
          'image/gif',
          'image/bmp',
          'video/mp4',
        ],
        */
       //Path for media-manager. Relative to the filesystem.
       'path'                => '/',
       'show_folders'        => true,
       'allow_upload'        => true,
       'allow_move'          => true,
       'allow_delete'        => true,
       'allow_create_folder' => true,
       'allow_rename'        => true,
   ],
];
