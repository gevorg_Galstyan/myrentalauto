<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RestoreOwnerCar extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $car)
    {
        $this->user = $user;
        $this->car = $car;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Восстановление удаленной машины')
                    ->line('Пользователь <strong>'.$this->user->full_name.'</strong> отправил запрос на восстановление машины')
                    ->action('$this->car->title', route('voyager.cars.restore',['id' => $this->car->id]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => 'Восстановление удаленной машины',
            'message' => 'Пользователь <strong>'.$this->user->full_name.'</strong> отправил запрос на восстановление машины <br><a href="">'.$this->car->title.'</a><br><a href="'.route('voyager.cars.restore',['id' => $this->car->id]).'">Восстановить</a>'
        ]);
    }
}
