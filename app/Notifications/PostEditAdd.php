<?php

namespace App\Notifications;

use App\Models\Post;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PostEditAdd extends Notification
{
    use Queueable;

    private $author;
    private $post;
    private $act;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Post $post, User $author, $act)
    {
        $this->post= $post;
        $this->author= $author;
        $this->act= $act;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Здравствуйте, дорогой '.$notifiable->first_name)
            ->line('пользователь  '.$this->author->full_name.($this->act == 'edit' ? 'обновил': 'добавил ').'   пост ')
            ->action('Страница поста ', route('voyager.posts.show', $this->post))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => $this->author->full_name.($this->act == 'edit' ? ' Обновление ': ' Добавление '),
            'message' => 'пользователь  '.$this->author->full_name.($this->act == 'edit' ? 'обновил': 'добавил ').'   пост 
            <br>
            <a href="'.route('voyager.posts.show', $this->post).'">Страница поста</a>'
        ]);
    }
}
