<div class="col-lg-3 col-11 m-mob-auto filter-scrollbar main-content mb-3">
    <div class="sidebar-container">
    <div class="sidebar  ml-3 bg-white py-2 px-3 border-5 w-auto h-auto">
        <div class="d-flex flex-column justify-content-between align-items-center">
            <h2 class="fw-700 border-bottom own-h4 h4 pb-2 w-100">{{__('car.main')}}</h2>
            <a href="" data-text="{{__('car.closed_filters')}}" class="sidebar-open text-center">{{__('car.open_filters')}}</a>
        </div>
        <div class="own-scroll pr-2">
{{--            <div class="mob-db mb-4">--}}
{{--                <div class="single-filter">--}}
{{--                    <h6 class="color-light font-uppercase fw-700">--}}
{{--                        {{__('car.price')}}--}}
{{--                        <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="left"--}}
{{--                              title="{{__('car.currency_can_be')}}">--}}
{{--                            <i class="fas fa-info-circle"></i>--}}
{{--                        </span>--}}
{{--                    </h6>--}}
{{--                    <div class="form-group">--}}
{{--                        <div class="range-slider text-center">--}}
{{--                            <span id="rs-bullet"--}}
{{--                                  class="rs-label ff-st">{!! request()->price ? request()->price.' '. currency_symbol() : currency(getBackCurrency(500, 'USD'), '') !!} </span>--}}
{{--                            <input id="rs-range-line" class="rs-range car-params" type="range"--}}
{{--                                   data-id="#filter-price"--}}
{{--                                   name="price"--}}
{{--                                   form="searchForm"--}}
{{--                                   value="{!!request()->price ?? currency(getBackCurrency(500, 'USD'), '', false) !!}"--}}
{{--                                   min="0"--}}
{{--                                   max="{!! currency(getBackCurrency(500, 'USD'), '', false) !!}">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="single-filter d-md-none d-block ">--}}
{{--                    <h6 class="font-uppercase fw-700">{{__('car.transmission')}}</h6>--}}
{{--                    <div class="pl-2">--}}
{{--                        <label data-on="Авт." data-off="Мех." class="toggleSwitch checkbox transmission">--}}
{{--                            <!--<span class="fs-14 fw-700 ml-2">Автомат</span>-->--}}
{{--                            <input type="checkbox" class="car-params" form="searchForm"--}}
{{--                                   name="transmission"/>--}}
{{--                            <span class="knob"></span>--}}
{{--                        </label>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="mob-sidebar">
                <div class="single-filter">
                    <h2 class="color-light  fs-14 font-uppercase fw-700">
                        {{__('car.price')}}
                        <span class="k-pointer" data-toggle="tooltip" data-html="true"
                              title="{{__('car.currency_can_be')}}">
                            <i class="fas fa-info-circle"></i>
                        </span>
                    </h2>
                    <div class="form-group">
                        <div class="range-slider text-center">
                            <span id="rs-bullet"
                                  class="rs-label ff-st"><span class="ff-st">{!! request()->price ? request()->price.' '. currency_symbol() : currency(getBackCurrency(500, 'USD'), '') !!} </span></span>
                            <input id="rs-range-line" class="rs-range car-params" type="range"
                                   data-id="#filter-price"
                                   name="price"
                                   form="searchForm"
                                   value="{!!request()->price ?? currency(getBackCurrency(500, 'USD'), '', false) !!}"
                                   min="0"
                                   max="{!! currency(getBackCurrency(500, 'USD'), '', false) !!}">
                        </div>
                    </div>
                </div>
                {{--        RENT        --}}
                <div class="single-filter ">
                    <h2 class="font-uppercase  fs-14 fw-700">{{__('car.rent')}}</h2>
                    <div class="pl-2">
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{__('car.instantly')}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0">
                                <input type="checkbox" form="searchForm"
                                       class="car-params"
                                       id="filter-instant-booking"
                                       name="instant_booking" {{request()->input('instant_booking')  ? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{__('car.request')}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0">
                                <input type="checkbox" class="car-params" form="searchForm"
                                       id="filter-on-request"
                                       name="on_request" {{request()->input('on_request') ? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="single-filter ">
                    <div class="btn-group car-type justify-content-between">
                        <h2 class="font-uppercase  fs-14 fw-700 m-0">{{__('car.year')}} </h2>
                        <a class="dropdown-toggle py-0 px-2 fs-14 fw-700" data-toggle="dropdown" href="#">
                            @if(request()->input('age'))
                                {{__('general.less_years', ['year' => request()->input('age')])}}
                            @else
                                {{ __('general.all')}}

                            @endif
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="">
                                    {{__('general.all')}}
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="5">
                                    {{__('general.less_years', ['year' => 5])}}
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="10">
                                    {{__('general.less_years', ['year' => 10])}}
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="15">
                                    {{__('general.less_years', ['year' => 15])}}
                                </a>
                            </li>

                        </ul>
                        <input type="hidden" form="searchForm" class="type-select car-params" name="age"
                               id="filter-age"
                               value="{{request()->input('age')}}">
                    </div>
                </div>
                <hr>
                <div class="single-filter ">
                    <div class="btn-group car-type justify-content-between">
                        <h2 class="font-uppercase  fs-14 fw-700 m-0">{{__('car.seats')}}</h2>
                        <a class="dropdown-toggle py-0 px-2 fs-14 fw-700" data-toggle="dropdown" href="#">
                            @if(request()->input('seats'))
                                {{ request()->input('seats')}}
                            @else
                                {{ __('general.all')}}

                            @endif
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="">
                                    {{__('general.all')}}
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="2">
                                    2
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="3">
                                    3
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="4">
                                    4
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="5">
                                    5
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="6">
                                    6
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="7">
                                    7
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="8">
                                    8
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="9">
                                    9
                                </a>
                            </li>
                        </ul>
                        <input type="hidden" form="searchForm" class="type-select car-params" name="seats"
                               id="filter-seats"
                               value="{{request()->input('seats')}}">
                    </div>
                </div>
                <hr>
                <div class="single-filter ">
                    <div class="btn-group car-type justify-content-between">
                        <h2 class="font-uppercase  fs-14 fw-700 m-0">{{__('car.door_count')}}</h2>
                        <a class="dropdown-toggle py-0 px-2 fs-14 fw-700" data-toggle="dropdown" href="#">
                            @if(request()->input('seats'))
                                {{ request()->input('seats')}}
                            @else
                                {{ __('general.all')}}

                            @endif
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="">
                                    {{__('general.all')}}
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="2">
                                    2
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="3">
                                    3
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="4">
                                    4
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="5">
                                    5
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="6">
                                    6
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="7">
                                    7
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="8">
                                    8
                                </a>
                            </li>
                            <li class="dropdown-item p-0">
                                <a href="#" class="d-block py-1 px-3 fs-14" data-select="9">
                                    9
                                </a>
                            </li>
                        </ul>
                        <input type="hidden" form="searchForm" class="type-select car-params" name="doors"
                               id="filter-doors"
                               value="{{request()->input('doors')}}">
                    </div>
                </div>
                <hr>
                <div class="single-filter ">
                    <h2 class="font-uppercase  fs-14 fw-700">{{__('car.fuel')}}</h2>
                    <div class="pl-2">
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.petrol')}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0" for="filter-petrol">
                                <input type="checkbox" form="searchForm"
                                       class="car-params"
                                       id="filter-petrol"
                                       value="petrol"
                                       name="fuel[]" {{in_array('petrol', array_values (request()->fuel??[])) ? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.diesel')}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0">
                                <input type="checkbox" class="car-params" form="searchForm"
                                       id="filter-diesel"
                                       value="diesel"
                                       name="fuel[]" {{in_array('diesel', array_values (request()->fuel??[])) ? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.electric')}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0">
                                <input type="checkbox" class="car-params" form="searchForm"
                                       id="filter-electric"
                                       value="electric"
                                       name="fuel[]" {{in_array('electric', array_values (request()->fuel??[])) ? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{__('general.hybrid')}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0">
                                <input type="checkbox" class="car-params" form="searchForm"
                                       id="filter-hybrid"
                                       value="hybrid"
                                       name="fuel[]" {{in_array('hybrid', array_values(request()->fuel??[])) ? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                    </div>

                </div>
                <hr>

                <div class="single-filter ">
                    <div class="">
                        <h2 class="font-uppercase   fs-14 fw-700 m-0 mb-3">{{__('car.consumption')}}</h2>

                        <div class="d-flex align-items-center justify-content-between flex-wrap flex-row">
                            <div class="col-6">
                                <input type="text" name="fuel_consumption_min" form="searchForm"
                                       class="w-100 fs-13 px-2 car-params-input"
                                       id="filter-fuel_consumption_min"
                                       value="{{request()->input('fuel_consumption_min')??''}}"
                                       placeholder="{{__('car.min', ['i' => 1.4])}}"/>
                            </div>
                            <div class="col-6">
                                <input type="text" name="fuel_consumption_max" form="searchForm"
                                       class="w-100 fs-13 px-2 car-params-input"
                                       id="filter-fuel_consumption_max"
                                       value="{{request()->input('fuel_consumption_max')??''}}"
                                       placeholder="{{__('car.max', ['i' => 3.5])}}"/>
                            </div>
                        </div>

                    </div>
                </div>

                <hr>
                <div class="single-filter text-center">
                    <a href="" data-text="{{__('car.less')}}" class="collapse-button more-filter-open ">{{__('car.more')}}</a>
                </div>
            </div>

            <div class="more-filters">
                @foreach($filters as $filter)
                    <div class="single-filter ">
                        <div class="position-relative d-flex align-items-center justify-content-between mb-2">
                            <span class="fs-13 fw-700 ml-2 fil-name">{{$filter->myTranslate()->title}}</span>
                            <label data-on="{{__('general.yes')}}" data-off="{{__('general.no')}}"
                                   class="toggleSwitch checkbox mb-0">
                                <input type="checkbox" form="searchForm"
                                       id="filter-attributes-{{$filter->id}}"
                                       class="car-params" value="{{$filter->id}}"
                                       name="attributes[]" {{in_array($filter->id, array_values (request()->input('attributes')??[]))? 'checked':''}}/>
                                <span class="knob"></span>
                            </label>
                        </div>
                    </div>
                    {!! $loop->last ? '' : '<hr>' !!}
                @endforeach
            </div>
        </div>
    </div>
    </div>
</div>
