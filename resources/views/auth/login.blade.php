@extends('layouts.app')
@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop
@section('content')
<div class="container">
    <div class="auth login m-5">
        <h1 class="text-center fw-600 fs-30 sub-heading text-uppercase m-0 mb-2">{{getMetaTags('login_page') ?getMetaTags('login_page')->translate()->h1 : ''}}</h1>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card p-5">
                    <div class="modal-header text-center justify-content-center pb-1">
                        <h4 class="modal--title">{{__('auth.login_with')}}</h4>
                    </div>
                    <div class="modal-body py-1">
                        <div class="box">
                            <div class="content p-2">
                                <div class="social d-flex align-items-center justify-content-center mb-3">
                                    <a class="facebook-login social-login mr-2" href="{{route('oauth',['facebook'])}}" rel="nofollow">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a class="vk-login social-login" href="{{route('oauth',['vkontakte'])}}" rel="nofollow">
                                        <i class="fab fa-vk"></i>
                                    </a>
                                </div>
                                <div class="division">
                                    <div class="line l"></div>
                                    <span>{{__('auth.or')}}</span>
                                    <div class="line r"></div>
                                </div>
                                <div class="error"></div>
                                <div class="form">
                                    <form id="loginForm" method="POST" action="{{ route('login') }}" accept-charset="UTF-8">
                                        @csrf
                                        <input id="email_l" type="email"
                                               class="form-control mb-2"
                                               name="email" value="{{ old('email') }}" required
                                               placeholder="{{__('auth.email')}}">

                                        <input id="password_l" type="password"
                                               class="form-control "
                                               name="password" placeholder="{{__('auth.password')}}" required>
                                        <div class="d-flex align-items-center justify-content-center">

                                            <label for="remember" class="d-block ml-2 mb-0 w-100 font-weight-normal">
                                                <input class="position-relative opacity-1" type="checkbox" name="remember"
                                                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                {{__('auth.remember_me')}}
                                            </label>
                                        </div>

                                        <div class="text-center mt-2">
                                            <button type="submit"
                                                    class="own-btn text-capitalize fs-16 d-inline-block btn-shadow ora btn-submit-login">
                                                {{__('auth.login')}}
                                            </button>
                                        </div>
                                        <br>
                                        @if (Route::has('password.request'))
                                            <div class="text-center mt-2">
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('auth.forgot_password') }}
                                                </a>
                                            </div>
                                        @endif
                                        <div class="alert-danger d-inline-block"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer mt-0 pt-0">
                        <div class="forgot login-footer">
                             <a
                                href="{{ route('register') }}">{{__('auth.create_an_account')}}</a>

                        </div>
                        <div class="forgot rest-password-footer" style="display:none">
                             <a
                                href="{{ route('register') }}">{{__('auth.create_an_account')}}</a>

                        </div>
                        <div class="forgot register-footer" style="display:none">
                            <span>{{__('auth.already_account')}}</span>
                            <a href="{{ route('login') }}">{{__('auth.login')}}</a>
                        </div>
                    </div>

                </div>
                {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}
                {{--<form method="POST" action="{{ route('login') }}">--}}
                {{--@csrf--}}

                {{--<div class="form-group row">--}}
                {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                {{--<div class="col-md-6">--}}
                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                {{--@if ($errors->has('email'))--}}
                {{--<span class="invalid-feedback" role="alert">--}}
                {{--<strong>{{ $errors->first('email') }}</strong>--}}
                {{--</span>--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="form-group row">--}}
                {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                {{--<div class="col-md-6">--}}
                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                {{--@if ($errors->has('password'))--}}
                {{--<span class="invalid-feedback" role="alert">--}}
                {{--<strong>{{ $errors->first('password') }}</strong>--}}
                {{--</span>--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="form-group row">--}}
                {{--<div class="col-md-6 offset-md-4">--}}
                {{--<div class="form-check">--}}
                {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                {{--<label class="form-check-label" for="remember">--}}
                {{--{{ __('Remember Me') }}--}}
                {{--</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="form-group row mb-0">--}}
                {{--<div class="col-md-8 offset-md-4">--}}
                {{--<button type="submit" class="btn btn-primary">--}}
                {{--{{ __('Login') }}--}}
                {{--</button>--}}

                {{--@if (Route::has('password.request'))--}}
                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                {{--{{ __('Forgot Your Password?') }}--}}
                {{--</a>--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>
@endsection
