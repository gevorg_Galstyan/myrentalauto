<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Voyager\VoyagerBaseController;
use Illuminate\Http\Request;

class FaqController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        // get the slug, ex. 'posts', 'pages', etc.
        $slug = $this->getslug($request);

        // get the datatype based on the slug
        $datatype = voyager::model('datatype')->where('slug', '=', $slug)->first();

        // check permission
        $this->authorize('browse', app($datatype->model_name));

        $getter = $datatype->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $datatype->server_side ? array_keys(schemamanager::describetable(app($datatype->model_name)->gettable())->toarray()) : '';
        $orderby = $request->get('order_by', $datatype->order_column);
        $sortorder = $request->get('sort_order', null);
        $usessoftdeletes = false;
        $showsoftdeleted = false;
        $ordercolumn = [];
        if ($orderby) {
            $index = $datatype->browserows->where('field', $orderby)->keys()->first() + 1;
            $ordercolumn = [[$index, 'desc']];
            if (!$sortorder && isset($datatype->order_direction)) {
                $sortorder = $datatype->order_direction;
                $ordercolumn = [[$index, $datatype->order_direction]];
            } else {
                $ordercolumn = [[$index, 'desc']];
            }
        }

        // next get or paginate the actual content from the model that corresponds to the slug datatype
        if (strlen($datatype->model_name) != 0) {
            $model = app($datatype->model_name);

            if ($datatype->scope && $datatype->scope != '' && method_exists($model, 'scope'.ucfirst($datatype->scope))) {
                $query = $model->{$datatype->scope}();
            } else {
                $query = $model::select('*');
            }

            // use withtrashed() if model uses softdeletes and if toggle is selected
            if ($model && in_array(softdeletes::class, class_uses($model)) && \auth::user()->can('delete', app($datatype->model_name))) {
                $usessoftdeletes = true;

                if ($request->get('showsoftdeleted')) {
                    $showsoftdeleted = true;
                    $query = $query->withtrashed();
                }
            }

            // if a column has a relationship associated with it, we do not want to show that field
            $this->removerelationshipfield($datatype, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'like';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderby && in_array($orderby, $datatype->fields())) {
                $querysortorder = (!empty($sortorder)) ? $sortorder : 'desc';
                $datatypecontent = call_user_func([
                    $query->orderby($orderby, $querysortorder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $datatypecontent = call_user_func([$query->latest($model::created_at), $getter]);
            } else {
                $datatypecontent = call_user_func([$query->orderby($model->getkeyname(), 'desc'), $getter]);
            }

            // replace relationships' keys for labels and create read links if a slug is provided.
            $datatypecontent = $this->resolverelations($datatypecontent, $datatype);
        } else {
            // if model doesn't exist, get data from table name
            $datatypecontent = call_user_func([db::table($datatype->name), $getter]);
            $model = false;
        }

        // check if bread is translatable
        if (($ismodeltranslatable = is_bread_translatable($model))) {
            $datatypecontent->load('translations');
        }

        // check if server side pagination is enabled
        $isserverside = isset($datatype->server_side) && $datatype->server_side;

        // check if a default search key is set
        $defaultsearchkey = $datatype->default_search_key ?? null;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return voyager::view($view, compact(
            'datatype',
            'datatypecontent',
            'ismodeltranslatable',
            'search',
            'orderby',
            'ordercolumn',
            'sortorder',
            'searchable',
            'isserverside',
            'defaultsearchkey',
            'usessoftdeletes',
            'showsoftdeleted'
        ));
    }
}
