<div class="row justify-content-between bg-white p-2  mr-lg-0  border-5">
    <div class="btn-group car-type auto-type">
        <span class="fw-600">{{__('car.type')}}:</span>
        <a class="btn btn-primary dropdown-toggle py-0 px-2 fs-14 fw-700" data-toggle="dropdown"
           href="#">
            @if(request()->input('type'))
                @if(count(request()->input('type')) > 1)
                    Выбрано: {{count(request()->input('type'))}}
                @else
                    {{$car_types->where('id', request()->input('type')[0])->first()->myTranslate()->name}}
                @endif
            @else
                {{ __('general.all')}}

            @endif

            {{--            {{request()->input('type') ? $car_types->where('id', request()->input('type'))->first()->myTranslate()->name : __('general.all') }}--}}
            <span class="color-light fs-12">({{$cars->total()}})</span> <span class="caret"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-right ">

            <li class="dropdown-item p-0">
                <a href="#" class="d-block py-1 px-3 fs-14">
                    {{__('car.all')}}
                    <span class="color-light fs-12">({{request()->data->count()}})</span>
                </a>
            </li>

            @foreach($car_types as $type)
                <li class="dropdown-item car-types p-0" data-value="{{$type->id}}" data-target="type-{{$type->id}}">
                    <div class="d-block py-1 px-3 fs-14 d-flex" tabIndex="-1">
                        <input type="checkbox" {{in_array($type->id, request()->input('type')??[]) ? 'checked' : ''}}/>&nbsp;{{$type->myTranslate()->name}}
                        <span class="color-light fs-12">
                            {{request()->data->where('car_type_id', $type->id)->count()}}
                        </span>
                    </div>
                </li>
            @endforeach
                <a href="#" class="search-types">Применить</a>
        </ul>
        @foreach(request()->input('type')??[] as $item)
            <input type="hidden" id="type-{{$item}}" form="searchForm" class="type-select" name="type[]"
                   value="{{$item}}">
        @endforeach
    </div>
    <div class="filter-group">
        <div class="sort-price">
            <div class="btn-group car-type">
                <span class="fw-600">{{__('car.sort_by')}}:</span>
                <a class="btn btn-primary dropdown-toggle py-0 px-2 fs-14 fw-600" data-toggle="dropdown"
                   href="#"> {{request()->input('order') ? __('general.order.'.request()->input('order')):__('general.all')}}
                    <span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    {{--                    <li class="dropdown-item p-0">--}}
                    {{--                        <a href="#" class="d-block py-1 px-3 fs-14" data-select="">{{__('general.all')}}</a>--}}
                    {{--                    </li>--}}
                    <li class="dropdown-item p-0">
                        <span
                            class="d-block py-1 px-3 color-black fw-700 text-right border-bottom fs-13">{{__('general.order.price')}}</span>
                    </li>
                    <li class="dropdown-item p-0">
                        <a href="#" class="d-block py-1 px-3 fs-14" data-select="asc">{{__('general.order.asc')}}</a>
                    </li>
                    <li class="dropdown-item p-0">
                        <a href="#" class="d-block py-1 px-3 fs-14" data-select="desc">{{__('general.order.desc')}}</a>
                    </li>
                    <li class="dropdown-item p-0">
                        <span
                            class="d-block py-1 px-3 color-black fw-700 text-right border-bottom fs-13">{{__('general.order.place')}}</span>
                    </li>
                    <li class="dropdown-item p-0">
                        <a href="#" class="d-block py-1 px-3 fs-14"
                           data-select="immediate">{{__('general.order.immediate')}}</a>
                    </li>
                    {{--                    <li class="dropdown-item p-0">--}}
                    {{--                        <a href="#" class="d-block py-1 px-3 fs-14"--}}
                    {{--                           data-select="further">{{__('general.order.further')}}</a>--}}
                    {{--                    </li>--}}
                    <li class="dropdown-item p-0">
                        <span
                            class="d-block py-1 px-3 color-black fw-700 text-right border-bottom fs-13">{{__('general.order.rating')}}</span>
                    </li>
                    <li class="dropdown-item p-0">
                        <a href="#" class="d-block py-1 px-3 fs-14"
                           data-select="rating">{{__('general.order.rating')}}</a>
                    </li>
                </ul>
                <input type="hidden" form="searchForm" class="type-select car-params" name="order"
                       id="filter-order"
                       value="{{request()->input('order')}}">
            </div>
        </div>
        <div class="sort-sectional"></div>
    </div>
</div>

<div class="row justify-content-between align-items-center flex-wrap mt-3">
    <div class="tags">
        @if (request()->input('instant_booking'))
            <button type="button" data-status="#filter-instant-booking" class="btn btn-xs btn-tag fs-12 mr-1">
                <span class="mr-2">{{__('car.instantly')}}</span>
                <span class="close-filter" data-type="checkbox" data-target="#filter-instant-booking"><i
                        class="fas fa-times-circle"></i></span>
            </button>
        @endif
        @if (request()->input('on_request'))
            <button type="button" data-status="#filter-on-request" class="btn btn-xs btn-tag fs-12 mr-1">
                <span class="mr-2">{{__('car.request')}}</span>
                <span class="close-filter" data-type="checkbox" data-target="#filter-on-request"><i
                        class="fas fa-times-circle"></i></span>
            </button>
        @endif
        @if (is_array(request()->fuel))
            @foreach (request()->fuel as $fuel)
                <button type="button" data-status="#filter-{{$fuel}}" class="btn btn-xs btn-tag fs-12 mr-1">
                    <span class="mr-2">{{__('general.'. $fuel)}}</span>
                    <span class="close-filter" data-type="checkbox" data-target="#filter-{{$fuel}}"><i
                            class="fas fa-times-circle"></i></span>
                </button>
            @endforeach
        @endif

        @if (request()->input('transmission'))
            <button type="button" data-status="#filter-transmission" class="btn btn-xs btn-tag fs-12 mr-1">
                <span class="mr-2">{{__('general.'.request()->input('transmission'))}}</span>
                <span class="close-filter" data-type="transmission" data-target="#filter-transmission"><i
                        class="fas fa-times-circle"></i></span>
            </button>
        @endif
        @foreach(request()->input('type')??[] as $k => $type)
            <button type="button" data-status="#type-{{$type}}"
                    class="btn btn-xs btn-tag fs-12 mr-1">
                <span
                    class="mr-2">{{$car_types->where('id', request()->input('type')[$k])->first()->myTranslate()->name }}</span>
                <span class="close-filter" data-type="input-type-checkbox" data-target="#type-{{$type}}">
                             <i class="fas fa-times-circle"></i>
                        </span>
            </button>
        @endforeach
        @if(request()->price && request()->price < currency(getBackCurrency(500, 'USD'), '', false))
            <button type="button" data-status="#filter-price" class="btn btn-xs btn-tag fs-12 mr-1">
                <span
                    class="mr-2">{!!  __('general.to') .' - '. request()->price .' <span class="ff-st">'. currency_symbol().'</span/'!!}</span>
                <span class="close-filter" data-type="range" data-target="#filter-price"><i
                        class="fas fa-times-circle"></i></span>
            </button>
        @endif


        @if (request()->input('seats'))
            <button type="button" data-status="#filter-seats"
                    class="btn btn-xs btn-tag fs-12 mr-1">
                <span class="mr-2">{{__('general.seats', ['seats' => request()->input('seats')])}}</span>
                <span class="close-filter" data-dropdown="true" data-type="input" data-target="#filter-seats">
                    <i class="fas fa-times-circle"></i>
                </span>
            </button>
        @endif
        @if (request()->input('doors'))
            <button type="button" data-status="#filter-doors"
                    class="btn btn-xs btn-tag fs-12 mr-1">
                <span class="mr-2">{{request()->input('doors')}} {{__('car.doors')}}</span>
                <span class="close-filter" data-dropdown="true" data-type="input" data-target="#filter-doors">
                    <i class="fas fa-times-circle"></i>
                </span>
            </button>
        @endif
        @if (request()->input('age'))
            <button type="button" data-status="#filter-type"
                    class="btn btn-xs btn-tag fs-12 mr-1">
                <span class="mr-2">{{__('general.less_years', ['year' => request()->input('age')])}}</span>
                <span class="close-filter" data-type="input" data-dropdown="true" data-target="#filter-age">
                    <i class="fas fa-times-circle"></i>
                </span>
            </button>
        @endif
        @if (is_array(request()->input('attributes')))
            @foreach (request()->input('attributes') as $attribute)
                <button type="button" data-status="#filter-attributes-{{$attribute}}"
                        class="btn btn-xs btn-tag fs-12 mr-1">
                    <span class="mr-2">{{$filters->where('id', $attribute)->first()->myTranslate()->title}}</span>
                    <span class="close-filter" data-type="checkbox" data-target="#filter-attributes-{{$attribute}}"><i
                            class="fas fa-times-circle"></i></span>
                </button>
            @endforeach
        @endif
        @if (request()->input('fuel_consumption_min'))
            <button type="button" data-status="#filter-fuel_consumption_min"
                    class="btn btn-xs btn-tag fs-12 mr-1">
                <span
                    class="mr-2">{{__('general.more') .' '.request()->input('fuel_consumption_min') . ' ' . __('general.l').'.'}}</span>
                <span class="close-filter" data-type="input" data-target="#filter-fuel_consumption_min"><i
                        class="fas fa-times-circle"></i></span>
            </button>
        @endif
        @if (request()->input('fuel_consumption_max'))
            <button type="button" data-status="#filter-fuel_consumption_max"
                    class="btn btn-xs btn-tag fs-12 mr-1">
                <span
                    class="mr-2">{{__('general.less') .' '.request()->input('fuel_consumption_max') . ' ' . __('general.l').'.'}}</span>
                <span class="close-filter" data-type="input" data-target="#filter-fuel_consumption_max"><i
                        class="fas fa-times-circle"></i></span>
            </button>

        @endif

    </div>


    <button class="map-button">{{__('car.open_map')}}
        <img src="{{asset('storage/img/icons/location.png')}}" class="img-fluid ml-2" alt="location">
    </button>
</div>
<div id="map"
     class="row justify-content-between bg-white my-3  mr-lg-0  border-5" {!! request()->input('open_map') ? 'style="display: block !important;"':'' !!} ></div>
<!--<h2 class="text-center mb-5 fw-700 sub-heading">Машины в прокате</h2>-->
<div class="card-deck search-cars mt-2   flex-wrap mt-3 {{--justify-content-between--}}  mt-3  mr-lg-0  ">
    {{--@foreach($cars as $car)--}}

    @forelse($cars as $index => $car)
        @include('cars.car-cart',['index' => $index%3])
    @empty
        <h4 class="text-center w-100 align-middle my-auto">
            {{__('car.empty_result')}}
        </h4>
    @endforelse

</div>
<div class="car-paginate">

    {{$cars->links()}}
</div>

@if (request()->ajax())
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function init() {

            var suggestView = new ymaps.SuggestView('location', {
                offset: [0, 8],
                provider: {
                    suggest: function (request, options) {
                        var parseItems = ymaps.suggest((locale == 'en' ? "Belarus, " : "Беларусь, ") + request).then(function (items) {
                            for (var i = 0; i < items.length; i++) {
                                var displayNameArr = items[i].displayName.split(',');

                                var newDisplayName = [];
                                for (var j = 0; j < displayNameArr.length; j++) {
                                    if (displayNameArr[j].indexOf('район') == -1) {
                                        newDisplayName.push(displayNameArr[j]);
                                    }
                                }
                                items[i].displayName = newDisplayName.join();
                            }
                            return items;
                        });
                        return parseItems;
                    }
                }
            });


            suggestView.events.add(["add", "select"], function (event) {
                $('.currentLocationBtn').removeClass('currentLocActive')
                getCars();
            });

            clusterer = new ymaps.Clusterer({
                preset: 'islands#blueCircleIcon',
                groupByCoordinates: false,
                clusterDisableClickZoom: false,
                clusterHideIconOnBalloonOpen: false,
                geoObjectHideIconOnBalloonOpen: false,
                hasBalloon: false,
            });


            var center = [{{request()->input('lat')}}, {{request()->input('lng')}}];
            var geolocation = ymaps.geolocation;
            var myMap = new ymaps.Map('map', {
                    center: center,
                    zoom: 12,
                    controls: []
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаем коллекцию.
                // myCollection = new ymaps.GeoObjectCollection(),
                // Создаем массив с данными.
                myPoints = [@foreach(request()->get('map-cars')??[] as $car) {
                    coords: [{{$car->getCoordinates()[0]['lat']}} , {{$car->getCoordinates()[0]['lng']}}],
                    title: '<strong> {{$car->title}} </strong>',
                    text: '<div class="map-car min-width-320">' +
                        '<div class="map-car-body d-flex align-items-center">' +
                        '<div class="map-car-img-part"><img src="{{Voyager::image($car->image_1)}}" alt="{{$car->title}}" title="{{$car->title}}" class="rounded" width="100px"></div>' +
                        '<div class="map-car-info-part">' +
                        '<div class="d-flex align-items-center  ml-3">' +
                        '<span>{{$car->title}}</span>' +
                        '</div>' +
                        '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
                        '<div class="stars-info d-flex align-items-center">' +
                        '<div class="stars stars--large mr-1 fs-16">' +
                        '<span style="width: {{$car->avg_score * 20}}%"></span>' +
                        '</div>' +
                        '<span>({{$car->reviews->count()}})</span>' +
                        '</div>' +
                        '<span class="px-1">|</span>' +
                        '<div class="ml-1"> {{$car->age}}</div>' +
                        '<span class="px-1">|</span>' +
                        '<div class="ml-1"> {{$car->number_of_seats}} мест </div>' +
                        '</div>' +
                        '<div class="d-flex align-items-center fs-12 color-light ml-3">' +
                        '<p class="fs-12 color-light mb-2">\n' +
                        '<img src="{{asset('storage/img/icons/location.png')}} " alt="location" class="inline-img-width" style="width: 17px !important"/>\n' +
                        '<span>{{distanceM($car->distance)}}</span>\n' +
                        '<span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="От выбранного места до этого автомобиля {{distanceM($car->distance)}}"> ' +
                        '<i class="fas fa-info-circle"></i></span>\n' +
                        '</p>' +
                        '<div class="text-center mt-1 ml-3">' +
                        '<a href="{{route('car.single', array_merge(['slug' => $car->slug], request()->only('location', 'date', 'attributes')))}}" class="own-btn rounded d-inline-block ora px-2 py-1 fs-12"><span class="ff-st">{!! currency($car->price_1) !!}</span> / {{__('car.day')}}</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                } {!! $loop->last ? '' : ',' !!}  @endforeach];
            // Заполняем коллекцию данными.
            geoObjects = [];
            for (var i = 0; i < myPoints.length; i++) {
                var point = myPoints[i];
                geoObjects[i] = new ymaps.Placemark(
                    point.coords, {
                        balloonContentBody: point.text,
                        // clusterCaption: '<strong> ' + point.title + ' </strong>',
                    }, {
                        preset: 'islands#blueAutoIcon'
                    }
                );
            }

            // clusterer.options.set({
            //     gridSize: 60,
            //     clusterDisableClickZoom: true
            // });

            clusterer.add(geoObjects);

            //Кнопки плюс - минус на карте
            myMap.controls.add('zoomControl', {
                size: 'small',
                float: 'none',
                position: {
                    bottom: '50px',
                    right: '30px'
                }
            });


            // myMap.setBounds(clusterer.getBounds(), {
            //     checkZoomRange: true
            // });
            var myPlacemark = new ymaps.Placemark(center, {}, {
                preset: 'islands#redDotIcon'
            });

            myMap.geoObjects.add(myPlacemark);
            // Добавляем коллекцию меток на карту.
            myMap.geoObjects.add(clusterer);
            myMap.behaviors.disable('scrollZoom');
            // Создаем круг.
            var myCircle = new ymaps.Circle([
                // Координаты центра круга.
                [center[0], center[1]],
                // Радиус круга в метрах.
                2000
            ], {
                // Описываем свойства круга.
                // Содержимое балуна.
                balloonContent: "<span style='vertical-align: sub' class='fw-600'>Радиус круга - 2 км</span>",
                // Содержимое хинта.
                hintContent: ""
            }, {
                // Задаем опции круга.
                // Включаем возможность перетаскивания круга.
                draggable: false,
                // Цвет заливки.
                // Последний байт (77) определяет прозрачность.
                // Прозрачность заливки также можно задать используя опцию "fillOpacity".
                fillOpacity: 0.5,
                opacity: 0.5,
                fillColor: "#f6993f",
                // Цвет обводки.
                strokeColor: "#990066",
                // Прозрачность обводки.
                strokeOpacity: 0.8,
                // Ширина обводки в пикселях.
                strokeWidth: 0
            });
            // Добавляем круг на карту.
            myMap.geoObjects.add(myCircle)

        }

        @if(request()->input('open_map'))
        ymaps.ready(init);
        @endif
    </script>
@endif





