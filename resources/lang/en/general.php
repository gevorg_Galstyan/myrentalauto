<?php
return [
    'title' => 'Аренда авто без водителя и прокат от локальных прокатчиков в Беларусии',
    'transmission' => 'Transmission',
    'automatic' => 'Auto',
    'manual' => 'Manual',
    'petrol' => 'Petrol',
    'diesel' => 'Diesel',
    'gas' => 'Gas',
    'electric' => 'Electric',
    'hybrid' => 'Hybrid',
    'yes' => 'Yes',
    'no' => 'No',
    'price' => 'Price',
    'to' => 'To',
    'l' => 'L',
    'more' => 'more',
    'less' => 'less',
    'all' => 'All',
    'less_years' => 'Less than :year years',
    'seats' => ':seats Seats',
    'order' => [
        'price' => 'Price',
        'asc' => 'Ascending',
        'desc' => 'Descending',
        'place' => 'Place',
        'immediate' => 'Immediate',
        'further' => 'Further',
        'rating' => 'Rating',
        'request' => 'Request',
        'response' => 'Response',
        'canceled' => 'Canceled',
        'user' => 'User',
        'admin' => 'Администратор',
        'owner' => 'Owner',
        'confirm' => 'Confirm',
        'rejected' => 'Reject',
        'requested' => 'Requested',
        'completed' => 'Completed',
        'paid' => 'Paid',
        'fully_paid' => 'Paid',
        'pending' => 'No Paid',
        'book_again' => 'Book again',
        'cancel' => 'Cancel',
        'details' => 'Details',
        'failed' => 'Failed',
        'confirmed' => 'Confirmed',
        'no_paid' => 'No Paid',
        'expired' => 'Expired',
        'servicing_bank' => 'Servicing bank',
        'terminal_id' => 'Terminal id',
        'order_id' => 'Order ID',
        'transaction_status' => 'Transaction Status',
        'status_date_and_time_of_operation' => 'Status Date and time of operation',
        'transaction_amount' => 'Transaction amount',
        'belgazprombank' => 'Belgazprombank OJSC',
        'go_full_payment' => 'To pay',
    ],
    'single' => [
        'days' => 'Days'
    ],
    'save' => 'Save',
    'back' => 'Back',
    'delete' => 'Delete',
    'km' => 'KM',
    'of' => 'of',
    'tenant' => 'Tenant',
    'owner' => 'Owner',
    'error-404' => 'Unfortunately, we didn’t come there, you can start from the Main.',
    'cancel_booking' => 'Cancel car reservation'

];
