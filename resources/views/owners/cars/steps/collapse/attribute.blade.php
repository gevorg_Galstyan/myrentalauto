<div id="accordion-attribute">
    <div class="card shadow-sm  bg-light  rounded">
        <div class="card-header" id="heading-attribute">
            <h5 class="mb-0">
                <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-attribute"
                   aria-expanded="true" aria-controls="collapse-attribute">
                    <i class="fa"></i>
                    Доп. Атрибуты
                </a>
            </h5>
        </div>

        <div id="collapse-attribute" class="collapse {{$dataTypeContent->filters()->count()?'show':''}}" aria-labelledby="heading-attribute"
             data-parent="#accordion-attribute">
            <div class="card-body">
                @foreach(\App\Models\Filter::all() as $item)
                    <div class="d-flex align-content-center mt-2">
                        <div class="form-check col-2 align-self-center">
                            <label class="form-check-label " for="checkbox-{{$item->id}}">
                                <input class="form-check-input  change-price" type="checkbox"
                                       value="{{$item->id}}"
                                       data-target="#price-{{$item->id}}"
                                       {{$dataTypeContent->filters->contains($item->id)?'checked': ''}}
                                       name="filter[]">
                                {{$item->title}}
                            </label>
                        </div>
                        <div class="col-3">
                            <input type="text" id="price-{{$item->id}}" class="form-control"
                                   name="filter_price[]"
                                   value="{{$dataTypeContent->filters->contains($item->id)? $dataTypeContent->filters->where('id', $item->id)->first()->pivot->price : ''}}"
                                   placeholder="Цена (BYN)"
                                   {{$dataTypeContent->filters->contains($item->id)?'': 'disabled'}} required>
                        </div>
                       <div class="align-self-center">
                            <span class="k-pointer" data-toggle="tooltip" data-html="true"
                                  title=" {{ tooltips($toolips, $item->disposable ? 'owner_help_attributes_disposable_price' : 'car_help_attributes_price') }}">
                                <i class="fas fa-info-circle"></i>
                            </span>
                       </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
