<?php
return [
    'load_more' => 'load more',
    'reply' => 'Reply',
    'enter_your_message_here' => 'Enter your message here:',
    'send' => 'send',
    'cancel' => 'cancel',
    'reply_to_comment' => 'Reply to comment',
    'edit' => 'edit',
    'edit_comment' => 'Edit Comment',
    'update_your_message_here' => 'Update your message here:',
    'update' => 'Update',
    'no_comments_yet' => 'No comments yet.',
    'authorization_required' => 'Authorization required',
    'you_must_be_logged_in_to_leave_a_comment' => 'You must be logged in to leave a comment.',
];
