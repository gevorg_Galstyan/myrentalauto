@extends('layouts.app')
@section('meta')
    @parent
    {!! Robots::metaTag() !!}
@stop
@section('css')

@stop

@section('content')

    <div class="own-container ">
        <div class="row bg-white shadow py-4 mb-4 mt-4 align-items-center">

            <!-- edit form column -->
            <div class="col-md-12  ff-st row ">
                <div class="col-sm-4 mb-3">
                    <a href="{{url()->previous()}}" class="btn own-btn ora   d-inline-block  border-radius-20"
                       title="{{__('general.back')}}">
                        <i class="fas fa-arrow-left"></i>
                        <span class="">{{__('general.back')}}</span>
                    </a>
                </div>
                <div class="col-sm-4 mb-3">
                    <h1 class="text-center">{{__('user.posts')}}</h1>
                </div>
                <div class="col-sm-4 mb-3 text-right">
                    <a href="{{route('posts.create')}}" class="btn btn-primary   d-inline-block  border-radius-20"
                       title="{{__('user_post.add')}}">
                        <i class="fas fa-plus"></i>
                        <span class="">{{__('user_post.add')}}</span>
                    </a>
                </div>

                <div class="own-container bg-white border-radius-20 p-5">
                    <div class="row">
                        <!-- Blog Entries Column -->
                        <div class="col-md-12 order-md-1 order-2">
                            <div class="post-content row" id="blog">
                                @foreach($posts as $post)
                                    <div class="card border-0 shadow mb-5 border-radius-20">
                                        <div class="card-body p-3">
                                            <div class="row">
                                                <div class="col-12 d-flex justify-content-between">
                                                    <div class="">
                                                        @if(isset($post->status) && $post->status == 'PUBLISHED')
                                                            <span
                                                                class="badge badge-success">{{ __('voyager::post.status_published') }}</span>
                                                        @endif
                                                        @if(isset($post->status) && $post->status == 'DRAFT')
                                                            <span
                                                                class="badge badge-secondary">{{ __('voyager::post.status_draft') }}</span>
                                                        @endif
                                                        @if(isset($post->status) && $post->status == 'PENDING')
                                                            <span
                                                                class="badge badge-primary">{{ __('voyager::post.status_pending') }}</span>
                                                        @endif
                                                    </div>
                                                    <div class="">
                                                        <a title="{{__('user_post.edit')}}"
                                                           href="{{route('posts.edit', $post)}}">
                                                            <span><i class="fas fa-edit fa-2x"></i></span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div
                                                    class="col-md-4 d-flex align-items-center justify-content-center flex-column">
                                                    <img class="img-fluid w-100 border-radius-20"
                                                         src="{{asset("storage/".($post->image ?? 'posts/default.jpg'))}}"
                                                         alt="{{$post->translate()->title}}">
                                                </div>

                                                <div class="col-12 col-md-8">
                                                    <div class="row  mt-3 ">
                                                        <div class="col-12">
                                                            <h4 class="card-title">
                                                                <a href="{{route('post.single', ['slug' => $post->slug])}}">{!! strip_tags($post->translate()->title) !!}</a>
                                                            </h4>
                                                            @if ($post->updated_at->diffInHours($post->created_at) > 24)
                                                                <div class="small text-muted mb-3"><span>{{__('post.updated')}}:</span><span
                                                                        class="pl-2">{{$post->updated_at->format('Y-m-d')}}</span>
                                                                </div>
                                                            @else
                                                                <div
                                                                    class="small text-muted mb-3">{{$post->created_at->format('Y-m-d')}}</div>
                                                            @endif
                                                        </div>

                                                        <div class="author d-flex  col-12 mb-3">
                                                            <div class="avatar mr-3"
                                                                 style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                                                            </div>
                                                            <div>
                                <span
                                    class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</span>
                                                                <small
                                                                    class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 ff-ms">
                                                            <p class="mb-4">
                                                                {{$post->translate()->excerpt}}
                                                                <a href="{{route('post.single', ['slug' => $post->slug])}}"
                                                                   class="d-inline-block read-post">
                                                                    {{__('post.read_article')}} →
                                                                </a>
                                                            </p>
                                                        </div>

                                                        <div class="col-12 text-center">
                                                            <ul class="list-unstyled mb-0 d-flex flex-wrap">
                                                                @if(isset($selected_tag) && $selected_tag)
                                                                    <li class="tag ">
                                                                        <a href="{{route('posts', ['slug' => $selected_tag->slug])}}"
                                                                           class="badge badge-secondary selected-tag">
                                                                            {{$selected_tag->name}}
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                                @foreach($post->tags->where('id', '!=', (isset($selected_tag) && $selected_tag? $selected_tag->id:''))->sortByDesc('rating')->slice(0, 4) as $tag)
                                                                    <li class="tag ">
                                                                        <a href="{{route('posts', ['slug' => $tag->slug])}}"
                                                                           class="badge badge-secondary  ">{{$tag->name}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <div class="post-paginate">
                                    <!-- Pagination -->
                                    {{$posts->links()}}

                                </div>
                            </div>
                            <!-- Blog Post -->
                        </div>
                    </div>
                    <!-- /.container -->
                </div>

            </div>

        </div>
    </div>
@endsection

@section('script')

@stop

