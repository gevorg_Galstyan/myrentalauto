<?php
/**
 * Created by PhpStorm.
 * User: Maximum Code
 * Date: 12.04.2019
 * Time: 18:17
 */

namespace App\Traits;

trait MyCache
{
    public function myTranslate()
    {
        return $this->translate();
        return \Cache::remember($this->cacheKey() . ':translate', setting('site.cache_time'), function () {
            return $this->translate();
        });
    }

    public function cacheKey()
    {
        if ($this->updated_at){
            return sprintf(
                "%s_%s_%s",
                $this->getTable(),
                $this->getKey(),
                $this->updated_at->timestamp
            );
        }else{
            return sprintf(
                "%s_%s",
                $this->getTable(),
                $this->getKey()
            );
        }

    }
}
