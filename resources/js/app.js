require('./bootstrap');

if (userId && userId > 0){
    Echo.private('App.User.' + userId)
        .notification((notification) => {

            // const Toast = Swal.mixin({
            //     toast: true,
            //     position: 'top-right',
            //     showConfirmButton: false,
            //     showCloseButton: true,
            // });
            // Toast.fire({
            //     html: '<strong>'+notification.title+'</strong><br><p>'+notification.message+'</p>',
            // });
            toastr.info(notification.message,'<strong>'+notification.title+'</strong>', {timeOut: 10000});
            playNotify();
        });

    function playNotify() {
        var audio = new Audio('/storage/notify.mp3');
        audio.play();
    }
}

