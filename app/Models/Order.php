<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_id', 'customer_id'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        Order::where('status', 'confirmed')->where('updated_at', '<', Carbon::parse('-2 hours'))->update(['status' => 'expired']);
        Order::where('status', 'pending')->where('updated_at', '<', Carbon::parse('-20 minutes'))->update(['status' => 'no_paid']);
    }

    public function dates()
    {
        return $this->hasOne(OrderDate::class, 'order_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(\App\User::class, 'customer_id', 'id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(OrderHitory::class, 'order_id', 'id');
    }

//    public function save(array $options = [])
//    {
//        $latestOrder = Order::orderBy('created_at','DESC')->first();
//        $number = isset($latestOrder->id) && $latestOrder->id ? $latestOrder->id + 1 : 1;
//        if (!$this->barcode_number) {
//            $this->barcode_number = str_pad($number, 8, "0", STR_PAD_LEFT);
//        }
//
//        parent::save();
//    }

    public function fullPay()
    {
        return $this->hasOne(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }
}
