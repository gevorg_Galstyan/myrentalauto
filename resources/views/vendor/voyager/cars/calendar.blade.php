@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="far fa-calendar-alt"></i> Календарь {{ $dataTypeContent->title }} &nbsp;
        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>

@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered d-flex" style=" padding:15px;">
                    <div style="flex: 0 0 35%;padding: 0 15px ;">
                        <ul class="list-group">
                            <li class="list-group-item"><strong>Марка и модель </strong> : <a
                                        href="{{route('voyager.cars.show', $dataTypeContent->id)}}">{{ $dataTypeContent->title}}</a>
                            </li>
                            <li class="list-group-item"><strong>Владелец </strong>
                                : {{ $dataTypeContent->owner->fullName?? 'Нет владельца ' }}</li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{Voyager::image($dataTypeContent->image_1)}}" class="img-rounded" width="100%">
                                    </div>
                                </div>

                            </li>
                        </ul>
                    </div>
                    <div style="flex: 0 0 65%; padding: 0 15px;">
                        <ul class="list-group">
                            <li class="list-group-item row">
                                <div class="col-md-4 border-1 text-center m-2">
                                    <h5> Арендован
                                        <span class="pl-2"
                                              aria-hidden="true"
                                              data-toggle="tooltip" data-html="true"
                                              data-placement="top"
                                              title="Этими цветами обозначены те дни в которых автомобиль будет в аренде"><i
                                                    class="fas fa-question-circle"></i></span>
                                    </h5>
                                    <span style="width: 20px; height: 20px; display: inline-block; background-color:#4ebf22 "
                                          class="m-2 "></span>
                                    <span style="width: 20px; height: 20px; display: inline-block; background-color:#82e25d"
                                          class="m-2 "></span>
                                </div>
                                <div class="col-md-4 border-1 text-center m-2">
                                    <h5> Занято
                                        <span class="pl-2"
                                              aria-hidden="true"
                                              data-toggle="tooltip" data-html="true"
                                              data-placement="top"
                                              title="Этими цветами обозначены те дни которые добились вручную как занятость"><i
                                                    class="fas fa-question-circle"></i></span>
                                    </h5>
                                    <span style="width: 20px; height: 20px; display: inline-block; background-color:#32A0DA "
                                          class="m-2 "></span>

                                </div>

                            </li>
                        </ul>
                        {!! $calendar->calendar() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="makeOrder" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Добавить Занятость!</h4>
                </div>
                <div class="modal-body" id="date-block">
                    <form action="{{route('voyager.order-dates.store')}}" method="POST" id="makeOrderForm">
                        @csrf
                        <div class="form-group" >
                            <input name="date" class="form-control add-event" placeholder="Дата" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="comment"> Замечание :</label>
                            <textarea name="note" class="form-control" rows="5" id="comment"
                                      placeholder="Замечание"></textarea>
                        </div>
                        <input type="hidden" name="car_id" value="{{$dataTypeContent->id}}">
                        <button type="submit" class="btn btn-default">Добавит</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    @php($all_dates = [])
    @foreach($dataTypeContent->busyDays as $item)
        <?php
        $startDate = new \Carbon\Carbon($item->start);
        $endDate = new \Carbon\Carbon($item->end);
        while ($startDate->lte($endDate)) {
            $a = $startDate;

            $all_dates[] = \Carbon\Carbon::parse($a)->format('d-m-Y');

            $startDate->addDay();
        }
        ?>
    @endforeach
@stop

@section('javascript')
    {!! $calendar->script() !!}
    <script src="{{asset('js/jquery.daterangepicker.js')}}"></script>


    <script>
        var locale = '{{LaravelLocalization::getCurrentLocale()}}';
        var urgency = '{{currency(setting('site.urgency'))}} ';
        var urgency_text = "{!! trim(strip_tags( tooltips($toolips, 'zasrochnost', currency(setting('site.urgency'))))) !!}";
        var ne_rabochie_vremya = '{!! trim(strip_tags( tooltips($toolips, 'ne_rabochie_vremya', currency(setting('site.during_off_hours'))))) !!}';

        order_date = jQuery.parseJSON('{!! json_encode($all_dates) !!}');
        $('.add-event').dateRangePicker({
            startOfWeek: 'monday',
            container: '#date-block',
            separator: ' ~ ',
            format: 'DD.MM.YYYY HH:mm',
            autoClose: false,
            language: locale,
            swapTime: false,

            startDate: moment().format('DD-MM-YYYY'),
            beforeShowDay: function (t) {
                var myDate = moment(t);

                var _class = '';
                var _tooltip = '';
                if (!(order_date.indexOf(moment(t).format('DD-MM-YYYY')) == -1)) {
                    _class = 'red';
                    _tooltip = 'На этот день машина занята ';

                }
                var valid = (order_date.indexOf(moment(t).format('DD-MM-YYYY')) == -1);  //disable saturday and sunday
                return [valid, _class, _tooltip];
            },
        });
        $('#makeOrderForm').submit(function (form) {
            form.preventDefault();
            if ($('input[name="date"]').val()) {
                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.success) {
                            $('#makeOrder').modal('hide');
                            $('input[name="date"] ,  textarea[name="note"]').val('');
                            calendar.addEvent({
                                color : '#32A0DA',
                                rendering: 'background',
                                className: 'busy-day',
                                title: 'The Title',
                                    description: data.data.note,
                                start: moment(data.data.start).format('YYYY-MM-DD'),
                                end: moment(data.data.end).add(1, 'days').format('YYYY-MM-DD'),
                            });
                            toastr.success("Занятость добавлена");

                        }
                    }
                })
            }

        });
        $('[data-toggle="tooltip"]').tooltip();
    </script>
@stop
