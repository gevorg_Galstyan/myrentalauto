require('./admin-bootstrap');

Echo.private('App.User.' + userId)
    .notification((notification) => {

        // const Toast = Swal.mixin({
        //     toast: true,
        //     position: 'top-right',
        //     showConfirmButton: false,
        //     showCloseButton: true,
        // });
        // Toast.fire({
        //     html: '<strong>' + notification.title + '</strong><br><p>' + notification.message + '</p>',
        // });

        toastr.info(notification.message,'<strong>'+notification.title+'</strong>', {timeOut: 10000});
        playNotify();
    });

function playNotify() {
    var audio = new Audio('/storage/notify.mp3');
    audio.play();
}

Echo.private(`BreadDataAdded.${userId}`)
    .listen('BreadDataAdded', (event) => {
        if (event.type == 'Added' && event.dataType.name == "filters") {
            $('.filter-content').append('<div class="d-flex">\n' +
                '                                            <div class="form-check col-2">\n' +
                '                                                <label class="form-check-label " for="checkbox-'+event.data.id+'">\n' +
                '                                                    <input class="form-check-input  change-price" type="checkbox"\n' +
                '                                                           value="'+event.data.id+'"\n' +
                '                                                           data-target="#price-'+event.data.id+'"\n' +
                '                                                           name="filter[]" id="checkbox-'+event.data.id+'">\n' +
                '                                                    '+event.data.title+'\n' +
                '                                                </label>\n' +
                '                                            </div>\n' +
                '                                            <div class="col-3 mt-2">\n' +
                '                                                <input type="text" id="price-'+event.data.id+'" class="form-control"\n' +
                '                                                       name="filter_price[]"\n' +
                '                                                       value=""\n' +
                '                                                       placeholder="Цена" disabled required>\n' +
                '                                            </div>\n' +
                '                                        </div>')
        }
    });
