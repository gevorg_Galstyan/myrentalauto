<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Contracts\User;

class UserPolicy extends \TCG\Voyager\Policies\UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show_admin(User $user, $data)
    {
        if ($data->id != 1) {
            return true;
        } else {
            return $user->hasPermission('show_admin_users') && $data->id != $user->id;
        }
    }

//    public function browse(User $user, $model)
//    {
//        dd($model);
//        // Does this record belong to the current user?
//        $current = $user->id === $model->id;
//
//        return $current || $this->checkPermission($user, $model, 'read');
//    }

}
