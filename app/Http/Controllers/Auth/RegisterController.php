<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\OwnerProfile;
use App\Models\Profile;
use App\Models\Toolip;
use App\Notifications\CarRequestAdmin;
use App\Repositories\CarOrderRepository;
use App\User;
use App\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
        $this->redirectTo = url()->previous();

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $regSendOredr = false)
    {
        if (!$regSendOredr) {
            $validateData = [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'role' => [
                    'nullable',
//                'required',
                    'in:2,3',
                ],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ];
        } else {
            $validateData = [
                'guestFirstName' => ['required', 'string', 'max:255'],
                'guestLastName' => ['required', 'string', 'max:255'],
                'guestEmail' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
//                'phone' => 'unique:users,number',
                'phone' => 'phone:AUTO|unique:users,number',
            ];
        }
        return Validator::make($data, $validateData);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data, $regSendOredr = false)
    {
        if ($regSendOredr) {
            $default_password = \Str::random(10);
            $user = User::create([
                'first_name' => $data['guestFirstName'],
                'last_name' => $data['guestLastName'],
                'email' => $data['guestEmail'],
                'avatar' => 'users/default.png',
                'role_id' => 2,
                'password' => Hash::make($default_password),
                'default_password' => $default_password,
                'number' => $data['phone'],
            ]);

            $profile = new Profile();


            $profile->user_id = $user->id;
            $profile->save();
        } else {
            $user = User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'avatar' => 'users/default.png',
                'role_id' => isset($data['role']) ? $data['role'] : 2,
                'password' => Hash::make($data['password']),
            ]);
            if (isset($data['role']) && $data['role'] == 3) {
                $profile = new OwnerProfile();
            } else {
                $profile = new Profile();
            }

            $profile->user_id = $user->id;
            $profile->save();
        }


        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if ($request->ajax()) {
            return response()->json([
                'intended' => $this->redirectPath(),
            ]);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['errors' => $validator->messages()]);
            } else {
                $validator->validate();
            }
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function guestReg(Request $request, Car $car)
    {
        $validator = $this->validator($request->guest, true);
        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['errors' => $validator->messages()]);
            } else {
                $validator->validate();
            }
        }

        event(new Registered($user = $this->create($request->guest, true)));

        $this->guard()->login($user);
        $details = $request->order;
        $dates = CarOrderRepository::getDate(@$details['date']);
        $request['days'] = CarOrderRepository::getDays(CarOrderRepository::getDate(@$details['date']));
        $request['start_day'] = $dates['start'];
        $request['end_day'] = $dates['end'];
        $order = CarOrderRepository::getOrder();
        CarOrderRepository::fillingOrder($request->all(), $order, $car, $dates, auth()->user(), 'requested');
        $users = User::where('role_id', 1)->get();
        Notification::send($users, new CarRequestAdmin($order));
        return response()->json([
            'success' => true,
            'title' => Toolip::where('key','title_after_register_of_car_page')->first()->translate()->value,
            'message' => Toolip::where('key','text_after_register_of_car_page')->first()->translate()->value
        ]);
    }
}
