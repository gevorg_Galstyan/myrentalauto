<?php
return [
    'success' => [
        'title' => 'PAYMENT PASSED SUCCESSFULLY',
        'text' => 'thank you for your cooperation',
    ],
    'error' => [
        'title' => 'PAYMENT ERROR',
        'text' => 'come back check for correctness',
    ],
    'return_home_page' => 'Home page'
];
