<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Location extends Model
{
    use Translatable;

    protected $translatable = ['location', 'title', 'Keyword', 'description', 'h1'];

    public $timestamps = false;

}
