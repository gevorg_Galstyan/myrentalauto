@if(Auth::guest())
    <div class="text-center mt-3">
        <button type="button"
                data-toggle="modal"
                data-target="#auth-booking-modal"
                class="own-btn ora rounded-0 border-0 border-5 m-auto">
            {{__('auth.login_before_order')}}
            <span data-toggle="tooltip" data-html="true"
                  title='{!! tooltips($toolips, 'login_before_order') !!}'>
                <i class="fas fa-info-circle"></i>
            </span>
        </button>
    </div>
@else

    @if ($car->busy(request()->input('start_day'), request()->input('end_day')))
        @if (request()->get('orderId') && checkBarCode(request()->get('orderId')))
            <div class="form-row justify-content-between mt-2">
                <div class="form-group col-md-12  ">
            <textarea name="comment" form="single-car-params" class="form-control own-shadow"
                      rows="4" placeholder="{{__('car.your_comment')}}">{{request()->input('comment')}}</textarea>
                </div>
            </div>
            <div class="text-center mt-3">
                <button type="submit" form="single-car-params"
                        class="own-btn ora rounded-0 border-0 border-5 m-auto">{{__('car.to_pay')}}
                </button>
            </div>
        @else
            <span class="fs-22 text-center d-block text-danger">
            {{__('car.busy')}}
            <span data-toggle="tooltip" data-html="true" data-placement="top"
                  title="{{tooltips($toolips, 'car_busy', implode(' , ', matchingDays(request()->input('start_day'), request()->input('end_day'), $car)))}}">
                <i class="fas fa-info-circle"></i>
            </span>
        </span>
        @endif

    @else
        @if(auth()->user()->profile->status == 'for_consideration')
            <div class="text-center mt-3">
                <button type="button" form="single-car-params"
                        class="own-btn ora rounded-0 border-5 border-0 m-auto " disabled="disabled">
                    {{__('car.waiting_for_an_answer')}}
                    <span data-toggle="tooltip" data-html="true" data-placement="top"
                          title="{{tooltips($toolips, 'order_for_consideration')}}">
                    <i class="fas fa-info-circle"></i>
                </span>
                </button>
            </div>
        @elseif(profileIsFull(auth()->user()->profile, true))
            <div class="form-row justify-content-between mt-2">
                <div class="form-group col-md-12  ">
            <textarea name="comment" form="single-car-params" class="form-control own-shadow"
                      rows="4" placeholder="{{__('car.your_comment')}}">{{request()->input('comment')}}</textarea>
                </div>
            </div>
            @if ($car->on_request == 1)
                <div class="text-center mt-3">
                    <button type="submit" form="single-car-params"
                            class="own-btn ora rounded-0 border-0 border-5 m-auto">
                        {{__('car.to_request')}}
                    </button>
                </div>
            @else
                <div class="text-center mt-3">
                    <button type="submit" form="single-car-params"
                            class="own-btn ora rounded-0 border-0 border-5 m-auto">{{__('car.to_pay')}}
                    </button>
                </div>
            @endif
        @elseif(auth()->user()->role_id != 2)
            <div class="text-center mt-3">
                {{__('car.login_as_tenant')}}
            </div>
        @else
            <div class="text-center mt-3">
                <a href="{{route('profile.edit')}}"
                   class="own-btn ora rounded-0 border-0 border-5 m-auto d-inline-block">
                    {{__('car.go_to_profile_fill')}}
                    <span data-toggle="tooltip" data-html="true" data-placement="top"
                          title="{{tooltips($toolips, 'order_profile_fill')}}">
                    <i class="fas fa-info-circle"></i>
                </span>
                </a>
            </div>

        @endif
    @endif

@endif


{{--@if(Auth::guest())--}}
{{--    <div class="text-center mt-3">--}}
{{--        <button type="button"--}}
{{--                data-toggle="modal"--}}
{{--                data-target="#loginModal"--}}
{{--                class="own-btn ora rounded-0 border-0 border-5 m-auto">--}}
{{--            {{__('auth.login')}}--}}
{{--            <span data-toggle="tooltip" data-html="true" data-placement="top" title="{{tooltips($toolips, 'login_before_order')}}">--}}
{{--                <i class="fas fa-info-circle"></i>--}}
{{--            </span>--}}
{{--        </button>--}}
{{--    </div>--}}
{{--@else--}}

{{--    @if(auth()->user()->profile->status == 'for_consideration')--}}
{{--        <div class="text-center mt-3">--}}
{{--            <button type="button" form="single-car-params"--}}
{{--                    class="own-btn ora rounded-0 border-5 border-0 m-auto " disabled="disabled">--}}
{{--                Ожидание ответа--}}
{{--                <span data-toggle="tooltip" data-html="true" data-placement="top"--}}
{{--                      title="{{tooltips($toolips, 'order_for_consideration')}}">--}}
{{--                    <i class="fas fa-info-circle"></i>--}}
{{--                </span>--}}
{{--            </button>--}}
{{--        </div>--}}
{{--    @elseif(profileIsFull(auth()->user()->profile, true))--}}
{{--        <div class="form-row justify-content-between mt-2">--}}
{{--            <div class="form-group col-md-12 pr-md-3 mob-padding-0">--}}
{{--            <textarea name="comment" form="single-car-params" class="form-control own-shadow"--}}
{{--                      rows="6" placeholder="{{__('car.your_comment')}}"></textarea>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        @if ($car->on_request == 1)--}}
{{--            <div class="text-center mt-3">--}}
{{--                <button type="submit" form="single-car-params"--}}
{{--                        class="own-btn ora rounded-0 border-0 border-5 m-auto">--}}
{{--                    ЗАПРОСИТЬ--}}
{{--                </button>--}}
{{--            </div>--}}
{{--        @else--}}
{{--            <div class="text-center mt-3">--}}
{{--                <button type="submit" form="single-car-params"--}}
{{--                        class="own-btn ora rounded-0 border-0 border-5 m-auto">{{__('car.to_pay')}}--}}
{{--                </button>--}}
{{--            </div>--}}
{{--        @endif--}}

{{--    @else--}}
{{--        <div class="text-center mt-3">--}}
{{--            <a href="{{route('profile.edit')}}" type="submit"--}}
{{--               class="own-btn ora rounded-0 border-0 m-auto">--}}
{{--                Перейти к заполнению профиля--}}
{{--                <span data-toggle="tooltip" data-html="true" data-placement="top"--}}
{{--                      title="{{tooltips($toolips, 'order_profile_fill')}}">--}}
{{--                    <i class="fas fa-info-circle"></i>--}}
{{--                </span>--}}
{{--            </a>--}}
{{--        </div>--}}

{{--    @endif--}}
{{--@endif--}}

