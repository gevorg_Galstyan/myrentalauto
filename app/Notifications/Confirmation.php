<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\{BroadcastMessage , MailMessage};
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class Confirmation extends Notification
{
    use Queueable;


    private $subject;
    private $message;
    private $status;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($status, $message = null, $subject = null)
    {
        $this->status = $status;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $data = ['database'];
        if ($this->status == 'confirm'){
            $data[] = 'broadcast';
        }
        if ($this->message){
            $data[] = 'mail';
        }

        return $data;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($user)
    {
        return (new MailMessage)
            ->subject($this->subject)
                    ->line($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'status' => $this->status,
            'subject' => $this->subject,
            'message' => $this->message,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => $this->subject??'',
            'message' => 'Ваши данные подтверждены',
        ]);
    }
}
