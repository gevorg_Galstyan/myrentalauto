<?php

namespace App\Actions;

use App\User;
use TCG\Voyager\Actions\AbstractAction;

class ShowModerator extends AbstractAction
{
    public function getTitle()
    {

        return 'Модераторы';
    }

    public function getPolicy()
    {
        return 'show_all_moderator_button';
    }

    public function getIcon()
    {
        return 'fas fa-user-cog';
    }

    public function getAttributes()
    {
        return [

            'class' => 'btn btn-sm btn-primary ',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.users.index', [
            'key' => 'role_id',
            'filter' => 'equals',
            's' => '4',
        ]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }

    public function massAction($ids, $comingFrom)
    {
        return redirect($comingFrom);
    }

    public function checkedPolicy()
    {
        return User::class;
    }

    public function methodType()
    {
        return 'GET';
    }

}