<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class CurrencyController extends Controller
{

    public function setCurrency(Request $request){

        $currencies = config('currency.needed');
        if(in_array($request->currency, $currencies)){

            if (Session::get('currency') != $request->currency)
            {
                Session::put('currency', $request->currency);
            }

            if (Cookie::get('currency') != $request->currency)
            {
                Cookie::queue('currency', $request->currency, 0);
            }
        }
        return back();

    }

    public function getCurrency(){

    }
}
