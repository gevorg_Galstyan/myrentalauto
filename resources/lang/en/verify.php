<?php return [
    'text_1' => 'Verify Email Address',
    'text_2' => 'Please click the button below to verify your email address.',
    'text_3' => 'Verify Email Address',
    'text_4' => 'If you did not create an account, no further action is required.',
    'text_5' => 'your password - :password',
    'text_6' => 'Change password after first login',
];
