<?php
return [
    'email' => 'Эл. адрес',
    'phone' => 'Номер телефона',
    'birthday' => 'День рождения',
    'age' => 'Возраст',
    'driving_experience' => 'Водительский стаж',
    'country' => 'Страна регистрация',
    'city' => 'Город',
    'region' => 'Регион',
    'address' => 'Address',
    'registered' => 'Зарегистрирован в',
    'check_profile' => 'Сохранить и отправить для подтверждения',
    'check_profile_info' => 'У вас есть новое подтверждение',
    'check_profile_send' => 'Ваша подтверждения отправлено',
    'pass_change' => 'Ваша пароль обновлен!',
    'pass_change_error' => 'Текущий пароль не совпадает.',
    'profile_update' => 'Ваша личная информация обновляется!',
    'rejected' => 'Ваши данные несовместимы с нашими условиями!',
    'confirm' => 'Ваш профиль активирован!',
    'rejected_name' => 'Отклонено',
    'confirm_name' => 'Подтвердить',
    'confirm_message' => 'Отправьте сообщение с подтверждением...',

    'active' => 'Активные',
    'story' => 'История',

    'profile' => 'Профиль',
    'edit_profile' => 'Редактировать',
    'upload_avatar' => 'Загрузить другое фото...',
    'upload' => 'Загрузить изображение',

    'posts' => 'Посты',

    'full_name' => 'Полное имя',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'patronymic' => 'Отчество',
    'citizenship' => 'Гражданство',
    'save' => 'Сохранить',

    'password' => 'Пароль',
    'new_password' => 'Новый пароль',
    'currently_password' => 'Текущий пароль',
    'change_password' => 'Изменить пароль',
    'add_password' => 'Добавить пароль',
    'confirm_password' => 'Подтвердите Пароль',
    'save_password' => 'Сохранить Пароль',

    'documents' => 'Документы',
    'description_profile' => 'Сервис гарантирует конфиденциальность данных. Ваши документы не будут отображаться в публичном профиле.',
    'upload_passport' => 'Загрузите фотографию паспорта',
    'upload_license' => 'Загрузите фотографию водительского удостоверения',

    'no_active_cars' => 'Нет активыных машин',
    'no_story' => 'У вас пока нет истории проката',
    'no_favorites' => 'У вас пока нет избранных машин',
    'choose_avatar_photo' => 'Выбрать <i class="fas fa-user-circle"></i>',


    'passport_documents' => 'Документы паспорта',
    'vod_documents_certificates' => 'Документы вод. удостоверения',
    'to_book_a_ar_you_need_to_fill_out_a_profile_and_agree_to_the_terms' => 'для бронирования автомобиля вам надо заполнить профиль и согласиться с условиями',

    'back_profile' => 'Профиль',

    'delete_profile' => 'Удалить профиль',

    'delete' => [
        'are_you_sure' => 'Уверены ли вы?',
        'yes_delete' => 'Да, удалить',
        'cancel' => 'отменить',
    ],
    'personal_area' => 'Личный кабинет',

    'order' => [
        'photo' => 'Фото',
        'car' => 'CarАвтомобиль',
        'rental_date' => 'Дата Аренды',
        'return_date' => 'Дата Возврата',
        'transaction_date' => 'Дата Сделки',
        'status' => 'Статус',
    ]

];
