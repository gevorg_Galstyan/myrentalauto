<div
    class="single-auto col-lg-4 col-md-4 px-0 {{--{{!isset($single_class) && $index % 3 != 0 ? 'ml-4' : ''}}--}}  card own-card"
    id="car-{{$car->slug}}">
    <div class="position-relative">
        <div class="new-options bg-white">
            <div class="single-new-option py-1 px-1 bb-1 text-center k-pointer">
                @if(Auth::guest())
                    <a data-toggle="modal" href="javascript:void(0)" data-target="#loginModal"
                       class="wishlist">
                        <div class="car-options">
                            <span class="heart-ox d-inline-block heart-o"></span>
                        </div>

                    </a>
                @else
                    <a data-href="{{route('profile.action-favorite', $car->id)}}"
                       class="wishlist ajaxFavorite" data-target="#car-{{$car->slug}}" data-delete="{{$delete??''}}">
                        <div class="car-options">
                            @if ($car->isFavorited())
                                <span class="heart-ox d-inline-block heart"></span>
                            @else
                                <span class="heart-ox d-inline-block heart-o"></span>
                            @endif
                        </div>
                    </a>
                @endif
            </div>
            <div class="single-new-option py-1 px-1 bb-1 text-center">
                <div class="car-options">
                    <span class="new-eye d-inline-block hands align-middle"></span>
                    <span class="new-option-count fs-12 fw-700 align-middle">{{$car->orders->where('status','completed')->count()}}</span>
                </div>
            </div>
            <div class="single-new-option py-1 px-1">
                <div class="car-options">
                    <span class="new-eye d-inline-block align-middle"></span>
                    <span class="new-option-count fs-12 fw-700 align-middle">{{ Counter::show('car', $car->id) }}</span>
                </div>
            </div>
        </div>
        <img data-src="{{ ownMakeImage(381, 0, 75,$car->image_1)}}"
             data-srcset="{{ownMakeImage(381, 0, 75,$car->image_1)}}"
             src="{{Voyager::image(str_replace('.jpg', '-tumble.jpg',$car->image_1))}}"
             data-sizes="auto" class="card-img-top lazyload" alt="{{$car->title}}"
             title="{{$car->title}}">
        <div class="card-footer d-flex w-100 m-0 px-0 py-1 ">
            <div class="col-auto p-0  m-auto  car-params">
                <div class="svg-icon mr-1  year-car-icon"></div>
                <span class="fs-10 fs-md-8 fw-700">{{$car->age}}</span>
            </div>
            |
            <div class="col-auto p-0 m-auto car-params">
                <div class="svg-icon mr-1 motor text-nowrap"></div>
                <span class="fs-10 fs-md-8 fw-700">{{__('general.'.$car->fuel)}} {{$car->motor}} {{__('general.l')}}.</span>

            </div>
            |
            <div class="col-auto p-0 m-auto car-params">
                <div class="svg-icon mr-1 gear text-nowrap"></div>
                <span class="fs-10 fs-md-8 fw-700">{{__('general.'.$car->transmission)}}</span>
            </div>

        </div>
    </div>
    <div class="card-body d-flex flex-wrap align-items-center justify-content-between py-3">
        <div class="row m-0 pb-3 w-100">
            <div class="col-md-10  col-sm-12 px-0 mob-desc">
                <h3 class="card-title cart-after position-relative fw-600 mb-0">{{$car->make->name}}
                    <br class="d-none d-sm-block"> {{$car->model->name}}</h3>
            </div>
            <div class="col-2 pl-0   d-none d-sm-block">
                @if (!$car->on_request)
                    <span class="instant-booking k-pointer float-right car-instant-booking bg-size-100"
                          data-toggle="tooltip" data-html="true" data-placement="top"
                          title='{!! tooltips($toolips, 'instant_booking') !!}'>
                    </span>

                @endif
            </div>
            <div class="col-12 pl-0   d-flex d-sm-none mt-3">
                <p class="fs-13 color-light float-left col p-0 d-flex align-items-center d-block d-sm-none">
                   <span class="location-icon-svg d-inline-block"></span>
                    <span>{{distanceM($car->distance)}}</span>
                    <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                          title="От выбранного места до этого автомобиля {{distanceM($car->distance)}}"><i
                            class="fas fa-info-circle"></i></span>
                </p>
                @if (!$car->on_request)
                    <span class="instant-booking k-pointer float-right car-instant-booking bg-size-100"
                          data-toggle="tooltip" data-html="true" data-placement="top"
                          title='{!! tooltips($toolips, 'instant_booking') !!}'>
                    </span>
                @endif
            </div>
        </div>

        <div class="col-md-7 pl-0 d-none d-sm-block">
            <p class="fs-13 color-light mb-2">
                <span class="location-icon-svg d-inline-block"></span>
                <span>{{distanceM($car->distance)}}</span>
                <span class="k-pointer" data-toggle="tooltip" data-html="true" data-placement="top"
                      title="От выбранного места до этого автомобиля {{distanceM($car->distance)}}"><i
                        class="fas fa-info-circle"></i></span>
            </p>
            <div class="fs-13 d-md-flex d-none flex-wrap align-items-center ">
                <div class="stars stars--large mr-1">
                    <span style="width: {{$car->avg_score * 20}}%"></span>
                </div>
                <span>({{$car->reviews->count()}})</span>
            </div>
        </div>

        <div class="col-md-5 px-md-0 d-none d-sm-block">
            <div class="price-block px-0">
                <span class="price ff-st fs-18"> {!! currency(getCarPrice($car, request()->input('days')), false, false) !!} <span class="currency-symbol"></span></span>
                <br class="d-none d-md-block">
                <span class="day"></span>
            </div>
        </div>
        <div class="px-md-0 d-block d-sm-none">
            <div class="price-block  search-page-price">
                <span class="price ff-st"> {!! currency(getCarPrice($car, request()->input('days')), false, false) !!} <span class="currency-symbol"></span></span>
                <br class="d-none d-md-block">
                <span class="day"></span>
            </div>
        </div>
    </div>
    <div class="card-footer p-0 m-0 bg-transparent border-none pb-3">
        <div class="col-12 text-center">
            <a href="{{route('car.single', ['slug' => $car->slug])}}"
               data-href="{{http_build_query( request()->only('location', 'date', 'attributes', 'transmission', 'price', 'fuel_consumption_min', 'fuel_consumption_max', 'type', 'seats','doors', 'age' , 'fuel', 'instant_booking', 'on_request'))}}"
               class="own-btn text-uppercase d-inline-block btn-shadow ora single-page">{{__('car.rent_it')}}</a>
        </div>
    </div>
</div>
