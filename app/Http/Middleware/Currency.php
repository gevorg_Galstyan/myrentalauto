<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class Currency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd($request->cookie('currency'));
        if (!$request->cookie('currency')) {
            $currency = config('currency.default');
//            Session::put('currency', $currency);
            Cookie::queue('currency', $currency, 0);
//        } elseif (!Session::get('currency')){
//            Session::put('currency', $request->cookie('currency'));
        }
        Session::put('currency', $request->cookie('currency'));
        return $next($request);
    }
}
