@extends('layouts.app')

{{--@section('meta')--}}
{{--    <title>{{$item ? $item->translate()->title : getMetaTags('faq_'.$for_whom.'_page')->translate()->title}}</title>--}}
{{--    <meta name="keywords" content="{{getMetaTags('faq_'.$for_whom.'_page')->translate()->Keyword}}"/>--}}
{{--    <meta name="description" content="{{$item?Str::words(str_replace(["\n", "/\s/", "\xc2\xa0", "&nbsp;"],'', strip_tags($item->body) ), 40, ''):getMetaTags('faq_'.$for_whom.'_page')->translate()->description}}"/>--}}

{{--    <meta property="og:locale" content="{{LaravelLocalization::getLocalesOrder()[LaravelLocalization::getCurrentLocale()]['regional']}}"/>--}}
{{--    <meta property="og:type" content="website"/>--}}
{{--    <meta property="og:title" content="{{$item ? $item->translate()->title : getMetaTags('faq_'.$for_whom.'_page')->translate()->title}}"/>--}}
{{--    <meta property="og:description" content="{{$item?Str::words(str_replace(["\n", "/\s/", "\xc2\xa0", "&nbsp;"],'', strip_tags($item->body) ), 40, ''):getMetaTags('faq_'.$for_whom.'_page')->translate()->description}}"/>--}}
{{--    <meta property="og:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}"/>--}}
{{--    <meta property="og:url" content="{{url()->current()}}"/>--}}
{{--    <meta property="og:site_name" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}"/>--}}

{{--    <meta name="twitter:card" content="summary">--}}
{{--    <meta name="twitter:site" content="{{LaravelLocalization::getCurrentLocale() == 'en' ? setting('site.title_en') : setting('site.title')}}">--}}
{{--    <meta name="twitter:title" content="{{$item ? $item->translate()->title : getMetaTags('faq_'.$for_whom.'_page')->translate()->title}}">--}}
{{--    <meta name="twitter:description" content="{{$item?Str::words(str_replace(["\n", "/\s/", "\xc2\xa0", "&nbsp;"],'', strip_tags($item->body) ), 40, ''):getMetaTags('faq_'.$for_whom.'_page')->translate()->description}}">--}}
{{--    <meta name="twitter:image" content="{{Voyager::image('img/slider-cover-new-mobile.jpg')}}">--}}
{{--    <link href="{{ LaravelLocalization::getNonLocalizedURL(request()->url()).(request()->get('ex') ? '?ex='.request()->get('ex') : '') }}" rel="canonical">--}}

{{--@stop--}}

@section('css')
    <link rel="stylesheet" href="{{asset('css/faq.css')}}">
@stop
@section('content')
    <div class="faq-search-container">
        <div class="row">
            <div class="slider-logo ">
                <div class="slider-logo mt-2"><span class="own-blue">My</span><span class="own-orange">Rent</span><span class="own-blue">Auto</span></div>
            </div>
        </div>
        <div class="my-container  d-flex flex-wrap justify-content-center mb-5 mt-md-0 mt-5 mt-sm-5 px-0">
            <div class="col-md-12 mb-md-0 mb-4">
                <div class="text-center fw-600 fs-30 sub-heading text-uppercase m-0">{{getMetaTags('faq_page')->translate()->h1}}</div>
            </div>
            <div class="col-md-6 align-self-center mt-4">
                <form class="input-group faq-search " action="{{{route('faq', ['search' => true])}}}">
                    <input type="text" class="form-control brd-color bw-2 py-4 border-radius-left-5 faq-search"
                           name="keyword"
                           placeholder="{!! __('faq.search_for_a_question_here') !!}" aria-label="find answer" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button
                            class="btn own-btn border-radius-0 py-2 px-3 border-radius-right-5 ora faq-search-button"
                            type="button"
                            id="button-addon2"><i class="fas fa-search fs-20"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="faq-content">
        <div class="my-container" id="faq-container">
            <div class="tab-content bg-white border-radius-20 p-3 p-md-5  mb-5" id="faq-content">
                <div class="d-flex justify-content-around mb-3">
                    <div class="text-center faq-title py-1 px-3 px-md-5 m-0 align-middle">
                        <span class="color-white fs-20 fs-sm-14">Арендатор</span>
                    </div>
                    <div class="text-center faq-title py-1 px-3 px-md-5 m-0 align-middle">
                        <span class="color-white fs-20 fs-sm-14">Владелец</span>
                    </div>
                </div>
                <div class="d-flex flex-wrap">
                    <div class="col" id="tenant">
                        <div class="col-12 ff-ms">
                            <div class="">
                                @foreach($categories['tenant'] as $category)
                                    <div class="d-flex align-items-center border-bottom">
                                        <a href="{{route('faq', ['for_whom' => $category->for_whom, 'cat' => $category->slug])}}"
                                           class="d-flex justify-content-end w-100 align-items-center">
                                            <span class="col-auto text-right fs-sm-14">{{$category->translate()->name}}</span>
                                            <span class="col-auto faq-icon mx-2"
                                                  style="background: url({{$category->icon ? Voyager::image($category->icon) : asset('storage/img/icons/read.svg')}}) no-repeat"></span>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-12 border-t d-flex w-100 d-md-none my-4">
                        <div class="separator-4"></div>
                        <div class="separator-5 mx-3"></div>
                        <div class="separator-6"></div>
                    </div>
                    <div class="">
                        <span class="d-none flex-column d-md-flex separator-1 h-100"></span>
                    </div>
                    <div class="col" id="car-owner">
                        <div class="col-12 ff-ms">
                            <div class="list-group list-group-flush">
                                @foreach($categories['car-owner'] as $category)
                                    <div class="d-flex align-items-center border-bottom">
                                        <a href="{{route('faq', ['for_whom' => $category->for_whom, 'cat' => $category->slug])}}"
                                           class="d-flex justify-content-start w-100 align-items-center">
                                            <span class="col-auto faq-icon mx-2"
                                                  style="background: url({{$category->icon ? Voyager::image($category->icon) : asset('storage/img/icons/read.svg')}}) no-repeat"></span>
                                            <span class="col-auto text-left fs-sm-14">{{$category->translate()->name}}</span>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="{{asset('js/faq.js')}}"></script>
    <script>
        $(document).on('click', '.faq-like', function (event) {
            event.preventDefault();
            var url = $(this).data('href');
            var id = $(this).data('target');
            var container = $('[data-status="' + id + '"]');
            container.preloader();
            axios.get(url).then((response) => {
                if (response.data.success) {
                    container.preloader('remove');
                    $(id).html('<p class="text-center">{!! __('faq.thanks_for_rating_this_will_hel_us_to_become_better') !!}</p>')
                }
            })
        })
        $('.faq-search').keyup(function () {
            faqSearch();
        });
        $('.faq-search-button').click(function () {
            faqSearch();
        });

        function faqSearch() {
            var form = $('.faq-search');
            var param = form.serialize();
            axios.get('?' + param).then((response) => {
                if (response.data.success) {
                    $('.faq-content').html(response.data.html);
                }
                if ($('[name="keyword"]').val()) {
                    $('.faq-cat').hide()
                } else {
                    $('.faq-cat').show()
                }
            })
        }
    </script>
@stop
