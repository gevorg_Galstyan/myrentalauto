<?php

namespace App\Notifications;

use App\Models\WrongFillCar;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OwnerWrong extends Notification
{
    use Queueable;

    private $message;
    private $car;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(WrongFillCar $message)
    {
        $this->message = $message;
        $this->car = $message->car;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Здравствуйте, дорогой ' . $notifiable->first_name)
            ->subject('Редактирование авто')
            ->line('Пожалуйста исправьте ошибку')
            ->line($this->message->text)
            ->action('Страница автомобиля', route('cars.edit', ['id' => $this->car->id]))
            ->salutation(__('notification.salutation', ['name' => config('app.name')]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
