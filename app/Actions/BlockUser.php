<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class BlockUser extends AbstractAction
{
    public function getTitle()
    {

        return 'блок. | разблок.';
    }
    public function getPolicy()
    {
        return 'block';
    }

    public function getIcon()
    {
        return $this->data->block == 1 ? 'fas fa-lock-open' :'fas fa-lock ';
    }

    public function getAttributes()
    {
        return [

            'class' => 'btn btn-sm btn-'.($this->data->block == 1?'danger': 'primary').' pull-right ',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.users.block', ['user' => $this->data, 'status' => ($this->data->block == 1 ? 'unblock' : 'block')]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }


}