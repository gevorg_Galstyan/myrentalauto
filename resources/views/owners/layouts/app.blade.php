<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('owners.partials.head')
    @section('css')@show

</head>

<body class="">
<div class="page">
    <div class="flex-fill">

        @include('owners.partials.header')

        <div class="my-3 my-md-5">
            <div class="container">
                @if (session('info-message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <div class="text-center">{!! session('info-message') !!}</div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
        <!--AUTH_MODALS-->
        @guest
            @include('auth.auth_modals')
        @endguest
    <!--FOOTER-->
        @include('owners.partials.footer')

    <!--SCRIPT-->
        @include('sweetalert::alert')

    </div>
</div>


@include('owners.partials.script')
@section('script')@show
</body>
</html>
