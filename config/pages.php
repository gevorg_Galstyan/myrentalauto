<?php

return [
    'route' => [
        '2' => 'pages.for_whom_owner',
        '3' => 'pages.for_tenants',
        '4' => 'pages.insurance',
    ]
];
