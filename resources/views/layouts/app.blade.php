<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.head')
    @section('css')@show
</head>
<body>
@include('partials.header')
{{--@include('partials.notificatiob')--}}
<!--CONTENT-->
@yield('content')
<!--AUTH_MODALS-->
@guest
    @include('auth.auth_modals')
@endguest
<!--FOOTER-->

@include('partials.footer')
@include('cookieConsent::index')
<!--SCRIPT-->
@include('sweetalert::alert')
@include('partials.script')
@section('script')@show
@section('schema')@show

</body>
</html>
