@extends('voyager::master')

@section('css')
{{--    <link rel="stylesheet" href="{{asset('css/main.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
@stop

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }}
        &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}"
                   title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore"
                   data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete"
                   data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span
                        class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
@stop

@section('content')
    @php($post = $dataTypeContent)
    <div class="container-fluid">
        <div class="blog-search-container" style=" background-image: url({{Voyager::image($post->image)}});">

            {{--        <div class="row col-3  d-inline-block">--}}
            <div class="slider-logo position-absolute mt-3 ">
                <span class="own-blue">My</span><span class="own-orange">Rent</span><span class="own-blue">Auto</span>
            </div>
            {{--        </div>--}}

            <h1 class="py-5 px-sm-5 px-0 text-center fw-700 post-title" style="color: {{$post->title_color}};">
                {!! $post->translate()->title !!}
            </h1>
        </div>
        <div class="posts mt-5">
            <div class="my-container bg-white border-radius-20 p-3 p-md-5">
                <div class="row">
                    <div class="container card border-0 ">
                        <div class="row justify-content-center justify-content-md-between align-items-center">
                            <div class="col-md-6 author d-flex order-1 order-md-0 order-sm-1">
                                <div class="avatar mr-3"
                                     style="background-image: url({{Voyager::image($post->authorId->avatar)}})">

                                </div>
                                <div>
                                    <a href="#0"
                                       class="text-default">{{$post->authorId->first_name.' '. $post->authorId->last_name}}</a>
                                    <small
                                        class="d-block text-muted">{{__('role.'.$post->authorId->role->name)}}</small>
                                    @if ($post->updated_at->addDay(1)->ne($post->created_at))
                                        <p>{{__('post.updated')}} : {{$post->updated_at->format('Y-m-d')}}</p>
                                    @else
                                        <p>{{__('post.updated')}} : {{$post->created_at->format('Y-m-d')}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 order-0 order-md-1 mb-3 mb-md-0 mb-sm-3 order-sm-0">
                                <form action="{{route('posts')}}" method="get" class="input-group " role="search"
                                      id="search-form">
                                    <div class="input-group">
                                        <input type="text"
                                               class="form-control brd-orange bw-2 py-4 border-radius-left-5 faq-search"
                                               name="search"
                                               placeholder="{{__('post.search')}}.." aria-label="find answer"
                                               aria-describedby="button-addon2">
                                        <div class="input-group-append">
                                            <button
                                                class="btn own-btn border-radius-0 py-2 px-3 border-radius-right-5 ora "
                                                type="submit"
                                                id="button-addon2"><i class="fas fa-search fs-20"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <hr>

                        <div class="ff-ms">
                            {!! $post->translate()->body !!}
                        </div>

                    </div>

                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
    </div>

@stop
