<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CarOrderOwnerResponse extends Notification
{
    use Queueable;

    protected $order;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.car-response-owner.title', [
                'name' => $notifiable->first_name,
            ]))
            ->line(__('notification.car-response-owner.text-1', [
                'car' => $this->order->car->title.' '.route('voyager.cars.show', $this->order->car),
            ]))
            ->line(__('notification.car-response-owner.text-2'))
            ->line(__('notification.car-response-owner.text-3', [
                'order' => route('voyager.orders.show', $this->order),
            ]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title' => __('notification.car-response-owner.text-1'),
            'message' => '<a href="'.route('voyager.orders.show', $this->order).'">'. __('notification.car-response-owner.text-3',['order' => '']).'</a>',

        ]);
    }
}
